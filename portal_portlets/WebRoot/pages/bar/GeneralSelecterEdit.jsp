<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String pageId = (String)request.getAttribute("pageId"); 
String portletId = (String)request.getAttribute("portletId");
String isConvertText = (String)request.getAttribute("isConvertText");
String isExtendLink = (String)request.getAttribute("isExtendLink"); 
%>

<form id="<portlet:namespace/>SearchConfig">
<table width="600" border="1" >
    <tr>
    	<td width="140" nowrap="nowrap">查询域扩展</td>
      	<td><input type="button" name="extendConfigButton" id="extendConfigButton" value="扩展配置" onclick="popupQueryFieldsConfig('<%=pageId%>','${portletId}')" /></td>
    </tr>
	<tr>
		<td width="140" nowrap="nowrap">EX是否文本中文转码</td>
      <td nowrap="nowrap">
        <input type="radio" name="isConvertText" id="isConvertTextY" value="Y" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isConvertText" id="isConvertTextN" value="N" />
      否</td>
	</tr>     
<tr>
	  <td width="140" nowrap="nowrap">EX扩展枚举数据URL</td>
	  <td nowrap="nowrap"><input type="text" size="50" name="extendEnumDataURL" id="extendEnumDataURL" value="${extendEnumDataURL}" /></td>
	</tr>
    <tr>
      <td width="140" nowrap="nowrap">是否扩展链接</td>
      <td nowrap="nowrap">
        <input type="radio" name="isExtendLink" id="isExtendLinkY" value="Y" onclick="controlLinkButton<portlet:namespace/>()" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isExtendLink" id="isExtendLinkN" value="N" onclick="controlLinkButton<portlet:namespace/>()" />
      否&nbsp;&nbsp;&nbsp;<input type="button" name="extendLinkButton" id="extendLinkButton" disabled="disabled" value="扩展配置" onclick="popupLinkBarConfig('<%=pageId%>','${portletId}')" /></td>
    </tr>
    <tr>
      <td colspan="2" align="center" nowrap="nowrap">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>SearchConfig'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>        
</table>
</form>
<script language="javascript">
<%if ("Y".equals(isExtendLink)){%>
	$('#<portlet:namespace/>SearchConfig #isExtendLinkY').attr("checked","checked");
	$('#<portlet:namespace/>SearchConfig #extendLinkButton').attr("disabled","");
<%}else{%>
	$('#<portlet:namespace/>SearchConfig #isExtendLinkN').attr("checked","checked");
	$('#<portlet:namespace/>SearchConfig #extendLinkButton').attr("disabled","disabled");
<%}%>

<%if ("Y".equals(isConvertText)){%>
	$('#<portlet:namespace/>SearchConfig #isConvertTextY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>SearchConfig #isConvertTextN').attr("checked","checked");
<%}%>

function controlLinkButton<portlet:namespace/>(){
	if ($('#<portlet:namespace/>SearchConfig #isExtendLinkY').attr("checked") == "checked"){
		$('#<portlet:namespace/>SearchConfig #extendLinkButton').enable();	
	}else{
		$('#<portlet:namespace/>SearchConfig #extendLinkButton').disable();
	}
}
controlLinkButton<portlet:namespace/>();

function popupQueryFieldsConfig(pageId,portletId){
	var boxTitle = "扩展查询配置";
	var targetURL = "/portal_portlets/index?ExtGeneralQueryConfigManageList&pageId="+pageId+"&portletId="+portletId;
	//$(document.body).css('overflow','hidden');
	qBox.iFLoad({title:boxTitle,scrolling:'auto',src:targetURL,center:true,draggable: false,w:900,h:500,afterHide:function(){
		//$(document.body).css('overflow','visible');
	}});
}
</script>