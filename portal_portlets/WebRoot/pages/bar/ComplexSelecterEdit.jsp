<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String pageId = (String)request.getAttribute("pageId"); 
String portletId = (String)request.getAttribute("portletId");

String showYear = (String)request.getAttribute("showYear"); 
String showYearMonth = (String)request.getAttribute("showYearMonth"); 
String showYearQuarter = (String)request.getAttribute("showYearQuarter"); 
String showYearQuarterMonth = (String)request.getAttribute("showYearQuarterMonth"); 
String showDate = (String)request.getAttribute("showDate");
String onlyDate = (String)request.getAttribute("onlyDate");

String isExtendBar = (String)request.getAttribute("isExtendBar"); 
String isShowTimeRange = (String)request.getAttribute("isShowTimeRange"); 
String relativePosition = (String)request.getAttribute("relativePosition"); 
String isOnlyOneLine = (String)request.getAttribute("isOnlyOneLine"); 
String isConvertText = (String)request.getAttribute("isConvertText");

String isExtendLink = (String)request.getAttribute("isExtendLink"); 
%>

<form id="<portlet:namespace/>SearchConfig">
<table width="600" border="1" >
	<tr>
	  <td width="140" nowrap="nowrap">显示种类&nbsp;<span style="color: red;">*</span></td>
	  <td nowrap="nowrap"><input type="checkbox" name="showYear" id="showYear" value="Y" />年&nbsp;
			<input type="checkbox" name="showYearMonth" id="showYearMonth" value="Y" />年月&nbsp;
			<input type="checkbox" name="showYearQuarter" id="showYearQuarter" value="Y" />年季度&nbsp;
			<input type="checkbox" name="showYearQuarterMonth" id="showYearQuarterMonth" value="Y" />年季度月&nbsp;
			<input type="checkbox" name="showDate" id="showDate" value="Y" />日期段&nbsp;
            <input type="checkbox" name="onlyDate" id="onlyDate" value="Y" />指定日期&nbsp;
            </td>
	</tr>
	<tr>
	  <td width="140" nowrap="nowrap">默认显示</td>
	  <td nowrap="nowrap"><select name="defaultShow" id="defaultShow">
			<option value="year">年</option>
			<option value="yearMonth">年月</option>
			<option value="yearQuarter">年季度</option>
			<option value="yearQuarterMonth">年季度月</option>
			<option value="date">日期段</option>
            <option value="onlyDate">指定日期</option>
		</select></td>
	</tr>
    <tr>
      <td width="140" nowrap="nowrap">是否扩展查询域</td>
      <td nowrap="nowrap">
        <input type="radio" name="isExtendBar" id="isExtendBarY" value="Y" onclick="controlConfigButton<portlet:namespace/>()" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isExtendBar" id="isExtendBarN" value="N" onclick="controlConfigButton<portlet:namespace/>()" />
      否&nbsp;&nbsp;&nbsp;<input type="button" name="extendConfigButton" id="extendConfigButton" disabled="disabled" value="扩展配置" onclick="popupQueryBarConfig('<%=pageId%>','${portletId}')" /></td>
    </tr> 
    <tr>
      <td width="140" nowrap="nowrap">EX是否显示时间区</td>
      <td nowrap="nowrap">
        <input type="radio" name="isShowTimeRange" id="isShowTimeRangeY" value="Y" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isShowTimeRange" id="isShowTimeRangeN" value="N" />
      否</td>
    </tr>         
	<tr>
		<td width="140" nowrap="nowrap">EX相对时间段位置</td>
      <td nowrap="nowrap">
        <input type="radio" name="relativePosition" id="relativePositionB" value="before" /> 
        前
      &nbsp;&nbsp;
        <input type="radio" name="relativePosition" id="relativePositionA" value="after" />
        后
      </td>
	</tr>
	<tr>
		<td width="140" nowrap="nowrap">EX扩展域是否换行</td>
      <td nowrap="nowrap">
        <input type="radio" name="isOnlyOneLine" id="isOnlyOneLineY" value="Y" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isOnlyOneLine" id="isOnlyOneLineN" value="N" />
      否</td>
	</tr>
	<tr>
		<td width="140" nowrap="nowrap">EX是否文本中文转码</td>
      <td nowrap="nowrap">
        <input type="radio" name="isConvertText" id="isConvertTextY" value="Y" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isConvertText" id="isConvertTextN" value="N" />
      否</td>
	</tr>    
	<tr>
		<td width="120" nowrap="nowrap">EX多选值分隔符&nbsp;</td>
	  <td nowrap="nowrap"><input type="text" size="50" name="checkBoxValueSpliter" id="checkBoxValueSpliter" value="${checkBoxValueSpliter}" /></td>
	</tr>     
<tr>
	  <td width="140" nowrap="nowrap">EX扩展枚举数据URL</td>
	  <td nowrap="nowrap"><input type="text" size="50" name="extendEnumDataURL" id="extendEnumDataURL" value="${extendEnumDataURL}" /></td>
	</tr>    
	<tr>
		<td width="140" nowrap="nowrap">转向文本</td>
	  <td nowrap="nowrap"><input type="text" size="50" name="redirectText" id="redirectText" value="${redirectText}" /></td>
	</tr>      
<tr>
	  <td width="140" nowrap="nowrap">转向URL</td>
	  <td nowrap="nowrap"><input type="text" size="50" name="redirectURL" id="redirectURL" value="${redirectURL}" /></td>
	</tr>
    <tr>
      <td width="140" nowrap="nowrap">是否扩展链接</td>
      <td nowrap="nowrap">
        <input type="radio" name="isExtendLink" id="isExtendLinkY" value="Y" onclick="controlLinkButton<portlet:namespace/>()" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isExtendLink" id="isExtendLinkN" value="N" onclick="controlLinkButton<portlet:namespace/>()" />
      否&nbsp;&nbsp;&nbsp;<input type="button" name="extendLinkButton" id="extendLinkButton" disabled="disabled" value="扩展配置" onclick="popupLinkBarConfig('<%=pageId%>','${portletId}')" /></td>
    </tr>         
	<tr>
	  <td width="140" nowrap="nowrap">默认值</td>
	  <td nowrap="nowrap"><input type="text" size="50" name="defaultVariableValues" id="defaultVariableValues" value="${defaultVariableValues}" /></td>
	</tr>
    <tr>
      <td colspan="2" align="center" nowrap="nowrap">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>SearchConfig'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>        
</table>
</form>
<script language="javascript">
$("#<portlet:namespace/>SearchConfig #defaultShow").val("${defaultShow}");

<%if ("Y".equals(showYear)){%>
	$("#<portlet:namespace/>SearchConfig #showYear").attr("checked",true);
<%}%>

<%if ("Y".equals(showYearMonth)){%>
	$("#<portlet:namespace/>SearchConfig #showYearMonth").attr("checked",true);
<%}%>

<%if ("Y".equals(showYearQuarter)){%>
	$("#<portlet:namespace/>SearchConfig #showYearQuarter").attr("checked",true);
<%}%>

<%if ("Y".equals(showYearQuarterMonth)){%>
	$("#<portlet:namespace/>SearchConfig #showYearQuarterMonth").attr("checked",true);
<%}%>

<%if ("Y".equals(showDate)){%>
	$("#<portlet:namespace/>SearchConfig #showDate").attr("checked",true);
<%}%>

<%if ("Y".equals(onlyDate)){%>
$("#<portlet:namespace/>SearchConfig #onlyDate").attr("checked",true);
<%}%>


<%if ("Y".equals(isExtendBar)){%>
	$('#<portlet:namespace/>SearchConfig #isExtendBarY').attr("checked","checked");
	$('#<portlet:namespace/>SearchConfig #extendConfigButton').attr("disabled","");
<%}else{%>
	$('#<portlet:namespace/>SearchConfig #isExtendBarN').attr("checked","checked");
	$('#<portlet:namespace/>SearchConfig #extendConfigButton').attr("disabled","disabled");
<%}%>

<%if ("Y".equals(isExtendLink)){%>
	$('#<portlet:namespace/>SearchConfig #isExtendLinkY').attr("checked","checked");
	$('#<portlet:namespace/>SearchConfig #extendLinkButton').attr("disabled","");
<%}else{%>
	$('#<portlet:namespace/>SearchConfig #isExtendLinkN').attr("checked","checked");
	$('#<portlet:namespace/>SearchConfig #extendLinkButton').attr("disabled","disabled");
<%}%>

<%if ("Y".equals(isShowTimeRange)){%>
	$('#<portlet:namespace/>SearchConfig #isShowTimeRangeY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>SearchConfig #isShowTimeRangeN').attr("checked","checked");
<%}%>

<%if ("before".equals(relativePosition)){%>
	$('#<portlet:namespace/>SearchConfig #relativePositionB').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>SearchConfig #relativePositionA').attr("checked","checked");
<%}%>

<%if ("Y".equals(isOnlyOneLine)){%>
	$('#<portlet:namespace/>SearchConfig #isOnlyOneLineY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>SearchConfig #isOnlyOneLineN').attr("checked","checked");
<%}%>

<%if ("Y".equals(isConvertText)){%>
	$('#<portlet:namespace/>SearchConfig #isConvertTextY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>SearchConfig #isConvertTextN').attr("checked","checked");
<%}%>


function controlConfigButton<portlet:namespace/>(){
	if ($('#<portlet:namespace/>SearchConfig #isExtendBarY').attr("checked") == "checked"){
		$('#<portlet:namespace/>SearchConfig #extendConfigButton').enable();	
	}else{
		$('#<portlet:namespace/>SearchConfig #extendConfigButton').disable();
	}
}
controlConfigButton<portlet:namespace/>();

function controlLinkButton<portlet:namespace/>(){
	if ($('#<portlet:namespace/>SearchConfig #isExtendLinkY').attr("checked") == "checked"){
		$('#<portlet:namespace/>SearchConfig #extendLinkButton').enable();	
	}else{
		$('#<portlet:namespace/>SearchConfig #extendLinkButton').disable();
	}
}
controlLinkButton<portlet:namespace/>();
</script>