<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects />
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<%
	String portletId = (String) request.getAttribute("portletId");
%>
<script type="text/javascript">
$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
		sendAction('${getAjaxDataURL}', {dataType:'text', onComplete:function(responseText) {
			if (responseText && responseText != ""){
				var tabModels = eval(responseText);
				var html = "";
				//Tab头
				html += "<ul class='tabbtn' id='normaltab<portlet:namespace/>'>";
				for (var i = 0; i < tabModels.length; i++) {
					var tabConfig = tabModels[i];
					if (i == 0) {
						html += "<li class='current'><span>"+ tabConfig.tabTitle +"</span></li>";
					} else {
						html += "<li><span>"+ tabConfig.tabTitle +"</span></li>";
					}
				}
				html += "</ul>";
				
				//Tab体
				html += "<div class='tabcon' id='normalcon<portlet:namespace/>' style='padding:1px;'>";
				for (var i = 0; i < tabModels.length; i++) { 
					var tabConfig = tabModels[i];
					
					//table体
					var tabDatas = tabConfig.datas;
					
					html += "<div class='info_list_default'>";
					if(tabDatas.length == 0){
						html += "<table class='${tabListStyle}' style='border: 0px;'>";
					}else{
						html += "<table class='${tabListStyle}'>";
					}						
					
					//table头
					var showHeader = tabConfig.showHeader;
					var propertyConfigs = tabConfig.propertyConfigs;
					if (showHeader) {
						html += "<thead>";
						for (var j = 0; j < propertyConfigs.length; j++) {
							var propertyConfig = propertyConfigs[j];
							if (!propertyConfig.isLinkURL) {
								html += "<th style='white-space:nowrap;'>"+ propertyConfig.name +"</th>";
							}
						}
						html += "</thead>";
					}
					
					//设置最大行数
					var maxCount = '${maxCount}';
					var dataLength = tabDatas.length;
					if(maxCount < dataLength) {
						dataLength = maxCount;
					}
					if(tabDatas.length == 0){
						html += "<tr>";
						html += "<td colspan='2' style='border: 0px;'>无记录</td>";
						html += "</tr>";
					}else{
						for (var j = 0; j < dataLength; j++) {
							html += "<tr>";
							var dataRow = tabDatas[j];
							for (var k = 0; k < propertyConfigs.length; k++) {
								var propertyConfig = propertyConfigs[k];
								
								var code = propertyConfig.code;
								var isLink = propertyConfig.isLink;
								var isLinkURL = propertyConfig.isLinkURL;
								var width = 100;
								if (propertyConfig.width){
									width = propertyConfig.width;
								}
								cssName = "";
								if(propertyConfig.cssName){
									cssName = "class = '"+ propertyConfig.cssName  +"'";
								}
								var style = 'overflow:hidden;text-overflow:ellipsis;white-space:nowrap';
								if (propertyConfig.style){
									style = propertyConfig.style;
								}
								
								if (!isLinkURL) {//如果不是超链接数据
									if (isLink) {
										//console.log(code);
										//console.log(dataRow[code + "Link"]);
										html += "<td width='"+width+"' "+ cssName +"  style='"+style+"'><a title='"+ dataRow[code] +"' href="+ dataRow[code + "Link"] +">"+ dataRow[code] +"</a></td>";
									} else {
										html += "<td width='"+width+"' "+ cssName +" style='"+style+"'>"+ dataRow[code] +"</td>";
									}
								}
							}
							html += "</tr>";
						}
					}
					html += "</table>";
					html += "<div class='infomore'>";
					html += "<a href="+ tabConfig.moreURL +">更多</a>";
					html += "</div>";
					html += "</div>";
				}
				html += "</div>";
				$('#tabskeleton<portlet:namespace/>').html(html);
				$("#normaltab<portlet:namespace/>").tabso({
					cntSelect : "#normalcon<portlet:namespace/>",
					tabEvent : "click",
				});
			}else{
				$('#tabskeleton').html("未获取相关数据.");
			}
		}});
	};
	__renderPortlets.put("<%=portletId%>__renderPortlet<portlet:namespace/>");
		__renderPortlet<portlet:namespace/>();
	});
</script>
<!-- Tab Portlet -->
<div id="tabskeleton<portlet:namespace/>" class="${tabStyle}" style="padding:1px;"></div>