<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isCache = (String)request.getAttribute("isCache"); 
%>
<style>
.tabPortletEditTb{
	width:100%;
	border:1px solid #cad9ea;
	border-collapse: collapse;
}
.tabPortletEditTb td {
	height: 25px;
}
</style>
<form id="<portlet:namespace/>Form">
	<table class="tabPortletEditTb" border="1">
		<tr>
			<td>Tab风格</td>
			<td>
				<select name="tabStyle" id="tabStyle" style="width: 200px;">
			        <option value="tab_default">默认风格</option>
			        <option value="tab_blue">标准绿色</option>
		      	</select>
			</td>
		</tr>
		<tr>
			<td>列表风格</td>
			<td>
				<select name="tabListStyle" id="tabListStyle" style="width: 200px;">
			        <option value="tab_table_default">默认风格</option>
			        <option value="tab_table_btmdashed">虚线底框</option>
		      	</select>
			</td>
		</tr>
		<tr>
		<tr>
			<td>数据URL</td>
			<td title="dataURL"><input name="dataURL"
				type="text" id="dataURL" value="${dataURL}" size="30" /></td>
		</tr>
		<tr>
			<td>记录条数</td>
			<td><input name="maxCount" type="text" id="maxCount"
				value="${maxCount}" size="30" /></td>
		</tr>
		<tr>
			<td>是否缓存</td>
			<td><input type="radio" name="isCache" id="isCacheY" value="Y" />是&nbsp;&nbsp;
				<input type="radio" name="isCache" id="isCacheN" value="N" />否</td>
		</tr>
		<tr>
			<td>缓存时间</td>
			<td><input type="text" name="cacheMinutes" id="cacheMinutes"
				value="${cacheMinutes}" /> 分钟</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="button"
				name="button" id="button" value="保存"
				onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" />
				&nbsp; <input type="button" name="button2" id="button2" value="取消"
				onclick="fireAction('${editURL}')" /></td>
		</tr>
	</table>
</form>
<script language="javascript">
$('#<portlet:namespace/>Form #tabStyle').val("${tabStyle}");
$('#<portlet:namespace/>Form #tabListStyle').val("${tabListStyle}");

<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>Form #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isCacheN').attr("checked","checked");
<%}%>
</script>
