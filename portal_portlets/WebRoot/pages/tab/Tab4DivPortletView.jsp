<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects />
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
<style>
 div.daywork{
 	height:210px;
    box-sizing: border-box;
    margin: 0px;
    padding: 0px;
    overflow-y: auto;
    word-break: break-word;
    word-wrap: break-word;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
    display: block;
}

div.daywork ol{
	display: block;
    list-style-type: decimal;
    -webkit-margin-before: 0em;
    -webkit-margin-after: 0em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    -webkit-padding-start: 40px;
}

div.daywork ol li{
	margin: 0px;
	display: list-item;
    text-align: -webkit-match-parent;
	list-style-type: decimal;
}

div.daywork ol ul{
    -webkit-margin-before: 0px;
    -webkit-margin-after: 0px;
    list-style-type: circle;
    display: block;
    padding-left: 20px;
}

div.daywork ol ul li{
    display: list-item;
    list-style-type: circle;
    
}

div.daywork ol ul ul{
   list-style-type: square;
   padding-left: 20px;
}

div.daywork ol ul ul li{
   list-style-type: square;
   display: list-item;
}
</style>
</portlet:resourceURL>
<%
	String portletId = (String) request.getAttribute("portletId");
%>
<script type="text/javascript">
$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
		sendAction('${getAjaxDataURL}', {dataType:'text', onComplete:function(responseText) {
			if (responseText && responseText != ""){
				var tabModels = eval(responseText);
				var html = "";
				//Tab头
				html += "<ul class='tabbtn' id='normaltab<portlet:namespace/>'>";
				for (var i = 0; i < tabModels.length; i++) {
					var tabConfig = tabModels[i];
					if (i == 0) {
						html += "<li class='current'><span>"+ tabConfig.tabTitle +"</span></li>";
					} else {
						html += "<li><span>"+ tabConfig.tabTitle +"</span></li>";
					}
				}
				html += "</ul>";
				//Tab体
				html += "<div class='daywork' id='normalcon<portlet:namespace/>'>";
				for (var i = 0; i < tabModels.length; i++) { 
					var tabConfig = tabModels[i];
					html += "<div class='daywork'>";
					
					//table体
					var tabDatas = tabConfig.datas;
					//设置最大行数
					var dataLength = tabDatas.length;

					for (var j = 0; j < dataLength; j++) {
						var dataRow = tabDatas[j];
						var code = dataRow.text;
						var width = 100;
						html += code;
					}
					html += "<div class='infomore'>";
					html += "<a href="+ tabConfig.moreURL +">更多</a>";
					html += "</div>";
					html += "</div>";
				}
				html += "</div>";
				$('#tabskeleton<portlet:namespace/>').html(html);
				$("#normaltab<portlet:namespace/>").tabso({
					cntSelect : "#normalcon<portlet:namespace/>",
					tabEvent : "click",
				});
			}else{
				$('#tabskeleton').html("未获取相关数据.");
			}
		}});
	};
	__renderPortlets.put("<%=portletId%>__renderPortlet<portlet:namespace/>");
		__renderPortlet<portlet:namespace/>();
	});
</script>
<!-- Tab Portlet -->
<div id="tabskeleton<portlet:namespace/>" class="${tabStyle}" style="padding:1px;"></div>