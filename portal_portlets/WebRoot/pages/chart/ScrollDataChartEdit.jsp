<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<%
String isTransparent = (String)request.getAttribute("isTransparent");
String isCache = (String)request.getAttribute("isCache");
%>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>

<form id="<portlet:namespace/>ChartConfig">
  <table width="90%" border="1">
    <tr>
      <td width="120">图表类型</td>
      <td>
        <select name="chartType" id="chartType">
        	<option value="">请选择</option>
          	<option value="<%=request.getContextPath()%>/charts/ScrollColumn2D.swf">柱形图(2D)</option>
          	<option value="<%=request.getContextPath()%>/charts/ScrollLine2D.swf">线形图(2D)</option>
          	<option value="<%=request.getContextPath()%>/charts/ScrollArea2D.swf">区域图(2D)</option>
			<option value="<%=request.getContextPath()%>/charts/ScrollStackedColumn2D.swf">堆栈式柱状图(2D)</option>
          	<option value="<%=request.getContextPath()%>/charts/ScrollCombi2D.swf">组合图(2D)</option>
          	<option value="<%=request.getContextPath()%>/charts/ScrollCombiDY2D.swf">双Y轴组合图(2D)</option>
        </select>
      </td>
    </tr>
    <tr>
      <td width="120">高度</td>
      <td>
        <input type="text" name="chartHeight" id="chartHeight" value="${chartHeight}" />
      </td>
    </tr>
    <tr>
      <td width="120">数据路径</td>
      <td>
        <input name="dataURL" type="text" id="dataURL" value="${dataURL}" size="60" /></td>
    </tr>
    <tr>
      <td width="120">默认值</td>
      <td>
        <input type="text" name="defaultVariableValues" size="60" id="defaultVariableValues" value="${defaultVariableValues}" />      </td>
    </tr>
    <tr>
      <td width="120">是否透明</td>
      <td>
        <input type="radio" name="isTransparent" id="isTransparentY" value="Y" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isTransparent" id="isTransparentN" value="N" />
      否</td>
    </tr>
    <tr>
      <td width="120">是否缓存</td>
      <td>
        <input type="radio" name="isCache" id="isCacheY" value="Y" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isCache" id="isCacheN" value="N" />
      否</td>
    </tr> 
    <tr>
      <td width="120">缓存时间</td>
      <td>
        <input type="text" name="cacheMinutes" id="cacheMinutes" value="${cacheMinutes}" />
        分钟</td>
    </tr>    
    <tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>ChartConfig'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')" /></td>
    </tr>
  </table>
</form>
<script language="javascript">
$('#<portlet:namespace/>ChartConfig #chartType').val('${chartType}');
<%if ("Y".equals(isTransparent)){%>
	$('#<portlet:namespace/>ChartConfig #isTransparentY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>ChartConfig #isTransparentN').attr("checked","checked");
<%}%>
<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>ChartConfig #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>ChartConfig #isCacheN').attr("checked","checked");
<%}%>

</script>