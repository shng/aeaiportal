<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<%
String isSetting = (String)request.getAttribute("isSetting");
if("true".equals(isSetting)){
String isVisiable = (String)request.getAttribute("isVisiable");
String timerRangeListHtml = (String)request.getAttribute("timerRangeListHtml");
String defaultRange = (String)request.getAttribute("defaultRange");

String style = !"Y".equals(isVisiable)?"style='display:none'":"";
%>
<div id="<portlet:namespace/>Container" <%=style%>>&nbsp;间隔列表
<select id="timerRangeList" name="timerRangeList" onchange="resetAutoRefresh<portlet:namespace/>()"><%=timerRangeListHtml%></select>秒
<input type="button" name="refreshImmediateBtn" id="refreshImmediateBtn" value="立即刷新" onclick="immedateRefresh<portlet:namespace/>()" />
<input type="button" name="trigerRefreshBtn" id="trigerRefreshBtn" value="停止自动刷新" onclick="trigerRefresh<portlet:namespace/>()" />
<span id="tipText">5秒钟后进行刷新操作...</span>
</div>
<script language="javascript">
var defaultRange<portlet:namespace/> = <%=defaultRange%>;
var leftRange<portlet:namespace/> = defaultRange<portlet:namespace/>;

var timerHandler = {
	_excute:${jsHandler}
};

var intervalId;
function immedateRefresh<portlet:namespace/>(){
	timerHandler._excute();
}
function changeTip(){
	leftRange<portlet:namespace/> = leftRange<portlet:namespace/>-1;
	if (leftRange<portlet:namespace/> == 0){
		$("#<portlet:namespace/>Container #tipText").val('正在执行刷新操作！');
		timerHandler._excute();
		leftRange<portlet:namespace/> = defaultRange<portlet:namespace/>;
	}
	$("#<portlet:namespace/>Container #tipText").html(leftRange<portlet:namespace/>+'秒钟后进行刷新操作...');
}
$(function(){
	intervalId = setInterval(changeTip,1000);
})

function trigerRefresh<portlet:namespace/>(){
	var trigerRefreshBtn = $("#<portlet:namespace/>Container #trigerRefreshBtn");
	if (trigerRefreshBtn.val() == '停止自动刷新'){
		clearInterval(intervalId);
		trigerRefreshBtn.val('启动自动刷新');
	}
	else{
		intervalId = setInterval(changeTip,1000);
		trigerRefreshBtn.val('停止自动刷新');
	}
}

function resetAutoRefresh<portlet:namespace/>(){
	defaultRange<portlet:namespace/> = $("#timerRangeList").val();
	leftRange<portlet:namespace/> = defaultRange<portlet:namespace/>;
	
	var trigerRefreshBtn = $("#<portlet:namespace/>Container #trigerRefreshBtn");
	if (trigerRefreshBtn.val() == '停止自动刷新'){
		clearInterval(intervalId);
		intervalId = setInterval(changeTip,1000);
	}
}
</script>
<%
}else{
	out.println("请正确相关设置属性！");
} 
%>