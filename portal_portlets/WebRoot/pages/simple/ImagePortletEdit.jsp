<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:actionURL name="goBack" var="goBackURL"></portlet:actionURL>
<script language="javascript">
function <portlet:namespace/>saveConfig(button){
	var imageSrc = $('#imageSrc').val();
	var imageWidth = $('#imageWidth').val();
	var imageHeight = $('#imageHeight').val();
	
	if (validation.checkNull(imageSrc)){
		alert('图片路径不能为空！');
		return;
	}
	if (validation.checkNull(imageWidth)){
		alert('图片宽度不能为空！');
		return;
	}
	if (validation.checkNull(imageHeight)){
		alert('图片高度不能为空！');
		return;
	}
	submitAction('${saveConfigURL}',{form:closestForm(button)});
}
</script>
<form>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="<portlet:namespace/>saveConfig(this)"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="fireAction('${goBackURL}')"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0" style="margin:1px 0 1px 1px;">
<tr>
	<th width="100" nowrap>图片路径</th>
	<td width="80%"><input id="imageSrc" name="imageSrc" type="text" value="${imageSrc}" size="46" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>图片宽度</th>
	<td width="80%"><input id="imageWidth" name="imageWidth" type="text" value="${imageWidth}" size="24" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>图片高度</th>
	<td width="80%"><input id="imageHeight" name="imageHeight" type="text" value="${imageHeight}" size="24" class="text" /></td>
</tr>
</table>
<br />
</form>