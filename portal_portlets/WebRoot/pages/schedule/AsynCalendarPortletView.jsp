<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String portletId = (String)request.getAttribute("portletId");
String year = (String)request.getAttribute("year");
String month = (String)request.getAttribute("month");
%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<style>
.Calendar {
	font-family: Verdana;
	font-size: 12px;
	/*background-color: #e0ecf9;*/
	text-align: center;
	width:100%;
	/*height:300px;*/
	/*padding: 10px;*/
	line-height: 1.5em;
}

.Calendar a {
	color: #1e5494;
}

.Calendar table {
	width: 100%;
	border: 0;
}

.Calendar table thead {
	background: #3f526f;
	color: #fff;
}

.Calendar table thead tr{
	height:28px;
}

.Calendar table tbody tr{
	height:28px;
}


.Calendar table td {
	font-size: 11px;
	padding: 1px;
}

#idCalendarPre {
	cursor: pointer;
	float: left;
	padding-right: 5px;
}

#idCalendarNext {
	cursor: pointer;
	float: right;
	padding-right: 5px;
}

#idCalendarYear,#idCalendarMonth,#idCalendarPre,#idCalendarNext {
	height:28px;
	line-height:28px;
}

#idCalendar td {
	color: blue;
}

#idCalendar td.onWeekend {
	font-weight: bold;
	/*background: #ffff99;*/
}

#idCalendar td.onToday {
	font-weight: bold;
	color: #C60;
	background: #ff9900;
}

.Weekend {
	/*background: #ffff99;*/
	color: white;
}
</style>
<script type="text/javascript">
function doPopupPageBox(url, params) {
	qBox.iFLoad({
		title : params.title,
		src : url,
		w : params.width,
		h : params.height
	});
}

	$(function(){
		var __renderPortlet<portlet:namespace/> = function(){
			sendAction('${getAjaxDataURL}',{dataType:'json',onComplete:function(obj){
				var tempDays = obj.lightDays;
				cale.lightDays = tempDays.split(',');
				cale.Draw();
			}});
		};
		__renderPortlets.put('<%=portletId%>',__renderPortlet<portlet:namespace/>);
		doAjaxRefreshSelf('year:<%=year%>,month:<%=month%>','<%=portletId%>');
	});

	var Class = {
		create : function() {
			return function() {
				this.initialize.apply(this, arguments);
			};
		}
	};

	Object.extend = function(destination, source) {
		for ( var property in source) {
			destination[property] = source[property];
		}
		return destination;
	};

	var Calendar = Class.create();
	Calendar.prototype = {
		initialize : function(container, options) {
			this.Container = document.getElementById(container);//容器(table结构) 
			this.Days = [];//日期对象列表 
			this.lightDays = [];
			this.SetOptions(options);

			this.Year = this.options.Year;
			this.Month = this.options.Month;
			this.onWeekendDay = this.options.onWeekendDay;
			this.onToday = this.options.onToday;
			this.onFinish = this.options.onFinish;
			this.Draw();
		},
		//设置默认属性 
		SetOptions : function(options) {
			this.options = {//默认值 
				Year : new Date().getFullYear(),//显示年 
				Month : new Date().getMonth() + 1,//显示月 
				onWeekendDay : function() {
				},
				onToday : function() {
				},//在当天日期触发 
				onFinish : function() {
				}//日历画完后触发 
			};
			Object.extend(this.options, options || {});
		},
		//上一个月 
		PreMonth : function() {
			//先取得上一个月的日期对象 
			var d = new Date(this.Year, this.Month - 2, 1);
			//再设置属性 
			this.Year = d.getFullYear();
			this.Month = d.getMonth() + 1;
			//重新画日历 
			this.Draw();
		},
		//下一个月 
		NextMonth : function() {
			var d = new Date(this.Year, this.Month, 1);
			this.Year = d.getFullYear();
			this.Month = d.getMonth() + 1;
			this.Draw();
		},
		//画日历 
		Draw : function() {
			//用来保存日期列表 
			var arr = [];
			//用当月第一天在一周中的日期值作为当月离第一天的天数 
			for ( var i = 1, firstDay = new Date(this.Year, this.Month - 1,
					1).getDay(); i <= firstDay; i++) {
				arr.push(" ");
			}
			//用当月最后一天在一个月中的日期值作为当月的天数 
			for ( var i = 1, monthDay = new Date(this.Year, this.Month, 0)
					.getDate(); i <= monthDay; i++) {
				arr.push(i);
			}

			var frag = document.createDocumentFragment();

			this.Days = [];

			while (arr.length > 0) {
				//每个星期插入一个tr 
				var row = document.createElement("tr");
				//每个星期有7天 
				for ( var i = 1; i <= 7; i++) {
					var cell = document.createElement("td");
					cell.innerHTML = " ";

					if (arr.length > 0) {
						var d = arr.shift();
						cell.innerHTML = d;
						if (d > 0) {
							this.Days[d] = cell;
							//判断是否是周末 
							if (i == 1 || i == 7) {
								this.onWeekendDay(cell);
							}
							//判断是否今日 
							if (this.IsSame(new Date(this.Year,
									this.Month - 1, d), new Date())) {
								this.onToday(cell);
							}
						}
					}
					row.appendChild(cell);
				}
				frag.appendChild(row);
			}

			//先清空内容再插入(ie的table不能用innerHTML) 
			while (this.Container.hasChildNodes()) {
				this.Container.removeChild(this.Container.firstChild);
			}
			this.Container.appendChild(frag);

			this.onFinish();
		},
		//判断是否同一日 
		IsSame : function(d1, d2) {
			return (d1.getFullYear() == d2.getFullYear()
					&& d1.getMonth() == d2.getMonth() && d1.getDate() == d2
					.getDate());
		}
	};
	</script>
<div class="Calendar">
	<div id="idCalendarPre"><<</div>
	<div id="idCalendarNext">>></div>
	<div style="color: darkslateblue;font-weight: bold;">
		<span id="idCalendarYear">2014 </span> 年 <span id="idCalendarMonth"> 1 </span> 月
	</div>
	<table cellspacing="0">
		<thead>
			<tr>
				<td class="Weekend">日</td>
				<td>一</td>
				<td>二</td>
				<td>三</td>
				<td>四</td>
				<td>五</td>
				<td class="Weekend">六</td>
			</tr>
		</thead>
		<tbody id="idCalendar">
		</tbody>
	</table>
</div>
<script language="JavaScript">
	var cale = new Calendar(
			"idCalendar",
			{
				onWeekendDay : function(o) {
					o.className = "onWeekend";
				},
				onToday : function(o) {
					o.className = "onToday";
				},
				onFinish : function() {
					document.getElementById("idCalendarYear").innerHTML = this.Year;
					document.getElementById("idCalendarMonth").innerHTML = this.Month;
					
					var day = []; //days of each month
					if(this.Month==1||this.Month==3||this.Month==5||this.Month==7||this.Month==8||this.Month==10||this.Month==12){
						day = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
					}else if(this.Month==4||this.Month==6||this.Month==9||this.Month==11){
						day = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];
					}else{
						if (((this.Year % 4 == 0) && (this.Year % 100 != 0)) || (this.Year % 400 == 0)){
							day = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29];
						}else{
							day = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28];
						}
					}
					
					var dateFrom = this.Year+"-"+this.Month+"-"+"01";
					var dateTo = this.Year+"-"+this.Month+"-"
						+ new Date(this.Year, this.Month, 0).getDate();
					
					var url = '/portal_portlets/index?DailyScheduleManageList&userCode='+$("#currentUserId").val() +'&sDate='+dateFrom+'&eDate='+dateTo;
					
					var leng = day.length;
					for (var i = 0; i < leng; i++) {
						this.Days[day[i]].innerHTML = 
							"<a href=\"javascript:doPopupPageBox('"+url+"',{title:'日程安排',width:'900',height:'450'});\">" 
							+ day[i] + "</a>"; 
					}

					var len = this.lightDays.length;
					for (var i = 0; i < len; i++) {
						if (this.Days[this.lightDays[i]]){
							this.Days[this.lightDays[i]].style.backgroundColor="red";							
						}
					}
				}
			});

	document.getElementById("idCalendarPre").onclick = function() {
		var d = new Date(cale.Year, cale.Month - 2, 1);
		var iyear = d.getFullYear();
		var imonth = d.getMonth() + 1;
		cale.Year = iyear;
		cale.Month = imonth;
		doAjaxRefreshSelf('year:'+iyear+',month:'+imonth,'<%=portletId%>');
	};
	document.getElementById("idCalendarNext").onclick = function() {
		var d = new Date(cale.Year, cale.Month, 1);
		var iyear = d.getFullYear();
		var imonth = d.getMonth() + 1;
		cale.Year = iyear;
		cale.Month = imonth;
		doAjaxRefreshSelf('year:'+iyear+',month:'+imonth,'<%=portletId%>');
	};
	
</script>
