<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isCache = (String)request.getAttribute("isCache"); 
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
	<tr>
		<td width="120">数据URL&nbsp;<span style="color: red;">*</span></td>
		<td><input type="text" size="50" name="dataURL" id="dataURL" value="${dataURL}" /></td>
	</tr>
     <tr>
      <td width="120">默认值</td>
      <td>
        <input type="text" name="defaultVariableValues" id="defaultVariableValues" size="50" value="${defaultVariableValues}" />
      </td>
    </tr>
    <tr>
      <td width="120">是否缓存</td>
      <td>
        <input type="radio" name="isCache" id="isCacheY" value="Y" />是&nbsp;&nbsp;
        <input type="radio" name="isCache" id="isCacheN" value="N" />否</td>
    </tr> 
    <tr>
      <td width="120">缓存时间</td>
      <td>
        <input type="text" name="cacheMinutes" id="cacheMinutes" value="${cacheMinutes}" /> 分钟</td>
    </tr>    	    
	<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">	
<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>Form #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isCacheN').attr("checked","checked");
<%}%>
</script>
