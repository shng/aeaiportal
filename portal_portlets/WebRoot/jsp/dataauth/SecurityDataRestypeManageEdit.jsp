<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>数据权限-资源类型</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save',checkUnique:'true'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>资源编码</th>
	<td><input id="SEC_RES_CODE" label="资源编码" name="SEC_RES_CODE" type="text" value="<%=pageBean.inputValue("SEC_RES_CODE")%>" size="48" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>资源名称</th>
	<td><input id="SEC_RES_NAME" label="资源名称" name="SEC_RES_NAME" type="text" value="<%=pageBean.inputValue("SEC_RES_NAME")%>" size="48" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>实体编码</th>
	<td><input id="SEC_ENTITY_CODE" label="实体编码" name="SEC_ENTITY_CODE" type="text" value="<%=pageBean.inputValue("SEC_ENTITY_CODE")%>" size="48" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><textarea id="SEC_DESC" label="描述" name="SEC_DESC" cols="60" rows="5" class="textarea"><%=pageBean.inputValue("SEC_DESC")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="SEC_ID" name="SEC_ID" value="<%=pageBean.inputValue4DetailOrUpdate("SEC_ID","")%>" />
</form>
<script language="javascript">
charNumUnderlineValidator.add("SEC_RES_CODE");
requiredValidator.add("SEC_RES_CODE");
requiredValidator.add("SEC_RES_NAME");
requiredValidator.add("SEC_ENTITY_CODE");
lengthValidators[0].set(32).add("SEC_RES_CODE");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
