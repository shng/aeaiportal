<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>消息提醒-收件箱</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link href="<%=request.getContextPath()%>/css/abutton.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/aeditors/kdeditor/themes/default/default.css" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/aeditors/kdeditor/kindeditor.js"></script>
<style>
	.form_table{
		width:100%;
		background-color: #FFF;
		font: 80%/1.5 Arial,Helvetica,sans-serif;
		color: #111; 
		background-color: #FFF; 
	}
	
	.form_label{
		text-align:right;
		height:30px;
		width:100px;
	}
	
	.form_input{
		height:20px;
	}
	
	.form_select{
		height:25px;
		width:200px;
	}
</style>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
	<td>&nbsp;</td>
	<div class="div_button">
		<a href="javascript:saveMsg();" class="a_linked_button">发&nbsp;送</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="javascript:goToBack();" class="a_linked_button">返&nbsp;回</a>
	</div>
	<td>&nbsp;</td>
	<table class="detailTable" cellspacing="0px">
	  <tr>
		<th width="100" nowrap>主题</th>
		<td class="form_ele"><input id="MSG_THEME" name="MSG_THEME" label="主题" type="text" value="<%=pageBean.inputValue("MSG_THEME")%>" class="form_input" size="50" />最多30个字</td>
	  </tr>
	  <tr>
		<th width="100" nowrap>收件人</th>
		<td>		
			<input id="MSG_RECEIVEOR_NAME" name="MSG_RECEIVEOR_NAME" type="text" value="<%=pageBean.inputValue("MSG_RECEIVEOR_NAME")%>" class="form_input" size="50" readonly="readonly" />
			<input type="hidden" id="MSG_RECEIVEOR_CODE" name="MSG_RECEIVEOR_CODE" label="收件人" value="<%=pageBean.inputValue("MSG_RECEIVEOR_CODE")%>" />
			<img id="msgReceiveorCodeSelectImage" src="images/sta.gif" width="16" height="16" onclick="openMsgReceiveorCodeBox()" />
		</td>
	  </tr>
	  <tr>
		<th width="100" nowrap>通知方式</th>
		<td>
			<select id="MSG_NOTICE_TYPE" class="form_select" name="MSG_NOTICE_TYPE" disabled="disabled"><%=pageBean.selectValue("MSG_NOTICE_TYPE")%></select>
		</td>
	  </tr>
	  <tr>
	  	<th width="100" nowrap>消息内容</th>
		<td><textarea cols="100" id="MSG_NOTICE_CONTENT" name="MSG_NOTICE_CONTENT" rows="10"><%=pageBean.inputValue("MSG_NOTICE_CONTENT")%></textarea></td>
	  </tr>
	</table>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="userCode" id="userCode" value="<%=pageBean.inputValue("userCode")%>"/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="GUID" name="GUID" value="<%=pageBean.inputValue4DetailOrUpdate("GUID","")%>" />
</form>

<script type="text/javascript">
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="MSG_NOTICE_CONTENT"]', {
			resizeType : 1,
			width : '700px',
			height : '300px',			
			allowPreviewEmoticons : false,
			allowImageUpload : false,
			items : [
				'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'emoticons','link']
		});
	});
	
	var msgReceiveorCodeBox;
	function openMsgReceiveorCodeBox(){
		var handlerId = "ExtMsgUserTreeSelectSend";
		if (!msgReceiveorCodeBox){
			msgReceiveorCodeBox = new PopupBox('msgReceiveorCodeBox','请选择接收人：',{size:'normal',width:'280px',height:'400px',top:'2px'});
		}
		var url = 'index?'+handlerId;
		msgReceiveorCodeBox.sendRequest(url);
	}
	
	requiredValidator.add("MSG_THEME");
	requiredValidator.add("MSG_RECEIVEOR_CODE");
	new LengthLimiter("MSG_THEME",30);
	requiredValidator.add("MSG_NOTICE_TYPE");
	requiredValidator.add("MSG_RECEIVEOR_ID");
	initDetailOpertionImage();
	
	
	function saveMsg(){
		editor.sync();
		doSubmit({actionType:'save'});
	}
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
