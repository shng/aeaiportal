<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>消息提醒-详细</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link href="<%=request.getContextPath()%>/css/abutton.css" rel="stylesheet" type="text/css">
<style type="text/css">
	.bordered {
		padding-top:5px;
	}
    
	.bordered td{
	    border: 1px solid #ccc;
	    padding: 10px;
	    text-align: left;    
	}
	.div_button{
		margin-top: 10px;
	}
}
</style>
</head>
<body style="height:auto"> 
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
	<%@include file="/jsp/inc/message.inc.jsp"%>
	<div class="div_button">
		<a class="a_linked_button" href="javascript:goToBack();">返回消息列表</a>
	</div>
	<td>&nbsp;</td>
    <table class="detailTable" cellspacing="0px">
	<tr>
	<th width="100" nowrap>主题</th>
	<td><lable label="主题" name="MSG_THEME" class="text"><%=pageBean.inputValue("MSG_THEME")%></lable>
		</td>
	</tr>
	<tr>
	<th width="100" nowrap>收件人</th>
	<td><lable label="收件人" name="MSG_RECEIVEOR_NAME" class="text"><%=pageBean.inputValue("MSG_RECEIVEOR_NAME")%></lable>
		</td>
	</tr>
	<tr>
	<th width="100" nowrap>发送时间</th>
	<td><lable label="发送时间" name="MSG_CREATE_TIME" class="text"><%=pageBean.inputValue("MSG_CREATE_TIME")%></lable>
		</td>
	</tr>
	<tr>
	<th width="100" nowrap>来自</th>
	<td><lable label="来自" name="MSG_SEND_NAME" class="text"><%=pageBean.inputValue("MSG_SEND_NAME")%></lable>
		</td>
	</tr>
	<tr>
	<th width="100" nowrap>通知方式</th>
	<td><lable label="通知方式" name="MSG_NOTICE_TYPE_NAME" class="text"><%=pageBean.inputValue("MSG_NOTICE_TYPE_NAME")%></lable>
		</td>
	</tr>
	  <th width="100" nowrap>消息内容</th>
		<td colspan="3" ><div style="overflow-y:auto; overflow-x:auto; width:100%; height:255px;"><%=pageBean.inputValue("MSG_NOTICE_CONTENT")%></div></td>
	  </th>
	</table>
	<input type="hidden" name="actionType" id="actionType" value=""/>
	<input type="hidden" name="msgTheme" id="msgTheme" value="<%=pageBean.inputValue("msgTheme")%>"/>
	<input type="hidden" name="userCode" id="userCode" value="<%=pageBean.inputValue("userCode")%>"/>
	<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>