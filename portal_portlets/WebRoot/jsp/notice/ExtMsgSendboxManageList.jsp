<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="<%=request.getContextPath()%>/css/abutton.css" rel="stylesheet" type="text/css">
<title>消息提醒-发件箱</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function doDeleteReq(){	
	var ids = "";
	var confirmsubMsg="您确认删除该数据吗？";
	$("input:[name = 'SEND_ID'][checked]").each(function(){ 
		ids = ids+$(this).val()+",";
	});
	if (ids.length > 0){
		ids = ids.substring(0,ids.length-1);
	}
	if (ids == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if(confirm(confirmsubMsg)) {
		$("#ids").val(ids);
		doSubmit({actionType:'batchDelete'});
	}
}
function selectedCheck(indexId){	
	var idInt = parseInt(indexId);
	var currentIndexId = idInt ;
	if($("#ec_table tr:eq("+currentIndexId+") input[name = 'SEND_ID']").is(':checked')){
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'SEND_ID']").attr('checked',false);
	}else{
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'SEND_ID']").attr('checked',true);
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div style="padding-top: 10px; padding-bottom: 10px;">
	<table width="100%" border="0" cellpadding="0" cellspacing="1">
		<tr>
		   <td width="60px;"><a href="javascript:doRequest('insertRequest');" class="a_linked_button">新&nbsp;增</a></td>
		  <!--  <td width="60px"><a href="javascript:doRequest('updateRequest');" class="a_linked_button">编&nbsp;辑</a></td> -->
		   <td width="60px;"><a href="javascript:doRequest('copyRequest');" class="a_linked_button">复&nbsp;制</a></td>
		   <td width="60px;"><a href="javascript:showDetail('viewDetail');" class="a_linked_button">查&nbsp;看</a></td>   
		   <td width="60px;"><a href="javascript:doDeleteReq();" class="a_linked_button">删&nbsp;除</a></td>
		   <td>&nbsp;</td>
		   <td align="right" width="205px"><input name="msgTheme" type="text" value="<%=pageBean.inputValue("msgTheme")%>" style="height: 20px; width: 200px;"/></td>
		   <td align="right" width="60px"><a href="javascript:doQuery();" class="a_linked_button">查&nbsp;询</a></td>
		</tr>
	</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="消息提醒-发件箱.csv"
retrieveRowsCallback="process" xlsFileName="消息提醒-发件箱.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="13"
listWidth="100%" 
height="355px"
>
<ec:row styleClass="odd" ondblclick="showDetail('viewDetail')" onclick="selectedCheck('${GLOBALROWCOUNT}');selectRow(this,{GUID:'${row.SEND_ID}'})">
	<ec:column width="25" style="text-align:center" property="SEND_ID" cell="checkbox" headerCell="checkbox" onclick="$(this).parents('tr').trigger('click');"/>
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="70" property="MSG_RECEIVEOR_NAME" title="接收人" />		
	<ec:column width="190" property="MSG_THEME" title="消息主题" />
	<ec:column width="110" property="MSG_CREATE_TIME" title="发送时间" />
</ec:row>
</ec:table>
<input type="hidden" name="GUID" id="GUID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="userCode" id="userCode" value="<%=pageBean.inputValue("userCode")%>"/>
<input type="hidden" id="ids" name="ids" value="<%=pageBean.inputValue("ids")%>" />
<script language="JavaScript">
setRsIdTag('GUID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');

function showDetail(actionType) {
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	
	showSplash(waitMsg);
	$("#form1").attr('target',"_self");
	$("#actionType").val(actionType);
	$("#form1").attr('action', '<%=request.getContextPath() %>/index?ExtMsgSendboxManageDetail&operaType=detail');
	$("#form1").submit();
}
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
