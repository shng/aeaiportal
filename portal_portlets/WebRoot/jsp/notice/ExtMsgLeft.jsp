<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<title>左侧列表</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="../../js/jquery-1.4.2.js" language="javascript"></script>
</head>
<style>
	.div_msg_frame_left {
		float:left;
		width:145px;
		height:418px;
		border:solid 1px #0078C1;
	}
	.div_msg_frame_left ul{
		width:100%;	
		list-style-type: none;
		padding-left:0;
	
	}
	.div_msg_frame_left div{
		background:url(../../images/notice/msg_li_bg.png) no-repeat -2px 0px;
		height:28px;
		text-align:center;
		padding-top:10px;
		font-size:12px;
		font-style:"Microsoft YaHei";
		color: #FFFFFF;
	}
	
	.li_default{
		text-align:center;	
		line-height:25px;
		cursor: pointer; 
		color: #666666;
	}
	
	.li_selected{
		text-align:center;
		line-height:25px;
		cursor: pointer;
		color: #FFFFFF; 
		background:url(../../images/notice/select_focus.png) repeat -2px 0px;
	}
	
	.span_01{
		background:url(../../images/notice/jishou.png) no-repeat left;
		font-size:12px;
		font-family:"Microsoft YaHei";
		width: 100px;
    	display: inline-block;
	}
	.span_02{
		background:url(../../images/notice/fasong.png) no-repeat left;
		font-size:12px;
		font-family:"Microsoft YaHei";
		width: 100px;
    	display: inline-block;
	}
	.span_03{
		background:url(../../images/notice/huishou.png) no-repeat left;
		font-size:12px;
		font-family:"Microsoft YaHei";
		width: 100px;
    	display: inline-block;		
	}
	.span_04{
		background:url(../../images/notice/caogao.png) no-repeat;
		font-size:12px;
		font-family:"Microsoft YaHei";
		width: 100px;
    	display: inline-block;		
	}
	
	a:link { text-decoration: none;color: #666666}
　　a:active { text-decoration:blink }
　　a:hover { text-decoration:underline;color:#FFFFFF}
　　a:visited { text-decoration: none;color:#FFFFFF}
</style>
<script type="text/javascript">

	$(document).ready(function() {
		$('li.li_default').click(function(){
			$("li.li_selected").removeClass('li_selected');
			$(this).addClass('li_selected');
		});
	});
	
	function sendRequest(handlerId) {
		if (parent.mainFrame.showSplash){
			parent.mainFrame.showSplash('正在加载页面…，请等候！');	
		}
		var userCode = parent.document.getElementById("main").attributes["value"].nodeValue;
		parent.mainFrame.document.location.href = "<%=request.getContextPath()%>/index?" + handlerId + "&userCode=" + userCode;
	}
	
</script>
<body>
<input type="hidden" id="userCode" value="admin">
<div class="div_msg_frame_left">
 <div>我的文件夹</div>
    <ul>
 		<li class="li_default li_selected" id="span_1" onclick="sendRequest('ExtMsgReceiveboxManageList');">
 			<span class="span_01">已接收</span>
 		</li>
      	<li class="li_default" id="span_2" onclick="sendRequest('ExtMsgSendboxManageList');">
      		<span class="span_02">已发送</span>
      	</li>
		<!--       	
		<li class="li_default" id="span_3">
      		<span class="span_03">草稿箱</span>
      	</li>
	  	<li class="li_default" id="span_4">
	  		<span class="span_04">已删除</span>
	  	</li>
	  	 -->
    </ul>		
</div>
</body>
</html>