<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>表格字段管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var importRecordsBox;
function importRecords(){
	if (!importRecordsBox){
		importRecordsBox = new PopupBox('importRecordsBox','复制记录',{size:'normal',width:'600px',height:'460px',top:'10px'});
	}
	var url = 'index?GridListPickSelectList&currentPageId='+$('#pageCode').val()+'&currentPortletId='+$('#portletCode').val();
	importRecordsBox.sendRequest(url);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">
&nbsp;<input type="hidden" id="pageCode" name="pageCode" value="<%=pageBean.inputValue("pageCode")%>" />
&nbsp;<input type="hidden" id="portletCode" name="portletCode" value="<%=pageBean.inputValue("portletCode")%>" />

&nbsp;<input type="button" name="button" id="button" value="刷新" class="formbutton" onclick="doQuery()" />
</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="importRecords()"><input value="&nbsp;" title="导入" type="button" class="relateImgBtn" />导入</td>      
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doRequest('moveUp')" class="bartdx" align="center"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doRequest('moveDown')" class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.qBox.Close();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>   
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="表格字段管理.csv"
retrieveRowsCallback="process" xlsFileName="表格字段管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="auto"
>
<ec:row styleClass="odd" ondblclick="doRequest('viewDetail')" oncontextmenu="selectRow(this,{ID:'${row.ID}'});refreshConextmenu()" onclick="selectRow(this,{ID:'${row.ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="NAME" title="名称"   />
	<ec:column width="100" property="CODE" title="编码"   />
	<ec:column width="100" property="FIELD_TYPE" title="数据类型"   mappingItem="FIELD_TYPE"/>
	<ec:column width="100" property="WIDTH" title="宽度"   />
	<ec:column width="100" property="ALIGN_TYPE" title="对齐类型"   mappingItem="ALIGN_TYPE"/>
	<ec:column width="100" property="IS_HIDDEN" title="是否隐藏"   mappingItem="IS_HIDDEN"/>
</ec:row>
</ec:table>
<input type="hidden" name="ID" id="ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
