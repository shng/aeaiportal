package com.agileai.portal.bizmoduler.tips;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface TitleQuery extends StandardService {
	DataRow queryPortletTemptRecord(DataParam param);
	DataRow queryPageTemptRecord(DataParam param);
	DataRow queryColumnRecord(DataParam param);
	DataRow queryContentRecord(DataParam param);
	DataRow queryStaticDataRecord(DataParam param);
	DataRow queryMenuDataRecord(DataParam param);
	String getTitle(DataParam param);
}