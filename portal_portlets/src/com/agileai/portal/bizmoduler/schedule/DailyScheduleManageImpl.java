package com.agileai.portal.bizmoduler.schedule;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class DailyScheduleManageImpl
        extends StandardServiceImpl
        implements DailyScheduleManage {
    public DailyScheduleManageImpl() {
        super();
    }
    
	public List<DataRow> findRecords(DataParam param) {
		String sDateTime = param.get("sDate")+" 00:00:00";
		String eDateTime = param.get("eDate")+" 23:59:59";
		param.put("sDateTime",sDateTime);
		param.put("eDateTime",eDateTime);
		String statementId = sqlNameSpace+"."+"findRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
