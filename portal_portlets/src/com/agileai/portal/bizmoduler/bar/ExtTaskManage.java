package com.agileai.portal.bizmoduler.bar;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface ExtTaskManage
        extends StandardService {

	List<DataRow> findTaskRecords(String userCode, String state);
	String getTaskCount(String userCode, String state);
	DataRow getTaskRecord(String taskId);
	DataRow getUserInfoRecord(String userCode);
}
