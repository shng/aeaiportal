package com.agileai.portal.bizmoduler.notice;

import java.util.List;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface ExtMsgReceiveboxManage
        extends StandardService {
	public List<DataRow> findRemindRecords(DataParam param);
	
	public List<DataRow> findWinRecords(DataParam param);
	
	public DataRow findWinTotal(DataParam param);

}
