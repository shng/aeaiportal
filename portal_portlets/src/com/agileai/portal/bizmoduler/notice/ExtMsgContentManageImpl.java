package com.agileai.portal.bizmoduler.notice;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;


public class ExtMsgContentManageImpl
        extends StandardServiceImpl
        implements ExtMsgContentManage {
	
    public ExtMsgContentManageImpl() {
        super();
    }
    
	public void createRecord(DataParam param) {
		//设置常量
		Object noticeType = param.get("MSG_NOTICE_TYPE");
		if (noticeType == null 
				|| noticeType.toString().equals("")) {
			param.put("MSG_NOTICE_TYPE", "portalIM");
		}
		param.put("MSG_SEND_FLAG", 1);
		param.put("MSG_DELETE_FLAG", 0);
		param.put("MSG_VIEW_FLAG", 0);
		param.put("MSG_CREATE_TIME", DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL));
		
		//报存内容表
		String contentKey = KeyGenerator.instance().genKey();
		param.put("GUID", contentKey);
		param.put("MSG_ID", contentKey);
		this.daoHelper.insertRecord("ExtMsgContent.insertRecord", param);
		
		String receiveUserCodes = param.get("MSG_RECEIVEOR_CODE");
		if (!StringUtil.isNullOrEmpty(receiveUserCodes)) {
			String[] userCodeArray = receiveUserCodes.split(",");
			for (int i = 0; i < userCodeArray.length; i++) {
				param.put("MSG_RECEIVEOR_CODE", userCodeArray[i]);
				
				//保存发件箱表
				param.put("GUID", KeyGenerator.instance().genKey());
				this.getSendManager().createRecord(param);
				
				//保存收件箱表
				param.put("GUID", KeyGenerator.instance().genKey());
				this.getReceiveManager().createRecord(param);
			}
		} else {
			throw new RuntimeException("发送的对象为空");
		}
	}
	
	private ExtMsgReceiveboxManage getReceiveManager() {
		return (ExtMsgReceiveboxManage) BeanFactory.instance().getBean("extMsgReceiveboxManageService");
	}
	
	private ExtMsgSendboxManage getSendManager() {
		return (ExtMsgSendboxManage) BeanFactory.instance().getBean("extMsgSendboxManageTarget");
	}
    
}
