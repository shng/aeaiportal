package com.agileai.portal.bizmoduler.notice;

import java.util.List;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;

public class SecurityUserTreeSelectImpl extends TreeSelectServiceImpl implements
		SecurityUserTreeSelect {
	public List<DataRow> findChildGroupRecords(String parentId) {
		String statementId = this.sqlNameSpace + ".findChildGroupRecords";
		DataParam param = new DataParam(new Object[] { "parentId", parentId });
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
	
	@Override
	public List<DataRow> retrieveUserList(String resourceType, String resourceId) {
		String statementId = this.sqlNameSpace + ".retrieveUserList";
		DataParam param = new DataParam("resourceType",resourceType,"resourceId",resourceId);
		return this.daoHelper.queryRecords(statementId, param);
	}	
}