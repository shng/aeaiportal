package com.agileai.portal.bizmoduler.notice;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.QueryModelService;

public interface SecurityUserQuery
        extends QueryModelService {
	
	public void addUserTreeRelation(String roleId,String[] userIds);
	public void delUserTreeRelation(String roleId,String userId);
	public String getUserIdByCode(DataParam param);
	
	public String[] getUserByIds(List<String> userIds);
	public DataRow getUserByRoleId(List<String> roleIds);
	public DataRow getUserByGroupId(List<String> groupIds);
}
