package com.agileai.portal.bizmoduler.grid;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.util.MapUtil;

public class GridFieldsConfigManageImpl
        extends StandardServiceImpl
        implements GridFieldsConfigManage {
    public GridFieldsConfigManageImpl() {
        super();
    }
    public int getMaxSortNO(DataParam param){
    	int result = 0;
    	String statementId = getSqlNameSpace()+".getMaxSortNO";
    	DataRow row = this.daoHelper.getRecord(statementId, param);
    	if (!MapUtil.isNullOrEmpty(row)){
    		result = row.getInt("SORT_NO");
    	}
    	result = result +1;
    	return result;
    }
	public void updateSortNO(List<DataParam> paramList) {
		String statementId = getSqlNameSpace()+".updateSortNO";
		this.daoHelper.batchUpdate(statementId, paramList);
	}
}
