package com.agileai.portal.bizmoduler.grid;

import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.PickFillModelServiceImpl;
import com.agileai.util.ListUtil;

public class GridListPickSelectImpl
        extends PickFillModelServiceImpl
        implements GridListPickSelect {
    public GridListPickSelectImpl() {
        super();
    }

	public void copyGridListRecords(String selectPageId,
			String selectPortletId, String currentPageId,
			String currentPortletId) {
	    	String statementId = getSqlNameSpace()+".findRecords";
	    	DataParam param = new DataParam();
	    	param.put("pageCode",selectPageId,"portletCode",selectPortletId);
	    	List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
	    	statementId = getSqlNameSpace()+".insertRecord";
	    	List<DataParam> paramList = ListUtil.toParamList(records);
	    	for (int i=0;i < paramList.size();i++){
	    		DataParam tempParam = paramList.get(i);
	    		tempParam.put("ID",KeyGenerator.instance().genKey());
	    		tempParam.put("pageCode",currentPageId);
	    		tempParam.put("portletCode",currentPortletId);	    		
	    	}
	    	this.daoHelper.batchInsert(statementId, paramList);
	}
	public void copyGridListRecords(String selectPageId,
			String selectPortletId, String currentGridId) {
	    	String statementId = getSqlNameSpace()+".findRecords";
	    	DataParam param = new DataParam();
	    	param.put("pageCode",selectPageId,"portletCode",selectPortletId);
	    	List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
	    	statementId = "grid4boxfieldsconfig.insertRecord";
	    	List<DataParam> paramList = ListUtil.toParamList(records);
	    	for (int i=0;i < paramList.size();i++){
	    		DataParam tempParam = paramList.get(i);
	    		tempParam.put("ID",KeyGenerator.instance().genKey());
	    		tempParam.put("gridId",currentGridId);
	    	}
	    	this.daoHelper.batchInsert(statementId, paramList);
	}	
}
