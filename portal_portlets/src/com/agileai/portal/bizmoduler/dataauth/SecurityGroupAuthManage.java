package com.agileai.portal.bizmoduler.dataauth;


import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface SecurityGroupAuthManage
        extends StandardService {

	void saveGroupAuth(DataParam param);

	void delGroupAuth(DataParam param);
}
