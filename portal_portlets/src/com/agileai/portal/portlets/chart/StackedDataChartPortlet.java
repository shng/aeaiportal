package com.agileai.portal.portlets.chart;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;

import com.agileai.portal.driver.AttributeKeys;
import com.agileai.portal.driver.common.PortletRequestHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.driver.model.Decorator;
import com.agileai.portal.driver.model.PortletWindowConfig;
import com.agileai.portal.driver.model.Theme;
import com.agileai.portal.driver.service.PortalConfigService;
import com.agileai.portal.portlets.BaseMashupPortlet;

public class StackedDataChartPortlet extends BaseMashupPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String chartType = preferences.getValue("chartType", null);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		String chartHeight = preferences.getValue("chartHeight", null);
		
		String portletId = request.getWindowID();
		PortletWindowConfig portletWindowConfig = (PortletWindowConfig)request.getAttribute(portletId);
		
		if (!"auto".equals(portletWindowConfig.getHeight())){
			PortalConfigService portalConfigService = PortletRequestHelper.getPortalConfigService(request);
			String decoratorId = portletWindowConfig.getDecoratorId();
			if (Decorator.ExtendDecoratorId.equals(decoratorId)){
				Theme theme = (Theme)request.getAttribute(AttributeKeys.THEME_KEY);
				decoratorId = theme.getDecoratorId();
			}
			Decorator decorator = portalConfigService.getDecorator(decoratorId);
			chartHeight = String.valueOf(Integer.parseInt(portletWindowConfig.getHeight())-decorator.getHeaderHeight());
		}	
		
		String isTransparent = preferences.getValue("isTransparent", defaultIsTransparent);
		request.setAttribute("chartType", chartType);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		request.setAttribute("chartHeight", chartHeight);
		request.setAttribute("isTransparent", isTransparent);
		request.setAttribute("portletId", portletId);
		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String chartType = preferences.getValue("chartType", null);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		String chartHeight = preferences.getValue("chartHeight", null);
		String dataURL = preferences.getValue("dataURL", null);
		String isTransparent = preferences.getValue("isTransparent", defaultIsTransparent);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String cacheMinutes = preferences.getValue("cacheMinutes", defaultCacheMinutes);
		
		request.setAttribute("chartType", chartType);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		request.setAttribute("chartHeight", chartHeight);
		request.setAttribute("dataURL", dataURL);
		request.setAttribute("isTransparent", isTransparent);
		request.setAttribute("isCache", isCache);
		request.setAttribute("cacheMinutes", cacheMinutes);
		
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String chartType = request.getParameter("chartType");
		String defaultVariableValues = request.getParameter("defaultVariableValues");
		String chartHeight = request.getParameter("chartHeight");
		String dataURL = request.getParameter("dataURL");
		String isTransparent = request.getParameter("isTransparent");
		String isCache = request.getParameter("isCache");
		String cacheMinutes = request.getParameter("cacheMinutes");
		
		PreferencesWrapper preferWapper = new PreferencesWrapper();
		preferWapper.setValue("chartType", chartType);
		preferWapper.setValue("defaultVariableValues", defaultVariableValues);
		preferWapper.setValue("chartHeight", chartHeight);
		preferWapper.setValue("dataURL", dataURL);
		preferWapper.setValue("isTransparent", isTransparent);
		preferWapper.setValue("isCache", isCache);
		preferWapper.setValue("cacheMinutes", cacheMinutes);
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
}