package com.agileai.portal.portlets.reviews;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.sql.Timestamp;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.portal.bizmoduler.reviews.WcmInfoReviewManage;
import com.agileai.portal.driver.Resource;
import com.agileai.portal.driver.common.PortletRequestHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.portlets.BaseMashupPortlet;
import com.agileai.util.StringUtil;

public class ReviewInfoEntryPortlet extends BaseMashupPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		
		String contentIdParamKey = preferences.getValue("contentIdParamKey", null);
		
		Principal principal = request.getUserPrincipal();
		String currentUserId = principal.getName();

		String contentIdDefValue = preferences.getValue("contentIdDefValue", null);
		HttpServletRequest httpServletRequest = PortletRequestHelper.getRequestContext(request).getServletRequest();
		String contentId = httpServletRequest.getParameter(contentIdParamKey);
		if (StringUtil.isNullOrEmpty(contentId)) {
			contentId = contentIdDefValue;
		}
      
		request.setAttribute("infomationId", contentId);
		request.setAttribute("currentUserId", currentUserId);
		request.setAttribute("portletId", request.getWindowID());

		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String contentIdParamKey = preferences.getValue("contentIdParamKey", null);
		String contentIdDefValue = preferences.getValue("contentIdDefValue", null);

		request.setAttribute("contentIdParamKey", contentIdParamKey);
		request.setAttribute("contentIdDefValue", contentIdDefValue);
				
		super.doEdit(request, response);
	}


	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String contentIdParamKey = request.getParameter("contentIdParamKey");
		String contentIdDefValue = request.getParameter("contentIdDefValue");

		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("contentIdParamKey", contentIdParamKey);
		preferWapper.setValue("contentIdDefValue", contentIdDefValue);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	
	@Resource(id="saveReview")
	public void saveReview(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		DataParam param = new DataParam();
		param.put("REW_ANONYMITY",request.getParameter("REW_ANONYMITY"));
		param.put("REW_CONTENT",request.getParameter("REW_CONTENT"));
		param.put("USER_CODE",request.getParameter("USER_CODE"));
		param.put("INFO_ID",request.getParameter("INFO_ID"));
		param.put("REW_TIME", new Timestamp(System.currentTimeMillis()));
		
		getService().createRecord(param);
		PrintWriter writer = response.getWriter();
		writer.print("success");
		writer.close();
	}
	
	private StandardService getService() {
		WcmInfoReviewManage wcmInfoReviewManageService = (WcmInfoReviewManage) BeanFactory.instance().getBean("wcmInfoReviewManageService");
		return wcmInfoReviewManageService;
	}
}
