package com.agileai.portal.message.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(serviceName = "NoticeSend",targetNamespace = "http://service.message.portal.agileai.com/")
public interface NoticeSendProxy {
	
	@WebMethod
	public String sendMessage(@WebParam(name="userCodes")List<String> userIds, @WebParam(name="msgTitle")String msgTheme, @WebParam(name="msgContent")String msgContent, @WebParam(name="senderCode")String senderCode);

	@WebMethod
	public void kickoutOnline(@WebParam(name="userId")String userId,@WebParam(name="loginTime")String loginTime);
}
