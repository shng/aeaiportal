package com.agileai.portal.controller.grid;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.grid.Grid4boxFieldsConfigManage;

public class Grid4boxFieldsConfigManageListHandler
        extends StandardListHandler {
    public Grid4boxFieldsConfigManageListHandler() {
        super();
        this.editHandlerClazz = Grid4boxFieldsConfigManageEditHandler.class;
        this.serviceId = buildServiceId(Grid4boxFieldsConfigManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        initMappingItem("FIELD_TYPE",
                        FormSelectFactory.create("COLUMN_TYPE").getContent());
        initMappingItem("ALIGN_TYPE",
                        FormSelectFactory.create("ALIGN_TYPE").getContent());
        initMappingItem("IS_HIDDEN",
                        FormSelectFactory.create("BOOL_DEFINE").getContent());
    }
    @PageAction
	public ViewRenderer moveUp(DataParam param){
    	param.put("bigSortNoId",param.get("ID"));
    	List<DataRow> records = getService().findRecords(param);
    	if (records != null && records.size() <=1){
    		setErrorMsg("不能上移，请确认！");
    	}else{
    		List<DataParam> paramList = new ArrayList<DataParam>();
    		DataParam param0 = records.get(records.size()-1).toDataParam();
    		DataParam param1 = records.get(records.size()-2).toDataParam();
    		paramList.add(param0);
    		paramList.add(param1);
    		this.exchangeField(paramList,"SORT_NO");
    		getService().updateSortNO(paramList);
    	}
    	param.remove("bigSortNoId");
    	return prepareDisplay(param);
	}
    @PageAction
	public ViewRenderer moveDown(DataParam param){
    	param.put("smallSortNoId",param.get("ID"));
    	List<DataRow> records = getService().findRecords(param);
    	if (records != null && records.size() <=1){
    		setErrorMsg("不能下移，请确认！");
    	}else{
    		List<DataParam> paramList = new ArrayList<DataParam>();
    		DataParam param0 = records.get(0).toDataParam();
    		DataParam param1 = records.get(1).toDataParam();
    		paramList.add(param0);
    		paramList.add(param1);
    		this.exchangeField(paramList,"SORT_NO");
    		getService().updateSortNO(paramList);
    	}
    	param.remove("smallSortNoId");
    	return prepareDisplay(param);
	}
    private void exchangeField(List<DataParam> paramList,String field){
    	DataParam param0 = paramList.get(0);
    	DataParam param1 = paramList.get(1);
    	String value0 = param0.get(field);
    	String value1 = param1.get(field);
    	param0.put(field,value1);
    	param1.put(field,value0);
    }
    protected void initParameters(DataParam param) {
        initParamItem(param, "pageCode", "");
        initParamItem(param, "portletCode", "");
    }

    protected Grid4boxFieldsConfigManage getService() {
        return (Grid4boxFieldsConfigManage) this.lookupService(this.getServiceId());
    }
}
