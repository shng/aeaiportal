package com.agileai.portal.controller.notice;

import com.agileai.domain.*;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.portal.bizmoduler.notice.ExtMsgContentManage;

public class ExtMsgContentManageListHandler
        extends StandardListHandler {
    public ExtMsgContentManageListHandler() {
        super();
        this.serviceId = buildServiceId(ExtMsgContentManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    }

    protected ExtMsgContentManage getService() {
        return (ExtMsgContentManage) this.lookupService(this.getServiceId());
    }
}
