package com.agileai.portal.controller.notice;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.notice.ExtMsgSendboxManage;

public class ExtMsgSendboxManageListHandler
        extends StandardListHandler {
    public ExtMsgSendboxManageListHandler() {
        super();
        this.editHandlerClazz = ExtMsgSendboxManageEditHandler.class;
        this.serviceId = buildServiceId(ExtMsgSendboxManage.class);
    }
    
    public  ViewRenderer doBatchDeleteAction(DataParam param) {
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
        	String id = idArray[i];
        	DataParam idParam = new DataParam();
        	idParam.put("GUID", id);
        	getService().deletRecord(idParam);
    	}
		return prepareDisplay(param);
    	
   	}
    
    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "msgTheme", "");
    }

    protected ExtMsgSendboxManage getService() {
        return (ExtMsgSendboxManage) this.lookupService(this.getServiceId());
    }
}
