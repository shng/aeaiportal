package com.agileai.portal.controller.notice;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.notice.ExtMsgReceiveboxManage;
import com.agileai.portal.message.service.NoticeSendProxy;

public class ExtMsgReceiveboxManageEditHandler
        extends StandardEditHandler {
	
    public ExtMsgReceiveboxManageEditHandler() {
        super();
        this.listHandlerClass = ExtMsgReceiveboxManageListHandler.class;
        this.serviceId = buildServiceId(ExtMsgReceiveboxManage.class);
    }
    
    @PageAction
	public ViewRenderer viewDetail(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);	
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
	public ViewRenderer doBackAction(DataParam param){
		return new DispatchRenderer(getHandlerURL(listHandlerClass));
	}
    
    protected void processPageAttributes(DataParam param) {
        setAttribute("MSG_NOTICE_TYPE",
                FormSelectFactory.create("INFORM_WAY")
                                 .addSelectedValue(getOperaAttributeValue("MSG_NOTICE_TYPE",
                                                                          "portalIM")));
        setAttribute("userCode", param.get("userCode"));
        setAttribute("msgTheme", param.get("msgTheme"));
    }
    
	public ViewRenderer doSaveAction(DataParam param){
		
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			String receiveUsers = param.get("MSG_RECEIVEOR_CODE");
			List<String> userCodes = new ArrayList<String>();
			String[] receiveUsersArray = receiveUsers.split(",");
			for (int i = 0; i < receiveUsersArray.length; i++) {
				userCodes.add(receiveUsersArray[i]);
			}
			//发送消息
			NoticeSendProxy port = getProxyService();
	        port.sendMessage(userCodes,param.get("MSG_THEME"),
	        		param.get("MSG_NOTICE_CONTENT"),param.get("userCode"));
		}
		else if (OperaType.UPDATE.equals(operateType)){
			//getService().updateRecord(param);	
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	
    @PageAction
    public ViewRenderer updateReceive(DataParam param) {
    	getService().updateRecord(param);
    	return new AjaxRenderer("SUCCESS");
    }

    protected ExtMsgReceiveboxManage getService() {
        return (ExtMsgReceiveboxManage) this.lookupService(this.getServiceId());
    }
    
    protected NoticeSendProxy getProxyService() {
        return (NoticeSendProxy) this.lookupService("noticeSendProxy");
    }
}
