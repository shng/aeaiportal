package com.agileai.portal.controller.notice;

import java.util.*;
import com.agileai.domain.*;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.*;
import com.agileai.portal.bizmoduler.notice.ExtMsgSendboxManage;
import com.agileai.portal.message.service.NoticeSendProxy;

public class ExtMsgSendboxManageEditHandler
        extends StandardEditHandler {
	
    public ExtMsgSendboxManageEditHandler() {
        super();
        this.listHandlerClass = ExtMsgSendboxManageListHandler.class;
        this.serviceId = buildServiceId(ExtMsgSendboxManage.class);
    }
    
    @PageAction
	public ViewRenderer viewDetail(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);	
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
	public ViewRenderer doBackAction(DataParam param){
		return new DispatchRenderer(getHandlerURL(listHandlerClass));
	}

    protected void processPageAttributes(DataParam param) {
        setAttribute("MSG_NOTICE_TYPE",
                FormSelectFactory.create("INFORM_WAY")
                                 .addSelectedValue(getOperaAttributeValue("MSG_NOTICE_TYPE",
                                                                          "portalIM")));
        setAttribute("userCode", param.get("userCode"));
        setAttribute("msgTheme", param.get("msgTheme"));
    }
    
	public ViewRenderer doSaveAction(DataParam param){
		
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			String receiveUsers = param.get("MSG_RECEIVEOR_CODE");
			List<String> userCodes = new ArrayList<String>();
			String[] receiveUsersArray = receiveUsers.split(",");
			for (int i = 0; i < receiveUsersArray.length; i++) {
				userCodes.add(receiveUsersArray[i]);
			}
			//发送消息
			NoticeSendProxy port = getProxyService();
	        port.sendMessage(userCodes,param.get("MSG_THEME"),
	        		param.get("MSG_NOTICE_CONTENT"),param.get("userCode"));
		}
		else if (OperaType.UPDATE.equals(operateType)){
			//getService().updateRecord(param);	
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}

    protected ExtMsgSendboxManage getService() {
        return (ExtMsgSendboxManage) this.lookupService(this.getServiceId());
    }
    
    protected NoticeSendProxy getProxyService() {
        return (NoticeSendProxy) this.lookupService("noticeSendProxy");
    }
}
