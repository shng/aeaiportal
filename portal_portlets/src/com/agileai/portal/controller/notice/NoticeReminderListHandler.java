package com.agileai.portal.controller.notice;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.notice.ExtMsgReceiveboxManage;
import com.agileai.portal.message.service.NoticeSendProxy;

public class NoticeReminderListHandler 
		extends StandardListHandler{
	
	public ViewRenderer prepareDisplay(DataParam param){
		String responseText = null;
		String userCode = request.getParameter("userCode");
		param.put("userCode", userCode);
		List<DataRow> dataList = getService().findRemindRecords(param);		
		JSONArray jsonArray = new JSONArray();
		try {
			if (dataList != null && dataList.size() > 0) {
				for (int i=0; i<dataList.size(); i++) {				
					DataRow dataLine = dataList.get(i);
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("id", dataLine.stringValue("RECEIVE_ID"));
					jsonObject.put("title", dataLine.stringValue("MSG_THEME"));
					jsonObject.put("time", dataLine.stringValue("MSG_CREATE_TIME"));
					jsonArray.put(jsonObject);
					if(i>4){
						break;
					}
				}
			} 		
			JSONObject json = new JSONObject();
			json.put("datas", jsonArray);	
			responseText = json.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		
		return new AjaxRenderer(responseText);
	}
	
    @PageAction
	public ViewRenderer viewDetail(DataParam param) {
		DataRow record = getService().getRecord(param);
		String responseText =null;
		try {
			JSONObject jsonObject = new JSONObject();		
			jsonObject.put("MSG_THEME", record.stringValue("MSG_THEME"));
			jsonObject.put("MSG_NOTICE_CONTENT", record.stringValue("MSG_NOTICE_CONTENT"));
			jsonObject.put("MSG_NOTICE_TYPE_NAME", record.stringValue("MSG_NOTICE_TYPE_NAME"));
			jsonObject.put("MSG_SEND_NAME", record.stringValue("MSG_SEND_NAME"));
			jsonObject.put("MSG_RECEIVEOR_NAME", record.stringValue("MSG_RECEIVEOR_NAME"));
			jsonObject.put("MSG_CREATE_TIME", record.stringValue("MSG_CREATE_TIME"));
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		
		if(record.stringValue("MSG_VIEW_FLAG").equals("0")){
			DataParam updateParam = new DataParam("MSG_VIEW_FLAG", "1","GUID", record.get("RECEIVE_ID"));
			getService().updateRecord(updateParam);			
		}
		return new AjaxRenderer(responseText);
	}
	
    @PageAction
	public ViewRenderer mobileSendMessage(DataParam param){
		String responseText = FAIL;
		try {
			Profile profile = this.getProfile();
			String inputString = this.getInputString();
			JSONObject jsonObject = new JSONObject(inputString);
			String userCode = (String) profile.getUserId();
			String title = jsonObject.get("title").toString();
			String content = jsonObject.get("content").toString();
			String receiveCode = jsonObject.get("receiveCode").toString();
			//发送消息
			NoticeSendProxy port = getProxyService();
			List<String> userCodes = new ArrayList<String>();
			userCodes.add(receiveCode);
	        port.sendMessage(userCodes,title,content,userCode);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
    
    protected NoticeSendProxy getProxyService() {
        return (NoticeSendProxy) this.lookupService("noticeSendProxy");
    }
	protected ExtMsgReceiveboxManage getService() {
		return (ExtMsgReceiveboxManage) this.lookupService("extMsgReceiveboxManageService");
    }
}
