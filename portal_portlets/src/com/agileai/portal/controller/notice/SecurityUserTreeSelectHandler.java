package com.agileai.portal.controller.notice;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.notice.SecurityUserTreeSelect;
import com.agileai.util.StringUtil;

public class SecurityUserTreeSelectHandler extends TreeSelectHandler {
	public SecurityUserTreeSelectHandler() {
		this.serviceId = buildServiceId(SecurityUserTreeSelect.class);
		this.isMuilSelect = true;
		this.checkRelParentNode = false;
		this.mulSelectNodeTypes.add("Object");
	}

	public ViewRenderer prepareDisplay(DataParam param) {
		setAttributes(param);
		return new LocalRenderer(getPage());
	}

	protected TreeBuilder provideTreeBuilder(DataParam param) {
		return null;
	}

	@PageAction
	public ViewRenderer retrieveJson(DataParam param) {
		String responseText = "fail";
		try {
			JSONArray jsonArray = new JSONArray();
			String id = param.get("id", "00000000-0000-0000-00000000000000000");

			SecurityUserTreeSelect securityUserTreeSelect = getService();
			List<DataRow> records = securityUserTreeSelect.findChildGroupRecords(id);

			if ((records != null) && (!records.isEmpty())) {
				for (int i = 0; i < records.size(); i++) {
					DataRow row = (DataRow) records.get(i);
					JSONObject jsonObject = new JSONObject();
					String groupId = row.stringValue("GRP_ID");
					String groupName = row.stringValue("GRP_NAME");
					jsonObject.put("id", groupId);
					jsonObject.put("text", groupName);
					jsonObject.put("state", "closed");
					jsonArray.put(jsonObject);
				}
			}

			records = securityUserTreeSelect
					.queryPickTreeRecords(new DataParam(new Object[] {
							"groupId", id }));
			if ((records != null) && (!records.isEmpty())) {
				List<String> existUserCodes = getExistUserCodes(param);
				for (int i = 0; i < records.size(); i++) {
					DataRow row = (DataRow) records.get(i);
					String userCode = row.stringValue("USER_CODE");
					if (existUserCodes.contains(userCode)) {
						continue;
					}
					String userName = row.stringValue("USER_NAME");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("id", userCode);
					jsonObject.put("text", userName);
					jsonArray.put(jsonObject);
				}
			}
			responseText = jsonArray.toString();
		} catch (Exception e) {
			this.log.error(e);
		}
		return new AjaxRenderer(responseText);
	}

	private List<String> getExistUserCodes(DataParam param) {
		String resourceType = param.get("resourceType");
		String resourceId = param.get("resourceId");
		List<String> existUserCodes = new ArrayList<String>();
		if ((StringUtil.isNotNullNotEmpty(resourceType))
				&& (StringUtil.isNotNullNotEmpty(resourceId))) {
			SecurityUserTreeSelect securityUserTreeSelect = this.getService();
			List<DataRow> existsRecords = securityUserTreeSelect.retrieveUserList(
					resourceType, resourceId);
			for (int i = 0; i < existsRecords.size(); i++) {
				DataRow row = (DataRow) existsRecords.get(i);
				String userCode = row.stringValue("USER_CODE");
				if (existUserCodes.contains(userCode)) {
					continue;
				}
				existUserCodes.add(userCode);
			}
		}
		return existUserCodes;
	}

	@PageAction
	public ViewRenderer addUserRequest(DataParam param) {
		return prepareDisplay(param);
	}

	protected SecurityUserTreeSelect getService() {
		return (SecurityUserTreeSelect) lookupService(getServiceId());
	}
}