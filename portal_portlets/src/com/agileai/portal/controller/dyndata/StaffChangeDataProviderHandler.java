package com.agileai.portal.controller.dyndata;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.XmlUtil;

public class StaffChangeDataProviderHandler extends SimpleHandler{
	
	public StaffChangeDataProviderHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String resourceType = param.get("resourceType");
		StaffChangeDataBuilder builder = new StaffChangeDataBuilder();
		String datas = null;
		if (resourceType.equals("Histogram")){			
			String date = param.get("year")+"-"+param.get("month");
			DataRow xmlRow = builder.getXmlData(date);
			datas = this.buildHistogramXML(xmlRow);
		}			
		else if (resourceType.equals("BarChart")){
			String date = param.get("year")+"-"+param.get("month");
			DataRow xmlRow = builder.getXmlData(date);
			datas = this.buildBarChartXML(xmlRow);
		}
		else if (resourceType.equals("DataGrid")){
			String date = param.get("year")+"-"+param.get("month");
			DataSet gridSet = builder.getGridJsonData(date);
			datas = this.buildDataGridJson(gridSet);
		}
		else if (resourceType.equals("BoxData")){
			String classify = param.get("classify");
			DataSet boxSet = builder.getBoxJsonData(classify);
			datas = this.buildBoxJson(boxSet);
		}
		else if (resourceType.equals("HtmlData")){
			String date = param.get("date");
			String rowId = param.get("rowId");
			DataSet gridSet = builder.getGridJsonData(date);
			datas = this.buildHtmlJson(gridSet,rowId);
		}
		this.setAttribute("datas",datas);
		return new LocalRenderer(getPage());
	}
	
	private String buildHtmlJson(DataSet set,String rowId) {
		String result = null;
		String fields[] = {"ROWID","CLASSIFY","NEWEMP","QUITS","PROMOTION","DEMOTION"};
		try {
			JSONObject object = new JSONObject();
			DataRow row = set.getDataRow(Integer.parseInt(rowId)-1);
			for(int i=0;i<row.size()-1;i++){
				object.put(fields[i], row.stringValue(fields[i]));
			}
			result = object.toString();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private String buildDataGridJson(DataSet set) {
		String result = null;
		String fields[] = {"ROWID","DATE","CLASSIFY","NEWEMP","QUITS","PROMOTION","DEMOTION"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<set.size();i++){
				DataRow row = set.getDataRow(i);
				JSONObject object = new JSONObject();
				for(int j=0;j<row.size();j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}	
				jsonArray.put(object);
			}
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String buildBoxJson(DataSet set) {
		String result = null;
		String fields[] = {"MONTH","NEWEMP","QUITS","PROMOTION","DEMOTION"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<set.size();i++){
				JSONObject object = new JSONObject();
				DataRow row = set.getDataRow(i);
				for(int j=0;j<row.size();j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}
				jsonArray.put(object);
			}									
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private String buildBarChartXML(DataRow row) {
		String count1 = row.stringValue("SUB_NEWEMP");			
		String count2 = row.stringValue("SUB_QUITS");			
		String count3 = row.stringValue("SUB_PROMOTION");			
		String count4 = row.stringValue("SUB_DEMOTION");
		String result = "";
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart");
		charElement.addAttribute("formatNumberScale","0");
		charElement.addAttribute("caption","分支机构人员变动");
		charElement.addAttribute("clickURL", "javascript:doPopupWebPage('targetPage:03/popupdata-grid-change.ptml,classify:branch',{width:'900',height:'350'})");
		
		Element setElement1 = charElement.addElement("set");
		setElement1.addAttribute("value", count1);
		setElement1.addAttribute("label", "新增");
		
		Element setElement2 = charElement.addElement("set");
		setElement2.addAttribute("value", count2);
		setElement2.addAttribute("label", "离职");
		
		Element setElement3 = charElement.addElement("set");
		setElement3.addAttribute("value", count3);
		setElement3.addAttribute("label", "晋级");
		
		Element setElement4 = charElement.addElement("set");
		setElement4.addAttribute("value", count4);
		setElement4.addAttribute("label", "降级");
		
		result = document.asXML();
		return result;
	}

	private String buildHistogramXML(DataRow row){
		String count1 = row.stringValue("BUS_NEWEMP");			
		String count2 = row.stringValue("BUS_QUITS");			
		String count3 = row.stringValue("BUS_PROMOTION");			
		String count4 = row.stringValue("BUS_DEMOTION");
		String result = "";
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart");
		charElement.addAttribute("formatNumberScale","0");
		charElement.addAttribute("caption","总部人员变动");
		charElement.addAttribute("yAxisName","变动数目");
		charElement.addAttribute("xAxisName","变动类型");
		charElement.addAttribute("showNames","1");
		charElement.addAttribute("clickURL", "javascript:doPopupWebPage('targetPage:03/popupdata-grid-change.ptml,classify:head',{width:'900',height:'350'})");
		
		Element setElement1 = charElement.addElement("set");
		setElement1.addAttribute("value", count1);
		setElement1.addAttribute("label", "新增");
		
		Element setElement2 = charElement.addElement("set");
		setElement2.addAttribute("value", count2);
		setElement2.addAttribute("label", "离职");
		
		Element setElement3 = charElement.addElement("set");
		setElement3.addAttribute("value", count3);
		setElement3.addAttribute("label", "晋级");
		
		Element setElement4 = charElement.addElement("set");
		setElement4.addAttribute("value", count4);
		setElement4.addAttribute("label", "降级");
		
		result = document.asXML();
		return result;
	}
}