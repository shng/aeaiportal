package com.agileai.portal.controller.dyndata;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.XmlUtil;

public class CashFlowDataProviderHandler extends SimpleHandler{	

	public CashFlowDataProviderHandler(){       
		super();
	} 
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String resourceType = param.get("resourceType");
		CashFlowDataBuilder builder = new CashFlowDataBuilder();
		String datas = null;
		if (resourceType.equals("XMLData")){			
			String quarter = param.get("quarter");
			String project = param.get("project");
			String chartType = param.get("chartType");
			DataRow xmlRow = builder.getXmlData(quarter,project,chartType);
			datas = this.buildChartXML(xmlRow,quarter,project,chartType);
		}			
		else if (resourceType.equals("DataGrid")){
			String quarter = param.get("quarter");
			String project = param.get("project");
			DataSet gridSet = builder.getGridJsonData(quarter, project);
			datas = this.buildDataGridJson(gridSet);
		}
		else if (resourceType.equals("BoxData")){
			String quarter = param.get("quarter");
			String project = param.get("project");
			String chartType = param.get("chartType");			
			DataSet boxSet = builder.getBoxJsonData(quarter,project,chartType);
			datas = this.buildBoxJson(boxSet);
		}
		else if (resourceType.equals("HtmlData")){
			String id = param.get("id");
			String project = param.get("project");
			DataRow htmlRow = builder.getHtmlJsonData(id, project);
			datas = this.buildHtmlJson(htmlRow);
		}
		this.setAttribute("datas",datas);
		return new LocalRenderer(getPage());
	}
	
	private String buildDataGridJson(DataSet set) {		
		String result = null;
		String fields[] = {"ID","MONTH","PROJECT","INFLOW","OUTFLOW"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<set.size();i++){
				DataRow row = set.getDataRow(i);
				JSONObject object = new JSONObject();
				for(int j=0;j<row.size();j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}	
				jsonArray.put(object);
			}
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String buildBoxJson(DataSet set) {
		String result = null;
		String fields[] = {"ID","MONTH","PROJECT","FLOW"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<set.size();i++){
				JSONObject object = new JSONObject();
				DataRow row = set.getDataRow(i);
				for(int j=0;j<row.size();j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}
				jsonArray.put(object);
			}									
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String buildHtmlJson(DataRow row){
		String result = null;
		String fields[] = {"ID","MONTH","PROJECT","DESCRIPTION","INFLOW","OUTFLOW"};
		try {
			JSONObject object = new JSONObject();
			for(int i=0;i<row.size();i++){
				object.put(fields[i], row.stringValue(fields[i]));
			}
			result = object.toString();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private String buildChartXML(DataRow row,String quarter,String project,String chartType) {
		String count1 = row.stringValue("FIRST");			
		String count2 = row.stringValue("SECOND");			
		String count3 = row.stringValue("THIRD");			
		String result = "";		
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart"); 
		charElement.addAttribute("formatNumberScale","0");
		if(chartType.equals("line")){
			charElement.addAttribute("caption","现金流入情况");
		}else{
			charElement.addAttribute("caption","现金流出情况");
		}
		charElement.addAttribute("yAxisName","金额（万）");
		charElement.addAttribute("xAxisName","月份");
		charElement.addAttribute("showNames","1");
		charElement.addAttribute("clickURL", "javascript:doPopupWebPage('targetPage:03/cashflow-grid.ptml,quarter:"+quarter+",project:"+project+",chartType:"+chartType+"',{width:'900',height:'350'})");

		Element setElement1 = charElement.addElement("set");
		setElement1.addAttribute("value", count1);
		setElement1.addAttribute("label", "01");
		
		Element setElement2 = charElement.addElement("set");
		setElement2.addAttribute("value", count2);
		setElement2.addAttribute("label", "02");
		
		Element setElement3 = charElement.addElement("set");
		setElement3.addAttribute("value", count3);
		setElement3.addAttribute("label", "03");
		
		result = document.asXML();
		return result;
	}
}
