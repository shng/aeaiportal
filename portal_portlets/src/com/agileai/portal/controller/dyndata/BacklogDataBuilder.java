package com.agileai.portal.controller.dyndata;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.util.DateUtil;

public class BacklogDataBuilder {
	private DataSet data;
	
	public DataSet getData() {
		return data;
	}
	public void setData(DataSet data) {
		this.data = data;
	}

	public BacklogDataBuilder(){
		this.setData(new DataSet());
		String currentDate = DateUtil.getYear()+"-"+DateUtil.getMonth()+"-"+DateUtil.getDay();
		String lastDate = DateUtil.getYear()+"-"+String.valueOf(Integer.parseInt(DateUtil.getMonth())-1)+"-"+DateUtil.getDay();
		String preDate = DateUtil.getYear()+"-"+String.valueOf(Integer.parseInt(DateUtil.getMonth())-3)+"-"+DateUtil.getDay();
		DataRow row1 = new DataRow("ID","1","TITLE","综合计算工时","TYPE","工时统计","SPONSOR","zhangyu","DATE",currentDate,
				"CONTENT","综合计算工时工作制是指因工作性质特殊需要连续作业，或受季节和自然条件限制，不能实行标准工作时间制度，以周、月、季、半年、年等为周期综合计算工作时间的工作制度。");
		DataRow row2 = new DataRow("ID","2","TITLE","不定时工时","TYPE","工时统计","SPONSOR","liyiwei","DATE",lastDate,
				"CONTENT","不定时工作是指因生产特点、工作特殊需要或职责范围的关系，不能按标准工作时间衡量或需要机动作业，采取不确定工作时间的工作制度。");
		DataRow row3 = new DataRow("ID","3","TITLE","标准工时","TYPE","工时统计","SPONSOR","zhounan","DATE",preDate,
				"CONTENT","标准工时制度，是由立法确定一昼夜中工作时间长度，一周中工作日天数，并要求各用人单位和一般职工普遍实行的基本工时制度。标准工时制是标准和基础，是其它特殊工时制度的计算依据和参照标准。因此标准工时制具有至关重要的意义，也是各国劳动立法中的重要内容。");
		DataRow row4 = new DataRow("ID","6","TITLE","任务分解","TYPE","项目安排","SPONSOR","zhangyu","DATE",currentDate,
				"CONTENT","按照与委托方达成的协议，被委托方应在指定工作日内完成委托工作。项目经理要根据这一要求，结合以往的工作经验，对项目进度进行阶段划分，并提出进度计划。对于大中型项目应绘制进度横道图，以便进行检查。");
		DataRow row5 = new DataRow("ID","7","TITLE","团队组成与分工","TYPE","项目安排","SPONSOR","liyiwei","DATE",lastDate,
				"CONTENT","为完成项目工作任务，项目经理应拟定项目团队的名单、确定团队成员，并根据任务和团队成员各自特点进行分工。当然，工作大纲中的名单与分工，只是初步的，随着项目工作的深入，可能会进行一定的调整。");
		DataRow row6 = new DataRow("ID","8","TITLE","项目预算","TYPE","项目安排","SPONSOR","zhounan","DATE",preDate,
				"CONTENT","项目经理对项目的费用要有一个合理的估计，根据以往同类项目的工作经验和本项目的具体情况，结合有关规定完成费用预算计划。费用主要包括：旅费、人工费(含内部人员与外部人员)、办公费用等各项杂费，以及备用金等等。");
		DataRow row7 = new DataRow("ID","12","TITLE","采购付款报销","TYPE","报销手续","SPONSOR","zhangyu","DATE",currentDate,
				"CONTENT","采购员持有效单据到财务部结算，即必须附有正确及有效的票据来证实支出，发票复印件、白条一律不予受理。采购员采购时应要求供应商开具真实、合法的发票，单位名称、商品名称项目、数量、单价、金额必须填写清楚，否则财务部不予受理。");
		DataRow row8 = new DataRow("ID","13","TITLE","日常费用报销","TYPE","报销手续","SPONSOR","zhounan","DATE",preDate,
				"CONTENT","购买物品后需先填写报销单（必须提供正规发票），经副总经理签字确认落实，财务会计审核，并由总经理签字同意，方可向出纳支取款项。出差人员回公司后，出纳应及时提醒，督促报销。一周内必须完成报销工作。");
		
		this.getData().addDataRow(row1);
		this.getData().addDataRow(row2);
		this.getData().addDataRow(row3);
		this.getData().addDataRow(row4);
		this.getData().addDataRow(row5);
		this.getData().addDataRow(row6);
		this.getData().addDataRow(row7);
		this.getData().addDataRow(row8);
	}
	
	public JSONObject getTabJsonData(){
		JSONObject dataObject = new JSONObject();
		JSONArray array1 = new JSONArray(),array2 = new JSONArray(),array3 = new JSONArray();
		try {
			for(int i=0;i<this.getData().size();i++){
				JSONObject object = new JSONObject();
				DataRow row  = this.getData().getDataRow(i);
				object.put("taskTitle",row.get("TITLE"));
				object.put("taskSponsor",row.get("SPONSOR"));
				object.put("taskTime",row.get("DATE"));
				String id = row.getString("ID");
				object.put("taskTitleLink","javascript:doPopupWebPage('targetPage:03/backlog-detail.ptml,id:" + id + "',{width:900,height:350})");
				if ("工时统计".equals(row.get("TYPE"))){
					array1.put(object);
				}else if ("项目安排".equals(row.get("TYPE"))) {
					array2.put(object);
				}else if ("报销手续".equals(row.get("TYPE"))) {
					array3.put(object);
				}			
			}
			dataObject.put("datas1", array1);
			dataObject.put("datas2", array2);
			dataObject.put("datas3", array3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataObject;
	}
	
	public DataSet getGridJsonData(String type){
		DataSet gridSet = new DataSet();
		for(int i=0;i<this.getData().size();i++){
			DataRow row  = this.getData().getDataRow(i);
			if (type.equals(row.get("TYPE"))) {
				gridSet.addDataRow(row);
			}		
		}
		return gridSet;
	}
	
	public DataRow getBoxJsonData(String id){
		DataRow boxRow = new DataRow();
		for(int i=0;i<this.getData().size();i++){
			DataRow row  = this.getData().getDataRow(i);
			if (id.equals(row.get("ID"))) {
				boxRow = row;			
			}			
		}
		return boxRow;
	}
}
