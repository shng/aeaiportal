package com.agileai.portal.controller.tips;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.tips.TitleQuery;
import com.agileai.util.StringUtil;

public class TitleObtainHandler extends BaseHandler
{
	public TitleObtainHandler()
	{
		this.serviceId = buildServiceId(TitleQuery.class);
	}
	 
	@PageAction
	public ViewRenderer getTitle(DataParam param){
		String idParam = param.get("idParam");
		String title = "";
		if (StringUtil.isNotNullNotEmpty(idParam)){
			 title = getService().getTitle(param);			
		}
		return new AjaxRenderer(title);
	}

    protected TitleQuery getService() {
        return (TitleQuery)lookupService(getServiceId());
    }
}