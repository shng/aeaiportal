package com.agileai.portal.controller.schedule;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.schedule.DailyScheduleManage;

public class DailyScheduleManageEditHandler
        extends StandardEditHandler {
    public DailyScheduleManageEditHandler() {
        super();
        this.listHandlerClass = DailyScheduleManageListHandler.class;
        this.serviceId = buildServiceId(DailyScheduleManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    	User user = (User)this.getUser();
    	this.setAttribute("SCHEDULE_USER_ID", user.getUserCode());
    }

    protected DailyScheduleManage getService() {
        return (DailyScheduleManage) this.lookupService(this.getServiceId());
    }
}