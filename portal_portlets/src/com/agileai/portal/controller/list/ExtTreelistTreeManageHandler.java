package com.agileai.portal.controller.list;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeManageHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.portal.bizmoduler.list.ExtTreelistTreeManage;
import com.agileai.util.StringUtil;

public class ExtTreelistTreeManageHandler
        extends TreeManageHandler {
	private static final String DefaultListType= "Sample";
	
    public ExtTreelistTreeManageHandler() {
        super();
        this.serviceId = buildServiceId(ExtTreelistTreeManage.class);
        this.nodeIdField = "LIST_ID";
        this.nodePIdField = "LIST_PID";
        this.moveUpErrorMsg = "该节点是第一个节点，不能上移！";
        this.moveDownErrorMsg = "该节点是最后一个节点，不能下移！";
        this.deleteErrorMsg = "该节点还有子节点，不能删除！";
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        ExtTreelistTreeManage service = this.getService();
        String listType = param.get("listType",DefaultListType);
        param.put("listType",listType);
        List<DataRow> menuRecords = service.findTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(menuRecords, "LIST_ID",
                                                  "LIST_NAME", "LIST_PID");

        return treeBuilder;
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("LIST_ISVALID",
                     FormSelectFactory.create("SYS_VALID_TYPE")
                                      .addSelectedValue(getAttributeValue("LIST_ISVALID")));
        setAttribute("CHILD_LIST_ISVALID",
                     FormSelectFactory.create("SYS_VALID_TYPE")
                                      .addSelectedValue(getAttributeValue("CHILD_LIST_ISVALID",
                                                                          "1")));
        FormSelect listTypeFormSelect =  FormSelectFactory.create("TREE_LIST_TYPE");
        String listType = param.get("listType",DefaultListType);
        listTypeFormSelect.addSelectedValue(listType).addHasBlankValue(false);
        setAttribute("listType",listTypeFormSelect);
    }

    protected String provideDefaultNodeId(DataParam param) {
    	String result = param.get("listType",DefaultListType);
        return result;
    }

    protected boolean isRootNode(DataParam param) {
        boolean result = true;
        String nodeId = param.get(this.nodeIdField,
                                  this.provideDefaultNodeId(param));
        DataParam queryParam = new DataParam(this.nodeIdField, nodeId);
        DataRow row = this.getService().queryCurrentRecord(queryParam);

        if (row == null) {
            result = false;
        } else {
            String parentId = row.stringValue("LIST_PID");
            result = StringUtil.isNullOrEmpty(parentId);
        }

        return result;
    }

    protected ExtTreelistTreeManage getService() {
        return (ExtTreelistTreeManage) this.lookupService(this.getServiceId());
    }
}
