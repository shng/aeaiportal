package com.agileai.portal.controller.dataauth;

import com.agileai.domain.*;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.portal.bizmoduler.dataauth.SecurityDataRestypeManage;

public class SecurityDataRestypeManageListHandler
        extends StandardListHandler {
    public SecurityDataRestypeManageListHandler() {
        super();
        this.editHandlerClazz = SecurityDataRestypeManageEditHandler.class;
        this.serviceId = buildServiceId(SecurityDataRestypeManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "resName", "");
    }

    protected SecurityDataRestypeManage getService() {
        return (SecurityDataRestypeManage) this.lookupService(this.getServiceId());
    }
}
