function PortletContextMenu(menuName,portletId){
	this.keyMap = {Q:81,W:87,E:69,R:82,T:84,A:65,S:83,D:68,F:70,G:71,Z:90,X:88,C:67,V:86,B:66};
	this.handlerMap = new Map();
	this.menuName = menuName;
	this.portletId = portletId;
	eval(menuName+" = new ContextMenu('"+menuName+"','"+portletId+"');")
}

PortletContextMenu.prototype.bindItem = function(text,hotKey,handler){
	var menuName = this.menuName;
	var handlerMap = this.handlerMap;
	var keyMap = this.keyMap;
	
	eval(menuName+".addItem(text,text+'    ('+hotKey+')','"+menuName+"',handler);");
	handlerMap.put(hotKey,handler);
	
	$('#'+this.menuName+'_extmenu_'+this.menuName).attr('tabindex','0');
	$('#'+this.menuName+'_extmenu_'+this.menuName).bind('keydown',function(ievent){
		var evt = ievent || event;
		var key = evt.keyCode||evt.which||evt.charCode;
		$('#'+menuName+'_extmenu_'+menuName).hide();
		for (var i=0;i < handlerMap.size();i++){
			var element = handlerMap.element(i);
			var hotKey = element.key;
			if (key == eval('keyMap.'+hotKey)){
				var handler =  handlerMap.get(hotKey);
				handler.apply(handler.arguments)
				break;
			}	
		}
	});
	
	$('#'+this.portletId).bind('contextmenu',function(evt){
		eval(menuName+".onContextMenu(evt);");
	});
}

PortletContextMenu.prototype.setup = function(){
	var menuName = this.menuName;
	eval(menuName+".setup();");
}