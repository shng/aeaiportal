var processBox;
function viewTaskDetail(url){
	if (!processBox){
		processBox = new PopupBox('processBox','业务流程操作',{size:'normal',width:'900px',height:'700px',top:'3px',scroll:'yes'});
	}
	processBox.sendRequest(url);
}
var instanceGraphBox;
function viewInstanceGraphBox(processInstId,processId,bpmShowUrl){
	if (!instanceGraphBox){
			instanceGraphBox = new PopupBox('instanceGraphBox','业务流程图',{size:'normal',width:'900px',height:'500px',top:'3px',scroll:'yes'});
		}
		var url = bpmShowUrl+"&actionType=prepareDisplay&WFIP_ID="+processInstId+"&WFP_ID="+processId;
		instanceGraphBox.sendRequest(url);			
}