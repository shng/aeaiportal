<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
String sliderWidth = (String)request.getAttribute("sliderWidth");
String sliderHeight = (String)request.getAttribute("sliderHeight");
String swfheight = (String)request.getAttribute("swfheight");
String pics = (String)request.getAttribute("pics");
String links = (String)request.getAttribute("links");
String texts = (String)request.getAttribute("texts");
%>
<style>
.flashImageSlider{
	padding:0px;
}
</style>
<div class="flashImageSlider">
<span class="f14b">
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="${sliderWidth}" height="<%=swfheight%>">
<param name="allowScriptAccess" value="sameDomain">
<param name="movie" value="<%=request.getContextPath()%>/pages/slider/flash/pixviewer.swf">
<param name="quality" value="high">
<param name="bgcolor" value="#F0F0F0">
<param name="menu" value="false">
<param name="wmode" value="opaque">
<param name="FlashVars" value="pics=<%=pics%>&links=<%=links%>&texts=<%=texts%>&borderwidth=${sliderWidth}&borderheight=${sliderHeight}&textheight=${textHeight}">
<embed src="<%=request.getContextPath()%>/pages/slider/flash/pixviewer.swf" wmode="opaque" FlashVars="pics=<%=pics%>&links=<%=links%>&texts=<%=texts%>&borderwidth=${sliderWidth}&borderheight=${sliderHeight}&textheight=${textHeight}" menu="false" bgcolor="#F0F0F0" quality="high" width="${sliderWidth}" height="${swfheight}" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
</object>
 </span>
 <span id=focustext class=f14b>
</span>
</div>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>