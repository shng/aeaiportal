<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="increaseReadCount" var="increaseReadCountURL">
	<portlet:param name="infomationId" value="${infomationId}"/>
</portlet:resourceURL>
<style>
.contentMain{
    min-height:${minHeight}px; 
    height:auto !important; 
    height:${minHeight}px; 
    overflow:visible;
    display: inline-block;
}
</style>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
String content = (String)request.getAttribute("content");
String observeViewCount = (String)request.getAttribute("observeViewCount");
%>
<div class="contentMain">
<%=content%>
</div>
<%if("Y".equals(observeViewCount)){%>
<script type="text/javascript">
var url = "/portal/resource?ContentProvider&actionType=increaseReadCount&infomationId=${infomationId}";
sendAction(url,{dataType:'text',onComplete:function(responseText){
	//do nothing
}});
</script>
<%}%>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>