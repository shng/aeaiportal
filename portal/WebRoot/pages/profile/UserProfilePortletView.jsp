<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.agileai.hotweb.domain.core.User"%>
<%@ page import="com.agileai.hotweb.domain.core.Group"%>
<%@ page import="com.agileai.hotweb.domain.core.Role"%>
<portlet:defineObjects/>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
	String minHeight = (String)request.getAttribute("minHeight");
	String isShowRoleList = (String)request.getAttribute("isShowRoleList");
	User user = (User)request.getAttribute("user");
%>
<style type="text/css">
.profilePanel{
    min-height:${minHeight}px; 
    height:auto !important; 
    height:${minHeight}px; 
    overflow:visible;
}
.userGroup{
	height: 30px;
	line-height: 30px;
	vertical-align: top;
	text-indent: 20px;
}
.userRole{
	height: 30px;
	line-height: 30px;
	vertical-align: top;
	text-indent: 20px;
}
</style>
<div class="profilePanel">
<table width="90%">
<tbody>
    <tr>
        <td width="5%">&nbsp;&nbsp;&nbsp;</td>
        <td>
        <span class="userName" style="height: 30px; font-size:14px;line-height: 30px;margin: 10px 0 0 0;display:inline-block;"><%=user.getUserName()%>，欢迎您！</span></td>
    </tr>
    <tr>
        <td colspan="2">
        <hr width="90%" size="1px" align="left">
        </td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;</td>
        <td style="background: url(/portal/pages/profile/images/group.png) no-repeat 1% 50%;
    padding-left: 20px;"><b>所属部门</b></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;</td>
        <td>
        <span class="userGroups">
        <%
        for (int i=0;i < user.getGroupList().size();i++){
        	Group group = user.getGroupList().get(i);
        %>
        <div style="text-indent: 25px;font-size:14px;float: left;margin-left: -10px;" class="userGroup"><%=group.getGroupName()%></div>
        <%}%>
        </span>
        </td>
    </tr>
<%
if ("Y".equals(isShowRoleList)){
%>    
    <tr>
        <td>&nbsp;&nbsp;&nbsp;</td>
        <td style="background: url(/portal/pages/profile/images/role.png) no-repeat 1% 50%;
    padding-left: 20px;">
        <b>所属角色</b>
        </td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;</td>
        <td>
        <span class="userRoles">
        <%
        for (int i=0;i < user.getRoleList().size();i++){
        	Role role = user.getRoleList().get(i);
        %>
        <div style="text-indent: 25px;font-size:14px;float: left;margin-left: -10px;" class="userRole"><%=role.getRoleName()%></div>
        <%}%>
        </span>
        </td>
    </tr>
<%}%>    
    <tr>
        <td colspan="2">
        <hr width="90%" size="1px" align="left">
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
        <input type="button" name="logoutbtn" value="注 销" onclick="javascript:window.location='/portal/index?Login&actionType=logout';" />
        <input type="button" name="changePwdbtn" id="changePwd" value="修改密码" onclick="modifyPasswordRequest()" />
        </td>
    </tr>
</tbody>
</table>
</div>
<script type="text/javascript">
var modifyPasswordBox;
function modifyPasswordRequest(){
	if (!modifyPasswordBox){
		modifyPasswordBox = new PopupBox('modifyPasswordBox','修改个人密码',{size:'normal',height:'380px'});
	}
	var url = '/portal/index?ModifyPassword';
	modifyPasswordBox.sendRequest(url);	
}
</script>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>