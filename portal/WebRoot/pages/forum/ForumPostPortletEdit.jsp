<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isContent = (String)request.getAttribute("isContent");
String isCache = (String)request.getAttribute("isCache");
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
    <tr>
      <td width="120">展现模板ID</td>
      <td>
        <input name="templateId" type="text" id="templateId" value="${templateId}" size="60" /></td>
    </tr>           
    <tr>
      <td width="120">数据URL</td>
      <td>
        <input name="dataURL" type="text" id="dataURL" value="${dataURL}" size="60" /></td>
    </tr>
     <tr>
      <td width="120">正文页面URL</td>
      <td title="如：03/SomeContentPage.ptml">
        <input name="infoLinkURL" type="text" id="infoLinkURL" value="${infoLinkURL}" size="40" />
        </td>
    </tr>
<tr>
      <td width="120">更多页面URL</td>
      <td title="如：03/SomeSecondLevelPage.ptml">
        <input name="moreLinkURL" type="text" id="moreLinkURL" value="${moreLinkURL}" size="40" />
      </td>
    </tr>
    <tr>
      <td width="120">记录条数</td>
      <td>
        <input name="maxCount" type="text" id="maxCount" value="${maxCount}" size="40" /></td>
    </tr>   
	<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">
<%if ("Y".equals(isContent)){%>
	$('#<portlet:namespace/>Form #isContentY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isContentN').attr("checked","checked");
<%}%>
<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>Form #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isCacheN').attr("checked","checked");
<%}%>
$(document).ready(function(){
	var idParam1 = $('#<portlet:namespace/>Form #contentId').val();
	var idParam2 = $('#<portlet:namespace/>Form #dataURL').val();
	idParam2 = idParam2.substring(idParam2.length-36, idParam2.length);
	var url1 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam1 + '&type=content';
	var url2 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam2 + '&type=data';
	sendRequest(url1,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #contentId').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
	sendRequest(url2,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #dataURL').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
});
</script>