<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isCache = (String)request.getAttribute("isCache");
String title = (String)request.getAttribute("title");
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
	<tr>
		<td width="120">帖子ID标识 <span style="color: red;">*</span></td>
		<td><input type="text" size="50" name="contentIdParamKey" id="contentIdParamKey" value="${contentIdParamKey}" /></td>
	</tr>
     <tr>
      <td width="120">默认值</td>
      <td>
		<input type="text" name="contentIdDefValue" id="contentIdDefValue" size="50" value="${contentIdDefValue}" />
      </td>
    </tr>
	<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">	
<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>Form #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isCacheN').attr("checked","checked");
<%}%>
$(document).ready(function(){
	var idParam = $('#<portlet:namespace/>Form #contentIdDefValue').attr('value');
	var url = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam + '&type=content';
	sendRequest(url,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #contentIdDefValue').qtip( 
		{ 
			content: {
				text: responseText			
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
});
</script>