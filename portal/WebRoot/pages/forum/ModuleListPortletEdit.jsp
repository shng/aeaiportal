<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String sortPolicy = (String)request.getAttribute("sortPolicy");
String isCache = (String)request.getAttribute("isCache");
%>
<form id="<portlet:namespace/>Form">
 <table width="90%" border="1">
<tr>
      <td width="120">展现模板ID</td>
      <td>
        <input name="templateId" type="text" id="templateId" value="${templateId}" size="40" /></td>
    </tr>    
	<tr>
      <td width="120">论坛首页模块ID</td>
      <td>
        <input name="moduleId" type="text" id="moduleId" value="${moduleId}" size="40" /></td>
    </tr>
<tr>    
    <tr>
      <td width="120">正文页面URL</td>
      <td title="如：03/SomeContentPage.ptml">
        <input name="infoLinkURL" type="text" id="infoLinkURL" value="${infoLinkURL}" size="40" />
        </td>
    </tr>
<tr>
      <td width="120">更多页面URL</td>
      <td title="如：03/SomeSecondLevelPage.ptml">
        <input name="moreLinkURL" type="text" id="moreLinkURL" value="${moreLinkURL}" size="40" />
      </td>
    </tr>
	<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">
<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>Form #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isCacheN').attr("checked","checked");
<%}%>
$('#<portlet:namespace/>Form #sortPolicy').val('<%=sortPolicy%>');

$(document).ready(function(){
	var idParam1 = $('#<portlet:namespace/>Form #templateId').val();
	var idParam2 = $('#<portlet:namespace/>Form #columnId').attr('value');
	var url1 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam1 + '&type=portlet';
	var url2 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam2 + '&type=column';
	sendRequest(url1,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #templateId').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
	sendRequest(url2,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #columnId').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
});
</script>
