<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<%
String isSetting = (String)request.getAttribute("isSetting");
String menuId = (String)request.getAttribute("menuId");
Boolean isGuest = (Boolean)request.getAttribute("isGuest");
if ("Y".equals(isSetting)){
%>
<style type="text/css">
.search-box {
	width: 100%;
	margin:5px 0px;
}
.search-box .keyword-box {
	width: 99%;
	height: 1.6em;
}
.search-box  .search-button {
	background: url(<%=request.getContextPath()%>/pages/search/images/green-search.gif) no-repeat center;
	cursor: pointer;
	font-size: 0pt;
	height: 27px;
	overflow: hidden;
	width: 25px;
}
</style>
<form id="<portlet:namespace/>SearchForm" method="post">
<div class="search-box">
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td style="width:270px">
                    <input type="text" id="keyword" name="keyword" maxlength="60" class="keyword-box" onkeypress="checkEnterPress()" value="请输入关键字进行搜索"  onfocus='if(this.value=="请输入关键字进行搜索"){this.value="";}; '   
                        onblur='if(this.value==""){this.value="请输入关键字进行搜索";};' align="middle">
                </td>
                <td style="margin-right: 20px;display: inline-block;">
                    <input type="button" class="search-button" value="" onclick="doSimpleSearch()">
                </td>
                <%if(!isGuest.booleanValue()){ %>
                <td>
                 	<a href="javascript:void(0)" class="button-link orange" style="text-decoration: none;padding: 5px 15px;" onclick="createPostMessage()">发   帖</a>
                </td>
                <%} %>
            </tr>
        </tbody>
    </table>
    <div style="clear:both"></div>
</div>
<input type="hidden" name="menuId" id="menuId" value="<%=menuId%>" />
</form>
<script type="text/javascript">
function createPostMessage() {
	var menuId = $("#menuId").val();
	if(menuId==''||menuId=='null'){
		menuId = '164C406D-8E18-4AFD-8495-455A65E0092A';
		window.location.href = "/portal/request/05/post-message.ptml?contentId="+menuId;
	}else{
		window.location.href = "/portal/request/05/post-message.ptml?contentId="+menuId;
	}
}
function getHrefEnd(){
	var href = parsePageId();
	var searchWord = "";
	if(href.indexOf("/keyword-search-list.ptml") > -1){
		searchWord = $(location).attr('href').split('?')[1].split('&')[0].split('=')[1];
	}else{
		searchWord = "";
	}
	return searchWord;
}
$(document).ready(function(){ 
	var searchWord = getHrefEnd();
	$('#keyword').val(decodeURI(searchWord));
});
function doSimpleSearch(){
	var convertWord = trim($("#<portlet:namespace/>SearchForm #keyword").val());
	if(convertWord==''){
		convertWord = '请输入关键字进行搜索';
	}
	var href = parsePageId();
	if(href.indexOf("/keyword-search-list.ptml") > -1){
		window.location.href="${targetPageURL}?searchWord="+convertWord;
	}else{
		window.open("${targetPageURL}?searchWord="+convertWord);
	}
}
function checkEnterPress(){	
	if ( event.keyCode == 13 ) 
    { 
		doSimpleSearch(); 
    }
}
</script>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>