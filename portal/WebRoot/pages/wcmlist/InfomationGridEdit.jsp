<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String sortPolicy = (String)request.getAttribute("sortPolicy");
String isCache = (String)request.getAttribute("isCache");
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
<tr>
      <td width="120">展现模板ID</td>
      <td>
        <input name="templateId" type="text" id="templateId" value="${templateId}" size="60" /></td>
    </tr>
    <tr>
      <td width="120">最小高度</td>
      <td>
        <input name="minHeight" type="text" id="minHeight" value="${minHeight}" size="40" />      </td>
    </tr>	
    <tr>
      <td width="120">栏目参数Key</td>
      <td>
        <input name="columnIdParamKey" type="text" id="columnIdParamKey" value="${columnIdParamKey}" size="60" /></td>
    </tr>
<tr>
      <td width="120" nowrap="nowrap">栏目参数默认值</td>
      <td>
        <input name="columnIdDefValue" type="text" id="columnIdDefValue" value="${columnIdDefValue}" size="60" /></td>
    </tr>    
<tr>
      <td width="120">正文页面URL</td>
      <td title="如：03/SomeThirdLevelPage.ptml">
        <input name="infoLinkURL" type="text" id="infoLinkURL" value="${infoLinkURL}" size="40" />
        </td>
    </tr>
    <tr>
      <td width="120">排序策略</td>
      <td><select name="sortPolicy" id="sortPolicy">
        <option value="Top8PublishTime">置顶优先、发布时间倒序</option>
        <option value="PublishTimeDesc">仅发布信息时间倒序</option>
        <option value="Top8InfomationSort">置顶优先、信息自身排序</option>
        <option value="InfomationSort">仅参考信息自身排序</option>
        <option value="ReadCountDesc">阅读次数最高信息倒序</option>
      	</select>      </td>
    </tr> 
<tr>
      <td width="120">最大字符数</td>
      <td>
        <input name="maxCharNumber" type="text" id="maxCharNumber" value="${maxCharNumber}" size="60" /></td>
    </tr>        
<tr>
      <td width="120">每页记录条数</td>
      <td>
        <input name="recCountPerPage" type="text" id="recCountPerPage" value="${recCountPerPage}" size="60" /></td>
    </tr> 
<tr>
      <td width="120">主分页码个数</td>
      <td>
        <input name="mainNumCount" type="text" id="mainNumCount" value="${mainNumCount}" size="60" /></td>
    </tr>               
<tr>
      <td width="120">两边页码个数</td>
      <td>
        <input name="sideNumCount" type="text" id="sideNumCount" value="${sideNumCount}" size="60" /></td>
    </tr> 
    <tr>
      <td width="120">是否缓存</td>
      <td>
        <input type="radio" name="isCache" id="isCacheY" value="Y" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isCache" id="isCacheN" value="N" />
      否</td>
    </tr>              
    <tr>
      <td width="120">缓存时间</td>
      <td>
        <input type="text" name="cacheMinutes" id="cacheMinutes" value="${cacheMinutes}" />
        分钟</td>
    </tr>     
	<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">
<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>Form #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isCacheN').attr("checked","checked");
<%}%>
$('#<portlet:namespace/>Form #sortPolicy').val('<%=sortPolicy%>');

$(document).ready(function(){
	var idParam1 = $('#<portlet:namespace/>Form #templateId').val();
	var idParam2 = $('#<portlet:namespace/>Form #columnIdDefValue').attr('value');
	var url1 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam1 + '&type=portlet';
	var url2 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam2 + '&type=column';
	sendRequest(url1,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #templateId').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
	sendRequest(url2,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #columnIdDefValue').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
});
</script>