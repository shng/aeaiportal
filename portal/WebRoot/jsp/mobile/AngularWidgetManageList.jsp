<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>手机门户管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function reloadAll(){
	postRequest('form1',{actionType:'reloadAll',onComplete:function(responseText){
		if (responseText == 'success'){
			alert("重新加载成功！");
		}else{
			alert('加载缓存失败！！')
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="reloadAll()"><input value="&nbsp;" type="button" class="refreshImgBtn" id="refreshImgBtn" title="重新加载" />重新加载</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;标识&nbsp;<input id="awId" label="标识" name="awId" type="text" value="<%=pageBean.inputValue("awId")%>" size="30" class="text" />
&nbsp;编码&nbsp;<input id="awCode" label="编码" name="awCode" type="text" value="<%=pageBean.inputValue("awCode")%>" size="24" class="text" />
&nbsp;名称&nbsp;<input id="awName" label="名称" name="awName" type="text" value="<%=pageBean.inputValue("awName")%>" size="20" class="text" />
&nbsp;分组&nbsp;<select id="awGroup" label="分组" name="awGroup" class="select" onchange="doQuery()"><%=pageBean.selectValue("awGroup")%></select>
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="手机门户管理.csv"
retrieveRowsCallback="process" xlsFileName="手机门户管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{AW_ID:'${row.AW_ID}'});refreshConextmenu()" onclick="selectRow(this,{AW_ID:'${row.AW_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}"/>
	<ec:column width="70" property="AW_CODE" title="编码" maxLength="15" />
	<ec:column width="70" property="AW_NAME" title="名称" maxLength="10"  />
	<ec:column width="70" property="AW_GROUP" title="分组" mappingItem="AW_GROUP"/>
	<ec:column width="145" property="AW_TEMPLATEFILE" title="模板"  maxLength="55" />
	<ec:column width="145" property="AW_CONTROLERFILE" title="控制器" maxLength="55"  />
</ec:row>
</ec:table>
<input type="hidden" name="AW_ID" id="AW_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('AW_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
