<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>关键字管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function doRefresh(nodeId){
	$('#WORD_ID').val(nodeId);
	doSubmit({actionType:'refresh'});
}
var targetTreeBox;
function showParentSelectBox(){
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标目录',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "KeyWordsParentSelect";
	var url = 'index?'+handlerId+'&WORD_ID='+$("#WORD_ID").val();
	targetTreeBox.sendRequest(url);
}
function doChangeParent(){
	postRequest('form1',{actionType:'changeParent',onComplete:function(responseText){
		if (responseText == 'success'){
			doRefresh($('#WORD_ID').val());			
		}else {
			alert('迁移父节点出错啦！');
		}
	}});
}
function doSave(){
	if (checkSave()){
		$("#operaType").val('update');
		doSubmit({actionType:'save',checkUnique:'true'});
	}
}
function checkSave(){
	var result = true;
if (validation.checkNull($('#WORD_NAME').val())){
	writeErrorMsg($("#WORD_NAME").attr("label")+"不能为空!");
	selectOrFocus('WORD_NAME');
	return false;
}
if (validation.checkNull($('#WORD_CODE').val())){
	writeErrorMsg($("#WORD_CODE").attr("label")+"不能为空!");
	selectOrFocus('WORD_CODE');
	return false;
}
	return result;
}
function doMoveUp(){
	doSubmit({actionType:'moveUp'});
}
function doMoveDown(){
	doSubmit({actionType:'moveDown'});
}
function doDelete(){
	if (confirm('确定要进行节点删除操做吗？')){
		doSubmit({actionType:'delete'});
	}
}
function doInsertChild(){
	if (checkInsertChild()){
		$("#operaType").val('insert');
		doSubmit({actionType:'insertChild',checkUnique:'true'});
	}
}
function checkInsertChild(){
	var result = true;
if (validation.checkNull($('#CHILD_WORD_NAME').val())){
	writeErrorMsg($("#CHILD_WORD_NAME").attr("label")+"不能为空!");
	selectOrFocus('CHILD_WORD_NAME');
	return false;
}
if (validation.checkNull($('#CHILD_WORD_CODE').val())){
	writeErrorMsg($("#CHILD_WORD_CODE").attr("label")+"不能为空!");
	selectOrFocus('CHILD_WORD_CODE');
	return false;
}
	return result;
}
function doCancel(){
	doRefresh($('#WORD_ID').val());
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;">
<tr>
	<td valign="top">
	<div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;分组列表</h3>        
	<div id="treeArea" style="overflow:auto; height:420px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
	<%=pageBean.getStringValue("menuTreeSyntax")%></div>
    <b class="b9"></b>
    </div>
	</td>
	<td width="85%" valign="top">
	<fieldset id="currentArea" style="padding:5px;height:210px;">
	  <legend style="font-weight: bolder;">编辑当前节点</legend>
	    <div id="__ToolBar__" style="margin-top: 2px">
   		<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		<td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doSave()"<%}%> class="bartdx" hotKey="E" align="center"><input id="saveImgBtn" value="&nbsp;" title="保存" type="button" class="saveImgBtn" style="margin-right:0px;" />保存</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doDelete()"<%}%> class="bartdx" hotKey="D" align="center"><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn" style="margin-right:0px;" />删除</td>
		    
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="showParentSelectBox()"<%}%> class="bartdx" align="center"><input id="moveImgBtn" value="&nbsp;" title="迁移" type="button" class="moveImgBtn" style="margin-right:0px;" />迁移</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveUp()"<%}%> class="bartdx" align="center"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveDown()"<%}%> class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td>
	    </tr></table></div>    
	    <table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="WORD_NAME" label="名称" name="WORD_NAME" type="text" value="<%=pageBean.inputValue("WORD_NAME")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="WORD_CODE" label="编码" name="WORD_CODE" type="text" value="<%=pageBean.inputValue("WORD_CODE")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>是否启用</th>
	<td>&nbsp;<%=pageBean.selectRadio("WORD_ENABLED")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><textarea id="WORD_DESC" style="width:300px;height: 60px;"  label="描述" name="WORD_DESC" cols="40" rows="4" class="textarea"><%=pageBean.inputValue("WORD_DESC")%></textarea>
</td>
</tr>
	    </table>
	</fieldset>	
    
	<fieldset id="childArea" style="margin-top: 10px;padding:5px;height:208px;">
	  <legend style="font-weight: bolder;">添加子节点</legend>
	    <div id="__ToolBar__">
		<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doInsertChild()"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" style="margin-right:0px;" />新增</td>
		<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doCancel()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" style="margin-right:0px;" />取消</td>    
	    </tr></table>
	    </div>     
	    <table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="CHILD_WORD_NAME" label="名称" name="CHILD_WORD_NAME" type="text" value="<%=pageBean.inputValue("CHILD_WORD_NAME")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="CHILD_WORD_CODE" label="编码" name="CHILD_WORD_CODE" type="text" value="<%=pageBean.inputValue("CHILD_WORD_CODE")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>是否启用</th>
	<td>&nbsp;<%=pageBean.selectRadio("CHILD_WORD_ENABLED")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><textarea id="CHILD_WORD_DESC" style="width:300px;height: 60px;"  label="描述" name="CHILD_WORD_DESC" cols="40" rows="4" class="textarea"><%=pageBean.inputValue("CHILD_WORD_DESC")%></textarea>
</td>
</tr>
	    </table>
	</fieldset>          
    </td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value=""/>
<input type="hidden" id="WORD_ID" name="WORD_ID" value="<%=pageBean.inputValue("WORD_ID")%>" />
<input type="hidden" id="WORD_PID" name="WORD_PID" value="<%=pageBean.inputValue("WORD_PID")%>" />
<input type="hidden" id="WORD_SORT" name="WORD_SORT" value="<%=pageBean.inputValue("WORD_SORT")%>" />
</form>
</body>
</html>
<script language="javascript">
<%if(pageBean.getBoolValue("isRootNode")){%>
setImgDisabled('saveImgBtn',true);
setImgDisabled('delImgBtn',true);
setImgDisabled('copyImgBtn',true);
setImgDisabled('moveImgBtn',true);
setImgDisabled('upImgBtn',true);
setImgDisabled('downImgBtn',true);
<%}%>
$(window).load(function() {
	setTimeout(function(){
		resetTreeHeight(70);
		var areaHeight = $("#leftTree").height()/2;
		$("#currentArea").height(areaHeight-2);
		$("#childArea").height(areaHeight-7);			
	},1);	
});
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
<script language="javascript">
</script>
