<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<style type="text/css">
.detailTable tr th{
font-weight: bolder;
    height: 23px;
    color: #036;
    border: 1px #d9d9d9 solid;
    border-top: 0px;
    border-left: 0px;
    padding: 1px 1px 1px 10px;
    padding-left: 10px;
    background: #D9E9Ff;
    text-align: left;
    margin: 0px;
    width: 40px;
}
</style>
<script language="javascript">
function saveRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if (responseText == 'success'){
			parent.PopupBox.closeCurrent();
			alert("收藏成功！");
		}else{
			alert("收藏出问题了！");
		}
	}});	
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__" style="margin-bottom:5px">
   <span id="span1" onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveRecord();"><input value="&nbsp;" type="button" class="saveImgBtn" title="保存" />保存</span>
   <span id="span2" onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</span>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>标签</th>
	<td>&nbsp;&nbsp;<input id="FAV_LABLE" label="标签" name="FAV_LABLE" type="text" value="<%=pageBean.inputValue("FAV_LABLE")%>" style="width:280px;margin-top: -15px;" class="text" /></td>
</tr>
<tr>
<th width="50" nowrap>类型</th>
	<td>&nbsp;
	<label for="select"></label>
      <select name="select" size="5" id="select" style="height:150px;width:280px;margin-top: -15px;" >
      <%=pageBean.selectValue("TYPE_ID")%>
      </select>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" name="FAV_ID" id="FAV_ID" value="<%=pageBean.inputValue("FAV_ID")%>"/>
<input type="hidden" name="INFO_ID" id="INFO_ID" value="<%=pageBean.inputValue("INFO_ID")%>"/>
</form>
<script language="javascript">
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
