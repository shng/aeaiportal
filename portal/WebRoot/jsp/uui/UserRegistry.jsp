<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>数通畅联-用户注册</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function registryUser(){
	if (!validate()){
		return;
	}
	var pwd = $("#USER_PWD").val()
	var regExpression = /^[a-zA-Z0-9_]{6,18}$/;
	if (!regExpression.test(pwd)){
		writeErrorMsg("密码必须长度6~18之间，只能为字符、数字和下划线！");
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'checkUserInfo',onComplete:function(responseText){
		//alert("responseText is " + responseText);
		if (responseText == ''){
			doSubmit({actionType:'registryUser'});		
		}else{
			hideSplash();
			changeValideCode();
			$("#valideCode").val("");
			writeErrorMsg(responseText);
		}
	}});
}
function controlButton(){
	if ($("#agreeRegistryProtocol").is(':checked')){
		$("#regButton").enable();
	}else{
		$("#regButton").disable();
	}
}
function changeValideCode(){
	document.getElementById('valideCodeImg').src = "safecode?"+Math.floor(Math.random()*100000);
}
</script>
</head>
<body>
<div style="margin:auto;width:690px;">
<br />
<br /><br /><br />
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="80" nowrap>用户名</th>
	<td><input name="USER_CODE" type="text" class="text" id="USER_CODE" value="<%=pageBean.inputValue("USER_CODE")%>" size="30" maxlength="15" label="用户名" />英文或者数字</td>
</tr>
<tr>
	<th width="80" nowrap>昵称</th>
	<td><input id="USER_NAME" label="名称" name="USER_NAME" type="text" value="<%=pageBean.inputValue("USER_NAME")%>" size="30" maxlength="16" class="text" />中文名，或者别名</td>
</tr>
<tr>
	<th width="80" nowrap>密码</th>
	<td><input id="USER_PWD" label="密码" name="USER_PWD" type="text" value="<%=pageBean.inputValue("USER_PWD")%>" size="30" maxlength="32" class="text" />长度6~18之间，只能为字符、数字和下划线</td>
</tr>
<tr>
	<th width="80" nowrap>邮件</th>
	<td style="color:red;"><input id="USER_MAIL" label="邮件" name="USER_MAIL" type="text" value="<%=pageBean.inputValue("USER_MAIL")%>" size="30" maxlength="32" class="text" />用于激活账户和找回密码</td>
</tr>
<tr>
	<th width="80" nowrap>验证码</th>
	<td><span style="float: left;padding-top:3px;"><input id="valideCode" label="验证码" name="valideCode" type="text" value="" size="30" maxlength="16" class="text" /></span><span><span><img id="valideCodeImg" onclick="changeValideCode();" title="点击切换验证码图片" src="safecode"></img></span></span></td>
</tr>
<tr>
	<th width="80" style="text-align:right" nowrap><input type="checkbox" onclick="controlButton()" name="agreeRegistryProtocol" id="agreeRegistryProtocol" /></th>
	<td>同意用户注册协议，<a href="jsp/uui/UserRegistryProtocol.html" target="_blank">查看协议</a>
</tr>
</table>
<div><input id="regButton" type="button" name="button" disabled="disabled" value="注册用户" style="height: 30px;width: 100px;font-size: 15px;font-weight: bolder;margin-top: 10px;" onclick="registryUser()" /></div>
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
<script language="javascript">
emailValidator.add("USER_MAIL");
requiredValidator.add("USER_CODE");
requiredValidator.add("USER_NAME");
requiredValidator.add("USER_PWD");
requiredValidator.add("USER_MAIL");
requiredValidator.add("valideCode");
charNumValidator.add("USER_CODE");
charNumValidator.add("USER_PWD");

new BlankTrimer("USER_CODE");
new BlankTrimer("USER_NAME");
new BlankTrimer("USER_PWD");
new BlankTrimer("USER_MAIL");

$("#USER_CODE").focus();
</script>
</div>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
