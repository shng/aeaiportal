<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>导航管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function refeshType(navType){
	var targetUrl = "<%=pageBean.getHandlerURL()%>&actionType=refeshType&NAV_TYPE="+navType;
	sendRequest(targetUrl);
}

function saveRecord(){
	var navId = $('#NAV_ID').val();
	if (navId.length !=2){
		writeErrorMsg('编码应为两位数字或者小写字母组成！');
		return;
	}
	var reg = /^[0-9a-z-]+$/;
	if (!reg.test($("#CHILD_MENU_CODE").val())) {
		writeErrorMsg('编码应为两位数字或者小写字母组成！');
		return;
	}
	doSubmit({actionType:'save',checkUnique:'true'});
}

var personalPageBox;
function showPersonalPageBox(){
	if (!selectParentBox){
		selectParentBox = new PopupBox('selectParentBox','请选择目标父节点',{size:'big',width:'300px',height:'400px',top:'10px'});
	}
	var url = 'index?MenuTreeSelect&targetId=MENU_PID&navId='+$('#NAV_ID').val()+'&menuId='+$('#MENU_ID').val();
	selectParentBox.sendRequest(url);
}

var addThemeBox;
function addTheme(){
	var title = "选择个性主题";
	if (!addThemeBox){
		addThemeBox = new PopupBox('addThemeBox',title,{size:'big',width:'660px',height:'400px',top:'3px'});
	}
    var url = '/portal/index?PersonalThemeSelectList&comeFromNavEdit=Y&navId=<%=pageBean.inputValue("NAV_ID")%>';
    addThemeBox.sendRequest(url);
}

function delTheme(){
	if (!isValid($("#PERSONLA_THEMES").val())){
		writeErrorMsg('请先选中一条主题!');
	}
	if (confirm("确定要删除该个性主题么？")){
		postRequest('form1',{actionType:'delTheme',onComplete:function(responseText){
			loadPersonalThemes();
		}});		
	}
}

function loadPersonalThemes(){
	postRequest('form1',{actionType:'loadPersonalThemes',onComplete:function(responseText){
		eval(responseText);
	}});	
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div class="photobg1" id="tabHeader">
    <div class="newarticle1">基本信息</div>
<%if (!"insert".equals(pageBean.getOperaType())) {%>    
    <div class="newarticle1">安全管理</div>
<%}%>    
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:auto;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveRecord()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input name="NAV_ID" type="text" class="text" id="NAV_ID" <%=pageBean.readonly(!"insert".equals(pageBean.getOperaType()))%> value="<%=pageBean.inputValue("NAV_ID")%>" size="24" maxlength="2" label="编码" /></td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="NAV_NAME" label="名称" name="NAV_NAME" type="text" value="<%=pageBean.inputValue("NAV_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>类型</th>
	<td><%if ("insert".equals(pageBean.getOperaType())){%><select onchange="refeshType(this.value)" id="NAV_TYPE" label="类型" name="NAV_TYPE" class="select"><%=pageBean.selectValue("NAV_TYPE")%></select><%}else{%><%=pageBean.selectedText("NAV_TYPE")%><input type="hidden" id="NAV_TYPE" label="NAV_TYPE" name="NAV_TYPE" value="<%=pageBean.selectedValue("NAV_TYPE")%>" /><%}%>
</td>
</tr>
<tr>
    <th width="100" nowrap>默认主题</th>
    <td><select id="NAV_THEME" label="主题" name="NAV_THEME" class="select"><%=pageBean.selectValue("NAV_THEME")%></select></td>
</tr>
<tr>
	<th width="100" nowrap>个性主题</th>
	<td>
	<span style="float:left"><select name="PERSONAL_THEMES" size="8" id="PERSONAL_THEMES" style="width:300px;" class="xselect"></select></span>
     <%if (pageBean.isValid(pageBean.inputValue4DetailOrUpdate("NAV_ID",""))) {%>
     <span><input name="addThemeBtn" type="button" value="添加" style="margin:2px;" onclick="addTheme()" />
     <br />
     <input name="delThemeBtn" type="button" value="删除" style="margin:2px;" onclick="delTheme()" /></span>
     <%}%>	
	</td>
</tr>
<tr>
	<th width="100" nowrap>主题属性</th>
	<td><select id="NAV_THEME_PRO" label="主题属性" name="NAV_THEME_PRO" class="select"><%=pageBean.selectValue("NAV_THEME_PRO")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>是否有效</th>
	<td><select id="NAV_ISVALID" label="是否有效" name="NAV_ISVALID" class="select"><%=pageBean.selectValue("NAV_ISVALID")%></select>
</td>
</tr>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><input id="NAV_DESC" label="描述" name="NAV_DESC" type="text" value="<%=pageBean.inputValue("NAV_DESC")%>" size="48" class="text" />
	</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
</div>
<%if (!"insert".equals(pageBean.getOperaType())) {%>
<div class="photobox newarticlebox" id="Layer1" style="height:auto;display:none;overflow:hidden;">
<iframe id="SecurityFrame" src="index?SecurityAuthorizationConfig&resourceType=Navigater&resourceId=<%=pageBean.inputValue("NAV_ID")%>" width="100%" height="430" frameborder="0" scrolling="no"></iframe>
</div>
<%}%>
</form>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus(0);

lengthValidators[0].set(2).add("NAV_ID");
lengthValidators[1].set(32).add("NAV_NAME");
requiredValidator.add("NAV_ID");
requiredValidator.add("NAV_NAME");

<%if ("insert".equals(pageBean.getOperaType())){%>
requiredValidator.add("NAV_TYPE");
<%}else{%>
loadPersonalThemes();
<%}%>

requiredValidator.add("NAV_THEME");
lengthValidators[2].set(1).add("NAV_TYPE");
requiredValidator.add("NAV_ISVALID");
lengthValidators[3].set(1).add("NAV_ISVALID");
requiredValidator.add("NAV_THEME_PRO");
lengthValidators[4].set(20).add("NAV_THEME_PRO");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>