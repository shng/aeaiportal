﻿﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>菜单管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="js/jquery.zclip.js"></script>
<link rel="stylesheet" type="text/css" href="css/layout.css" />
<style type="text/css">
.td_border{
	border:0px !important;
}
.font_style{
	padding-left: 148px !important;
	font-weight: 600;
}
</style>
<script language="javascript">
var selectParentBox;
function showParentSelectBox(){
	var checkURL = "<%=pageBean.getHandlerURL()%>&actionType=isAlreadyExistSecurityRelation&menuId="+$('#MENU_ID').val();
	sendRequest(checkURL,{onComplete:function(responseText){
		if ("Y" == responseText){
			writeErrorMsg('当前节点已经授权，不能迁移 ！');
		}else{
			if (!selectParentBox){
				selectParentBox = new PopupBox('selectParentBox','请选择目标父节点',{size:'big',width:'300px',height:'400px',top:'10px'});
			}
			var url = 'index?MenuTreeSelect&targetId=MENU_PID&navId='+$('#NAV_ID').val()+'&menuId='+$('#MENU_ID').val();
			selectParentBox.sendRequest(url);			
		}
	}});
}
var createFoldBox;
function createFoldRequest(){
	if (!createFoldBox){
		createFoldBox = new PopupBox('createFoldBox','新建菜单目录',{size:'big',width:'500px',height:'250px',top:'10px'});
	}
	var url = 'index?MenuFolderCreateForm&NAV_ID=<%=pageBean.inputValue("NAV_ID")%>&MENU_ID=<%=pageBean.inputValue("MENU_ID")%>';
	createFoldBox.sendRequest(url);
}

var createPageBox;
function createPageRequest(){
	if (!createPageBox){
		createPageBox = new PopupBox('createPageBox','新建菜单页面',{size:'big',width:'500px',height:'300px',top:'10px'});
	}
	var url = 'index?MenuPageCreateForm&NAV_ID=<%=pageBean.inputValue("NAV_ID")%>&MENU_ID=<%=pageBean.inputValue("MENU_ID")%>';
	createPageBox.sendRequest(url);
}

var createLinkBox;
function createLinkRequest(){
	if (!createLinkBox){
		createLinkBox = new PopupBox('createLinkBox','新建菜单页面',{size:'big',width:'500px',height:'300px',top:'10px'});
	}
	var url = 'index?MenuLinkCreateForm&NAV_ID=<%=pageBean.inputValue("NAV_ID")%>&MENU_ID=<%=pageBean.inputValue("MENU_ID")%>';
	createLinkBox.sendRequest(url);
}

var createDummyMenuBox;
function createDummyMenuRequest(){
	if (!createDummyMenuBox){
		createDummyMenuBox = new PopupBox('createDummyMenuBox','新建虚拟菜单',{size:'big',width:'500px',height:'300px',top:'10px'});
	}
	var url = 'index?DummyMenuCreateForm&NAV_ID=<%=pageBean.inputValue("NAV_ID")%>&MENU_ID=<%=pageBean.inputValue("MENU_ID")%>';
	createDummyMenuBox.sendRequest(url);
}

function doRefresh(menuId){
	$('#MENU_ID').val(menuId);
	doSubmit({actionType:'refresh'});
}
function doSave(){
	var menuName = $('#MENU_NAME').val();
	if (validation.checkNull(menuName)){
		writeErrorMsg('当前节点名称不能为空！');
		return;
	}
	var menuCode = $('#MENU_CODE').val();
	if (validation.checkNull(menuCode)){
		writeErrorMsg('当前节点编码不能为空！');
		return;
	}

	var reg = /^[0-9A-Za-z-]+$/;
	if (!reg.test(menuCode)) {
		writeErrorMsg('节点编码只能由字符、数字或者短横线组成！');
		return;
	}
	
<%if ("L".equals(pageBean.selectedValue("MENU_TYPE"))){%>   	
	var menuTheme = $('#MENU_THEME').val();
	if (validation.checkNull(menuTheme)){
		writeErrorMsg('当前节点主题不能为空！');
		return;
	}
	<%}else if ("U".equals(pageBean.selectedValue("MENU_TYPE"))){%>
	var menuURL = $('#MENU_URL').val();
	if (validation.checkNull(menuURL)){
		writeErrorMsg('当前节点链接不能为空！');
		return;
	}	
	<%}%>
	doSubmit({actionType:'save'});
}

function doMoveUp(){
	doSubmit({actionType:'moveUp'});
}

function doMoveDown(){
	doSubmit({actionType:'moveDown'});
}

function doDelete(){
	if (confirm('确定要进行节点删除操做吗？')){
		doSubmit({actionType:'delete'});
	}
}

function doCancel(){
	doRefresh($('#MENU_ID').val());
}

function focusTab(tabId){
	var reqUrl = "<%=pageBean.getHandlerURL()%>&actionType=focusTab&currentTabId="+tabId;
	sendRequest(reqUrl,{onComplete:function(responseText){
		
	}});
}

function showLayout(){
	//var reqUrl = "<%=pageBean.getHandlerURL()%>&actionType=focusTab&currentTabId=layout";
	//sendRequest(reqUrl,{onComplete:function(responseText){
		if (!isValid($('#LayoutFrame').attr('src'))){
			var url = "index?LayoutManage&NAV_ID=<%=pageBean.inputValue("NAV_ID")%>&PAGE_ID=<%=pageBean.inputValue("PAGE_ID")%>&randomKey="+Math.random();
			$('#LayoutFrame').attr('src',url);
		}
		//tab.focus(1);
	//}});
}
function showSecurityConfig(){
	//var reqUrl = "<%=pageBean.getHandlerURL()%>&actionType=focusTab&currentTabId=security";
	//sendRequest(reqUrl,{onComplete:function(responseText){
		var url = "index?SecurityAuthorizationConfig&resourceType=Menu&resourceId=<%=pageBean.inputValue("MENU_ID")%>&randomKey="+Math.random();
		$('#SecurityFrame').attr('src',url);
		//tab.focus(2);
	//}});
}
var langConfBox;
function configLanguage(resourceId,resourceName){
	if (!langConfBox){
		langConfBox = new PopupBox('langConfBox','多语言配置',{size:'normal',width:'500px',height:'300px',top:'30px',scroll:'auto'});
	}
	var url = 'index?SysMultiLangManageEdit&resourceType=Menu&resourceId='+resourceId +'&resourceName='+resourceName;
	langConfBox.sendRequest(url);
}

function choicePagePro(){
	var choicePagePro = $('input[name="MOBILE_PAGE_PRO"]:checked').val();
	if("isPage" == choicePagePro){
		$("#iconEnableIconOnOffTr").css('display','none'); 
		$("#personaTr").css('display','none'); 
		$("#CustomeHeaderTr").css('display','none'); 
		$("#titleTr").css('display',''); 
		$("#stateURLStateViewTr").css('display',''); 
		$("#dependsModulesTr").css('display',''); 
		$("#pageCtrlFileTr").css('display',''); 
		$("#pageTmplFileTr").css('display',''); 
		$("#isModalPopupPopover").css('display',''); 
		$("#refresherLoaderTr").css('display',''); 
	}else if("isModal" == choicePagePro){
		$("#iconEnableIconOnOffTr").css('display','none'); 
		$("#personaTr").css('display','none'); 
		$("#CustomeHeaderTr").css('display',''); 
		$("#stateURLStateViewTr").css('display','none'); 
		$("#dependsModulesTr").css('display',''); 
		$("#pageCtrlFileTr").css('display','none'); 
		$("#pageTmplFileTr").css('display','none'); 
		$("#isModalPopupPopover").css('display','none'); 
		$("#refresherLoaderTr").css('display','none'); 
	}else if("isPopover" == choicePagePro){
		$("#iconEnableIconOnOffTr").css('display','none'); 
		$("#personaTr").css('display','none'); 
		$("#CustomeHeaderTr").css('display','none'); 
		$("#titleTr").css('display','none'); 
		$("#stateURLStateViewTr").css('display','none'); 
		$("#dependsModulesTr").css('display',''); 
		$("#pageCtrlFileTr").css('display','none'); 
		$("#pageTmplFileTr").css('display','none'); 
		$("#isModalPopupPopover").css('display','none'); 
		$("#refresherLoaderTr").css('display','none'); 
	}else if("isPopup" == choicePagePro){
		$("#iconEnableIconOnOffTr").css('display','none'); 
		$("#personaTr").css('display','none'); 
		$("#CustomeHeaderTr").css('display','none'); 
		$("#titleTr").css('display','none'); 
		$("#stateURLStateViewTr").css('display','none'); 
		$("#dependsModulesTr").css('display',''); 
		$("#pageCtrlFileTr").css('display','none'); 
		$("#pageTmplFileTr").css('display','none'); 
		$("#isModalPopupPopover").css('display','none'); 
		$("#refresherLoaderTr").css('display','none'); 
	}else if("isTab" == choicePagePro){
		$("#iconEnableIconOnOffTr").css('display',''); 
		$("#personaTr").css('display',''); 
		$("#CustomeHeaderTr").css('display','none'); 
		$("#titleTr").css('display','none'); 
		$("#stateURLStateViewTr").css('display','none'); 
		$("#dependsModulesTr").css('display',''); 
		$("#pageCtrlFileTr").css('display',''); 
		$("#pageTmplFileTr").css('display',''); 
		$("#isModalPopupPopover").css('display',''); 
		$("#refresherLoaderTr").css('display',''); 
	}else if("isFrame" == choicePagePro){
		$("#iconEnableIconOnOffTr").css('display','none'); 
		$("#personaTr").css('display','none'); 
		$("#CustomeHeaderTr").css('display','none'); 
		$("#titleTr").css('display','none'); 
		$("#stateURLStateViewTr").css('display','none'); 
		$("#dependsModulesTr").css('display','none'); 
		$("#pageCtrlFileTr").css('display','none'); 
		$("#pageTmplFileTr").css('display','none'); 
		$("#isModalPopupPopover").css('display','none'); 
		$("#refresherLoaderTr").css('display','none'); 
	}
}

var addConfigBox;
function showAddConfigBox(pagepro){
	if (!addConfigBox){
		addConfigBox = new PopupBox('addConfigBox','请选择',{size:'normal',height:'410px',width:'310px',top:'3px'});
	}
	var url = 'index?MobileMenuTreeSelect&actionType=addAddConfigTreeRequest&targetId=MENU_PID&navId='+$('#NAV_ID').val()+'&menuId='+$('#MENU_ID').val()+'&pagepro='+pagepro;
	addConfigBox.sendRequest(url);
}

function addConfigTreeInfos(ids,menuSource){
	if (ids != ""){
		if("isModal" == menuSource){
			$('#addModalIds').val(ids);
			postRequest('form1',{actionType:'addModalTreeInfos',onComplete:function(responseText){
				$("#modalList").append(responseText);
			}});
		}else if("isPopover" == menuSource){
			$('#addPopoverIds').val(ids);
			postRequest('form1',{actionType:'addPopoverTreeInfos',onComplete:function(responseText){
				$("#popoverList").append(responseText);
			}});
		}else if("isPopup" == menuSource){
			$('#addPopupIds').val(ids);
			postRequest('form1',{actionType:'addPopupTreeInfos',onComplete:function(responseText){
				$("#popupList").append(responseText);
			}});
		}
	}else{
		writeErrorMsg("请检查操作！");
	}
}

function deleteConfig(selectId){
	var obj = ele(selectId);
	var selectedIndex = obj.selectedIndex;
	if (selectedIndex == -1){
		writeErrorMsg("请先选中一条记录！");
		return;
	}	
	if (confirm('确认要进行删除操作吗？')){
		if('modalList' == selectId){
			var select = $("#modalList").find("option:selected").val();
			$("#modalList option[value='"+select+"']").remove();
			var modals = $("#modals").val();
			var bool = modals.indexOf(select);
			if(bool >=0){
				modals = modals.replace(select,''); 
				$("#modals").val(modals);
			}
			
			var addModalIds = $("#addModalIds").val();
			var addBool = addModalIds.indexOf(select);
			if(addBool >=0){
				addModalIds = addModalIds.replace(select,''); 
				$("#addModalIds").val(addModalIds);
			}
		}else if("popoverList" == selectId){
			var select = $("#popoverList").find("option:selected").val();
			$("#popoverList option[value='"+select+"']").remove();
			var popovers = $("#popovers").val();
			var bool = popovers.indexOf(select);
			if(bool >=0){
				popovers = popovers.replace(select,''); 
				$("#popovers").val(popovers);
			}
			
			var addPopoverIds = $("#addPopoverIds").val();
			var addBool = addPopoverIds.indexOf(select);
			if(addBool >=0){
				addPopoverIds = addPopoverIds.replace(select,''); 
				$("#addPopoverIds").val(addPopoverIds);
			}
		}else if("popupList" == selectId){
			var select = $("#popupList").find("option:selected").val();
			$("#popupList option[value='"+select+"']").remove();
			var popups = $("#popups").val();
			var bool = popups.indexOf(select);
			if(bool >=0){
				popups = popups.replace(select,''); 
				$("#popups").val(popups);
			}
			
			var addPopupIds = $("#addPopupIds").val();
			var addBool = addPopupIds.indexOf(select);
			if(addBool >=0){
				addPopupIds = addPopupIds.replace(select,''); 
				$("#addPopupIds").val(addPopupIds);
			}
		}
	}   
}

</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;padding:0px;" cellpadding="1" cellspacing="0">
<tr>
	<td valign="top">
    <div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;导航菜单</h3>        
        <div id="treeArea" style="overflow:auto; height:420px;width:200px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
    <%=pageBean.getStringValue("menuTreeSyntax")%></div>
    </div>
    <b class="b9"></b>
    </div>    
	</td>
	<td width="85%" valign="top">
<div class="photobg1" id="tabHeader">
<div class="newarticle1"  onclick="focusTab('base')">基本信息</div>
<%
int securityTabIndex = 1;
if ("L".equals(pageBean.selectedValue("MENU_TYPE"))){
	securityTabIndex = securityTabIndex+1;
%>
<div class="newarticle1" onclick="showLayout();focusTab('layout')">页面布局</div>
<%}%>
<%if (!pageBean.getBoolValue("isRootNode") && !pageBean.getBoolValue("isLoginBeforeMenuBar")){%>
<div class="newarticle1" onclick="showSecurityConfig();focusTab('security')">安全管理</div>
<%}%>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:427px;">
<div style="margin:2px">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if ("F".equals(pageBean.selectedValue("MENU_TYPE"))){%>
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" title="新增目录" class="bartdx" align="center" onclick="createFoldRequest()"><input value="&nbsp;" type="button" class="addImgBtn" style="margin-right:0px;" />新增目录</td>
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" title="新增页面" class="bartdx" align="center" onclick="createPageRequest()"><input value="&nbsp;" type="button" class="newImgBtn" style="margin-right:0px;" />新增页面</td>
	<%}%>
<%if (!pageBean.getBoolValue("isRootNode")){%>    
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doSave()"><input value="&nbsp;" title="保存" type="button" class="saveImgBtn" style="margin-right:0px;" />保存</td>
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete()"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" style="margin-right:0px;" />删除</td>
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="showParentSelectBox()"><input value="&nbsp;" title="迁移" type="button" class="moveImgBtn" style="margin-right:0px;" />迁移</td>
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doMoveUp()"><input value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doMoveDown()"><input value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td> 
<%}%>	
	<td onmouseover="onMover(this);" onmouseout="onMout(this);"  style="margin:0; padding:0" class="bartdx" hotKey="B" align="center" onclick="doSubmit({actionType:'back'});"><input value="&nbsp;" title="返回" type="button" class="backImgBtn" />返回</td>  
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" ><a id="copyInfoId" style="display:inline-block;width:74px;height:14px;">复制信息ID</a></td>           
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
    <tr>
        <th width="100" nowrap>编码</th>
        <td width="40%"><input id="MENU_CODE" label="编码" name="MENU_CODE" type="text" value="<%=pageBean.inputValue("MENU_CODE")%>" size="24" class="text" />    </td>
        <th width="100" nowrap>类型</th>
        <td width="40%"><%=pageBean.nullToNbsp(pageBean.selectedText("MENU_TYPE"))%><input type="hidden" id="MENU_TYPE" name="MENU_TYPE" value="<%=pageBean.selectedValue("MENU_TYPE")%>" />    </td>
    </tr>
    <tr>
        <th width="100" nowrap>名称</th>
        <td><input id="MENU_NAME" label="名称" name="MENU_NAME" type="text" value="<%=pageBean.inputValue("MENU_NAME")%>" size="24" class="text" />    
            <input type="button" class="formbutton" value="多语言" onclick="configLanguage('<%=pageBean.inputValue("MENU_ID")%>','<%=pageBean.inputValue("MENU_NAME")%>');" />
        </td>
        <th width="100" nowrap>显示</th>
        <td><%=pageBean.selectRadio("MENU_VISIABLE")%></td>        
    </tr>
     <tr>
        <th width="100" nowrap>主题</th>
        <td width="40%"><select id="MENU_THEME" label="主题" name="MENU_THEME" class="select"><%=pageBean.selectValue("MENU_THEME")%></select>    </td>
        <th width="100" nowrap>布局</th>
        <td width="40%"><%=pageBean.selectedText("MAIN_LAYOUT")%></td>
    </tr>
<% if ("L".equals(pageBean.selectedValue("MENU_TYPE"))){%>
    <tr>
        <th width="100" nowrap>页面属性</th>
        <td colspan="3"><label onchange="choicePagePro()"><%=pageBean.selectRadio("MOBILE_PAGE_PRO")%></label>
        </td>
    </tr>
     <tr id="refresherLoaderTr">
        <th width="100" nowrap>下拉刷新</th>
        <td width="40%"><%=pageBean.selectRadio("refresher")%></td>
        <th width="100" nowrap>上拉刷新</th>
        <td width="40%"><%=pageBean.selectRadio("reloader")%></td>
    </tr>
     <tr id="iconEnableIconOnOffTr">
        <th width="100" nowrap>图标</th>
        <td width="40%"><input id="icon" label="icon" name="icon" type="text" value="<%=pageBean.inputValue("icon")%>" size="24" class="text" />    </td>
        <th width="100" nowrap>启用图标</th>
        <td width="40%"><%=pageBean.selectRadio("enableIconOnOff")%></td>
    </tr>
    <tr id="personaTr">
        <th width="100" nowrap>个性化</th>
        <td colspan="3"><%=pageBean.selectRadio("personal")%></td>
    </tr>
     <tr id= "titleTr">
        <th width="100" nowrap>标题</th>
        <td colspan="3"><input id="title" label="title" name="title" type="text" value="<%=pageBean.inputValue("title")%>" size="24" class="text" />    </td>
    </tr>
     <tr id="CustomeHeaderTr">
        <th width="100" nowrap>个性标题</th>
        <td width="40%"><%=pageBean.selectRadio("customeHeader")%>
        <th width="100" nowrap>存在底部</th>
        <td width="40%"><%=pageBean.selectRadio("hasFooter")%>
        </td>
    </tr>
    <tr id="stateURLStateViewTr">
        <th width="100" nowrap>状态路径</th>
        <td width="40%"><input id="stateURL" label="stateURL" name="stateURL" type="text" value="<%=pageBean.inputValue("stateURL")%>" size="24" class="text" />    </td>
        <th width="100" nowrap>状态视图</th>
        <td width="40%"><input id="stateView" label="stateView" name="stateView" type="text" value="<%=pageBean.inputValue("stateView")%>" size="26" class="text" />    </td>
    </tr>
    <tr id="dependsModulesTr">
        <th width="100" nowrap>依赖模块</th>
        <td colspan="3"><input id="dependsModules" label="dependsModules" name="dependsModules" type="text" value="<%=pageBean.inputValue("dependsModules")%>" size="88" class="text" />
        </td>
    </tr>
    <tr id="pageCtrlFileTr">
        <th width="100" nowrap>控制文件</th>
        <td colspan="3"><input id="pageCtrlFile" label="pageCtrlFile" name="pageCtrlFile" type="text" value="<%=pageBean.inputValue("pageCtrlFile")%>" size="88" class="text" />
        </td>
    </tr>
    <tr id="pageTmplFileTr">
        <th width="100" nowrap>模板文件</th>
        <td colspan="3"><input id="pageTmplFile" label="pageTmplFile" name="pageTmplFile" type="text" value="<%=pageBean.inputValue("pageTmplFile")%>" size="88" class="text" />
        </td>
    </tr>
    <tr id="menuDesc" style="display:none">
        <th width="100" nowrap>备注信息</th>
        <td colspan="3">
        <textarea name="MENU_DESC" cols="80" rows="3" class="text" id="MENU_DESC" style="width: 77%;"><%=pageBean.inputValue("MENU_DESC")%></textarea></td>
    </tr>
<%} %>

<% if ("L".equals(pageBean.selectedValue("MENU_TYPE"))){%>
<tr id="isModalPopupPopover">
<td colspan="4">
<table width="100%" border="0">
  <tr>
    <td width="33%" valign="top" class="td_border">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx td_border" align="center" onclick="showAddConfigBox('isModal');"><input value="&nbsp;" type="button" class="addImgBtn td_border" id="addImgBtn" title="添加" />添加</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx td_border" align="center" onclick="deleteConfig('modalList');"><input value="&nbsp;" type="button" class="delImgBtn td_border" id="delImgBtn" title="删除" />删除</td>   
	<td class="td_border font_style"><span>模态列表</span></td>
</tr>
</table>
<select name="modalList" size="20" id="modalList" style="width:100%;height:270px;">
<%=pageBean.selectValue("modalList") %>
</select>
<input type="hidden" id="modals" name="modals" value="<%=pageBean.inputValue("modals")%>" />
    </td>
    
    <td width="33%" valign="top" class="td_border">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx td_border" align="center" onclick="showAddConfigBox('isPopup');"><input value="&nbsp;" type="button" class="addImgBtn td_border" id="addImgBtn" title="添加" />添加</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx td_border" align="center" onclick="deleteConfig('popupList');"><input value="&nbsp;" type="button" class="delImgBtn td_border" id="delImgBtn" title="删除" />删除</td>   
   <td class="td_border font_style"><span>弹出列表</span></td>
</tr>
</table>
<select name="popupList" size="20" id="popupList" style="width:100%;height:270px;">
<%=pageBean.selectValue("popupList") %>
</select>    
<input type="hidden" id="popups" name="popups" value="<%=pageBean.inputValue("popups")%>" />  
    </td>
    
    <td width="33%" valign="top" class="td_border">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx td_border" align="center" onclick="showAddConfigBox('isPopover');"><input value="&nbsp;" type="button" class="addImgBtn td_border" id="addImgBtn" title="添加" />添加</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx td_border" align="center" onclick="deleteConfig('popoverList');"><input value="&nbsp;" type="button" class="delImgBtn td_border" id="delImgBtn" title="删除" />删除</td>   
   <td class="td_border font_style"><span>浮动列表</span></td>
</tr>
</table>
<select name="popoverList" size="20" id="popoverList" style="width:100%;height:270px;">
<%=pageBean.selectValue("popoverList") %>
</select>      
<input type="hidden" id="popovers" name="popovers" value="<%=pageBean.inputValue("popovers")%>" />
    </td>
  </tr>
</table>
</td>
</tr>
<%}%>

</table>
</div>    
</div>
<%if ("L".equals(pageBean.selectedValue("MENU_TYPE"))){%>
<div class="photobox newarticlebox" id="Layer1" style="height:427px;display:none;overflow:auto;">
<iframe id="LayoutFrame" src="" width="100%" height="430" frameborder="0" scrolling="no"></iframe>
</div>
<%}%>
<%if (!pageBean.getBoolValue("isRootNode") && !pageBean.getBoolValue("isLoginBeforeMenuBar")){%>
<div class="photobox newarticlebox" id="Layer<%=securityTabIndex%>" style="height:427px;display:none;overflow:hidden;">
<iframe id="SecurityFrame" src="" width="100%" height="430" frameborder="0" scrolling="no"></iframe>
</div>
<%}%>
    </td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" id="NAV_ID" name="NAV_ID" value="<%=pageBean.inputValue("NAV_ID")%>" />
<input type="hidden" id="MENU_ID" name="MENU_ID" value="<%=pageBean.inputValue("MENU_ID")%>" />
<input type="hidden" id="MENU_SORT" name="MENU_SORT" value="<%=pageBean.inputValue("MENU_SORT")%>" />
<input type="hidden" id="MENU_PID" name="MENU_PID" value="<%=pageBean.inputValue("MENU_PID")%>" />
<input type="hidden" id="addModalIds" name="addModalIds" value="<%=pageBean.inputValue("addModalIds")%>" />
<input type="hidden" id="addPopupIds" name="addPopupIds" value="<%=pageBean.inputValue("addPopupIds")%>" />
<input type="hidden" id="addPopoverIds" name="addPopoverIds" value="<%=pageBean.inputValue("addPopoverIds")%>" />
</form>
</body>
</html>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
$(function(){
	<%if (!pageBean.getBoolValue("isRootNode") && "L".equals(pageBean.selectedValue("MENU_TYPE"))){%>
	if ("layout" == "<%=pageBean.getStringValue("currentTabId")%>"){
		showLayout();
		tab.focus(1);
	}
	<%}%>
	<%if (!pageBean.getBoolValue("isRootNode") && !pageBean.getBoolValue("isLoginBeforeMenuBar")){%>
	if ("security" == "<%=pageBean.getStringValue("currentTabId")%>"){
		showSecurityConfig();
		<%if ("L".equals(pageBean.selectedValue("MENU_TYPE"))){%>
			tab.focus(2);			
		<%}else if ("F".equals(pageBean.selectedValue("MENU_TYPE"))){%>
			tab.focus(1);		
		<%}else if ("U".equals(pageBean.selectedValue("MENU_TYPE"))){%>
			tab.focus(1);	
		<%}else if ("V".equals(pageBean.selectedValue("MENU_TYPE"))){%>
			tab.focus(1);				
		<%}%>		
	}
	<%}%>
});
$(function(){
	choicePagePro();
	$("#copyInfoId").zclip({
		path:"<%=request.getContextPath()%>/js/ZeroClipboard.swf",
		copy:$("#MENU_ID").val()
	});
	resetTabHeight(70);
	resetTreeHeight(70);
})
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>