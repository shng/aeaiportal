<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>导航管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function goConfigPage(currentThemePro){
	if("GeneralPortal" == currentThemePro){
		doSubmit({actionType:'configMenu'});	
	}else if("MobilePortal" == currentThemePro){
		doSubmit({actionType:'mobileConfigMenu'});
	}else{
		alert("请选择当前主题属性!");
	}
	
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('configMenu')"><input value="&nbsp;" title="配置" type="button" class="detailImgBtn" />配置</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable">
<tr><td>
名称<input id="navName" label="名称" name="navName" type="text" value="<%=pageBean.inputValue("navName")%>" size="10" class="text" />
类型<select id="navType" label="类型" name="navType" class="select" onchange="doQuery()"><%=pageBean.selectValue("navType")%></select>
主题属性<select id="navThemePro" label="主题属性" name="navThemePro" class="select" onchange="doQuery()"><%=pageBean.selectValue("navThemePro")%></select>
是否有效<select id="isValid" label="是否有效" name="isValid" class="select" onchange="doQuery()"><%=pageBean.selectValue("isValid")%></select>
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="导航管理.csv"
retrieveRowsCallback="process" xlsFileName="导航管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px" 
>
<ec:row styleClass="odd" ondblclick="goConfigPage('${row.NAV_THEME_PRO}')" oncontextmenu="selectRow(this,{NAV_ID:'${row.NAV_ID}'});refreshConextmenu()" onclick="selectRow(this,{NAV_ID:'${row.NAV_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="80" property="NAV_ID" title="编码"   />
	<ec:column width="100" property="NAV_NAME" title="名称"   />
	<ec:column width="100" property="NAV_TYPE" title="类型"   mappingItem="NAV_TYPE"/>
	<ec:column width="100" property="NAV_THEME_PRO" title="主题属性"   mappingItem="NAV_THEME_PRO"/>
	<ec:column width="100" property="NAV_ISVALID" title="是否有效"   mappingItem="NAV_ISVALID"/>
</ec:row>
</ec:table>
<input type="hidden" name="NAV_ID" id="NAV_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('NAV_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
