<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>个性文件夹设置</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link rel="stylesheet" href="css/jquery.ui.core.css" type="text/css" />
<script src="js/jquery.ui.core.js" language="javascript"></script>
<script src="js/jquery.ui.widget.js" language="javascript"></script>
<script src="js/jquery.ui.mouse.js" language="javascript"></script>
<script src="js/jquery.ui.draggable.js" language="javascript"></script>
<script src="js/jquery.ui.droppable.js" language="javascript"></script>
<script src="js/jquery.ui.sortable.js" language="javascript"></script>   
<style type="text/css">
#favoriteContent{
	font-family: "Trebuchet MS", "Arial", "Helvetica", "Verdana", "sans-serif";
	font-size: 13px;
	margin: 5px;
}
#sortable {
	list-style-type: none;
	margin: 0;
	padding: 0;
}
#sortable .ui-state-default {
	list-style-type:none;
	display: block;
	border: 1px solid #ccc;
	background: #fafafa;
	color: #444;
}
#sortable li {
	margin: 0 3px 3px 3px;
	padding: 0.4em;
	padding-left: 1.5em;
	height: 18px;
	line-height: 18px;
}
.delspan{
	width:40px;
	float:right;
	text-align:center;
}
#sortable li span {
	position: absolute;
	margin-left: -1.3em;
}
.ui-icon {
	width: 16px;
	height: 16px;
	background-image: url(images/ui-icons_222222_256x240.png)/*{iconsContent}*/;
}
.ui-state-default .ui-icon {
	background-image: url(images/ui-icons_888888_256x240.png)/*{iconsDefault}*/;
}
.ui-icon-arrowthick-2-n-s {
	background-position: -128px -48px;
}
</style>
<script language="javascript">
function deleteFavorite(favoriteId){
	if (confirm("确认要删除该收藏页面吗？")){
		$("#favoriteId").val(favoriteId);
		postRequest("form1",{actionType:'deleteFavorite',onComplete:function(responseText){
			if ("success" == responseText){
				parent.retrieveFavoritePages();
				doSubmit({actionType:"prepareDisplay"});				
			}else{
				alert("删除收藏页面操作出错！！");
			}
		}});
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="favoriteContent">
<ul id="sortable">
	<%
	for (int i=0;i < pageBean.getRsList().size();i++){
	%>
	<li class="ui-state-default" id="<%=pageBean.inputValue(i,"favoriteId")%>"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><%=pageBean.inputValue(i,"favoriteTitle")%><span class="delspan" style="position: relative"><a href="javascript:deleteFavorite('<%=pageBean.inputValue(i,"favoriteId")%>')">删除</a></span></li>
	<%}%>
</ul>
</div>
<input type="hidden" id="favoriteId" name="favoriteId" value="" />
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" id="menuItemId" name="menuItemId" value="<%=pageBean.inputValue("menuItemId")%>" />
<input type="hidden" id="navId" name="navId" value="<%=pageBean.inputValue("navId")%>" />
</form>
</body>
</html>
<script language="javascript">
$(function() {
	$( "#sortable" ).sortable({
		delay: 100,
		opacity: 0.6,
		update:function(event, ui){
			var favoriteIds = $(this).sortable('toArray').toString(); 
			var url = "/portal/index?PersonalFavoriteConfig&actionType=saveSortedFavorites&navId=<%=pageBean.inputValue("navId")%>&favoriteIds="+favoriteIds;
			sendRequest(url,{onComplete:function(responseText){
				parent.retrieveFavoritePages();
			}});
		}
	});
	$( "li" ).disableSelection();
});
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>