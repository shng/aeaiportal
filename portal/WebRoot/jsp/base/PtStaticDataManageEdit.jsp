<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>静态数据管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="js/jquery.zclip.js"></script>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save',checkUnique:'true'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
<%if (!pageBean.isOnCreateMode()){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" ><a id="copyInfoId" style="display:inline-block;width:74px;height:14px;">复制信息ID</a></td>
<%}%>      
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="DATA_CODE" label="编码" name="DATA_CODE" type="text" value="<%=pageBean.inputValue("DATA_CODE")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="DATA_NAME" label="名称" name="DATA_NAME" type="text" value="<%=pageBean.inputValue("DATA_NAME")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>类型</th>
	<td><select id="DATA_TYPE" label="类型" name="DATA_TYPE" class="select"><%=pageBean.selectValue("DATA_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>分组</th>
	<td><select id="DATA_GRP" label="分组" name="DATA_GRP" class="select"><%=pageBean.selectValue("DATA_GRP")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><textarea id="DATA_DESC" label="描述" name="DATA_DESC" cols="40" rows="3" class="textarea"><%=pageBean.inputValue("DATA_DESC")%></textarea>
</td>
</tr>
<%if (!pageBean.isOnCreateMode()){ %>
<tr>
	<th width="100" nowrap>地址</th>
	<td><input id="addressURL" name="id="addressURL"" type="text" value="http://localhost:<%=request.getLocalPort()%>/portal/resource?StaticDataProvider&actionType=retrieveContent&contentId=<%=pageBean.inputValue4DetailOrUpdate("DATA_ID","")%>" size="80" class="text" readonly="readonly" />
</td>
</tr>
<%}%>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="DATA_ID" name="DATA_ID" value="<%=pageBean.inputValue4DetailOrUpdate("DATA_ID","")%>" />
</form>
<script language="javascript">
requiredValidator.add("DATA_CODE");
charNumValidator.add("DATA_CODE");
requiredValidator.add("DATA_NAME");
requiredValidator.add("DATA_TYPE");
requiredValidator.add("DATA_GRP");
initDetailOpertionImage();
$(function(){
	$("#copyInfoId").zclip({
		path:"<%=request.getContextPath()%>/js/ZeroClipboard.swf",
		copy:$("#DATA_ID").val()
	});
})
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
