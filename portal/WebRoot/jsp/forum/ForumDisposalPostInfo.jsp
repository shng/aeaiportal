<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function fpmTypeCheckChanged(fpmType) {
	if("GARBAGE_POSTS" != fpmType){
		document.getElementById("frrTypeTr").style.display="none";
		document.getElementById("frrUserNameTr").style.display="none";
		document.getElementById("frrTimeTr").style.display="none";
	}else{
		document.getElementById("frrTypeTr").style.display="";
		document.getElementById("frrUserNameTr").style.display="";
		document.getElementById("frrTimeTr").style.display="";
	}
}

function createForumReportRel(){
	postRequest('DisposalPostBoxForm',{actionType:'createForumReportRel',onComplete:function(responseText){
	}});
}

function changeFpmType(){
	var fpmType = $('input[name="fpmType"]:checked').val();
	var fpmId = $('#fpfrId').val();
	var fpmTitle = $('#fpmTitle').val();
	var url = $('#url').val();
	var fraction = "";
	if('GARBAGE_POSTS' == fpmType){
		fraction = "RUBBISH_POST";
		createForumReportRel();
	}else if("KEY_POSTS" == fpmType){
		fraction = "EMPHASIS_POST";
	}else if("ESSENCE_POSTS" == fpmType){
		fraction = "ESSENCE_POST";
	}
	
	var url = '/portal/resource?ForumProvider&actionType=changeFpmType&fpmId='+fpmId+'&fraction='+fraction+'&fpmType='+fpmType+'&fpmTitle='+fpmTitle+'&url='+url;
	sendRequest(url,{actionType:'changeFpmType',onComplete:function(responeText){
		if (responeText != 'fail'){
			alert("处置成功!!");
			javascript:parent.PopupBox.closeCurrent();
			parent.refreshCurrentPage();
		}else{
			alert("处置失败,请检查操作!!");
			location.replace(location) 
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="DisposalPostBoxForm" id="DisposalPostBoxForm" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table class="detailTable" cellspacing="0" cellpadding="0">
  <tr>
  <th width="100" nowrap>帖子分类</th>
    <td>
		<input type="radio" id="fpmType" name="fpmType" value="GARBAGE_POSTS" checked="checked" onchange="fpmTypeCheckChanged('GARBAGE_POSTS')"/><a style="font-size:15px;">垃圾帖</a>
		<input type="radio" id="fpmType" name="fpmType" value="KEY_POSTS" onchange="fpmTypeCheckChanged('KEY_POSTS')" / ><a style="font-size:15px;">重点帖</a>
		<input type="radio" id="fpmType" name="fpmType" value="ESSENCE_POSTS" onchange="fpmTypeCheckChanged('ESSENCE_POSTS')" /><a style="font-size:15px;">精华帖</a>
		<input type="radio" id="fpmType" name="fpmType" value="ORDINARY_POSTS" onchange="fpmTypeCheckChanged('ORDINARY_POSTS')" /><a style="font-size:15px;">普通帖</a>
	</td>
  </tr>
  <tr id="frrTypeTr">
  <th width="100" nowrap>举报类型</th>
    <td>
		<input type="radio" id="frrType" name="frrType" value="PORN" checked="checked"/><a style="font-size:15px;">色情</a>
		<input type="radio" id="frrType" name="frrType" value="POLITICS" /><a style="font-size:15px;">政治</a>
		<input type="radio" id="frrType" name="frrType" value="ADVERTISEMENT" /><a style="font-size:15px;">广告</a>
		<input type="radio" id="frrType" name="frrType" value="FILTHY" /><a style="font-size:15px;">污秽</a>
		<input type="radio" id="frrType" name="frrType" value="VIOLENCE" /><a style="font-size:15px;">暴力</a>
		<input type="radio" id="frrType" name="frrType" value="OTHERS" /><a style="font-size:15px;">其他</a>
	</td>
  </tr>
  <tr id="frrUserNameTr">
  <th width="100" nowrap>举报人</th>
    <td colspan="3"><input id="FRR_USER_NAME" type="text" label="举报人" name="FRR_USER_NAME" value="<%=pageBean.inputValue("FRR_USER_NAME")%>" size="24"/>
    <input id="FRR_USERID" name="FRR_USERID" type="hidden" value="<%=pageBean.inputValue("FRR_USERID")%>" class="text" />
	</td>
  </tr>
  <tr id ="frrTimeTr">
  <th width="100" nowrap>举报时间</th>
   <td colspan="3"><input id="FRR_TIME" label="时间" name="FRR_TIME" type="text" value="<%=pageBean.inputDate("FRR_TIME")%>" size="24" class="text" />
   </td>
  </tr>
  <tr>
    <td colspan="4" align="center">
    <input class="formbutton" type="button" name="Button23" value="处置" onclick="changeFpmType()"/>&nbsp; &nbsp;
    <input class="formbutton" type="button" name="Button22" value="关闭" onclick="javascript:parent.PopupBox.closeCurrent();"/></td>
  </tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="fpfrId" id="fpfrId" value = "<%=pageBean.inputValue("fpfrId")%>" />
<input type="hidden" name="fpmTitle" id="fpmTitle" value = "<%=pageBean.inputValue("fpmTitle")%>" />
<input type="hidden" name="url" id="url" value = "<%=pageBean.inputValue("url")%>" />
</form>
<script language="javascript">
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
