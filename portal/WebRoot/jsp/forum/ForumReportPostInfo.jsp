<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function createForumReportRel(){
	postRequest('formReportPostBoxForm',{actionType:'createForumReportRel',onComplete:function(responseText){
		if (responseText == 'fail'){
			alert("举报失败请检查操作!!");
		}else {
			alert("感谢您的举报,我们会尽快处理!!");
			javascript:parent.PopupBox.closeCurrent();
			parent.refreshCurrentPage();
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="formReportPostBoxForm" id="formReportPostBoxForm" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table class="detailTable" cellspacing="0" cellpadding="0">
  <tr>
  <th width="100" nowrap>类型</th>
    <td colspan="3"><select id="FRR_RES_TYPE" label="类型" name="FRR_RES_TYPE" class="select"><%=pageBean.selectValue("FRR_RES_TYPE")%></select>
	</td>
  </tr>
  <tr>
  <th width="100" nowrap>举报类型</th>
    <td>
		<input type="radio" id="frrType" name="frrType" value="PORN" checked="checked" /><a style="font-size:15px;">色情</a>
		<input type="radio" id="frrType" name="frrType" value="POLITICS" /><a style="font-size:15px;">政治</a>
		<input type="radio" id="frrType" name="frrType" value="ADVERTISEMENT" /><a style="font-size:15px;">广告</a>
		<input type="radio" id="frrType" name="frrType" value="FILTHY" /><a style="font-size:15px;">污秽</a>
		<input type="radio" id="frrType" name="frrType" value="VIOLENCE" /><a style="font-size:15px;">暴力</a>
		<input type="radio" id="frrType" name="frrType" value="OTHERS" /><a style="font-size:15px;">其他</a>
	</td>
  </tr>
  <tr>
  <th width="100" nowrap>举报人</th>
    <td colspan="3"><input id="FRR_USER_NAME" type="text" label="举报人" name="FRR_USER_NAME" value="<%=pageBean.inputValue("FRR_USER_NAME")%>" size="24"/>
    <input id="FRR_USERID" name="FRR_USERID" type="hidden" value="<%=pageBean.inputValue("FRR_USERID")%>" class="text" />
	</td>
  </tr>
  <tr>
  <th width="100" nowrap>举报时间</th>
   <td colspan="3"><input id="FRR_TIME" label="时间" name="FRR_TIME" type="text" value="<%=pageBean.inputDate("FRR_TIME")%>" size="24" class="text" />
   </td>
  </tr>
  <tr>
    <td colspan="4" align="center">
    <input class="formbutton" type="button" name="Button23" value="举报" onclick="createForumReportRel()"/>&nbsp; &nbsp;
    <input class="formbutton" type="button" name="Button22" value="关闭" onclick="javascript:parent.PopupBox.closeCurrent();"/></td>
  </tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="fpfrId" id="fpfrId" value = "<%=pageBean.inputValue("fpfrId")%>" />
<input type="hidden" name="fpmTitle" id="fpmTitle" value = "<%=pageBean.inputValue("fpmTitle")%>" />
<input type="hidden" name="forumCurrentPostId" id="forumCurrentPostId" value = "<%=pageBean.inputValue("forumCurrentPostId")%>" />
</form>
<script language="javascript">
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
