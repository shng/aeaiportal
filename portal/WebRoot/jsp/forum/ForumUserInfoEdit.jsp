<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveRecord(){
	if (!validate()){
		return;
	}
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if (responseText == 'success'){
			alert("信息已保存！");
		}
	}});	
}
var addResourceRequestBox;
function openAddResourceRequestBox(){
	var title = "上传图片";
	if (!addResourceRequestBox){
		addResourceRequestBox = new PopupBox('addResourceRequestBox',title,{size:'big',width:'260px',height:'260px',top:'3px'});
	}
	var url = 'index?ForumUserProfileResouceUploader&BIZ_ID='+$('#FU_ID').val();
	addResourceRequestBox.sendRequest(url);	
}
function delResourceRequest(){
	if (confirm('是否确认要删除附件？')){
		var url = 'index?ForumUserProfileResouceUploader&actionType=delResource&REL_ID='+$('#REL_ID').val()+'&BIZ_ID='+$('#FU_ID').val();
		sendRequest(url,{onComplete:function(responseText){
			if ("success"==responseText){
				loadResourceList();
				document.location.reload(); 
			}
		}});		
	}
}
function loadResourceList(){
	var url = 'index?ForumUserProfileResouceUploader&actionType=loadResourceList&BIZ_ID='+$('#FU_ID').val();
	sendRequest(url,{onComplete:function(responseText){
		if ("fail" != responseText){
			var datas = $.parseJSON(responseText);
			if (datas){
				for (var i=0;i < datas.length;i++){
					var data = datas[i];
					var dataText = data.text;
					var dataValue = data.value;
					$("#REL_ID").val(dataValue);
				}				
				if (datas.length > 0){
					$('#addResourceBtn').disable();
					$('#delResourceBtn').enable();
				}else{
					$('#addResourceBtn').enable();
					$('#delResourceBtn').disable();
				}
			}else{
				$('#addResourceBtn').enable();
				$('#delResourceBtn').disable();
			}
		}else{
			$('#addResourceBtn').enable();
			$('#delResourceBtn').disable();
		}
	}});
}
function refreshPage(){
	loadResourceList();
	addResourceRequestBox.closeBox();
	document.location.reload(); 
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveRecord()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0" style="width:57%;">
<tr>
	<th width="100" nowrap>编号</th>
	<td><input id="FU_CODE" label="编号" name="FU_CODE" style="background:#CCCCCC" type="text" readonly="readonly" value="<%=pageBean.inputValue("FU_CODE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>昵称</th>
	<td><input id="FU_NAME" label="昵称" name="FU_NAME" type="text" value="<%=pageBean.inputValue("FU_NAME")%>" size="24" maxlength="60" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>用户等级</th>
	<td><input id="FU_LEVEL_TEXT" label="用户等级" style="background:#CCCCCC" name="FU_LEVEL_TEXT" readonly="readonly" type="text" size="24" value="<%=pageBean.selectedText("FU_LEVEL")%>">
		<input id="FU_LEVEL" label="用户等级" name="FU_LEVEL" type="hidden" value="<%=pageBean.selectedValue("FU_LEVEL")%>">
</td>
</tr>
<tr>
	<th width="100" nowrap>头像</th>
	<td>
	<img src="<%=pageBean.inputValue("FU_PROFILE")%>"  style="width:100px;height:100px;vertical-align:middle;">
	<span style="margin-left:2px;padding-left:2px;display: inline-block;">
	<div style="margin:1px"><input type="button" name="addResourceBtn" id="addResourceBtn" <%=pageBean.readonly(pageBean.isOnCreateMode())%> value="添加" onclick="openAddResourceRequestBox()" />
	<input type="button" name="delResourceBtn" id="delResourceBtn" <%=pageBean.disabled(pageBean.isOnCreateMode())%> value="删除" onclick="delResourceRequest()" /></div>
	<div style="margin:1px">上传图片最大为100×100px</br>格式为*.jpg;*.bmp;*.png;</div>
	</span>
	</td>
</tr>
<tr>
	<th width="100" nowrap>电话</th>
	<td ><input id="FU_TEL" label="电话" name="FU_TEL" type="text" value="<%=pageBean.inputValue("FU_TEL")%>" size="24" maxlength="20" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>邮箱</th>
	<td><input id="FU_EMAIL" label="邮箱" name="FU_EMAIL" type="text" value="<%=pageBean.inputValue("FU_EMAIL")%>" size="24" maxlength="40" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>注册日期</th>
	<td><input id="FU_CREATE_TIME" label="注册日期" name="FU_CREATE_TIME" style="background:#CCCCCC" type="text" readonly="readonly" value="<%=pageBean.inputTime("FU_CREATE_TIME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>积分</th>
	<td><input id="FU_INTEGRAL" label="积分" name="FU_INTEGRAL" style="background:#CCCCCC" readonly="readonly" type="text" value="<%=pageBean.inputValue("FU_INTEGRAL")%>" size="24" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="FU_ID" name="FU_ID" value="<%=pageBean.inputValue("FU_ID")%>" />
<input type="hidden" id="REL_ID" name="REL_ID" value="" />
</form>
<script language="javascript">
if (isValid($("#FU_ID").val())){
	loadResourceList();
}
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
