<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var operaRequestBox;
function openContentRequestBox(operaType,title,handlerId,subPKField){
	if ('insert' != operaType && !isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!operaRequestBox){
		operaRequestBox = new PopupBox('operaRequestBox',title,{size:'big',top:'3px'});
	}
	var columnIdValue = $("#columnId").val();
	var url = 'index?'+handlerId+'&CODE_ID='+columnIdValue+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	operaRequestBox.sendRequest(url);
}
function showFilterBox(){
	$('#filterBox').show();
	var clientWidth = $(document.body).width();
	var tuneLeft = (clientWidth - $("#filterBox").width())/2-2;	
	$("#filterBox").css('left',tuneLeft);	
}
function doRemoveContent(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (confirm('确认要移除该条记录吗？')){
		postRequest('form1',{actionType:'isLastRelation',onComplete:function(responseText){
			if (responseText == 'true'){
				if (confirm('该信息只有一条关联记录，确认要删除吗？')){
					doSubmit({actionType:'delete'});
				}
			}else{
				doSubmit({actionType:'removeContent'});
			}
		}});	
	}
}

function isSelectedTree(){
	if (isValid($('#columnId').val())){
		return true;
	}else{
		return false;
	}
}
function refreshTree(){
	doQuery();
}
function refreshContent(curNodeId){
	if (curNodeId){
		$('#columnId').val(curNodeId);
	}
	doSubmit({actionType:'query'});
}
var targetTreeBox;
function openTargetTreeBox(curAction){
	var columnIdValue = $("#columnId").val();
	if (!isSelectedTree()){
		writeErrorMsg('请先选中一个树节点!');
		return;
	}
	if (curAction == 'copyContent' || curAction == 'moveContent'){
		if (!isSelectedRow()){
			writeErrorMsg('请先选中一条记录!');
			return;
		}
		columnIdValue = $("#curColumnId").val()
	}	
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标分组',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "UserLevel8BBSUserInfoManageTreePick";
	var url = 'index?'+handlerId+'&CODE_ID='+columnIdValue;
	targetTreeBox.sendRequest(url);
	$("#actionType").val(curAction);
}
function doChangeParent(){
	var curAction = $('#actionType').val();
	postRequest('form1',{actionType:curAction,onComplete:function(responseText){
		if (responseText == 'success'){
			if (curAction == 'moveTree'){
				refreshTree();			
			}else{
				refreshContent($("#targetParentId").val());		
			}
		}else {
			writeErrorMsg('迁移父节点出错啦！');
		}
	}});
}
function clearFilter(){
	$("#filterBox input[type!='button'],select").val('');
	doQuery();
}
function syncUser(){
	postRequest('form1',{actionType:'syncUser',onComplete:function(responseText){
		if (responseText == 'success'){
			alert("同步用户成功！");
			doQuery(); 
		}else {
			writeErrorMsg('同步用户出错啦！');
		}
	}});
}
function doDelete(fuId){
	var confirmMsg="您确认要是删除数据吗？";
	if (!isValid(fuId)){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if(confirm(confirmMsg)) {
		postRequest('form1',{actionType:'delete',onComplete:function(responseText){
			if (responseText == 'success'){
				alert("删除用户成功！");
				doQuery();
			}else {
				alert(responseText);
			}
		}});
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;padding:0px;" cellpadding="1" cellspacing="0">
<tr>
	<td valign="top">
    <div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;分组列表 </h3>        
	<div id="treeArea" style="overflow:auto; height:420px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
    <%=pageBean.getStringValue("menuTreeSyntax")%></div>
    </div>
    <b class="b9"></b>
    </div>
    <input type="hidden" id="columnId" name="columnId" value="<%=pageBean.inputValue("columnId")%>" />
    <input type="hidden" id="targetParentId" name="targetParentId" value="" />
    </td>
	<td width="85%" valign="top">
<div id="__ToolBar__">
<span style="float:right;height:28px;line-height:28px;"><input style="vertical-align:middle; margin-top:-2px; margin-bottom:1px;" name="showChildNodeRecords" type="checkbox" id="showChildNodeRecords" onclick="doQuery()" value="Y" <%=pageBean.checked(pageBean.inputValue("showChildNodeRecords"))%> />&nbsp;显示子节点记录&nbsp;&nbsp;状态&nbsp;<select id="fuState" label="状态" name="fuState" onchange="doQuery()" class="select"><%=pageBean.selectValue("fuState")%></select></span>
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="syncUser()"><input value="&nbsp;" title="同步" type="button" class="createImgBtn" style="margin-right:" />同步</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="openContentRequestBox('update','论坛用户信息','ForumUserEdit','FU_ID')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="F" align="center" onclick="showFilterBox()"><input value="&nbsp;" title="过滤" type="button" class="filterImgBtn" />过滤</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
</tr>
</table>
</div>
<div id="rightArea">
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="论坛用户信息.csv"
retrieveRowsCallback="process" xlsFileName="论坛用户信息.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();openContentRequestBox('detail','论坛用户信息','ForumUserEdit','FU_ID')" oncontextmenu="selectRow(this,{FU_ID:'${row.FU_ID}',curColumnId:'${row.FU_ID}'});refreshConextmenu()" onclick="selectRow(this,{FU_ID:'${row.FU_ID}',curColumnId:'${row.FU_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="FU_NAME" title="用户名称"   />
	<ec:column width="100" property="FU_CODE" title="用户编码"   />
	<ec:column width="100" property="FU_EMAIL" title="用户邮箱"   />
	<ec:column width="100" property="FU_TEL" title="联系电话"   />
	<ec:column width="100" property="FU_CREATE_TIME" title="注册日期" cell="date" format="yyyy-MM-dd" />
	<ec:column width="100" property="FU_STATE" title="用户状态"   mappingItem="FU_STATE"/>
	<ec:column width="100" property="FU_INTEGRAL" title="用户积分"   />
</ec:row>
</ec:table>

<div id="filterBox" class="sharp color2" style="position:absolute;top:30px;display:none; z-index:10; width:480px;">
<b class="b9"></b>
<div class="content" style="background-color:white;">
<h3>&nbsp;&nbsp;条件过滤框</h3>
<table class="detailTable" cellpadding="0" cellspacing="0" style="width:99%;margin:1px;">
<tr>
	<th width="100" nowrap>用户姓名</th>
	<td><input id="fuName" label="用户姓名" name="fuName" type="text" value="<%=pageBean.inputValue("fuName")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>用户编码</th>
	<td><input id="fuCode" label="用户编码" name="fuCode" type="text" value="<%=pageBean.inputValue("fuCode")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>邮箱</th>
	<td><input id="fuEmail" label="邮箱" name="fuEmail" type="text" value="<%=pageBean.inputValue("fuEmail")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>联系电话</th>
	<td><input id="fuTel" label="联系电话" name="fuTel" type="text" value="<%=pageBean.inputValue("fuTel")%>" size="24" class="text" />
</td>
</tr>
</table>
<div style="width:100%;text-align:center;height:33px;line-height:33px;">
<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
&nbsp;&nbsp;
<input type="button" name="button" id="button" value="清空" class="formbutton" onclick="clearFilter()" />
&nbsp;&nbsp;<input type="button" name="button" id="button" value="关闭" class="formbutton" onclick="javascript:$('#filterBox').hide();" /></div>
</div>
<b class="b9"></b>
</div>
<input type="hidden" id="_tabId_" name="_tabId_" value="<%=pageBean.inputValue("_tabId_")%>" />
<input type="hidden" name="FU_ID" id="FU_ID" value="" />
<input type="hidden" name="curColumnId" id="curColumnId" value="" />
<script language="JavaScript">
setRsIdTag('FU_ID');
$(window).load(function() {
	setTimeout(function(){
		resetTreeHeight(70);
		resetRightAreaHeight(72);
	},1);	
});
</script>
</div>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
