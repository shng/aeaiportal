<%@ page contentType="text/html; charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html>
<head>
<title>企业门户平台--登陆页面</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<%@include file="/jsp/inc/resource.inc.jsp"%>
<style>
body{text-align:center}
#postionDiv{
	width:231px;
	height:143px;
	background-image:url("images/index/login_bg.gif");
}
</style>
<script type="text/javascript">
function doLogin(){
	postRequest('form1',{onComplete:function(responseText){
		var resut = responseText.split(";");
		if (resut[0]=='sucess'){
			top.location.href = resut[1];
		}else{
			alert(resut[1]);
		}
	}});
}
</script>
</head>
<body style="background-color:#F9F9F9;margin:0;padding:0;overflow:hidden;">
<form id="form1" name="form1" method="post" action="<%=pageBean.getHandlerURL()%>" style="text-align:center">
<div id="postionDiv">
  <table border="0" style="margin:42px 2px 2px 56px;">
    <tr>
      <td width="50" align="right" nowrap style="font-weight:bold">用户名</td>
      <td><input style="width:100px" label="用户名" class="formtext" name="userId" type="text" id="userId" size="10" maxlength="10"></td>
    </tr>
    <tr>
      <td width="50" align="right" nowrap style="font-weight:bold">密&nbsp;&nbsp;码</td>
      <td><input style="width:100px" label="密码" class="formtext" name="userPwd" type="password" id="userPwd" size="10" maxlength="10" onKeyDown="keyDownLogin(event)"></td>
    </tr>
    <tr>
      <td colspan="2" align="center" style="padding-top:6px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input class="formbutton" type="button" name="button" id="button" value="登 录" onClick="doLogin()">
        <input class="formbutton" type="reset" name="button2" id="button2" value="重 置"></td>
    </tr>
  </table>
</div>  
<input type="hidden" name="actionType" id="actionType" value="login"/>
</form>
<script language="javascript">
$('#userId').focus();
function keyDownLogin(e)
{
  var key = window.event ? e.keyCode:e.which;
  if (key == 13)
    {
        doLogin();
    }
} 
</script>
</body>
</html>