<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="com.agileai.portal.driver.util.DecoratorUtil" %>
<%@ page language="java" import="com.agileai.hotweb.domain.FormSelect" %>
<%@ page language="java" import="com.agileai.hotweb.domain.FormSelectFactory" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String canMin = pageBean.inputValue("canMin");
String canMax = pageBean.inputValue("canMax");
String circular = pageBean.inputValue("circular");

String iconPath = pageBean.inputValue("iconPath");
FormSelect iconPathSelect = FormSelectFactory.create("PORTLET_ICON_PATH");
iconPathSelect.setSelectedValue(iconPath);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>继承类型外观设置</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveConfig(){
	var title = $('#title').val();
	var height = $('#height').val();
	
	if (validation.checkNull(title)){
		alert('Porlet标题不能为空！');
		return;
	}
	if (validation.checkNull(height)){
		alert('Porlet高度不能为空！');
		return;
	}	
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if ("OK"==responseText){
			parent.window.location.reload();
//			parent.PopupBox.closeCurrent();
		}else{
			alert(responseText);
			parent.PopupBox.closeCurrent();
		}
	}});
}
function changeDecorator(){
	doSubmit({actionType:'prepareDisplay'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveConfig()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>标题</th>
	<td width="80%"><input id="title" name="title" type="text" value="<%=pageBean.inputValue("title") %>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>Portlet名称</th>
	<td width="80%"><%=pageBean.inputValue("portletResName") %></td>
</tr>
<tr>
	<th width="100" nowrap>装饰器</th>
	<td width="80%"><select style="width: 180px;" onchange="changeDecorator()" id="portletDecorator" label="装饰器" name="portletDecorator" class="select">
	<%=pageBean.selectValue("portletDecorator")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>高度</th>
	<td width="80%"><input id="height" name="height" type="text" value="<%=pageBean.inputValue("height") %>" size="32" class="text" />（默认值为：auto）
</td>
</tr>
<tr>
	<th width="100" nowrap>是否显示</th>
	<td width="80%"><%=pageBean.selectRadio("portletVisiable")%></td>
</tr>
<tr>
	<th width="100" nowrap>图标路径</th>
	<td width="80%"><div style="float:left;"><select onchange="changeIconPath(this.value)" id="iconPath" label="图标路径" name="iconPath" class="select"><%=iconPathSelect%></select>
	</div>
	<div style="margin-left: 5px;padding-left:5px;">&nbsp;&nbsp;<img id="preview" src="/portal/images/icon/default.png" /></div></td>
</tr>
<tr>
	<th width="100" nowrap>更多链接</th>
	<td width="80%"><input name="moreURL" type="text" class="text" id="moreURL" value="<%=pageBean.inputValue("moreURL") %>" size="52" maxlength="128" /></td>
</tr>
<tr>
	<th width="100" nowrap>主体样式</th>
	<td width="80%"><textarea name="portletBodyStyle" style="width:450px;" cols="52" rows="3" class="text" id="portletBodyStyle"><%=pageBean.inputValue("portletBodyStyle") %></textarea></td>
</tr>
</table>
<input type="hidden" id="containerId" name="containerId" value="<%=pageBean.inputValue("containerId")%>" />
<input type="hidden" id="portletId" name="portletId" value="<%=pageBean.inputValue("portletId")%>" />
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
</body>
</html>
<script language="javascript">
function changeIconPath(asrc){
	if (isValid(asrc)){
		$("#preview").attr('src',asrc);		
	}else{
		$("#preview").attr('src','/portal/images/icon/default.png');
	}
}
changeIconPath($("#iconPath").val());
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
