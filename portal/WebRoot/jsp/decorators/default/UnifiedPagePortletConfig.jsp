<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="com.agileai.portal.driver.util.DecoratorUtil" %>
<%@ page language="java" import="com.agileai.hotweb.domain.FormSelect" %>
<%@ page language="java" import="com.agileai.hotweb.domain.FormSelectFactory" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String canMin = pageBean.inputValue("canMin");
String canMax = pageBean.inputValue("canMax");
String circular = pageBean.inputValue("circular");

String colour = pageBean.inputValue("colour");
FormSelect colourFormSelect = DecoratorUtil.getPortletColorSelect(colour);
String colourSyntax = DecoratorUtil.buildColorFormSelectSyntax(colourFormSelect);

String iconPath = pageBean.inputValue("iconPath");
FormSelect iconPathSelect = FormSelectFactory.create("PORTLET_ICON_PATH");
iconPathSelect.setSelectedValue(iconPath);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Portlet外观设置</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveConfig(){
	var colour = $('#colour').val();
	
	if (validation.checkNull(colour)){
		alert('Porlet颜色不能为空！');
		return;
	}	
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if ("OK"==responseText){
			alert("保存成功!");
			parent.PopupBox.closeCurrent();
		}else{
			alert(responseText);
			parent.PopupBox.closeCurrent();
		}
	}});
}
function changeColor(){
	var obj = ele('colour');
	var selectedIndex = obj.selectedIndex;
	obj.style.background = obj.options[selectedIndex].style.background;
	$("#portletHeight").focus();
}
function initColor(){
	var obj = ele('colour');
	var selectedIndex = obj.selectedIndex;
	if (selectedIndex != -1){
		obj.style.background = obj.options[selectedIndex].style.background;
	}
}
function changeDecorator(){
	doSubmit({actionType:'prepareDisplay'});
}
</script>
</head>
<body onload="initColor()">
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveConfig()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>装饰器</th>
	<td width="80%"><select style="width: 180px;" onchange="changeDecorator()" id="portletDecorator" label="装饰器" name="portletDecorator" class="select">
	<%=pageBean.selectValue("portletDecorator")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>颜色</th>
	<td width="80%"><select style="width:180px;height:20px;" onchange="changeColor()" id="colour" label="类型" name="colour" class="select">
	  <%=colourSyntax%>
	  </select></td>
</tr>
<tr>
	<th width="100" nowrap>是否圆角</th>
	<td width="80%">
	<input type="radio" name="circular" id="circularY" value="Y" />&nbsp;是&nbsp;&nbsp;&nbsp;<input type="radio" name="circular" id="circularN" value="N" />&nbsp;否		
	</td>
</tr>
<tr>
	<th width="100" nowrap>是否最大化</th>
	<td width="80%">
	<input type="radio" name="canMax" id="canMaxY" value="Y" />&nbsp;是&nbsp;&nbsp;&nbsp;<input type="radio" name="canMax" id="canMaxN" value="N" />&nbsp;否
	</td>
</tr>
<tr>
	<th width="100" nowrap>是否最小化</th>
	<td width="80%">
	<input type="radio" name="canMin" id="canMinY" value="Y" />&nbsp;是&nbsp;&nbsp;&nbsp;<input type="radio" name="canMin" id="canMinN" value="N" />&nbsp;否
	</td>
</tr>
<tr>
	<th width="100" nowrap>图标路径</th>
	<td width="80%"><div style="float:left;"><select onchange="changeIconPath(this.value)" id="iconPath" label="图标路径" name="iconPath" class="select"><%=iconPathSelect%></select>
	</div>
	<div style="margin-left: 5px;padding-left:5px;">&nbsp;&nbsp;<img id="preview" src="/portal/images/icon/default.png" /></div></td>
</tr>
</table>
<input type="hidden" id="containerId" name="containerId" value="<%=pageBean.inputValue("containerId")%>" />
<input type="hidden" id="themeId" name="themeId" value="<%=pageBean.inputValue("themeId")%>" />
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
</body>
</html>
<script language="javascript">
<%if ("Y".equals(canMin)){%>
$('#form1 #canMinY').attr("checked","checked");
<%}else{%>
$('#form1 #canMinN').attr("checked","checked");
<%}%>

<%if ("Y".equals(canMax)){%>
$('#form1 #canMaxY').attr("checked","checked");
<%}else{%>
$('#form1 #canMaxN').attr("checked","checked");
<%}%>

<%if ("Y".equals(circular)){%>
$('#form1 #circularY').attr("checked","checked");
<%}else{%>
$('#form1 #circularN').attr("checked","checked");
<%}%>

function changeIconPath(asrc){
	if (isValid(asrc)){
		$("#preview").attr('src',asrc);		
	}else{
		$("#preview").attr('src','/portal/images/icon/default.png');
	}
}
changeIconPath($("#iconPath").val());
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
