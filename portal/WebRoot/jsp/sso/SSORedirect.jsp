<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<%@ page language="java" import="com.agileai.portal.bizmoduler.sso.SSOTicket" %>
<%@ page language="java" import="com.agileai.portal.bizmoduler.sso.SSOTicketEntry" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html>
<head>
<title>正在跳转....</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<script src="js/jquery1.7.1.js" language="javascript"></script>
<script src="js/util.js" language="javascript"></script>
</head>
<body style="background-color:#F9F9F9;margin:0;padding:0;overflow:hidden;">
<%
SSOTicket ssoTicket = (SSOTicket)pageBean.getAttribute("ssoTicket");
String userId = pageBean.getStringValue("userId");
if (ssoTicket != null){
if ("BS".equals(ssoTicket.getAppType())){
if ("FORM".equals(ssoTicket.getAuthType())){
List<SSOTicketEntry> entryList = ssoTicket.getSsoTicketEntryList();
%>

<%
if (ssoTicket.getRedirectURL() == null){
%>
<form id="form1" name="form1" method="post" action="<%=ssoTicket.getAppURL()%>" style="text-align:center">
<%
for (int i=0;i < entryList.size();i++){
SSOTicketEntry entry = entryList.get(i);
String paramCode = entry.getParamCode();
String paramValue = entry.getParamValue();
%>
<input type="hidden" name="<%=paramCode%>" value="<%=paramValue%>"/>
<%}%>
</form>
<script language="javascript">
document.getElementById('form1').submit();
</script>

<%}else{ %>

<iframe id="reloginFrame" name="reloginFrame" src="" width="1" height="0" frameborder="0"></iframe>
<form id="form1" name="form1" method="post" action="<%=ssoTicket.getAppURL()%>" style="text-align:center" target="reloginFrame">
<%
for (int i=0;i < entryList.size();i++){
SSOTicketEntry entry = entryList.get(i);
String paramCode = entry.getParamCode();
String paramValue = entry.getParamValue();
%>
<input type="hidden" name="<%=paramCode%>" value="<%=paramValue%>"/>
<%}%>
</form>
<script language="javascript">
document.getElementById('form1').submit();
$('#reloginFrame').load(function(){
	window.location.href="<%=ssoTicket.getRedirectURL()%>";
});
</script>
<%}%>

<%}else if ("CAS".equals(ssoTicket.getAuthType())){%>
<script language="javascript">
window.location.href="<%=ssoTicket.getAppURL()%>";
</script>
<%}}else{%>
<script language="javascript">
function runLocalExe(strPath) {
	var objShell = new ActiveXObject("wscript.shell");
    objShell.Run(strPath);
    objShell = null;

}
runLocalExe('<%=ssoTicket.getExeInvokeURL(userId)%>');
window.opener=null;
window.open('','_self');
window.close();
</script>
<%}}else{%>
单点登录凭证不合法，请确认！
<%}%>
</body>
</html>