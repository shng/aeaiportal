<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String errorMsg = (String)request.getAttribute("errorMsg");
%>
<html>
<head>
<title>登录跳转中....</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
</head>
<body style="background-color:#F9F9F9;margin:0;padding:0;">
    <form method="post" action="/cas/login?service=<%=pageBean.inputValue("service")%>">
        <input type="hidden" name="username" value="<%=pageBean.inputValue("username")%>" />
        <input type="hidden" name="password" value="<%=pageBean.inputValue("password")%>" />
        <input type="hidden" name="comeFromKey" value="<%=pageBean.inputValue("comeFromKey")%>" />
        <input type="hidden" name="service" value="<%=pageBean.inputValue("service")%>" />
    </form>
<%if (errorMsg != null){ %>
    <p><%=errorMsg%></p>
<%}%>    
</body>
</html>
<%if (errorMsg == null || errorMsg.trim().equals("")){ %>
<script type="text/javascript">
document.forms[0].submit();
</script>
<%}%>