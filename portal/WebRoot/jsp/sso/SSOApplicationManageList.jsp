<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>单点应用管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function deleteRecord(itemValue){
	if (!isValid(itemValue)){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	var url = '<%=pageBean.getHandlerURL()%>&actionType=isExistRelation&appId='+itemValue;
	sendRequest(url,{onComplete:function(responseText){
		if (responseText == 'existRelApplication'){
			writeErrorMsg('该应用已被关联不能删除');
		}
		else if (responseText == 'existSecurityRelation'){
			writeErrorMsg('该应用存在授权关联，请先删除授权关联！');
		}		
		else{
			doDelete(itemValue);
		}
	}});
}
var configSecurityBox;
function configSecurityRequest(){
	var appId = $("#APP_ID").val();
	if (!isValid(appId)){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!configSecurityBox){
		configSecurityBox = new PopupBox('configSecurityBox','安全配置',{size:'normal',width:'700px',height:'450px',top:'2px'});
	}
	var url = 'index?SecurityAuthorizationConfig&resourceType=Application&resourceId='+appId;
	configSecurityBox.sendRequest(url);	
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="configSecurityRequest();"><input value="&nbsp;" type="button" class="assignImgBtn" id="saveImgBtn" title="安全设置" />安全设置</td>      
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="deleteRecord($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable">
<tr><td>
名称<input id="appName" label="名称" name="appName" type="text" value="<%=pageBean.inputValue("appName")%>" size="10" class="text" />
类型<select id="appType" label="类型" name="appType" class="select"><%=pageBean.selectValue("appType")%></select>
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="单点应用管理.csv"
retrieveRowsCallback="process" xlsFileName="单点应用管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="doRequest('viewDetail')" oncontextmenu="selectRow(this,{APP_ID:'${row.APP_ID}'});refreshConextmenu()" onclick="selectRow(this,{APP_ID:'${row.APP_ID}'})">
	<ec:column width="40" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="150" property="APP_NAME" title="名称" />
	<ec:column width="80" property="APP_TYPE" title="类型"   mappingItem="APP_TYPE"/>
    <ec:column width="80" property="APP_GROUP" title="分组"  mappingItem="APP_GROUP"/>
	<ec:column width="400" property="APP_PATH" title="访问URL" maxLength="80" />    
</ec:row>
</ec:table>
<input type="hidden" name="APP_ID" id="APP_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('APP_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
