<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String navCode = menuBar.getId();

String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
String modeText = "view".equals(windowMode)?"编辑组件":"展示信息";
String hostIp = request.getRemoteAddr();
if ("0:0:0:0:0:0:0:1".equals(hostIp)){
	hostIp = "localhost";
}
%>
<html>
<head>
<meta charset="utf-8">
<title>手机数通畅联</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.structure-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.inline-png-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.theme-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/css/iconfonts/iconfont.css" rel="stylesheet" type="text/css" />
<%if ("edit".equals(windowMode)){%>	
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
<style type="text/css">
body {
	font-size:1em;
}
div,td,span,th {
	font-size:1em;
}
</style>
<%}%>
<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=menuItem.getCustomCssURL()%>" type="text/css" />
<%}%>	
<script src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.min.js" type="text/javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
<%if ("edit".equals(windowMode)){%>	
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script>
<%}%>
<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>	
<script type="text/javascript">
<%if ("edit".equals(windowMode)){%>	
$.ajaxSetup({cache:false});
var __renderPortlets = new Map();	
var _directConfigPortletBox;
function directConfigDecoratorRequest(portletId){
	if (!_directConfigPortletBox){
		_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'380px'});
	}
	var url = '/portal/index?PagePortletConfig&portletId='+portletId;
	_directConfigPortletBox.sendRequest(url);	
}
$(function(){
	<%if ("edit".equals(windowMode)){%>
	$("#content_container > h3").hover(
	  function () {
	    $(this).find("span[id=stateAnchor]").fadeIn('fast');
	  }, 
	  function () {
	    $(this).find("span[id=stateAnchor]").fadeOut('fast');
	  }
	);
	<%}%>
	$("#content_container > h3").find("span[id=stateAnchor]").hide();
	PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
});
<%}%>
$(document).ready(function() { 
    jQuery.mobile.ajaxEnabled = false; 
}); 
function viewDetail(infomationId){
	window.location.href="/portal/website/<%=navCode%>/content.ptml/<%=menuItem.getCode()%>/"+infomationId
}
function goTop(){
	 $("body,html").animate({scrollTop:0},300);
}
</script>
<style type="text/css">
.head-item{
	height:45px;
	line-height:45px;
}
.foot-item{
	height:46px;
	line-height:46px;
	font-size:14px;
}
.ui-footer>div.ui-grid-a{
	background: #2c3e50;
	border-color: #2c3e50;
	text-shadow:none;
	color:#FFF;
}

.wrap {margin:0px;font:0.80em/1.6 sans-serif;font-weight:normal;font-family: serif;}

.ui-line-divider {
	height: 10px;
	line-height: 10px;
	background-color: #197fcf;
	position: relative;
}

.ul-list{padding:0px;margin:0px;}
.ul-list li {overflow:hidden;}
.ul-list li {border-top: 1px solid #f7f7f7;border-bottom: 1px solid #ccc;padding:5px 0;line-height:1.6;}
.ul-list li a {text-decoration:none;color:#000;font-weight:normal;}
.ul-list li a:visited,.ul-list li a:hover {text-decoration:none;color:#000;}

.ul-list p {text-align:right;clear:both;}
.ul-list li b {width:100%;display:block;font:18px/1.8 sans-serif;line-height:1.2;margin:5px 0;color:#000;font-weight:bold;}
.ul-list li span {float:left;margin:5px 5px 0 0;position:relative;overflow:hidden;height:80px;width:80px;}
.ul-list li span img {box-shadow:0 0 1px #ccc;width:80px;height:80px;}

#footer{background-color:#000;padding:10px 7px;text-shadow:none;color:#eee;font-size:13px;}
#footer span{color:#eee;height:20px;line-height:20px;}
#footer span a{text-decoration:none;color:#eee;font-weight:normal;}
#footer span a:visited,#footer span a:hover{text-decoration:none;color:#eee;font-weight:normal;}
#footer span img{vertical-align: middle;}

ul li#viewModeLi a{
	text-decoration: none;
}
</style>
</head>
<body>
<div data-role="page" id="page">
  <div data-role="header" style="background-color:#FFF" data-position="fixed" data-tap-toggle="false">   
  <h3 style="font-size:1em;">
  </h3>
  	<a href="/portal/website/<%=navCode%>/index.ptml" data-role="none"><img src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/images/logo.jpg" width="133" height="37"></a>
	<a href="/portal/casindex?Redirect" data-role="none"><i style="display:inline-block;font-size:23px;margin-right: 5px;" class="icon iconfont"></i></a>  
    <div data-role="navbar">
        <ul>
            <li><a href="/portal/website/<%=navCode%>/index.ptml" class="ui-btn<%if (menuItem.getCode().equals("index")){%> ui-state-persist ui-btn-active<%}%>">主页</a></li>
            <li><a href="/portal/website/<%=navCode%>/products.ptml" class="ui-btn<%if (menuItem.getCode().equals("products")){%> ui-state-persist ui-btn-active<%}%>">产品</a></li>
            <li><a href="/portal/website/<%=navCode%>/solutions.ptml" class="ui-btn<%if (menuItem.getCode().equals("solutions")){%> ui-state-persist ui-btn-active<%}%>">方案</a></li>
            <li><a href="/portal/website/<%=navCode%>/others.ptml" class="ui-btn<%if (menuItem.getCode().equals("others")){%> ui-state-persist ui-btn-active<%}%>">其他</a></li>
        </ul>
    </div>
  </div>
<div data-role="none" class="wrap" style="padding:0px 7px 0px 7px;background-color:#eee">
	<pt:mlayout/>   
	<ul data-role="none" class="ul-list">
	    <li style="border-bottom:0px solid #f7f7f7;height:1px;"></li>
	</ul>
</div>
<div id="footer" data-role="none">
	<span style="float:right;"><a href="javascript:goTop();"><img border="0" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/images/gotop.png"></a></span>
	<span style="float:left;"><script type="text/javascript">
		var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
		document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3Fb095c3e94483b44547a8f311f04871fd' 		type='text/javascript'%3E%3C/script%3E"));
		</script></span>
	<ul style="margin:0px;padding-left:20px;">
		<li style="margin:2px 10px;list-style:none;">版权所有© 沈阳数通畅联软件技术有限公司</li>
		<li style="margin:2px 10px;list-style:none;">辽ICP备14005586号</li>
		<li style="margin:2px 10px;list-style:none;">服务支持:service@agileai.com&nbsp;&nbsp;&nbsp;电话:024-22962011</li>
		<li style="margin:2px 10px;list-style:none;" id="viewModeLi">手机版&nbsp;|&nbsp;<a style="color: #FF0000; font-weight:lighter" href="/portal/website/01/index.ptml">电脑版</a></li>
    </ul>
</div>
</body>
</html>