<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt"%>
<%@ page import="com.agileai.hotweb.domain.core.Profile"%>
<%@ page import="com.agileai.portal.extend.forum.ForumPrivilegeHelper"%>
<%@ page import="com.agileai.hotweb.domain.core.User"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%
Profile profile = (Profile)request.getSession(false).getAttribute(Profile.PROFILE_KEY);
User user = (User)profile.getUser();
ForumPrivilegeHelper forumPrivilegeHelper = new ForumPrivilegeHelper(user);
String userName = forumPrivilegeHelper.getFuName();
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String menuBarJson = menuBar.getMenuBarJson(user,menuItem.getCode(),request);
String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
%>
<html lang="zh-CN" el:lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>开发社区</title>
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/themes/in/forum_default/css/homepage.css" type="text/css" >
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/page.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/layout.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css" type="text/css" />	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/dcmegamenu.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skins/red.css" type="text/css" />		
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/boxy.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/gt_grid.css" type="text/css" />	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/china/skinstyle.css"  type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/vista/skinstyle.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/mac/skinstyle.css" type="text/css" />
    <link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/decorators/material/Portlet.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/themes/in/forum_default/css/cs_box.css" id="colors" type="text/css"  />    	
	<%if ("edit".equals(windowMode)){%>
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
	<%}%>	
<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=menuItem.getCustomCssURL()%>" type="text/css" />
<%}%>			
    <style type="text/css">
	body, tbody, th, td{
		font-family:Verdana,Arial,Helvetica,sans-serif;
		font-size:12px;
		margin:0;
		padding:0;
	}
    </style>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery1.7.1.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.hoverIntent.minified.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.dcmegamenu.1.3.3.js" language="javascript"></script>       
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/FusionCharts_pc.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.iframe.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/draggabilly.pkgd.min.js" language="javascript"></script>            
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>
    <script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_msg_cn.js"></script>
	<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_grid_all.js"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/themes/in/forum_default/js/cs_box.js"></script>
<%if ("edit".equals(windowMode)){%>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>	
<%}%>	
<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
	<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>	
	<script type="text/javascript">
	$.ajaxSetup({cache:false});
	
	var __renderPortlets = new Map();	
	var _directConfigPortletBox;
	function directConfigDecoratorRequest(portletId){
		if (!_directConfigPortletBox){
			_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'380px'});
		}
		var url = '/portal/index?PagePortletConfig&portletId='+portletId;
		_directConfigPortletBox.sendRequest(url);	
	}
	$(function(){
		<%if ("edit".equals(windowMode)){%>
		$(".content > h3").hover(
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeIn('fast');
		  }, 
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeOut('fast');
		  }
		);
		<%}%>
		$(".content > h3").find("span[id=stateAnchor]").hide();
		PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
	}); 	
	</script>
</head>
<body class="bbs_desktop">
<div class="main">
  <div class="header_wrap">
    <div class="header"><a href="/portal/request/05/index.ptml" class="logo"></a>
      <ul class="header_menu">
        <li><a href="/portal/request/05/index.ptml">开发社区</a></li>
		<li><a href="/portal/website/01/index.ptml">官方网站</a></li>
		<li><a href="/portal/request/04/index.ptml">知识中心</a></li>
        <li><a href="/portal/casindex?Redirect">应用中心</a></li>
        <li><a href="/portal/request/05/myfocus.ptml">我的关注</a></li>
      </ul>
        <div class="nav-info" >
		<span id="logout" style="float:right;color: white;margin-top: 20px;width: auto;margin-right: 3px;">
		您好，<a href="javascript:doPopupWebPage('targetPage:05/forum-user-info.ptml,level:seniormanager,classify:head',{width:'450',height:'400'})" title="<%=userName%>" style="text-decoration:none;"><%=userName%>&nbsp;&nbsp;<span style="font: 12px arial;">个人设置</span></a>&nbsp;&nbsp;<input type="hidden" name="currentUserId" id="currentUserId" value="<%=user.getUserCode()%>">
		<%if ("guest".equals(user.getUserCode())){%>
		<a href="/portal/index?UserRegistry" class="button-link green" style="text-decoration: none;padding: 5px 15px;line-height:0px;font-size: 12px;">注册</a>
		<%} %>		
		<a href="javascript:void(0)" class="button-link orange" style="text-decoration: none;padding: 5px 15px;line-height:0px;font-size: 12px;" onClick="logout()">退出</a>
		</span>
      </div>      
    </div>
  </div>

  <div class="portletbody body_990 clearfix">
  <pt:layout />
  </div>
</div>
<div class="footer_wrap">
	<div class="footer">
		<div class="footer_menu">
		|&nbsp;<a href="/portal/request/05/index.ptml">开发社区</a>&nbsp;|&nbsp;
		<a href="/portal/website/01/index.ptml">官方网站</a>&nbsp;|&nbsp;
	   	<a href="/portal/casindex?Redirect">应用中心</a>&nbsp;|&nbsp;
	    <a href="/portal/request/04/index.ptml">知识中心</a>
	    </div>
		<div style="float:left; padding:0px 10px 10px 10px; color:#F9F5F5;"> 沈阳数通畅联软件技术有限公司 &copy; 版权所有 
		</div>
		<div style="float:right;" id="scroll-top-top"><a href="#"></a></div>
		<span style="float:right;">
		<div>
			<a href="http://www.miitbeian.gov.cn/" style="color:#F9F5F5;padding: 0px 10px 10px 10px;text-decoration: none;">辽ICP备14005586号</a>
		</div>
		</span>
	</div>
</div>


<div id='cs_box'>
<span class='cs_close' style="color:#FFFFFF; margin-top:0px">x</span>
<table width="140px" height="270" border="0" cellspacing="0">
  <tr>
    <td height="32" bgcolor="#C00000"><span id="box_title">欢迎咨询</span></td>
  </tr>
  <tr>
    <td>
    <span class="cs_btn">QQ群：299719834</span>
    <span class="cs_btn">电话：024-22962011</span>
    <span class="cs_btn">微信平台：数通畅联</span>
    <div class='cs_img'></div>    
   </td>
  </tr>
</table>
</div>
</body>
</html>
<script>
$(function(){
	cs_box.set({
		img_path : '<c:out value="${pageContext.request.contextPath}"/>/themes/in/forum_default/images/weixin.jpg',
		qq : '299719834'
	});
});	
</script>