<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="java.util.List"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%@ page import="com.agileai.portal.driver.model.Page"%>
<%@ page import="com.agileai.portal.extend.mobile.MobileHelper"%>
<%@ page import="com.agileai.portal.driver.mobile.AngularWidget"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MobileHelper mobileHelper = new MobileHelper((HttpServletRequest)request);
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
Page pageModel = (Page)request.getAttribute("_currentPage_");

String title = menuItem.getName();
boolean refresher = false;
boolean reloader = false;
if (menuItem.getProperties() != null && menuItem.getProperties().has("title")){
	  title = menuItem.getProperties().getString("title");
}
if (menuItem.getProperties() != null && menuItem.getProperties().has("refresher")){
	refresher = menuItem.getProperties().getBoolean("refresher");
}
if (menuItem.getProperties() != null && menuItem.getProperties().has("reloader")){
	reloader = menuItem.getProperties().getBoolean("reloader");
}
List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(pageModel);
%>
<ion-view view-title="<%=title%>" class="view-tabs-top" hide-tabs="<%=mobileHelper.getHideTabs(menuItem)%>">
  <%=mobileHelper.getPageTemplateContent(menuItem)%>
  <div class="bar-subheader tabs tabs-striped tabs-top tab-assertive tabs-color" ng-init="initTabs(<%=mobileHelper.getTabCodeJsArray(angularWidgetList)%>)">
<%
for (int i=0;i < angularWidgetList.size();i++){
	AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(i);
%>
	<a class="tab-item" ng-class="{active:activeTab=='<%=angularWidget.getCode()%>'}" ng-click="setActiveTab('<%=angularWidget.getCode()%>')" href=""><%=angularWidget.getName()%></a>
<%}%> 
  </div>
  <ion-content class="has-subheader">
  	<ion-spinner ng-if="onInitState" icon="android" class="aeai-spinner"></ion-spinner>
	<%if(refresher){ %>
	<ion-refresher pulling-text="下拉刷新.."  on-refresh="doRefresh()"></ion-refresher>
	<%}%>
<%
for (int i=0;i < angularWidgetList.size();i++){
	AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(i);
%>

	<div class="content" on-swipe-left="onSwipeLeft()" on-swipe-right="onSwipeRight()"  ng-show="activeTab=='<%=angularWidget.getCode()%>'">
	<div ui-view="<%=angularWidget.getCode()%>"></div>
	</div>
<%}%>
	<%if(reloader){ %>
    <ion-infinite-scroll ng-if="hasMore()" on-infinite="doReload()" distance="1%"></ion-infinite-scroll>
    <%}%>	
  </ion-content>	 
</ion-view>