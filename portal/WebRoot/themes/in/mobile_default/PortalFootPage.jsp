<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="java.util.List"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%@ page import="com.agileai.portal.driver.model.Page"%>
<%@ page import="com.agileai.portal.extend.mobile.MobileHelper"%>
<%@ page import="com.agileai.portal.driver.mobile.AngularWidget"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MobileHelper mobileHelper = new MobileHelper((HttpServletRequest)request);
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
Page pageModel = (Page)request.getAttribute("_currentPage_");

String title = menuItem.getName();
boolean refresher = false;
boolean reloader = false;
if (menuItem.getProperties() != null && menuItem.getProperties().has("title")){
	  title = menuItem.getProperties().getString("title");
}
if (menuItem.getProperties() != null && menuItem.getProperties().has("refresher")){
	refresher = menuItem.getProperties().getBoolean("refresher");
}
if (menuItem.getProperties() != null && menuItem.getProperties().has("reloader")){
	reloader = menuItem.getProperties().getBoolean("reloader");
}
%>
<ion-view view-title="<%=title%>" hide-tabs="<%=mobileHelper.getHideTabs(menuItem)%>">
  <%=mobileHelper.getPageTemplateContent(menuItem)%>  
  <ion-content class="light-bg has-footer">
  <ion-spinner ng-if="onInitState" icon="android" class="aeai-spinner"></ion-spinner>
  <%if(refresher){ %>
  <ion-refresher pulling-text="下拉刷新.."  on-refresh="doRefresh()"></ion-refresher>
  <%}%>  
<%
List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(pageModel);
for (int i=0;i < angularWidgetList.size()-1;i++){
	AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(i);
%>
	<div ui-view="<%=angularWidget.getCode()%>"></div>
<%}%>
  <%if(reloader){ %>
  <ion-infinite-scroll ng-if="hasMore()"  on-infinite="doReload()" distance="1%"></ion-infinite-scroll>
  <%}%>   
  </ion-content>
    <%AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(angularWidgetList.size()-1);%>
	<%=mobileHelper.getWidgetTemplateContent(menuItem, angularWidget)%>
</ion-view>