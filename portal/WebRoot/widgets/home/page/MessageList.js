angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	
	var url = '/portal/resource?MobileLatestMessagesDataProvider&actionType=findLatestMessages&userCode=admin';
	var promise = AppKit.getJsonApi(url);
	promise.success(function(rspJson){
		if(rspJson.sendMessages.length == 0){
			$scope.sendNoRecord = true;
		}else{
			$scope.sendNoRecord = false;
		}
		$scope.sendMessages = rspJson.sendMessages;
		if(rspJson.receiveMessages.length == 0){
			$scope.receiveNoRecord = true;
		}else{
			$scope.receiveNoRecord = false;
		}
		$scope.receiveMessages = rspJson.receiveMessages;
		$scope.sendMessages = rspJson.sendMessages;
	});
	
	$scope.fullHeight = {"min-height":(AppKit.getConfig("ScreenHeight")-90)+"px","background-color":"white"};
	
	$scope.openModal = function(id,text,category) {
		$scope.currentId = id;
		$scope.modalTitle = '消息正文';
		$scope.title = text;
		$scope.category = category;
		AppKit.createModal("msg-modal",$scope);
	}
});