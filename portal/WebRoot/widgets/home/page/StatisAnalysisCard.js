angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,$state,AppKit){
    $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
    $scope.series = ['项目一', '项目二'];
    $scope.data = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
    ];	
    
    $scope.showStats = function(){
    	$state.go("tab.static-list");
    }
});