angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.resetMsg();
	
	$scope.openUserList = function(){
		$scope.modalTitle = "用户列表";
		AppKit.createModal("user-list-modal",$scope);		
	}
	
	$scope.doSelectUser = function(){
		$scope.receiveList = [];
		for(var i=0;i < $scope.userList.length;i++){
			var item =  $scope.userList[i];
			if(item.checked){
				$scope.receiveList.push(item);
			}
		}
		
		$scope.msg.receiveCode = "";
		$scope.userNames = "";
		for(var i=0;i < $scope.receiveList.length;i++){
			var userInfo =  $scope.receiveList[i];
			if(i+1 != $scope.receiveList.length){
				$scope.msg.receiveCode = $scope.msg.receiveCode + userInfo.userCode + ",";
				$scope.userNames = $scope.userNames + userInfo.userName + ",";
			}else{
				$scope.msg.receiveCode = $scope.msg.receiveCode + userInfo.userCode;
				$scope.userNames = $scope.userNames + userInfo.userName;				
			}
			
		}
	}
	
	var url = '/portal/index?MobileUserPersonalInfoDataProvider&actionType=findReceiverNames';
	var promise = AppKit.getJsonApi(url);
	promise.success(function(rspJson){
		$scope.userList = rspJson.datas;
	});
});