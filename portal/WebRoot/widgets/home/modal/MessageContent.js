angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",['$sce','$scope','$compile','AppKit',function($sce,$scope,$compile,AppKit){
	$scope.content = "";
	
	if("receive" == $scope.category){
		var url = '/portal/resource?MobileLatestMessagesDataProvider&actionType=getReceiveLatestMessage&ID='+$scope.currentId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.title = rspJson.title;
			if(rspJson.content){
				$scope.content = rspJson.content;
			}else{
				$scope.content = "无正文内容";
			}
			$scope.createTime = rspJson.createTime;
			$scope.sender = rspJson.sender;
			$scope.receiver = rspJson.receiver;
		});
	}else if("send" == $scope.category){
		var url = '/portal/resource?MobileLatestMessagesDataProvider&actionType=getSendLatestMessage&ID='+$scope.currentId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.title = rspJson.title;
			if(rspJson.content){
				$scope.content = rspJson.content;
			}else{
				$scope.content = "无正文内容";
			}
			$scope.createTime = rspJson.createTime;
			$scope.sender = rspJson.sender;
			$scope.receiver = rspJson.receiver;
		});
	}
	
	$scope.openBbsContentModal = function(id,text){
		$scope.modalTitle = '问答详情';
		$scope.currentInfoId = id;
		AppKit.createModal("bbs-content-modal",$scope);	
	}
}]);