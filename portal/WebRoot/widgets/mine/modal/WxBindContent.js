angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.changeValideCode = function (){
		document.getElementById('valideCodeImg').src = "safecode?"+Math.floor(Math.random()*100000);
	}
	
	$scope.userloginInfo = {"userCode":"","userPwd":"","valideCode":""};
	$scope.doBind = function(){
		var url = "/portal/index?WxConfig&actionType=wxBinding";
		AppKit.postJsonApi(url,$scope.userloginInfo).then(function(response){
			if ("success" == response.data){
				AppKit.successPopup({"title":"绑定成功",callback:function(){
					$scope.logout();					
				}});
			}else{
				AppKit.errorPopup({"title":response.data});
			}
		});
	}
});