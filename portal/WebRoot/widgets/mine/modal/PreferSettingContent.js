angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit,$ionicPopup,$timeout){
    $scope.data = {color: ''};
	$scope.loadMobileTheme = function(){
		var url = '/portal/index?MobileDataProvider&actionType=getMobileTheme';
		AppKit.getJsonApi(url).success(function(jsonData){
			 $scope.colors = jsonData;
//			 $scope.data.color = '';
		});
	}
	$scope.loadMobileTheme();
	
	$scope.initMobileTheme = function(){
		var url = '/portal/index?MobileDataProvider&actionType=initMobileTheme';
		AppKit.getJsonApi(url).success(function(jsonData){
			 $scope.data.color = jsonData;
		});
	}
	$scope.initMobileTheme();
    
	$scope.doConfirm = function(){
		var color = $scope.data.color;
		var url = "/portal/index?MobileDataProvider&actionType=saveThemeColor";
		AppKit.postJsonApi(url,$scope.data).then(function(response){
			if ("success" == response.data){
				AppKit.hideMask();
				var popup = $ionicPopup.show({
				    title: '设置成功',
				    buttons: [{
			                 text: '关闭',type: 'button-positive',onTap: function(e) {
			                	 popup.close();
			                	 window.location.reload();
			                 }
			               }
				    	]			     
					});
				$timeout(function() {popup.close();window.location.reload();}, 5000);
			}else{
				AppKit.errorPopup();
			}
		});
	}
});