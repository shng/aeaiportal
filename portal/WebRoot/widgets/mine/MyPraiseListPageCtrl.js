angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.openKmModal = function(id,text){
		$scope.modalTitle = '知讯详情';
		$scope.currentInfoId = id;
		$scope.addKmClickNumber(id);
		AppKit.createModal("km-info-modal",$scope);	
	}
	
	$scope.loadKnowData = function(){
		var url = '/portal/index?MobileInformationsDataProvider&actionType=getKnowledgeInformation&ID='+$scope.currentInfoId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.id = rspJson.id;
			$scope.title = rspJson.title;
			$scope.createTime = rspJson.createTime;
			$scope.content = rspJson.content;
			$scope.isPraised = rspJson.isPraised;
			$scope.isFavorited = rspJson.isFavorited;
			$scope.praiseCount = rspJson.praiseCount;
			$scope.infoFavCount = rspJson.infoFavCount;
			$scope.infoReviewCount = rspJson.infoReviewCount;
			$scope.readCount = rspJson.readCount;
			if(rspJson.reviewList.length == 0){
				$scope.reviewNoRecord = true;
			}else{
				$scope.reviewNoRecord = false;
			}
			$scope.reviewList = rspJson.reviewList;
		});
	}
	
	$scope.loadPraiseDatas = function (){
		var url = '/portal/resource?MobileUserPersonalInfoDataProvider&actionType=findUserPrivateInfos';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.datas = rspJson.datas;
		});
	}
	$scope.loadPraiseDatas();
	
	$scope.doRefresh = function(){
		var url = '/portal/resource?MobileUserPersonalInfoDataProvider&actionType=findUserPrivateInfos';
		AppKit.getJsonApi(url).success(function(rspJson){
			$scope.datas = rspJson.datas;
		}).finally(function(){
			AppKit.refreshOver($scope);
		});
	}
	
	$scope.doPraise = function(){
		var url = "/portal/index?ContentProvider&actionType=mobileChangePraiseCount&infoId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.praiseInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				if($scope.isPraised == 'N'){
					AppKit.successPopup({"title":"点赞成功"});	
				}else{
					AppKit.successPopup({"title":"取消成功"});
				}
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.favoriteInfo = {"title":"","favType":"OTHERS"};
	$scope.doFavorite = function(){
		var url = "/portal/index?InfomationFavEdit&actionType=mobileAddToFavorite&infoId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.favoriteInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				if($scope.isFavorited == 'N'){
					AppKit.successPopup({"title":"收藏成功"});	
				}else{
					AppKit.successPopup({"title":"取消成功"});	
				}			
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.openCommentModal = function(){
		AppKit.createModal("km-comment-modal",$scope);
	}
	
	$scope.commentInfo = {"rewContent":""};
	$scope.doComment = function(){
		var url = "/portal/resource?ContentProvider&actionType=mobileAddNewReview&ID="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.commentInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				AppKit.successPopup({"title":"评论成功"});
			}else{
				AppKit.errorPopup({"title":"评论失败"});
			}
		});
	}
	
	$scope.isValidCommentInfo = function(){
		var commentInfo = $scope.commentInfo;
		if (commentInfo.rewContent && commentInfo.rewContent != ''){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.addKmClickNumber = function(id){
		$scope.info = {"id":id};
		var url = "/portal/index?ContentProvider&actionType=mobileIncreaseInfoReadCounts"
		AppKit.postJsonApi(url,$scope.info,{noMask:true}).then(function(response){
		});
	}
});