angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.backgroundClass = true;
	$scope.isInWeixin = false;
	$scope.wxOpenId = "-1";
	
	//$scope.user = {"id":"1111","name":"章鱼","sex":"男","code":"A0001","desc":"分享SOA平台软件，传递敏捷集成机制"};
	$scope.loadUserData = function(){
		var url = '/portal/resource?MobileUserPersonalInfoDataProvider&actionType=retrieveUserInfo';
		AppKit.getJsonApi(url).success(function(jsonData){
			$scope.user = jsonData;
			if (jsonData.wxOpenId){
				$scope.wxOpenId = jsonData.wxOpenId;				
			}
			$scope.isInWeixin = jsonData.isInWeixin;
		});
	}
	$scope.loadUserData();
	
	$scope.setupProfile = function(){
		$scope.modalTitle = "我的个人设置";
		AppKit.createModal("mine-setting-modal",$scope);			
	}
	
	$scope.saveProfile = function(){
		var url = "/portal/index?ForumUserInfoEdit&actionType=mobileSaveProfile";
		AppKit.postJsonApi(url,$scope.user).then(function(response){
			if ("success" == response.data){
				AppKit.successPopup();				
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.isValidUserInfo = function(){
		var user = $scope.user;
		if (user.name && user.name != '' && user.email && user.email != '' 
				&& user.sex && user.sex != '' && user.sex != 'selected'){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.loadData = function(){
		var url = '/portal/index?MobileInformationsDataProvider&actionType=getKnowledgeInformation&ID='+$scope.currentInfoId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.id = rspJson.id;
			$scope.title = rspJson.title;
			$scope.createTime = rspJson.createTime;
			$scope.content = rspJson.content;
			$scope.isPraised = rspJson.isPraised;
			$scope.isFavorited = rspJson.isFavorited;
			$scope.praiseCount = rspJson.praiseCount;
			$scope.infoFavCount = rspJson.infoFavCount;
			$scope.infoReviewCount = rspJson.infoReviewCount;
			$scope.reviewList = rspJson.reviewList;
		});
	}
	
	$scope.logout = function(){
		var service = AppKit.getConfig("AppEntry");
		window.location='/portal/index?Login&actionType=logout&service='+service;
	}
});