angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.openModal = function(){
		$scope.modalTitle = "我的信息";
		AppKit.createModal("mine-setting-modal",$scope);
	}
	
	$scope.openPreferModal = function(){
		$scope.modalTitle = "偏好设置";
		AppKit.createModal("prefer-setting-modal",$scope);
	}
});