angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,$ionicModal,AppKit){
	$scope.setupProfile = function(){
		$scope.modalTitle = "我的个人设置";
		$scope.currentInfoId = '1';
		AppKit.createModal("mine-setting-modal",$scope);			
	}
});