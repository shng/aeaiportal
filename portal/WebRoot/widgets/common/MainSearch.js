angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.search = function(){
		var url = '/portal/resource?MobileSearchDataProvider&actionType=searchDataList&searchWord='+$scope.searchWord;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.corpNews = rspJson.corpNews;
			$scope.industryNews = rspJson.industryNews;
			$scope.techTrends = rspJson.techTrends;
			$scope.platform = rspJson.platform;
			$scope.applicationplatform = rspJson.applicationplatform;
			$scope.supportplatform = rspJson.supportplatform;
		});
	}
	
	$scope.openViceModal = function(code,text){
		$scope.modalCode = code;
		$scope.modalTitle = text;
		if(code == 'corpNews'){
			AppKit.createModal("vice-km-search-modal",$scope);
		}
		if(code=='industryNews'){
			AppKit.createModal("vice-km-search-modal",$scope);
		}
		if(code=='techTrends'){
			AppKit.createModal("vice-km-search-modal",$scope);
		}
		if(code == 'platform'){
			AppKit.createModal("vice-bbs-search-modal",$scope);
		}
		if(code=='applicationplatform'){
			AppKit.createModal("vice-bbs-search-modal",$scope);
		}
		if(code=='supportplatform'){
			AppKit.createModal("vice-bbs-search-modal",$scope);
		}
	}
});