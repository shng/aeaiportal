angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.title = $scope.modalTitle;
	$scope.search = function(){
		var url = '/portal/resource?MobileSearchDataProvider&actionType=searchDataList&category='+$scope.modalCode+'&searchWord='+$scope.searchWord;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.datas = rspJson.datas;
		});
	}
	
//	$scope.openModal = function(id,text){
//		$scope.modalTitle = text;
//		$scope.currentInfoId = id;
//		if($scope.modalCode == 'corpNews'||$scope.modalCode=='industryNews'||$scope.modalCode=='techTrends'){
//			AppKit.createModal("km-info-modal",$scope);
//		}else if($scope.modalCode == 'platform'||$scope.modalCode=='applicationplatform'||$scope.modalCode=='supportplatform'){
//			AppKit.createModal("bbs-content-modal",$scope);
//		}
//	}
});