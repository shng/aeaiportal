<%@page import="com.agileai.common.KeyGenerator"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page language="java" import="com.agileai.portal.driver.model.PortletWindowConfig" %>
<%@ page language="java" import="com.agileai.util.StringUtil" %>
<%@ page language="java" import="com.agileai.portal.driver.AttributeKeys" %>
<%@ page language="java" import="com.agileai.portal.driver.model.Theme" %>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%
String windowMode = (String)request.getAttribute("WINDOW_MODE_KEY");
String portletId = (String)request.getAttribute("portlet");
Theme theme = (Theme)request.getAttribute(AttributeKeys.THEME_KEY);
PortletWindowConfig windowConfig = (PortletWindowConfig)request.getAttribute(portletId);
String height = windowConfig.getHeight();
if (!"auto".equals(height)){
	height = height+"px";
}
String bodyStyle = "style=\"";
String defStyle = windowConfig.getBodyStyle();
bodyStyle = bodyStyle + defStyle;
bodyStyle = bodyStyle+"\"";
boolean isCircular = windowConfig.getPropertyBoolValue(pageContext.getServletContext(),theme,"circular");
String color = windowConfig.getPropertyValue(pageContext.getServletContext(),theme,"colour");

String stateAnchorMarginTop = isCircular?"3":"5";
String modeDropDownMarginTo = isCircular?"0":"1";

String shortPortletId = KeyGenerator.instance().shortKey(portletId);
String portletMode = (String)request.getAttribute(portletId+"PortletMode");
%>
<pt:portlet portletId="${portlet}">
<div id="portlet<%=shortPortletId%>" class="sharp <%=color%>">
	<%if(isCircular){%><b class="b1"></b><b class="bb2"></b><b class="bb3"></b><b class="bb4"></b><%}else{%><b class="b9"></b><%}%>
    <div class="content" portletId="${portlet}" style="height:<%=height%>">
		<%
		Boolean minimized = (Boolean)pageContext.getAttribute("minimized");
		if (!minimized){
		%>        
        <div <%=bodyStyle%>  class="portletBox"><pt:render/></div><%}%>
     </div>
     <%if(isCircular){%><b class="b5"></b><b class="b6"></b><b class="b7"></b><b class="b8"></b><%}else{%><b class="b9"></b><%}%>
  </div>
<%if ("edit".equals(windowMode)){%>
<pt:buildModeURL/>
<%}%>
</pt:portlet>
<%if ("edit".equals(windowMode)){%>
<script>
var portletContextMenu<%=shortPortletId%> = new PortletContextMenu('contextMenu<%=shortPortletId%>','portlet<%=shortPortletId%>');
<%if ("edit".equals(portletMode)){%>
portletContextMenu<%=shortPortletId%>.bindItem("View","V",function(){self.location='<%=pageContext.getAttribute("ViewURL"+portletId)%>'});
<%}else if("view".equals(portletMode)){%>
portletContextMenu<%=shortPortletId%>.bindItem("Edit","E",function(){self.location='<%=pageContext.getAttribute("EditURL"+portletId)%>'});
<%}%>
portletContextMenu<%=shortPortletId%>.setup();
</script>
<%}%>