<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page language="java" import="com.agileai.portal.driver.model.PortletWindowConfig" %>
<%@ page language="java" import="com.agileai.util.StringUtil" %>
<%@ page language="java" import="com.agileai.portal.driver.AttributeKeys" %>
<%@ page language="java" import="com.agileai.portal.driver.model.Theme" %>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%
String windowMode = (String)request.getAttribute("WINDOW_MODE_KEY");
String porletId = (String)request.getAttribute("portlet");
Theme theme = (Theme)request.getAttribute(AttributeKeys.THEME_KEY);
PortletWindowConfig windowConfig =(PortletWindowConfig)request.getAttribute(porletId);
String portletMode = (String)request.getAttribute(porletId+"PortletMode");
String height = windowConfig.getHeight();
if (!"auto".equals(height)){
	height = height+"px";
}
if ("edit".equals(portletMode)){
	height = "auto";
}
String bodyStyle = "style=\"";
String defStyle = windowConfig.getBodyStyle();
bodyStyle = bodyStyle + defStyle;
bodyStyle = bodyStyle+"\"";
boolean isCircular = windowConfig.getPropertyBoolValue(pageContext.getServletContext(),theme,"circular");
String iconPath = windowConfig.getPropertyValue(pageContext.getServletContext(),theme,"iconPath");
boolean hasIcon = !StringUtil.isNullOrEmpty(iconPath);
String fullIconPath = null;
if (hasIcon){
	fullIconPath = iconPath;
}
String moreURL = windowConfig.getPropertyValue(pageContext.getServletContext(),theme,"moreURL");
boolean hasMoreURL = !StringUtil.isNullOrEmpty(moreURL);
String fullMoreURL = null;
if (hasMoreURL){
	if (moreURL.toLowerCase().startsWith("http://") || moreURL.toLowerCase().startsWith("https://")){
		fullMoreURL = moreURL;
	}else{
		fullMoreURL = request.getContextPath() + "/" + moreURL;
	}
}
String stateAnchorMarginTop = isCircular?"3":"5";
String modeDropDownMarginTo = isCircular?"0":"1";
String decorCode = windowConfig.getDecoratorCode(pageContext.getServletContext(),theme);
%>
<pt:portlet portletId="${portlet}">
<div class="sharp">
    <div class="content portlet-area-<%=decorCode%>" portletId="${portlet}" style="height:<%=height%>">
        <div class="portlet-header-<%=decorCode%>">
        <h3 class="portletTitle" <%if(!isCircular){%> style="line-height:27px;height:27px;"<%}%>><span class="portlet-collapse"  onclick="toggleCollapse('${portlet}');">
		<img src="/portal/images/decorator/collapsed_no.gif" title="收起/展开" alt="收起/展开">
		</span><%if(hasIcon){%><span><img style="float:left;margin-left:5px;<%if(isCircular){%>margin-top:2px;<%}else{%>margin-top:4px;<%}%>" src="<%=fullIconPath%>" /></span><%}%><%if("edit".equals(windowMode)){%><span id="modeDropDown" style="float:right;margin-right:5px;margin-top:<%=modeDropDownMarginTo%>px;"><pt:modeDropDown /></span><%}%><span id="stateAnchor" style="display:block;float:right;margin-right:5px;margin-top:<%=stateAnchorMarginTop%>px;"><pt:windowStateAnchor /></span><span class="portlet-title"><pt:title/></span><%if(hasMoreURL){%><span style="float:right;margin-right:5px;margin-top:<%=stateAnchorMarginTop%>px;"><a hideFocus="true" href="<%=fullMoreURL%>" target="_blank">更多</a></span><%}%></h3>
		</div>
		<%
		Boolean minimized = (Boolean)pageContext.getAttribute("minimized");
		if (!minimized){
		%>        
        <div <%=bodyStyle%> class="portletBox portlet-box-<%=decorCode%>"><pt:render/></div><%}%>
     </div>
  </div>
</pt:portlet>