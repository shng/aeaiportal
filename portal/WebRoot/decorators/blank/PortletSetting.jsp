<%@ page language="java" import="com.agileai.portal.driver.model.PortletWindowConfig" %>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%
String portletId = (String)request.getAttribute("portlet");
String columnIndex = (String)request.getAttribute("columnIndex");
String menuItemId = (String)request.getAttribute("menuItemId");
PortletWindowConfig windowConfig = (PortletWindowConfig)request.getAttribute(portletId);
String height = windowConfig.getHeight();
if (!"auto".equals(height)){
	height = height+"px";
}
String bodyStyle = "style=\"";
String defStyle = windowConfig.getBodyStyle();
bodyStyle = bodyStyle + defStyle;
bodyStyle = bodyStyle+"\"";
%>
<pt:portlet portletId="${portlet}">
<div class="sharp color3">
	<b class="b9"></b>
    <div class="content droppable" style="height:<%=height%>" portletId="<%=portletId%>" columnIndex="<%=columnIndex%>">
		<%
		Boolean minimized = (Boolean)pageContext.getAttribute("minimized");
		if (!minimized){
		%>        
        <div <%=bodyStyle%> class="portletBox"><pt:render/></div><%}%>
    </div>
    <b class="b9"></b>
</div>            
</pt:portlet>