package com.agileai.portal.controller.forum;

import java.util.Date;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;
import com.agileai.util.DateUtil;

public class ForumDisposalPostInfoHandler
        extends TreeAndContentManageEditHandler {
    public ForumDisposalPostInfoHandler() {
        super();
    }

    public ViewRenderer prepareDisplay(DataParam param) {
    	String fpfrId = param.get("FPM_ID");
    	User user = (User)this.getUser();
    	ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
		String userId = forumSectionInfoTreeManage.getFuIdRecord(user.getUserCode());
		this.setAttribute("FRR_TIME", DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL,new Date()));
		this.setAttribute("FRR_USER_NAME", user.getUserName());
		this.setAttribute("FRR_USERID", userId);
		this.setAttribute("fpfrId", fpfrId);
		this.setAttribute("fpmTitle", param.get("fpmTitle"));
		this.setAttribute("url", param.get("url"));
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    protected void processPageAttributes(DataParam param) {
    }
    
    @PageAction
	public ViewRenderer createForumReportRel(DataParam param) {
		String responseText = FAIL;
		try {
			String fpfrId = param.get("fpfrId");
			String frrResType = "POSTMESSAGE";
			String frrType = param.get("frrType");
			String frrUserid = param.get("FRR_USERID");
			String frrTime = param.get("FRR_TIME");
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			forumSectionInfoTreeManage.createForumReportRel(fpfrId,frrResType,frrType,frrUserid,frrTime);
			forumSectionInfoTreeManage.updateFpmReport(fpfrId, "HAS_CONFIRM_REPORT");
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
    
}
