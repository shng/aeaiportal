package com.agileai.portal.controller.forum;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;

public class ForumEditReplyPostInfoHandler
        extends TreeAndContentManageEditHandler {
    public ForumEditReplyPostInfoHandler() {
        super();
    }

    public ViewRenderer prepareDisplay(DataParam param) {
    	String friId = param.getString("FRI_ID");
    	ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
    	DataRow row = forumSectionInfoTreeManage.getCurrentForumRepliesInfoRecord(friId);
    	String friContent  = row.getString("FRI_CONTENT");
    	String mobileReplayContent  = row.getString("FRI_CONTENT_MOBILE");
		this.setAttribute(param, this.columnIdField);
		this.setAttribute("friId", friId);
		this.setAttribute("friContent", friContent);
		this.setAttribute("mobileReplayContent", mobileReplayContent);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    @PageAction
	public ViewRenderer updateReplyPostInfo(DataParam param){
		String responseText = FAIL;
		try {
			String friId = param.getString("friId");
			String friContent = param.getString("FRI_CONTENT");
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			forumSectionInfoTreeManage.updateReplyPostInfo(friId, friContent);
			responseText = friContent;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
    
    @PageAction
	public ViewRenderer updateMobileReplyPostInfo(DataParam param){
		String responseText = FAIL;
		try {
			String friId = param.getString("friId");
			String mobileFriContent = param.getString("FPM_CONTENT_MOBILE");
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			forumSectionInfoTreeManage.updateMobileReplyPostInfo(friId,mobileFriContent);
			responseText = mobileFriContent;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
    
    protected void processPageAttributes(DataParam param) {
    }

}
