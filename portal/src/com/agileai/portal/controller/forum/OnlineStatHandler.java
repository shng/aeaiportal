package com.agileai.portal.controller.forum;


import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.common.HttpClientHelper;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;

public class OnlineStatHandler extends BaseHandler {
	public ViewRenderer prepareDisplay(DataParam param){
		JSONObject jsonObject = new JSONObject();
		try {
			
			DataRow postTodayRow = getService().getPostTodayRecord(param);
			jsonObject.put("postToday", postTodayRow.get("STATIS"));
			
			DataRow postYesterdayRow = getService().getPostYesterdayRecord(param);
			jsonObject.put("postYesterday", postYesterdayRow.get("STATIS"));
			
			DataRow postStatisRow = getService().getPostStatisRecord(param);
			jsonObject.put("postStatis", postStatisRow.get("STATIS"));
			
			DataRow usersTotalRow = getService().getUsersTotalRecord(param);
			jsonObject.put("usersTotal", usersTotalRow.get("STATIS"));
			
			String serverName = this.dispatchServlet.getServletContext().getInitParameter("serverName");
			HttpClientHelper clientHelper = new HttpClientHelper();
			String onlineProfiles = clientHelper.retrieveGetReponseText(serverName + "/cas/OnlineProfiles");
			JSONObject onlineObject = new JSONObject(onlineProfiles);
			jsonObject.put("totalUserCount", onlineObject.get("totalSessionCount"));
			if(onlineObject != null && onlineObject.has("userTicketCountInfo")){
				JSONObject userTicketObject = (JSONObject) onlineObject.get("userTicketCountInfo");
				if(userTicketObject != null && userTicketObject.has("guest")){
					jsonObject.put("guest", userTicketObject.get("guest"));
				}else{
					jsonObject.put("guest", 0);
				}
			}
			jsonObject.put("online",onlineProfiles);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new AjaxRenderer(jsonObject.toString());
	}
	
	protected ForumSectionInfoTreeManage getService() {
		return (ForumSectionInfoTreeManage) this.lookupService("forumSectionInfoTreeManageService");
    }

}
