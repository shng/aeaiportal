package com.agileai.portal.controller.forum;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class ForumPostLocatorHandler extends BaseHandler{
	private static List<String> MobileModes = new ArrayList<String>();
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String redirectURL = null;
		String contentId = param.get("contentId");
		if (isFromeMobile()){
			redirectURL = "/portal/request/m3/index.ptml#/tab/bbs/"+contentId;
		}else{
			redirectURL = "/portal/request/05/forum-info-page.ptml?contentId="+contentId;
		}
		return new RedirectRenderer(redirectURL);
	}
	
	private  boolean isFromeMobile(){
		boolean result = false;
		if (MobileModes.isEmpty()){
			MobileModes.add(".*iPhone.*");
			MobileModes.add(".*Android.*");
			MobileModes.add(".*Safari.*Pre.*");
			MobileModes.add(".*Nokia.*AppleWebKit.*");
		}
		String userAgent = request.getHeader("User-Agent");
		if (userAgent != null){
			for (int i=0;i < MobileModes.size();i++){
				String expression = MobileModes.get(i);
				if (userAgent.matches(expression)) {
					result = true;
					break;
				}
			}
		}else{
			result = false;
		}
		return result;
	}
}
