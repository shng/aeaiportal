package com.agileai.portal.controller.forum;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.portal.bizmoduler.forum.UserIdentitysTreeSelect;

public class UserIdentitysTreeSelectHandler
        extends TreeSelectHandler {
    public UserIdentitysTreeSelectHandler() {
        super();
        this.isMuilSelect = true;
        this.checkRelParentNode = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryUserIdentitys(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "ROLE_ID",
                                                  "ROLE_NAME", "ROLE_PID");
        String rootId = param.get("rootColumnId");
        treeBuilder.setRootId(rootId);

        return treeBuilder;
    }
    protected UserIdentitysTreeSelect getService() {
        return (UserIdentitysTreeSelect) this.lookupService(UserIdentitysTreeSelect.class);
    }
}
