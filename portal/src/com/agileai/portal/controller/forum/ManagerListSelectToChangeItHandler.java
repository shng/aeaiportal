package com.agileai.portal.controller.forum;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.portal.bizmoduler.forum.ManagerListSelectToChangeIt;

public class ManagerListSelectToChangeItHandler
        extends PickFillModelHandler {
    public ManagerListSelectToChangeItHandler() {
        super();
        this.serviceId = buildServiceId(ManagerListSelectToChangeIt.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "fuName", "");
    }

    protected ManagerListSelectToChangeIt getService() {
        return (ManagerListSelectToChangeIt) this.lookupService(this.getServiceId());
    }
}
