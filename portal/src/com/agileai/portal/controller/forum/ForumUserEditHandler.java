package com.agileai.portal.controller.forum;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.UserLevel8BBSUserInfoManage;
import com.agileai.util.ListUtil;

public class ForumUserEditHandler
        extends TreeAndContentManageEditHandler {
    public ForumUserEditHandler() {
        super();
        this.serviceId = buildServiceId(UserLevel8BBSUserInfoManage.class);
        this.tabId = "ForumUser";
        this.columnIdField = "CODE_ID";
        this.contentIdField = "FU_ID";
    }
    
    protected void processPageAttributes(DataParam param) {
        setAttribute("FU_IDENTITY",
                     FormSelectFactory.create("USER_IDENTITY")
                                      .addSelectedValue(getOperaAttributeValue("FU_IDENTITY",
                                                                               "ORDINARY_USERS")));
        setAttribute("FU_LEVEL",
                     FormSelectFactory.create("USER_LEVEL")
                                      .addSelectedValue(getOperaAttributeValue("FU_LEVEL",
                                                                               "")));
        setAttribute("FU_STATE",
                     FormSelectFactory.create("SYS_VALID_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("FU_STATE",
                                                                               "1")));
        FormSelect formSelect = this.buildUserIdentitysSelect(param);
        this.setAttribute("fu_identitys", formSelect);
    }

    @SuppressWarnings("unchecked")
	@PageAction
	public ViewRenderer addUserIdentitysRef(DataParam param){
    	String responseText = "";
    	UserLevel8BBSUserInfoManage userLevel8BBSUserInfoManage = getService();
        String fuId = param.get("FU_ID");
        List<DataRow> records = userLevel8BBSUserInfoManage.findUserIdentityRelations(fuId);
		
		String userIdentityRefs = param.get("userIdentityRefs");
		String[] userIdentityArray = userIdentityRefs.split(",");
		List<String> userIdentitysList = ListUtil.arrayToList(userIdentityArray);
		
		List<DataRow> pureMappingRefs = this.getFilteredList(records, userIdentitysList, fuId);
		if (pureMappingRefs.size() == 0){
			responseText = "alreadyAdded";
		}else{
			userLevel8BBSUserInfoManage.addUserIdentitysRelation(fuId,pureMappingRefs);
			FormSelect formSelect = this.buildUserIdentitysSelect(param);
	        responseText = formSelect.getScriptSyntax("fu_identitys");			
		}
		return new AjaxRenderer(responseText);
	}
    public ViewRenderer doSaveAction(DataParam param){
		String rspText = SUCCESS;
		TreeAndContentManage service = this.getService();
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			String colIdField = service.getTabIdAndColFieldMapping().get(this.tabId);
			String columnId = param.get(this.columnIdField);
			param.put(colIdField,columnId);
			getService().createtContentRecord(tabId,param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updatetContentRecord(tabId,param);	
		}
		return new AjaxRenderer(rspText);
	}
    private List<DataRow> getFilteredList(List<DataRow> records,List<String> userIdentitysList,String fuId){
    	List<DataRow> result = new ArrayList<DataRow>();
    	if (records != null){
    		for (int i=0;i < userIdentitysList.size();i++){
    			String roleId = userIdentitysList.get(i);
    			if (!containRoleId(records,roleId)){
    				DataRow row = new DataRow("ROLE_ID",roleId,"FU_ID",fuId);
        			result.add(row);    				
    			}
    		}
    	}else{
    		for (int i=0;i < userIdentitysList.size();i++){
    			String roleId = userIdentitysList.get(i);
    			DataRow row = new DataRow("ROLE_ID",roleId,"FU_ID",fuId);
    			result.add(row);
    		}
    	}
    	return result;
    }
    
    private boolean containRoleId(List<DataRow> records,String roleId){
    	boolean result = false;
    	for (int i=0;i < records.size();i++){
    		DataRow row = records.get(i);
    		String tempRoleId = row.getString("ROLE_ID");
    		if (tempRoleId.equals(roleId)){
    			result = true;
    			break;
    		}
    	}
    	return result;
    }
    
    private FormSelect buildUserIdentitysSelect(DataParam param){
        String fuId = param.get("FU_ID");
        List<DataRow> records = getService().findUserIdentityRelations(fuId);
        if (records == null){
        	records = new ArrayList<DataRow>();	
        }
        FormSelect formSelect = new FormSelect();
        formSelect.setKeyColumnName("ROLE_ID");
        formSelect.setValueColumnName("ROLE_NAME");
        formSelect.putValues(records);
        formSelect.addHasBlankValue(false);
        return formSelect;
    }
    
    @PageAction
   	public ViewRenderer delUserIdentityRef(DataParam param){
       	String responseText = "";
   		String fuId = param.get("FU_ID");
   		String userIdentityRefs = param.get("fu_identitys");
   		UserLevel8BBSUserInfoManage userLevel8BBSUserInfoManage = getService();
   		userLevel8BBSUserInfoManage.deluserIdentitysRelation(fuId, userIdentityRefs);
        FormSelect formSelect = this.buildUserIdentitysSelect(param);
        responseText = formSelect.getScriptSyntax("fu_identitys");
       	return new AjaxRenderer(responseText);
   	}
    
    
    protected UserLevel8BBSUserInfoManage getService() {
        return (UserLevel8BBSUserInfoManage) this.lookupService(this.getServiceId());
    }
}
