package com.agileai.portal.controller.sso;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.sso.UserSSOService;

public class SSOAppConfListHandler extends BaseHandler{
	public SSOAppConfListHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param){
		UserSSOService userSSOService = this.lookupService(UserSSOService.class);
		String userId = getUserId();
		List<DataRow> records = userSSOService.findEditApplications(userId);
		this.setRsList(records);
		this.setAttribute("userId", userId);
		return new LocalRenderer(getPage());
	}
	@PageAction
	public ViewRenderer deleteSSO(DataParam param) {
		UserSSOService userSSOService = this.lookupService(UserSSOService.class);
		String ticketId = request.getParameter("ticketId");
		userSSOService.delUserApplication(ticketId);
		return prepareDisplay(param);
	}
	@PageAction
	public ViewRenderer moveUp(DataParam param) {
		UserSSOService userSSOService = this.lookupService(UserSSOService.class);
		String userId = getUserId();
		String ticketId = request.getParameter("ticketId");
		userSSOService.changeApplicationSort(userId,ticketId,true);
		return prepareDisplay(param);
	}
	@PageAction
	public ViewRenderer moveDown(DataParam param) {
		UserSSOService userSSOService = this.lookupService(UserSSOService.class);
		String userId = getUserId();
		String ticketId =  request.getParameter("ticketId");
		userSSOService.changeApplicationSort(userId,ticketId,false);
		return prepareDisplay(param);
	}
	private String getUserId(){
		User user = (User)this.getUser();
		return user.getUserCode();
	}
}