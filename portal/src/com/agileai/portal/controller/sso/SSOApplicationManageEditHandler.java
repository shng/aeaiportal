package com.agileai.portal.controller.sso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.MasterSubEditMainHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.sso.SSOApplicationManage;

public class SSOApplicationManageEditHandler extends MasterSubEditMainHandler{
	public SSOApplicationManageEditHandler(){
		super();
		this.listHandlerClass = SSOApplicationManageListHandler.class;
		this.serviceId = buildServiceId(SSOApplicationManage.class);
		this.baseTablePK = "APP_ID" ;
	}
	protected void processPageAttributes(DataParam param) {
		setAttribute("APP_TYPE",FormSelectFactory.create("SSO_APP_TYPE").addSelectedValue(getAttributeValue("APP_TYPE","BS")));
		setAttribute("APP_GROUP",FormSelectFactory.create("SSO_APP_GROUP").addSelectedValue(getAttributeValue("APP_GROUP")));
		setAttribute("APP_AUTH_TYPE",FormSelectFactory.create("SSO_AUTH_TYPE").addSelectedValue(getAttributeValue("APP_AUTH_TYPE","CAS")).addHasBlankValue(false));
		setAttribute("ENCRIPTION", FormSelectFactory.createSwitchFormSelect());
	}
	protected String[] getEntryEditFields(String currentSubTableId){
		List<String> temp = new ArrayList<String>();
		if ("param".equals(currentSubTableId)){
			temp.add("PARAM_ID");
			temp.add("APP_ID");
			temp.add("PARAM_CODE");
			temp.add("PARAM_NAME");
			temp.add("PARAM_SORT");
			temp.add("ENCRIPTION");
			temp.add("VALUE_EXPRESSION");
		}
		return temp.toArray(new String[]{});
	}
	
	public ViewRenderer doAddEntryRecordAction(DataParam param){
		String currentSubTableId = param.get("currentSubTableId");
		int currentRecordSize = param.getInt("currentRecordSize");
		List<DataRow> subRecords = new ArrayList<DataRow>();
		String[] entryEditFields = this.getEntryEditFields(currentSubTableId);
		for (int i=0;i < currentRecordSize;i++){
			DataRow row = new DataRow();
			for (int j=0; j < entryEditFields.length;j++){
				String field = entryEditFields[j];
				row.put(field,param.get(field+"_"+i));					
			}
			row.put("_state",param.get("state_"+i));
			subRecords.add(row);
		}
		String foreignKey = this.getEntryEditForeignKey(currentSubTableId);
		
		DataRow newRow  = new DataRow();
		newRow.put("ENCRIPTION","N");
		newRow.put("PARAM_SORT",String.valueOf(currentRecordSize+1));
		newRow.put("_state","insert");
		newRow.put(foreignKey,param.get(baseTablePK));
		subRecords.add(newRow);
		String subRecordsKey = currentSubTableId + "Records";
		this.setAttribute(subRecordsKey, subRecords);
		return prepareDisplay(param); 
	}
	
	protected String getEntryEditTablePK(String currentSubTableId){
		HashMap<String,String> primaryKeys = new HashMap<String,String>();
		primaryKeys.put("param", "PARAM_ID");
		return primaryKeys.get(currentSubTableId);
	}
	protected String getEntryEditForeignKey(String currentSubTableId) {
		HashMap<String,String> foreignKeys = new HashMap<String,String>();
		foreignKeys.put("param", "APP_ID");
		return foreignKeys.get(currentSubTableId);
	}
}