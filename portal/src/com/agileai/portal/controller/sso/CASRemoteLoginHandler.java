package com.agileai.portal.controller.sso;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.CryptionUtil;
import com.agileai.util.StringUtil;

public class CASRemoteLoginHandler extends BaseHandler{
	
	public ViewRenderer prepareDisplay(DataParam param){
		AppConfig appConfig = this.getAppConfig();
		
		String comeFromKey = null;
		String username = null;
		String password = null;
		String service = null;

		if (isWxRedirect()){
			comeFromKey = (String)this.getSessionAttribute("comeFromKey");
			username = (String)this.getSessionAttribute("username");
			password = (String)this.getSessionAttribute("password");
			service = (String)this.getSessionAttribute("service");
		}else{
			comeFromKey = param.get("comeFromKey",appConfig.getConfig("RemoteCASConfig", "comeFromKey"));
			username = param.get("username",appConfig.getConfig("RemoteCASConfig", "username"));
			password = param.get("password",appConfig.getConfig("RemoteCASConfig", "password"));
			service = param.get("service",appConfig.getConfig("RemoteCASConfig", "service"));
		}
		
		if (StringUtil.isNotNullNotEmpty(comeFromKey)
				&& StringUtil.isNotNullNotEmpty(username) && StringUtil.isNotNullNotEmpty(password)
				&&  StringUtil.isNotNullNotEmpty(service)){
			String secretKey = buildSecretKey(username);
			String encPassword = CryptionUtil.encryption(password, secretKey);
			this.setAttribute("comeFromKey", comeFromKey);
			this.setAttribute("username", username);
			this.setAttribute("password", encPassword);
			this.setAttribute("service", service);
		}else{
			this.setErrorMsg("comeFromKey or username or password or service cannt be empty !!");
		}
		return new LocalRenderer(this.getPage());
	}
	
	private boolean isWxRedirect(){
		boolean result = false;
		String isWxRedirect = (String)this.getSessionAttribute("isWxRedirect");
		if (StringUtil.isNotNullNotEmpty(isWxRedirect)){
			result = Boolean.parseBoolean(isWxRedirect);
		}
		return result;
	}
	
	private String buildSecretKey(String username){
		String result = null;
		if (username.length() >= 8){
			result = username.substring(0,8);
		}else{
			result = (username+"xxxxxxxx").substring(0,8);
		}
		return result;
	}
}