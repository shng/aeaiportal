package com.agileai.portal.controller.mobile;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.Resource;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.wcm.InfomationFavEditManage;
import com.agileai.portal.bizmoduler.wcm.InfomationManage;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class MobileInformationsDataProviderHandler extends SimpleHandler {

	public MobileInformationsDataProviderHandler(){
		super();
	}
	private static String Y="Y";
	private static String N="N";
	private static int pageSize = 20;
	private static String reload="reload";
	private static String refresh="refresh";
	private static String CorpInfoList="CorpInfoList";
	private static String IndustryInfoList="IndustryInfoList";
	private static String TechInfoList="TechInfoList";
	private static String TrainingInfoList="TrainingInfoList";
	@PageAction
	public ViewRenderer findKnowledgeInformations(DataParam param){
		String responseText = null;
		try {
			String tabCode = param.getString("tabCode"); 
			String queryPolicy = param.getString("queryPolicy"); 
			int tabPage = param.getInt("tabPage"); 
			JSONObject jsonObject = new JSONObject();
			InfomationManage infomationManage = this.lookupService(InfomationManage.class);
			HashMap<String,Integer> infoCounts = infomationManage.findInfoListCount();
			String colCode1 = "corp-news";
			String colCode2 = "industry-news";
			String colCode3 = "tech-trends";
			String colCode4 = "traincenter";
			HashMap<String,String> colCodes = new HashMap<String,String>();
			if(colCodes.size()==0){
				colCodes.put(CorpInfoList, colCode1);
				colCodes.put(IndustryInfoList, colCode2);
				colCodes.put(TechInfoList, colCode3);
				colCodes.put(TrainingInfoList, colCode4);
				colCodes.put(colCode1, "corpNews");
				colCodes.put(colCode2, "industryNews");
				colCodes.put(colCode3, "techTrends");
				colCodes.put(colCode4, "traincenter");
			}
			if(StringUtil.isNullOrEmpty(tabCode) ){
				//公司动态
				InfomationManage.PraiseCountCache.clear();
				List<DataRow> results1 = infomationManage.getRightNews(colCode1,0,pageSize);
				JSONArray jsonArray1 = new JSONArray();
				for(int i=0;i<results1.size();i++){
					DataRow row = results1.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("title", row.stringValue("INFO_TITLE"));
					jsonObject1.put("id", row.stringValue("INFO_ID"));
					jsonObject1.put("outLine", row.stringValue("INFO_OUTLINE"));
					jsonObject1.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
					String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
					jsonObject1.put("publishTime", publishTime.substring(5));
					jsonObject1.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
					jsonObject1.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
					jsonObject1.put("favNums", String.valueOf(row.getInt("favNums")));
					jsonArray1.put(jsonObject1);
				}
				jsonObject.put(colCodes.get(colCode1), jsonArray1);
				jsonObject.put("corpNewsCount", infoCounts.get(colCode1));
				//业界资讯
				List<DataRow> results2 = infomationManage.getRightNews(colCode2,0,pageSize);
				JSONArray jsonArray2 = new JSONArray();
				for(int i=0;i<results2.size();i++){
					DataRow row = results2.get(i);
					JSONObject jsonObject2 = new JSONObject();
					jsonObject2.put("title", row.stringValue("INFO_TITLE"));
					jsonObject2.put("id", row.stringValue("INFO_ID"));
					jsonObject2.put("outLine", row.stringValue("INFO_OUTLINE"));
					jsonObject2.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
					String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
					jsonObject2.put("publishTime", publishTime.substring(5));
					jsonObject2.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
					jsonObject2.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
					jsonObject2.put("favNums", String.valueOf(row.getInt("favNums")));
					jsonArray2.put(jsonObject2);
				}
				jsonObject.put(colCodes.get(colCode2), jsonArray2);
				jsonObject.put("industryNewsCount", infoCounts.get(colCode2));
				//技术集锦
				List<DataRow> results3 = infomationManage.getRightNews(colCode3,0,pageSize);
				JSONArray jsonArray3 = new JSONArray();
				for(int i=0;i<results3.size();i++){
					DataRow row = results3.get(i);
					JSONObject jsonObject3 = new JSONObject();
					jsonObject3.put("title", row.stringValue("INFO_TITLE"));
					jsonObject3.put("id", row.stringValue("INFO_ID"));
					jsonObject3.put("outLine", row.stringValue("INFO_OUTLINE"));
					jsonObject3.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
					String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
					jsonObject3.put("publishTime", publishTime.substring(5));
					jsonObject3.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
					jsonObject3.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
					jsonObject3.put("favNums", String.valueOf(row.getInt("favNums")));
					jsonArray3.put(jsonObject3);
				}
				jsonObject.put(colCodes.get(colCode3), jsonArray3);
				jsonObject.put("techTrendsCount", infoCounts.get(colCode3));
				//培训中心
				List<DataRow> tempRecords = infomationManage.getRightNews(colCode4,0,pageSize);
				List<DataRow> results4 = this.getAuthedRecords(tempRecords);
				JSONArray jsonArray4 = new JSONArray();
				for(int i=0;i<results4.size();i++){
					DataRow row = results4.get(i);
					JSONObject jsonObject4 = new JSONObject();
					jsonObject4.put("title", row.stringValue("INFO_TITLE"));
					jsonObject4.put("id", row.stringValue("INFO_ID"));
					jsonObject4.put("outLine", row.stringValue("INFO_OUTLINE"));
					jsonObject4.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
					String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
					jsonObject4.put("publishTime", publishTime.substring(5));
					jsonObject4.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
					jsonObject4.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
					jsonObject4.put("favNums", String.valueOf(row.getInt("favNums")));
					jsonArray4.put(jsonObject4);
				}
				jsonObject.put(colCodes.get(colCode4), jsonArray4);
				jsonObject.put("traincenterCount", infoCounts.get(colCode4));
			}else{
				if(reload.equals(queryPolicy)){
					String colCode = colCodes.get(tabCode);
					int tabPageIndex = tabPage*pageSize;
					List<DataRow> tempRecords = infomationManage.getRightNews(colCode,tabPageIndex,pageSize);
					List<DataRow> results = this.getAuthedRecords(tempRecords);
					JSONArray jsonArray = new JSONArray();
					for(int i=0;i<results.size();i++){
						DataRow row = results.get(i);
						JSONObject json = new JSONObject();
						json.put("title", row.stringValue("INFO_TITLE"));
						json.put("id", row.stringValue("INFO_ID"));
						json.put("outLine", row.stringValue("INFO_OUTLINE"));
						json.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
						String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
						json.put("publishTime", publishTime.substring(5));
						json.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
						json.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
						json.put("favNums", String.valueOf(row.getInt("favNums")));
						jsonArray.put(json);
					}
					jsonObject.put(colCodes.get(colCode), jsonArray);
				}else if(refresh.equals(queryPolicy)){
					String colCode = colCodes.get(tabCode);
					int tabPageSize = pageSize;
					if(tabPage != 0){
						tabPageSize = tabPage*pageSize;
					}
					List<DataRow> tempRecords = infomationManage.getRightNews(colCode,0,tabPageSize);
					List<DataRow> results = this.getAuthedRecords(tempRecords);
					JSONArray jsonArray = new JSONArray();
					for(int i=0;i<results.size();i++){
						DataRow row = results.get(i);
						JSONObject json = new JSONObject();
						json.put("title", row.stringValue("INFO_TITLE"));
						json.put("id", row.stringValue("INFO_ID"));
						json.put("outLine", row.stringValue("INFO_OUTLINE"));
						json.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
						String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
						json.put("publishTime", publishTime.substring(5));
						json.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
						json.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
						json.put("favNums", String.valueOf(row.getInt("favNums")));
						jsonArray.put(json);
					}
					jsonObject.put(colCodes.get(colCode), jsonArray);
					jsonObject.put("tabCount", infoCounts.get(colCode));
				}
			}
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	private List<DataRow> getAuthedRecords(List<DataRow> tempRecords){
		List<DataRow> result = new ArrayList<DataRow>();
		Profile profile = this.getProfile();
		if (profile != null){
			User user = (User)profile.getUser();
			for (int i=0;i < tempRecords.size();i++){
				DataRow row = tempRecords.get(i);
				String authType = row.stringValue("INFO_AUTH_TYPE");
				if (InfomationManage.AuthType.Private.equalsIgnoreCase(authType)){
					String infomatonId = row.stringValue("INFO_ID");
					if (user.containResouce(Resource.Type.Infomation, infomatonId)
							|| "user".equals(user.getUserCode()) || "admin".equals(user.getUserCode())){
						result.add(row);
					}
				}else{
					result.add(row);
				}
			}
		}else{
			for (int i=0;i < tempRecords.size();i++){
				DataRow row = tempRecords.get(i);
				String authType = row.stringValue("INFO_AUTH_TYPE");
				if (InfomationManage.AuthType.Public.equalsIgnoreCase(authType)){
					result.add(row);
				}
			}
		}
		return result;
	}
	
	@SuppressWarnings("static-access")
	@PageAction
	public ViewRenderer getKnowledgeInformation(DataParam param){
		String responseText = null;
		try {
			String infoId = param.get("ID");
			User user = (User) this.getUser();
			String userCode = user.getUserCode();
			InfomationManage infomationManage = this.lookupService(InfomationManage.class);
			InfomationManage.ContentRowCache.clear();
			DataRow record = infomationManage.getContentRecord(infoId);
			String id = record.getString("INFO_ID");
			String title = record.getString("INFO_TITLE");
			String content = record.getString("INFO_MOBILE_CONTENT");
			Date date = (Date) record.get("INFO_CREATE_TIME");
			int readCount = record.getInt("INFO_READ_COUNT");
			String createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL,date);
			List<DataRow> records = infomationManage.findPraiseRecords(infoId, userCode);
			InfomationFavEditManage infomationFavEditManage = this.lookupService(InfomationFavEditManage.class);
			param.put("userCode", userCode,"infoId", infoId);
			List<DataRow> favRecords = infomationFavEditManage.findInfoRecords(param);
			String isPraised = N;
			if(records.size()>0){
				isPraised = Y;
			}
			String isFavorited = N;
			if(favRecords.size()>0){
				isFavorited = Y;
			}
			param.put("infomationId", infoId);
			int praiseCount = infomationManage.findPraiseCount(param);
			infomationManage.PraiseCountCache.remove(infoId);
			int infoFavCount = infomationFavEditManage.findInfoFavCount(infoId);
			int infoReviewCount = infomationManage.findInfoReviewCount(infoId);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", id);
			jsonObject.put("title", title);
			jsonObject.put("createTime", createTime);
			jsonObject.put("isPraised", isPraised);
			jsonObject.put("isFavorited", isFavorited);
			jsonObject.put("content", content);
			jsonObject.put("readCount",readCount);
			jsonObject.put("praiseCount",praiseCount);
			jsonObject.put("infoFavCount",infoFavCount);
			jsonObject.put("infoReviewCount",infoReviewCount);
			//评论
			List<DataRow> reviewList = infomationManage.findInfoReviweList(infoId);
			JSONArray jsonArray = new JSONArray();
			for (int i = 0; i < reviewList.size(); i++) {
				DataRow reviewRow = reviewList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("rewId", reviewRow.getString("REW_ID"));
				jsonObject1.put("rewContent", reviewRow.getString("REW_CONTENT"));
				jsonObject1.put("rewTime", DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, (Date)reviewRow.get("REW_TIME")));
				jsonObject1.put("rewName", reviewRow.getString("USER_NAME"));
				jsonObject1.put("isAnonymity", reviewRow.getString("REW_ANONYMITY"));
				jsonObject1.put("rewIp", reviewRow.getString("REW_IPADDRESS"));
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("reviewList", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
}
