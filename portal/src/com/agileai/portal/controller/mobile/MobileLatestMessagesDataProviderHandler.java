package com.agileai.portal.controller.mobile;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.core.Group;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.wcm.InfomationManage;
import com.agileai.portal.extend.RegexHelper;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class MobileLatestMessagesDataProviderHandler extends SimpleHandler {
	private static String ID = "0000-0000-0000-0000-0000-0000-0000-0";
	public MobileLatestMessagesDataProviderHandler(){
		super();
	}
	
	@PageAction
	public ViewRenderer findLatestMessages(DataParam param){
		String responseText = null;
		try {
			InfomationManage infomationManage = this.lookupService(InfomationManage.class);
			JSONObject jsonObject = new JSONObject();
			User user = (User) this.getUser();
			//判断是否为注册用户
			List<Group> groups = user.getGroupList();
			String isNewUser = null;
			for(int i=0;i<groups.size();i++){
				Group group = groups.get(i);
				String groupCode = group.getGroupCode();
				if("NewUsers".equals(groupCode)){
					isNewUser="isNweUsers";
					break;
				}
			}
			if("isNweUsers".equals(isNewUser)){
				//构造显示数据
				JSONArray jsonArray1 = new JSONArray();
				JSONObject jsonoObject1 = new JSONObject();
				jsonoObject1.put("id", ID);
				jsonoObject1.put("title", "我来到数通畅联了！");
				String time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING,new Date());
				jsonoObject1.put("createTime", time.substring(5));
				jsonoObject1.put("receiver", "管理员");
				jsonArray1.put(jsonoObject1);
				jsonObject.put("sendMessages", jsonArray1);
				
				JSONArray jsonArray2 = new JSONArray();
				JSONObject jsonoObject2 = new JSONObject();
				jsonoObject2.put("id", ID);
				jsonoObject2.put("title", "欢迎来到数通畅联！");
				jsonoObject2.put("createTime", time.substring(5));
				jsonoObject2.put("receiver", "管理员");
				jsonArray2.put(jsonoObject2);
				jsonObject.put("receiveMessages", jsonArray2);
			}else{
				String userCode = user.getUserCode();
				param.put("userCode", userCode);
				String card = param.get("type");
				if(StringUtil.isNotNullNotEmpty(card)){
					param.put("num", 5);
				}
				List<DataRow> sendRecords = infomationManage.findSendMessagesByUserCode(param);
				JSONArray jsonArray1 = new JSONArray();
				for (int i = 0; i < sendRecords.size(); i++) {
					DataRow row = sendRecords.get(i);
					JSONObject jsonoObject1 = new JSONObject();
					jsonoObject1.put("id", row.getString("SEND_ID"));
					jsonoObject1.put("title", row.getString("MSG_THEME"));
					String time = row.getString("MSG_CREATE_TIME");
					if(StringUtil.isNotNullNotEmpty(time)){
						time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING,DateUtil.getDate(time));
						time = time.substring(5);
					}
					jsonoObject1.put("createTime", time);
					jsonoObject1.put("receiver", row.getString("MSG_RECEIVEOR_NAME"));
					jsonArray1.put(jsonoObject1);
				}
				jsonObject.put("sendMessages", jsonArray1);
				List<DataRow> receiveRecords = infomationManage.findReceiveMessagesByUserCode(param);	
				JSONArray jsonArray2 = new JSONArray();
				for (int i = 0; i < receiveRecords.size(); i++) {
					DataRow row = receiveRecords.get(i);
					JSONObject jsonoObject2 = new JSONObject();
					jsonoObject2.put("id", row.getString("RECEIVE_ID"));
					jsonoObject2.put("title", row.getString("MSG_THEME"));
					String time = row.getString("MSG_CREATE_TIME");
					if(StringUtil.isNotNullNotEmpty(time)){
						time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING,DateUtil.getDate(time));
						time = time.substring(5);
					}
					jsonoObject2.put("createTime", time);
					jsonoObject2.put("sender", row.getString("MSG_SEND_NAME"));
					jsonArray2.put(jsonoObject2);
				}
				jsonObject.put("receiveMessages", jsonArray2);
			}
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getSendLatestMessage(DataParam param){
		String responseText = null;
		try {
			String sendId = param.getString("ID");
			JSONObject jsonObject = new JSONObject();
			User user = (User) this.getUser();
			if(ID.equals(sendId)){
				jsonObject.put("id", ID);
				jsonObject.put("title", "我来到数通畅联了！");
				jsonObject.put("content", "我来到数通畅联了！");
				String time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING,new Date());
				jsonObject.put("createTime", time.substring(5));
				jsonObject.put("sender",user.getUserName());
				jsonObject.put("receiver", "管理员");
			}else{
				InfomationManage infomationManage = this.lookupService(InfomationManage.class);
				DataRow record = infomationManage.getSendLatestMessage(sendId);
				jsonObject.put("id", record.getString("MSG_ID"));
				jsonObject.put("title", record.getString("MSG_THEME"));
				String content = record.getString("MSG_NOTICE_CONTENT");
				String regxp = "<a href=\"/portal/index\\?ForumPostLocator&contentId=.*? target=\"_blank\">";
				content = this.replaceExt(content, regxp);
				jsonObject.put("content", content);
				String time = record.getString("MSG_CREATE_TIME");
				if(StringUtil.isNotNullNotEmpty(time)){
					time = time.substring(5);
				}
				jsonObject.put("createTime", time);
				jsonObject.put("sender", record.getString("MSG_SEND_NAME"));
				jsonObject.put("receiver", record.getString("MSG_RECEIVEOR_NAME"));
			}
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getReceiveLatestMessage(DataParam param){
		String responseText = null;
		try {
			String receiveId = param.getString("ID");
			JSONObject jsonObject = new JSONObject();
			User user = (User) this.getUser();
			if(ID.equals(receiveId)){
				jsonObject.put("id", ID);
				jsonObject.put("title", "欢迎来到数通畅联！");
				jsonObject.put("content", "欢迎来到数通畅联！");
				String time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING,new Date());
				jsonObject.put("createTime", time.substring(5));
				jsonObject.put("sender","管理员");
				jsonObject.put("receiver", user.getUserName());
			}else{
				InfomationManage infomationManage = this.lookupService(InfomationManage.class);
				DataRow record = infomationManage.getReceiveLatestMessage(receiveId);
				jsonObject.put("id", record.getString("MSG_ID"));
				jsonObject.put("title", record.getString("MSG_THEME"));
				String content = record.getString("MSG_NOTICE_CONTENT");
				String regxp = "<a href='/portal/index\\?ForumPostLocator&contentId=.*? target='_blank'>";
				content = this.replaceExt(content, regxp);
				jsonObject.put("content", content);
				String time = record.getString("MSG_CREATE_TIME");
				if(StringUtil.isNotNullNotEmpty(time)){
					time = time.substring(5);
				}
				jsonObject.put("createTime", time);
				jsonObject.put("sender", record.getString("MSG_SEND_NAME"));
				jsonObject.put("receiver", record.getString("MSG_RECEIVEOR_NAME"));
			}
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	private String replaceExt(String content,String regxp){
		 String result = content;
		 Pattern pat = Pattern.compile(regxp);  
		 Matcher matcher = pat.matcher(content);  
		 RegexHelper regexHelper = new RegexHelper();
		 while (matcher.find()) { 
		     String temp = content.substring(matcher.start(),matcher.end());
		     String contentSegment = regexHelper.extractString(temp, "&contentId=.*?'");
		 
		 if (StringUtil.isNotNullNotEmpty(contentSegment)){
			 String contentId = contentSegment.substring(11,contentSegment.length()-1);
		     String replacement = "<a ng-click=\"openBbsModal('"+contentId+"','问答详情')\">";
			     result = content.replace(temp,replacement);	
			     break;
		     }
		 }     
		 System.out.println(result);
		 return result;
	}
}
