package com.agileai.portal.controller.mobile;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.mobile.AngularWidgetManage;

public class AngularWidgetManageListHandler
        extends StandardListHandler {
    public AngularWidgetManageListHandler() {
        super();
        this.editHandlerClazz = AngularWidgetManageEditHandler.class;
        this.serviceId = buildServiceId(AngularWidgetManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("awGroup",
                FormSelectFactory.create("AW_GROUP")
                                 .addSelectedValue(param.get("awGroup")));
    }
    
    @PageAction
    public ViewRenderer reloadAll(DataParam param){
    	String responseText = FAIL;
    	try {
    		MobileDataProviderHandler.removeCaches();
    		responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	
    	return new AjaxRenderer(responseText);
    }

    protected void initParameters(DataParam param) {
    	initParamItem(param, "awId", "");
        initParamItem(param, "awCode", "");
        initParamItem(param, "awName", "");
        initParamItem(param, "awGroup", "");
        initMappingItem("AW_GROUP",
                FormSelectFactory.create("AW_GROUP").getContent());
    }

    protected AngularWidgetManage getService() {
        return (AngularWidgetManage) this.lookupService(this.getServiceId());
    }
}
