package com.agileai.portal.controller.mobile;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;
import com.agileai.portal.extend.RegexHelper;
import com.agileai.portal.extend.forum.ForumPrivilegeHelper;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class MobileForumDataProviderHandler extends SimpleHandler {
	private static String GUEST = "GUEST";
	private static int pageSize = 20;
	private static String reload="reload";
	private static String refresh="refresh";
	private static String HotNewPosts="HotNewPosts";
	private static String IntegratePlatform="IntegratePlatform";
	private static String BracePlatform="BracePlatform";
	private static String ApplicationPlatform="ApplicationPlatform";
	public MobileForumDataProviderHandler(){
		super();
	}
	
	@PageAction
	public ViewRenderer findForumInformations(DataParam param){
		String responseText = null;
		try {
			String tabCode = param.getString("tabCode"); 
			String queryPolicy = param.getString("queryPolicy"); 
			int tabPage = param.getInt("tabPage"); 
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			
			HashMap<String,Integer> postCounts = forumSectionInfoTreeManage.findPostListCount();
			String bbsCode1 = "hotnew-post";
			String bbsCode2 = "platform";
			String bbsCode3 = "support-platform";
			String bbsCode4 = "application-platform";
			HashMap<String,String> bbsCodes = new HashMap<String,String>();
			if(bbsCodes.size()==0){
				bbsCodes.put(HotNewPosts, bbsCode1);
				bbsCodes.put(IntegratePlatform, bbsCode2);
				bbsCodes.put(BracePlatform, bbsCode3);
				bbsCodes.put(ApplicationPlatform, bbsCode4);
				bbsCodes.put(bbsCode1, "hotnewPosts");
				bbsCodes.put(bbsCode2, "integratePlatform");
				bbsCodes.put(bbsCode3, "bracePlatform");
				bbsCodes.put(bbsCode4, "applicationPlatform");
			}
			JSONObject jsonObject1 = new JSONObject();
			if(StringUtil.isNullOrEmpty(tabCode) ){
				//热点帖子
				List<DataRow> results1 = forumSectionInfoTreeManage.findHotPostRecords(0,pageSize);
				JSONArray jsonArray1 = new JSONArray();
				for(int i=0;i<results1.size();i++){
					DataRow row = results1.get(i);
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("title", row.stringValue("FPM_TITLE"));
					jsonObject.put("id", row.stringValue("FPM_ID"));
					jsonObject.put("name", row.stringValue("FU_NAME"));
					jsonObject.put("clickNum", row.stringValue("FPM_CLICK_NUMBER"));
					String createTime = row.stringValue("FPM_CREATE_TIME");
					createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
					createTime = createTime.substring(5, 16).replaceAll("/", "-");
					jsonObject.put("createTime", createTime);
					jsonObject.put("count", row.stringValue("FRI_ID_COUNT"));
					String type = "";
					if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "essence";
					}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "key";
					}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "ordinary";
					}
					jsonObject.put("category", row.stringValue("FCI_CODE"));
					jsonObject.put("type", type);
					String attentionCount = row.stringValue("FF_ID_COUNT");
					jsonObject.put("attentionCount", attentionCount);//关注数
					jsonArray1.put(jsonObject);
				}
				
				jsonObject1.put(bbsCodes.get(bbsCode1), jsonArray1);
				jsonObject1.put("hotnewPostsCount", postCounts.get(bbsCode1));
				//集成平台
				List<DataRow> results2 = forumSectionInfoTreeManage.findPostMessageRecords(bbsCode2,0,pageSize);
				JSONArray jsonArray2 = new JSONArray();
				for(int i=0;i<results2.size();i++){
					DataRow row = results2.get(i);
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("title", row.stringValue("FPM_TITLE"));
					jsonObject.put("id", row.stringValue("FPM_ID"));
					jsonObject.put("name", row.stringValue("FU_NAME"));
					jsonObject.put("clickNum", row.stringValue("FPM_CLICK_NUMBER"));
					String createTime = row.stringValue("FPM_CREATE_TIME");
					createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
					createTime = createTime.substring(5, 16).replaceAll("/", "-");
					jsonObject.put("createTime", createTime);
					jsonObject.put("category", row.stringValue("FCI_CODE"));
					jsonObject.put("top", row.stringValue("FPM_SECTION_TOP"));
					jsonObject.put("count", row.stringValue("FRI_ID_COUNT"));
					String type = "";
					if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "essence";
					}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "key";
					}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "ordinary";
					}
					jsonObject.put("type", type);
					String attentionCount = row.stringValue("FF_ID_COUNT");
					jsonObject.put("attentionCount", attentionCount);//关注数
					jsonArray2.put(jsonObject);
				}
				jsonObject1.put(bbsCodes.get(bbsCode2), jsonArray2);
				jsonObject1.put("integratePlatformCount", postCounts.get(bbsCode2));
				//支撑平台
				List<DataRow> results3 = forumSectionInfoTreeManage.findPostMessageRecords(bbsCode3,0,pageSize);
				JSONArray jsonArray3 = new JSONArray();
				for(int i=0;i<results3.size();i++){
					DataRow row = results3.get(i);
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("title", row.stringValue("FPM_TITLE"));
					jsonObject.put("id", row.stringValue("FPM_ID"));
					jsonObject.put("name", row.stringValue("FU_NAME"));
					jsonObject.put("clickNum", row.stringValue("FPM_CLICK_NUMBER"));
					String createTime = row.stringValue("FPM_CREATE_TIME");
					createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
					createTime = createTime.substring(5, 16).replaceAll("/", "-");
					jsonObject.put("createTime", createTime);
					jsonObject.put("category", row.stringValue("FCI_CODE"));
					jsonObject.put("top", row.stringValue("FPM_SECTION_TOP"));
					jsonObject.put("count", row.stringValue("FRI_ID_COUNT"));
					String type = "";
					if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "essence";
					}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "key";
					}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "ordinary";
					}
					jsonObject.put("type", type);
					String attentionCount = row.stringValue("FF_ID_COUNT");
					jsonObject.put("attentionCount", attentionCount);//关注数
					jsonArray3.put(jsonObject);
				}
				jsonObject1.put(bbsCodes.get(bbsCode3), jsonArray3);
				jsonObject1.put("bracePlatformCount", postCounts.get(bbsCode3));
				//应用平台
				List<DataRow> results4 = forumSectionInfoTreeManage.findPostMessageRecords(bbsCode4,0,pageSize);
				JSONArray jsonArray4 = new JSONArray();
				for(int i=0;i<results4.size();i++){
					DataRow row = results4.get(i);
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("title", row.stringValue("FPM_TITLE"));
					jsonObject.put("id", row.stringValue("FPM_ID"));
					jsonObject.put("name", row.stringValue("FU_NAME"));
					jsonObject.put("clickNum", row.stringValue("FPM_CLICK_NUMBER"));
					String createTime = row.stringValue("FPM_CREATE_TIME");
					createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
					createTime = createTime.substring(5, 16).replaceAll("/", "-");
					jsonObject.put("createTime", createTime);
					jsonObject.put("category", row.stringValue("FCI_CODE"));
					jsonObject.put("top", row.stringValue("FPM_SECTION_TOP"));
					jsonObject.put("count", row.stringValue("FRI_ID_COUNT"));
					String type = "";
					if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "essence";
					}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "key";
					}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "ordinary";
					}
					jsonObject.put("type", type);
					String attentionCount = row.stringValue("FF_ID_COUNT");
					jsonObject.put("attentionCount", attentionCount);//关注数
					jsonArray4.put(jsonObject);
				}
				jsonObject1.put(bbsCodes.get(bbsCode4), jsonArray4);
				jsonObject1.put("applicationPlatformCount", postCounts.get(bbsCode4));
				
			}else{
				if(reload.equals(queryPolicy)){
					String bbsCode = bbsCodes.get(tabCode);
					int tabPageIndex = tabPage*pageSize;
					List<DataRow> results1 = new ArrayList<DataRow>();
					if("HotNewPosts".equals(bbsCode)){
						results1 = forumSectionInfoTreeManage.findHotPostRecords(tabPageIndex,pageSize);
					}else{
						results1 = forumSectionInfoTreeManage.findPostMessageRecords(bbsCode,tabPageIndex,pageSize);
					}
					JSONArray jsonArray1 = new JSONArray();
					for(int i=0;i<results1.size();i++){
						DataRow row = results1.get(i);
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("title", row.stringValue("FPM_TITLE"));
						jsonObject.put("id", row.stringValue("FPM_ID"));
						jsonObject.put("name", row.stringValue("FU_NAME"));
						jsonObject.put("clickNum", row.stringValue("FPM_CLICK_NUMBER"));
						String createTime = row.stringValue("FPM_CREATE_TIME");
						createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
						createTime = createTime.substring(5, 16).replaceAll("/", "-");
						jsonObject.put("createTime", createTime);
						jsonObject.put("count", row.stringValue("FRI_ID_COUNT"));
						String type = "";
						if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
							type = "essence";
						}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
							type = "key";
						}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
							type = "ordinary";
						}
						jsonObject.put("category", row.stringValue("FCI_CODE"));
						jsonObject.put("type", type);
						String attentionCount = row.stringValue("FF_ID_COUNT");
						jsonObject.put("attentionCount", attentionCount);//关注数
						jsonArray1.put(jsonObject);
					}
					
					jsonObject1.put(bbsCodes.get(bbsCode), jsonArray1);
				}else if(refresh.equals(queryPolicy)){
					String bbsCode = bbsCodes.get(tabCode);
					int tabPageSize = pageSize;
					if(tabPage != 0){
						tabPageSize = tabPage*pageSize;
					}
					List<DataRow> results1 = new ArrayList<DataRow>();
					if("hotnew-post".equals(bbsCode)){
						results1 = forumSectionInfoTreeManage.findHotPostRecords(0,tabPageSize);
					}else{
						results1 = forumSectionInfoTreeManage.findPostMessageRecords(bbsCode,0,tabPageSize);
					}
					JSONArray jsonArray1 = new JSONArray();
					for(int i=0;i<results1.size();i++){
						DataRow row = results1.get(i);
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("title", row.stringValue("FPM_TITLE"));
						jsonObject.put("id", row.stringValue("FPM_ID"));
						jsonObject.put("name", row.stringValue("FU_NAME"));
						jsonObject.put("clickNum", row.stringValue("FPM_CLICK_NUMBER"));
						String createTime = row.stringValue("FPM_CREATE_TIME");
						createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
						createTime = createTime.substring(5, 16).replaceAll("/", "-");
						jsonObject.put("createTime", createTime);
						jsonObject.put("count", row.stringValue("FRI_ID_COUNT"));
						String type = "";
						if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
							type = "essence";
						}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
							type = "key";
						}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
							type = "ordinary";
						}
						jsonObject.put("category", row.stringValue("FCI_CODE"));
						jsonObject.put("type", type);
						String attentionCount = row.stringValue("FF_ID_COUNT");
						jsonObject.put("attentionCount", attentionCount);//关注数
						jsonArray1.put(jsonObject);
					}
					
					jsonObject1.put(bbsCodes.get(bbsCode), jsonArray1);
				}
			}
			
			responseText = jsonObject1.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getForumInformation(DataParam param){
		String fpmId = param.getString("fpmId");
		String responseText = null;
		try {
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			int fpmClickNumber = forumSectionInfoTreeManage.getFpmClickNumber(fpmId);
			forumSectionInfoTreeManage.updateFpmClickNumberRecord(fpmId, fpmClickNumber);
			DataRow postInfoRow = forumSectionInfoTreeManage.getForumPostMessageRecord(fpmId);
			User user = (User)this.getUser();
			String userCode = user.getUserCode();
			String fuId = forumSectionInfoTreeManage.getFuIdRecord(userCode);
			int count = forumSectionInfoTreeManage.getForumFavoriteRecord(fpmId, fuId);
			JSONObject jsonObject1 = new JSONObject();
			jsonObject1.put("id", postInfoRow.stringValue("FPM_ID"));
			jsonObject1.put("title", postInfoRow.stringValue("FPM_TITLE"));
			jsonObject1.put("name", postInfoRow.stringValue("FU_NAME"));
			jsonObject1.put("clickNum", postInfoRow.stringValue("FPM_CLICK_NUMBER"));
			String createTime = postInfoRow.stringValue("FPM_CREATE_TIME");
			createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, postInfoRow.getTimestamp("FPM_CREATE_TIME"));
			createTime = createTime.substring(5, 16).replaceAll("/", "-");
			jsonObject1.put("createTime", createTime);
			String mobileContent = postInfoRow.stringValue("FPM_CONTENT_MOBILE");
			
			if(mobileContent == null || mobileContent.isEmpty()){
				String pcContent =  postInfoRow.stringValue("FPM_CONTENT");
				pcContent = this.filterHtml(pcContent);
				jsonObject1.put("content",pcContent);
			}else{
				String regxp = "<a href=\"/portal/index\\?ForumPostLocator&contentId=.*? target=\"_blank\">";
				mobileContent = this.replaceExt(mobileContent, regxp);
				jsonObject1.put("content",mobileContent);
			}
			
			String type = "";
			if("ESSENCE_POSTS".equals(postInfoRow.stringValue("FPM_TYPE"))){
				type = "essence";
			}else if("KEY_POSTS".equals(postInfoRow.stringValue("FPM_TYPE"))){
				type = "key";
			}else if("ORDINARY_POSTS".equals(postInfoRow.stringValue("FPM_TYPE"))){
				type = "ordinary";
			}else if("GARBAGE_POSTS".equals(postInfoRow.stringValue("FPM_TYPE"))){
				type = "garbage";
			}
			if(count == 0){
				jsonObject1.put("isAttention", "N");
			}else{
				jsonObject1.put("isAttention", "Y");
			}
			jsonObject1.put("type", type);
			jsonObject1.put("category", postInfoRow.stringValue("FCI_CODE"));
			jsonObject1.put("count", postInfoRow.stringValue("FRI_ID_COUNT"));
			String attentionCount = postInfoRow.stringValue("FF_ID_COUNT");
			jsonObject1.put("attentionCount", attentionCount);//关注数
			List<DataRow> replyInfosList = forumSectionInfoTreeManage.findRepliesInfoRecords(fpmId);
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<replyInfosList.size();i++){
				DataRow row = replyInfosList.get(i);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", row.stringValue("FRI_ID"));
				jsonObject.put("userName", row.stringValue("FU_NAME"));
				String time = row.stringValue("FRI_TIME");
        		if(!time.isEmpty()){
        			time = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FRI_TIME"));
        			jsonObject.put("time", time);
        		}else{
        			continue;
        		}
				jsonObject.put("floor", i+1);//当前楼数
				String friReportNum =  row.stringValue("FRI_REPORT_NUM");
				if("楼主".endsWith(friReportNum)){
					jsonObject.put("reportNum", row.stringValue("FRI_REPORT_NUM"));
				}else{
					jsonObject.put("reportNum", row.stringValue("FRI_REPORT_NUM")+"楼");
				}
				String mobileComment = row.stringValue("FRI_CONTENT_MOBILE");
				
				if(mobileComment == null || mobileComment.isEmpty()){
					String comment = row.stringValue("FRI_CONTENT");
					comment = this.filterHtml(comment);
					jsonObject.put("comment", comment);
				}else{
					String regxp = "<a href=\"/portal/index\\?ForumPostLocator&contentId=.*? target=\"_blank\">";
					mobileComment = this.replaceExt(mobileComment, regxp);
					jsonObject.put("comment",mobileComment);
				}
				
				jsonArray1.put(jsonObject);
			}
			jsonObject1.put("comments", jsonArray1);
			responseText = jsonObject1.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findUserForumPostInfos(DataParam param){
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		String responseText = null;
		try {
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			List<DataRow> postInfoList = forumSectionInfoTreeManage.findForumPostInfosByUserCode(userCode);
			JSONObject jsonObject1 = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<postInfoList.size();i++){
				DataRow row = postInfoList.get(i);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", row.stringValue("FPM_ID"));
				jsonObject.put("title", row.stringValue("FPM_TITLE"));
				jsonObject.put("name", row.stringValue("FU_NAME"));
				jsonObject.put("clickNum", row.stringValue("FPM_CLICK_NUMBER"));
				Date date = (Date) row.get("FPM_CREATE_TIME");
				String time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING, date);
				jsonObject.put("createTime", time.substring(5).replaceAll("/", "-"));
				
				String type = "";
				if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "essence";
				}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "key";
				}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "ordinary";
				}else if("GARBAGE_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "garbage";
				}
				jsonObject.put("type", type);
				jsonObject.put("count", row.stringValue("FRI_ID_COUNT"));
				jsonObject.put("category", row.stringValue("FCI_CODE"));
				String attentionCount = row.stringValue("FF_ID_COUNT");
				jsonObject.put("attentionCount", attentionCount);//关注数
				jsonArray1.put(jsonObject);
			}
			jsonObject1.put("datas", jsonArray1);
			responseText = jsonObject1.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findUserAttentionForumPostInfos(DataParam param){
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		String responseText = null;
		try {
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			List<DataRow> postInfoList = forumSectionInfoTreeManage.findUserAttentionForumPostInfos(userCode);
			JSONObject jsonObject1 = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<postInfoList.size();i++){
				DataRow row = postInfoList.get(i);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", row.stringValue("FPM_ID"));
				jsonObject.put("title", row.stringValue("FPM_TITLE"));
				jsonObject.put("name", row.stringValue("FU_NAME"));
				jsonObject.put("clickNum", row.stringValue("FPM_CLICK_NUMBER"));
				Date date = (Date) row.get("FPM_CREATE_TIME");
				String time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING, date);
				jsonObject.put("createTime", time.substring(5).replaceAll("/", "-"));
				
				String type = "";
				if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "essence";
				}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "key";
				}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "ordinary";
				}else if("GARBAGE_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "garbage";
				}
				jsonObject.put("type", type);
				jsonObject.put("count", row.stringValue("FRI_ID_COUNT"));
				jsonObject.put("category", row.stringValue("FCI_CODE"));
				String attentionCount = row.stringValue("FF_ID_COUNT");
				jsonObject.put("attentionCount", attentionCount);//关注数
				jsonArray1.put(jsonObject);
			}
			jsonObject1.put("datas", jsonArray1);
			responseText = jsonObject1.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer mobileIsGuest(DataParam param){
		User user = (User)this.getUser();
		ForumPrivilegeHelper forumPrivilegeHelper = new ForumPrivilegeHelper(user);
		String userCode = user.getUserCode();
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			if(GUEST.equals(userCode) || forumPrivilegeHelper.isGuest()){
				jsonObject.put("isGuest", true);
			}else{
				jsonObject.put("isGuest", false);
			}
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findForumInfosByCode(DataParam param){
		String responseText = null;
		try {
			String sectionCode = param.get("sectionCode");
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			//版块下帖子
			List<DataRow> results1 = forumSectionInfoTreeManage.mobilefindForumInfosBySectionCode(sectionCode);
			JSONObject jsonObject1 = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<results1.size();i++){
				DataRow row = results1.get(i);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("title", row.stringValue("FPM_TITLE"));
				jsonObject.put("id", row.stringValue("FPM_ID"));
				jsonObject.put("name", row.stringValue("FU_NAME"));
				jsonObject.put("clickNum", row.stringValue("FPM_CLICK_NUMBER"));
				String createTime = row.stringValue("FPM_CREATE_TIME");
				createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
				jsonObject.put("createTime", createTime);
				jsonObject.put("count", row.stringValue("FRI_ID_COUNT"));//回帖数
				String type = "";//分类
				if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "essence";
				}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "key";
				}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "ordinary";
				}
				jsonObject.put("category", row.stringValue("FCI_CODE"));
				jsonObject.put("type", type);
				String attentionCount = row.stringValue("FF_ID_COUNT");
				jsonObject.put("attentionCount", attentionCount);//关注数
				jsonArray1.put(jsonObject);
			}
			
			jsonObject1.put("forumInfos", jsonArray1);
			responseText = jsonObject1.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	 private String filterHtml(String str) {
    	String regxp = "<([^>]*)>";
    	RegexHelper regexHelper = new RegexHelper();
    	String result = regexHelper.replace(str, regxp, "");
    	return result;
	}
	 
	 private String replaceExt(String content,String regxp){
		 String result = content;
		 Pattern pat = Pattern.compile(regxp);  
		 Matcher matcher = pat.matcher(content);  
		 RegexHelper regexHelper = new RegexHelper();
		 if (matcher.find()) { 
		     String temp = content.substring(matcher.start(),matcher.end());
		     String contentSegment = regexHelper.extractString(temp, "&contentId=.*?\"");
		     
		     if (StringUtil.isNotNullNotEmpty(contentSegment)){
		    	 String contentId = contentSegment.substring(11,contentSegment.length()-1);
			     String replacement = "<a ng-click=\"openBbsModal('"+contentId+"','问答详情')\">";
			     content = content.replace(temp,replacement);	
			     
			     result = this.replaceExt(content, regxp);
		     }
		 }   
		 System.out.println(result);
		 return result;
	}

}
