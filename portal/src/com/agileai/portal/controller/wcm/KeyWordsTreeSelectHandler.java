package com.agileai.portal.controller.wcm;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.portal.bizmoduler.wcm.KeyWordsTreeSelect;

public class KeyWordsTreeSelectHandler
        extends TreeSelectHandler {
    public KeyWordsTreeSelectHandler() {
        super();
        this.serviceId = buildServiceId(KeyWordsTreeSelect.class);
        this.isMuilSelect = true;
        this.checkRelParentNode = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "WORD_ID",
                                                  "WORD_NAME", "WORD_PID");
        String rootId = param.get("rootColumnId");
        treeBuilder.setRootId(rootId);

        return treeBuilder;
    }

    protected KeyWordsTreeSelect getService() {
        return (KeyWordsTreeSelect) this.lookupService(this.getServiceId());
    }
}
