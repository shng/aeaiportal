package com.agileai.portal.controller.wcm;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.domain.TreeModel;
import com.agileai.hotweb.domain.core.Resource;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.wcm.InfomationManage;
import com.agileai.util.ListUtil;

public class WcmColumnPickHandler
        extends TreeSelectHandler {
    public WcmColumnPickHandler() {
        super();
        this.serviceId = buildServiceId(InfomationManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "COL_ID",
                                                  "COL_NAME", "COL_PID");

        String excludeId = param.get("COL_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

	protected void buildTreeSyntax(StringBuffer treeSyntax,TreeModel treeModel){
		List<TreeModel> children = treeModel.getChildren();
		String parentId = treeModel.getId();
		User user = (User)this.getUser();
        for (int i=0;i < children.size();i++){
        	TreeModel child = children.get(i);
            String curNodeId = child.getId();
            if (user.containResouce(Resource.Type.InfoColumn, curNodeId)
            		|| "user".equals(user.getUserCode()) || "admin".equals(user.getUserCode())){
                String curNodeName = child.getName();
                if (!ListUtil.isNullOrEmpty(child.getChildren())){
                	treeSyntax.append("d.add('"+curNodeId+"','"+parentId+"','"+curNodeName+"',\"javascript:setSelectTempValue('"+curNodeId+"','"+curNodeName+"')\");").append(newRow);
                }else{
                	treeSyntax.append("d.add('"+curNodeId+"','"+parentId+"','"+curNodeName+"',\"javascript:setSelectTempValue('"+curNodeId+"','"+curNodeName+"')\");").append(newRow);
                }
                this.buildTreeSyntax(treeSyntax,child);
            }
        }
    }  
    
    protected InfomationManage getService() {
        return (InfomationManage) this.lookupService(this.getServiceId());
    }
}
