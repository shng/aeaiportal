package com.agileai.portal.controller.wcm;

import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.auth.SecurityGroupManage;
import com.agileai.portal.bizmoduler.wcm.InfomationFavEditManage;

public class InfomationFavEditHandler extends StandardEditHandler{
	 public InfomationFavEditHandler() {
        super();
        this.serviceId = buildServiceId(InfomationFavEditManage.class);
	 }
	 public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		this.setOperaType(operaType);
		String infoId = param.get("contentId");
		param.put("INFO_ID", infoId);
		DataRow row = getService().getRecord(param);
		setAttribute("FAV_LABLE", row.getString("INFO_TITLE"));
		setAttribute("INFO_ID", row.getString("INFO_ID"));
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	 }
	 protected void processPageAttributes(DataParam param) {
		FormSelect formSelect = FormSelectFactory.create("FAV_TYPE").addSelectedValue(getAttributeValue("TYPE_ID", "NEWS"));  
        setAttribute("TYPE_ID", formSelect.addHasBlankValue(false));
	 }
	 
	 public ViewRenderer doSaveAction(DataParam param){
	 	String responseText = null;
	 	User user = (User) this.getUser();
	 	param.put("userId", user.getUserId());
	 	param.put("TYPE_ID",param.getString("select"));
		List<DataRow> row = getService().findRecords(param);
		if(row.size()>0){
			param.put("USER_ID", user.getUserId());
			getService().updateRecord(param);
		}else{
			param.put("USER_ID", user.getUserId());
			getService().createRecord(param);			
		}
		responseText="success";
		return new AjaxRenderer(responseText);
	 }
	 
	@PageAction
    public ViewRenderer mobileAddToFavorite(DataParam param){
    	String responseText = FAIL;
    	try {
    		User user = (User) this.getUser();
    		String inputString = this.getInputString();
    		JSONObject jsonObject = new JSONObject(inputString);
        	String infoId = param.getString("infoId");
        	String infoTitle = jsonObject.getString("title");
        	String userCode = user.getUserCode();
        	String favType = jsonObject.getString("favType");
        	param.put("infoId",infoId,"userCode",userCode);
        	List<DataRow> records = getService().findInfoRecords(param);
        	if(records.size()<1){
        		DataParam queryParam = new DataParam("USER_CODE",userCode);
            	SecurityGroupManage securityGroupManage = lookupService(SecurityGroupManage.class);
    			DataRow record =  securityGroupManage.getContentRecord("SecurityUser", queryParam);
    			String userId = record.getString("USER_ID");
    			param.put("INFO_ID",infoId,"USER_ID",userId,"TYPE_ID",favType,"FAV_LABLE",infoTitle);
            	getService().createRecord(param);
            	responseText = SUCCESS;
        	}else{
        		DataRow row = records.get(0);
        		param.put("FAV_ID", row.getString("FAV_ID"));
        		getService().deletRecord(param);
            	responseText = SUCCESS;
        	}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
	 
	 protected InfomationFavEditManage getService() {
	        return (InfomationFavEditManage) this.lookupService(this.getServiceId());
	 }
}
