package com.agileai.portal.controller.wcm;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.wcm.InfomationFavEditManage;

public class InfomationFavListHandler extends BaseHandler{
	 public InfomationFavListHandler() {
	        super();
	 }
	 private static final String NEW_LINE = "\r\n";
	 
	 @PageAction
	 public ViewRenderer getFavTypeList(DataParam param) {
		 String responseText = null;
		 String favType = param.getString("favType");
		 StringBuffer temp = new StringBuffer();
		 temp.append("<?xml version=\"1.0").append("\"encoding=").append("\"utf-8").append("\"?>").append(NEW_LINE);
		 temp.append("<table width=\"100%").append("\" cellspacing=").append("\"1").append("\"cellpadding=").append("\"1").append("\">").append(NEW_LINE);
		 
		 List<DataRow> result = getService().findListRecords(param);
		 for(int i = 0;i<result.size();i++){
			 DataRow row = result.get(i);
			 String id = row.getString("CODE_ID");
			 String name = row.getString("CODE_NAME");
			 String selected = "";
			 if(favType.equals(id)){
				 selected = "selected";
			 }
			 temp.append("<tr><td ").append("class=\"").append(selected).append("\"").append("><a href=\"javascript:doAjaxRefreshPortalPage('favType:").append(id).append("\')").append("\">").append(name).append("</a>").append(NEW_LINE);
		 }
		 temp.append("</table>");
		 responseText = temp.toString();
		 return new AjaxRenderer(responseText);
	 }
	 
	 protected InfomationFavEditManage getService() {
	        return (InfomationFavEditManage) this.lookupService(InfomationFavEditManage.class);
	 }
}
