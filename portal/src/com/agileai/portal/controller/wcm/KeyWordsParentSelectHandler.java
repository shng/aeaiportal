package com.agileai.portal.controller.wcm;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.portal.bizmoduler.wcm.KeyWordsTreeManage;

public class KeyWordsParentSelectHandler
        extends TreeSelectHandler {
    public KeyWordsParentSelectHandler() {
        super();
        this.serviceId = buildServiceId(KeyWordsTreeManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "WORD_ID",
                                                  "WORD_NAME", "WORD_PID");

        String excludeId = param.get("WORD_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected KeyWordsTreeManage getService() {
        return (KeyWordsTreeManage) this.lookupService(this.getServiceId());
    }
}
