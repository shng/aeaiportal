package com.agileai.portal.controller.wcm;

import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.wcm.WcmPictureGroup8ContentManage;

public class WcmPictureResourceEditHandler
        extends TreeAndContentManageEditHandler {
    public WcmPictureResourceEditHandler() {
        super();
        this.serviceId = buildServiceId(WcmPictureGroup8ContentManage.class);
        this.tabId = "WcmPictureResource";
        this.columnIdField = "GRP_ID";
        this.contentIdField = "RES_ID";
    }
    @PageAction
    public ViewRenderer viewImage(DataParam param){
    	ViewRenderer result = null;
    	String resId = param.get("resId");
    	WcmPictureGroup8ContentManage manage = getService();
    	DataParam queryParam = new DataParam("RES_ID",resId);
    	DataRow row = manage.getContentRecord(this.tabId, queryParam);
    	String location = row.stringValue("RES_LOCATION");
    	this.setAttribute("imgSrc",location);
    	String pageURL = "wcm/WcmPictureResourceView.jsp";
    	result = new LocalRenderer(pageURL);
    	return result;
    }
    @PageAction
    public ViewRenderer getImageObject(DataParam param){
    	ViewRenderer result = null;
    	String resId = param.get("resId");
    	WcmPictureGroup8ContentManage manage = getService();
    	DataParam queryParam = new DataParam("RES_ID",resId);
    	DataRow row = manage.getContentRecord(this.tabId, queryParam);
    	String url = row.stringValue("RES_LOCATION");
    	String width = row.stringValue("RES_WIDTH");
    	String height = row.stringValue("RES_HEIGHT");
    	JSONObject jsonObject = new JSONObject();
    	try {
    		jsonObject.put("url", url);
        	jsonObject.put("width", width);
        	jsonObject.put("height", height);			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
    	result = new AjaxRenderer(jsonObject.toString());
    	return result;
    }
    
    protected void processPageAttributes(DataParam param) {
    }

    protected WcmPictureGroup8ContentManage getService() {
        return (WcmPictureGroup8ContentManage) this.lookupService(this.getServiceId());
    }
}
