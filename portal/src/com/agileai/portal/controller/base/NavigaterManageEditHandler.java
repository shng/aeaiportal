package com.agileai.portal.controller.base;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.base.NavigaterManage;
import com.agileai.portal.bizmoduler.base.PtThemeManage;
import com.agileai.portal.driver.model.MenuBar;
import com.agileai.util.ListUtil;
import com.agileai.util.StringUtil;

public class NavigaterManageEditHandler extends StandardEditHandler{
	public NavigaterManageEditHandler(){
		super();
		this.listHandlerClass = NavigaterManageListHandler.class;
		this.serviceId = buildServiceId(NavigaterManage.class);
	}
	
	protected void processPageAttributes(DataParam param) {
		String naviterType =this.getAttributeValue("NAV_TYPE");
		FormSelect themeSelect = new FormSelect();
		themeSelect.setKeyColumnName("THEME_ID");
		themeSelect.setValueColumnName("THEME_NAME");
		PtThemeManage themeManage = this.lookupService(PtThemeManage.class);
		DataParam queryParam = null;
		if (MenuBar.Type.LOGIN_BEFORE.equals(naviterType)){
			queryParam = new DataParam("themeType",MenuBar.Type.LOGIN_BEFORE);
		}else if (MenuBar.Type.LOGIN_AFTER.equals(naviterType)){
			queryParam = new DataParam("themeType",MenuBar.Type.LOGIN_AFTER);
		}
		List<DataRow> records = themeManage.findRecords(queryParam);
		themeSelect.putValues(records);
		setAttribute("NAV_THEME", themeSelect.addSelectedValue(this.getAttributeValue("NAV_THEME")));
		setAttribute("NAV_TYPE",FormSelectFactory.create("NAVIGATER_TYPE").addSelectedValue(getAttributeValue("NAV_TYPE")));
		setAttribute("NAV_ISVALID",FormSelectFactory.create("SYS_VALID_TYPE").addSelectedValue(getAttributeValue("NAV_ISVALID")));
		setAttribute("NAV_THEME_PRO",FormSelectFactory.create("SYS_THEME_PRO").addSelectedValue(getAttributeValue("NAV_THEME_PRO")));
	}
	
	public ViewRenderer doRefeshTypeAction(DataParam param){
		String naviterType =param.get("NAV_TYPE");
		FormSelect themeSelect = new FormSelect();
		themeSelect.setKeyColumnName("THEME_ID");
		themeSelect.setValueColumnName("THEME_NAME");
		PtThemeManage themeManage = this.lookupService(PtThemeManage.class);
		DataParam queryParam = null;
		if (MenuBar.Type.LOGIN_BEFORE.equals(naviterType)){
			queryParam = new DataParam("THEME_TYPE",MenuBar.Type.LOGIN_BEFORE);
		}else if (MenuBar.Type.LOGIN_AFTER.equals(naviterType)){
			queryParam = new DataParam("THEME_TYPE",MenuBar.Type.LOGIN_BEFORE);
		}
		List<DataRow> records = themeManage.findRecords(queryParam);
		themeSelect.putValues(records);
		String responseText = themeSelect.getScriptSyntax("NAV_THEME");
		return new AjaxRenderer(responseText);
	}
	@PageAction
	public ViewRenderer loadPersonalThemes(DataParam param){
		PtThemeManage themeManage = this.lookupService(PtThemeManage.class);
		List<DataRow> tempRecords = themeManage.findPersonalRecords(new DataParam());

		String navId = param.get("NAV_ID");
		NavigaterManage navigaterManage = this.lookupService(NavigaterManage.class);
		DataParam queryNavParam = new DataParam();
		queryNavParam.put("NAV_ID",navId);
		DataRow row = navigaterManage.getRecord(queryNavParam);
		String personalThemes = row.getString("PERSONAL_THEMES");
		List<String> existThemeIdList = new ArrayList<String>();
		if (personalThemes != null && personalThemes.length() > 0){
			String[] tempIds = personalThemes.split(",");
			ListUtil.addArrayToList(existThemeIdList, tempIds);
		}
		
		List<DataRow> records = new ArrayList<DataRow>(); 
		for (int i=0;i < tempRecords.size();i++){
			DataRow tempRow = tempRecords.get(i);
			String themeId = tempRow.stringValue("THEME_ID");
			if (existThemeIdList.contains(themeId)) {
				records.add(tempRow);
			}
		}
		FormSelect formSelect = this.buildPersonalThemeSelect(records);
		String responseText = formSelect.getScriptSyntax("PERSONAL_THEMES");
		return new AjaxRenderer(responseText);
	}
	
    private FormSelect buildPersonalThemeSelect(List<DataRow> records){
        FormSelect formSelect = new FormSelect();
        formSelect.setKeyColumnName("THEME_ID");
        formSelect.setValueColumnName("THEME_NAME");
        formSelect.putValues(records);
        formSelect.addHasBlankValue(false);
        return formSelect;
    }	
	
	@PageAction
	public ViewRenderer delTheme(DataParam param){
		String responseText = FAIL;
		String navId = param.get("NAV_ID");
		String themeId = param.get("PERSONAL_THEMES");
		
		NavigaterManage navigaterManage = this.lookupService(NavigaterManage.class);
		DataParam queryNavParam = new DataParam();
		queryNavParam.put("NAV_ID",navId);
		DataRow row = navigaterManage.getRecord(queryNavParam);
		String personalThemes = row.getString("PERSONAL_THEMES");
		
		List<String> existThemeIdList = new ArrayList<String>();
		if (personalThemes != null && personalThemes.length() > 0){
			String[] tempIds = personalThemes.split(",");
			ListUtil.addArrayToList(existThemeIdList, tempIds);
		}
		if (existThemeIdList.contains(themeId)){
			existThemeIdList.remove(themeId);
			String[] tempArray = existThemeIdList.toArray(new String[0]);
			String newPersonalThemes = StringUtil.append(tempArray, ",");
			
			navigaterManage.savePersonalThemes(navId, newPersonalThemes);
		}
		responseText = SUCCESS;
		return new AjaxRenderer(responseText);
	}
	
	protected NavigaterManage getService() {
		return (NavigaterManage)this.lookupService(this.getServiceId());
	}
}