package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PtPortletTemptManage;

public class PtPortletTemptManageEditHandler
        extends StandardEditHandler {
    public PtPortletTemptManageEditHandler() {
        super();
        this.listHandlerClass = PtPortletTemptManageListHandler.class;
        this.serviceId = buildServiceId(PtPortletTemptManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("TEMPT_GRP",
                     FormSelectFactory.create("PTLET_TEMPT_GRP")
                                      .addSelectedValue(getOperaAttributeValue("TEMPT_GRP",
                                                                               "default")));
    }

    protected PtPortletTemptManage getService() {
        return (PtPortletTemptManage) this.lookupService(this.getServiceId());
    }
}
