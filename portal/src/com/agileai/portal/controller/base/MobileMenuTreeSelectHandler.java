package com.agileai.portal.controller.base;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.TreeManage;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.base.MenuBarManage;
import com.agileai.portal.bizmoduler.base.MenuTreeSelect;
import com.agileai.portal.controller.mobile.AppModuleConfigManageEditHandler;

public class MobileMenuTreeSelectHandler
        extends TreeSelectHandler {
	private static final String ROOT_MENU_ID = "ROOT";
    public MobileMenuTreeSelectHandler() {
        super();
        this.serviceId = buildServiceId(MenuTreeSelect.class);
        this.isMuilSelect = true;
        this.checkRelParentNode = false;
        this.mulSelectNodeTypes.add("Object");
    }

	public ViewRenderer prepareDisplay(DataParam param){
		this.setAttributes(param);
		return new LocalRenderer(getPage());
	}    
    
    protected TreeBuilder provideTreeBuilder(DataParam param) {
        return null;
    }
    
	@PageAction
	public ViewRenderer retrieveJson(DataParam param){
		String responseText = FAIL;
		try {
			JSONArray jsonArray = new JSONArray();
			String id = param.get("id",ROOT_MENU_ID);
			String navId = param.get("navId");
			String pagepro = param.get("pagepro");
			String treeMenuId = param.get("treeMenuId");
			TreeManage service = this.lookupService(MenuBarManage.class);
			DataParam queryParam = new DataParam("MENU_ID",treeMenuId);
			DataRow record = service.queryCurrentRecord(queryParam);
			String menuDesc = record.getString("MENU_DESC");
			String modules = "";
			if(null != menuDesc){
				try {
					JSONObject jsonObject = new JSONObject(menuDesc);
					if("isModal".equals(pagepro)){
						String addModalIds = (String)this.getSessionAttributes().get(AppModuleConfigManageEditHandler.class.getName()+"addModalIds");
						if(addModalIds == null){
							if(menuDesc.contains("modals")){
								modules = jsonObject.get("modals").toString();
							}
						}else{
							if(menuDesc.contains("modals")){
								modules = jsonObject.get("modals").toString();
								modules = modules + "," + addModalIds;
							}else{
								modules = addModalIds;
							}
						}
					}else if("isPopover".equals(pagepro)){
						String addPopoverIds = (String)this.getSessionAttributes().get(AppModuleConfigManageEditHandler.class.getName()+"addPopoverIds");
						if(addPopoverIds == null){
							if(menuDesc.contains("popovers")){
								modules = jsonObject.get("popovers").toString();
							}
						}else{
							if(menuDesc.contains("popovers")){
								modules = jsonObject.get("popovers").toString();
								modules = modules + "," + addPopoverIds;
							}else{
								modules = addPopoverIds;
							}
						}
					}else if("isPopup".equals(pagepro)){
						String addPopupIds = (String)this.getSessionAttributes().get(AppModuleConfigManageEditHandler.class.getName()+"addPopupIds");
						if(addPopupIds == null){
							if(menuDesc.contains("popups")){
								modules = jsonObject.get("popups").toString();
							}
						}else{
							if(menuDesc.contains("popups")){
								modules = jsonObject.get("popups").toString();
								modules = modules + "," + addPopupIds;
							}else{
								modules = addPopupIds;
							}
						}
					}
					
				} catch (Exception e) {
					log.error(e.getLocalizedMessage(), e);
				}
			}
			String moduleCodes = this.modulesListSplit(modules);
		
	    	List<DataRow> records = getService().queryMobileMenuTreeRecords(navId,pagepro,moduleCodes);
	    	if("ROOT".equals(id)){
	    		id = navId;
	    		if (records != null && !records.isEmpty()){
					for (int i=0 ;i < records.size();i++){
						DataRow row = records.get(i);
						JSONObject jsonObject = new JSONObject();
						String menuId = row.stringValue("MENU_CODE");
						String menuName = row.stringValue("MENU_NAME");
						jsonObject.put("id",menuId);
						jsonObject.put("text",menuName);
						String menuType = row.stringValue("MENU_TYPE");
						if("F".equals(menuType)){
							jsonObject.put("state", "closed");
						}
						jsonArray.put(jsonObject);
					}	    		
		    	}
	    	}
			records = getService().queryPageMenuTreeRecords(id,navId,pagepro,moduleCodes);
			if (records != null && !records.isEmpty()){
				for (int i=0 ;i < records.size();i++){
					DataRow row = records.get(i);
					String menuId = row.stringValue("MENU_CODE");
					String menuName = row.stringValue("MENU_NAME");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("id",menuId);
					jsonObject.put("text",menuName);
					String menuType = row.stringValue("MENU_TYPE");
					if("F".equals(menuType)){
						jsonObject.put("state", "closed");
					}
					jsonArray.put(jsonObject);
				}					
			}

			responseText = jsonArray.toString();
		} catch (Exception e) {
			log.error(e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
	public String modulesListSplit(String modules){
		String moduleCodes = "";
		List<String> modulesList = new ArrayList<String>();
		MobileMenuBarManageHandler.addArrayToList(modulesList, modules.split(","));
		for(int i=0;i<modulesList.size();i++){
			if(i == modulesList.size()-1){
				String modal = modulesList.get(i).replaceAll("'", "");
				if(!modal.isEmpty()){
					moduleCodes = moduleCodes + "'" + modal +"'";
				}else{
					continue;
				}
			}else{
				String modal = modulesList.get(i).replaceAll("'", "");
				if(!modal.isEmpty()){
					moduleCodes = moduleCodes +  "'" + modal + "'" +",";
				}else{
					continue;
				}
			}
		}
		
		return moduleCodes;
    }    
	    
	
    @PageAction
    public ViewRenderer addAddConfigTreeRequest(DataParam param){
    	return this.prepareDisplay(param);
    }    
    
    protected MenuTreeSelect getService() {
		return (MenuTreeSelect)this.lookupService(this.getServiceId());
	}
}