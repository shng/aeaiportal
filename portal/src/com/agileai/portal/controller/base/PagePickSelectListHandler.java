package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PagePickSelect;

public class PagePickSelectListHandler
        extends PickFillModelHandler {
    public PagePickSelectListHandler() {
        super();
        this.serviceId = buildServiceId(PagePickSelect.class);
    }

    protected void processPageAttributes(DataParam param) {
    	String statementId = "menubar.findRecords";
    	FormSelect navgaterId = FormSelectFactory.create(statementId, param, "NAV_ID", "NAV_NAME");
        setAttribute("navgaterId",navgaterId.addSelectedValue(param.get("navgaterId")));
    }

    protected void initParameters(DataParam param) {
    }

    protected PagePickSelect getService() {
        return (PagePickSelect) this.lookupService(this.getServiceId());
    }
}
