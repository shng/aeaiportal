package com.agileai.portal.controller.base;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.auth.SecurityGroupManage;
import com.agileai.portal.bizmoduler.base.NavigaterManage;
import com.agileai.portal.bizmoduler.base.PtThemeManage;
import com.agileai.portal.driver.AttributeKeys;
import com.agileai.portal.driver.model.PersonalFavorite;
import com.agileai.portal.filter.UserCacheManager;
import com.agileai.util.ListUtil;
import com.agileai.util.StringUtil;

public class PersonalThemeSelectListHandler extends BaseHandler{
	public PersonalThemeSelectListHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String navId = param.get("navId");
		this.setAttributes(param);
		String comeFromNavEdit = param.get("comeFromNavEdit");
		
		User user = (User)this.getUser();
		DataParam queryParam = new DataParam();
		if (!"Y".equals(comeFromNavEdit)){
			PersonalFavorite personalFavorite = (PersonalFavorite)user.getExtendProperties().get(AttributeKeys.PERSONAL_FAVORITE_OBJECT_KEY);
			if (personalFavorite != null && !StringUtil.isNullOrEmpty(personalFavorite.getThemeId(navId))){
				queryParam.put("themeId",personalFavorite.getThemeId(navId));
			}			
		}
		PtThemeManage themeManage = this.lookupService(PtThemeManage.class);
		List<DataRow> tempRecords = themeManage.findPersonalRecords(queryParam);
		
		List<DataRow> records = new ArrayList<DataRow>();
		NavigaterManage navigaterManage = this.lookupService(NavigaterManage.class);
		DataParam queryNavParam = new DataParam();
		queryNavParam.put("NAV_ID",navId);
		DataRow row = navigaterManage.getRecord(queryNavParam);
		String personalThemes = row.getString("PERSONAL_THEMES");
		List<String> existThemeIdList = new ArrayList<String>();
		if (personalThemes != null && personalThemes.length() > 0){
			String[] tempIds = personalThemes.split(",");
			ListUtil.addArrayToList(existThemeIdList, tempIds);
		}
		
		if ("Y".equals(comeFromNavEdit)){
			for (int i=0;i < tempRecords.size();i++){
				DataRow tempRow = tempRecords.get(i);
				String themeId = tempRow.stringValue("THEME_ID");
				if (existThemeIdList.contains(themeId)) {
					continue;
				}
				records.add(tempRow);
			}
		}else{
			for (int i=0;i < tempRecords.size();i++){
				DataRow tempRow = tempRecords.get(i);
				String themeId = tempRow.stringValue("THEME_ID");
				if (existThemeIdList.contains(themeId)) {
					records.add(tempRow);
				}
			}
		}
		this.setAttribute("comeFromNavEdit", comeFromNavEdit);
		this.setRsList(records);
		return new LocalRenderer(getPage());
	}
	
	@PageAction
	public ViewRenderer selectTheme(DataParam param){
		String navId = param.get("navId");
		String themeId = param.get("themeId");
		String comeFromNavEdit = param.get("comeFromNavEdit");
		if ("Y".equals(comeFromNavEdit)){
			NavigaterManage navigaterManage = this.lookupService(NavigaterManage.class);
			DataParam queryParam = new DataParam();
			queryParam.put("NAV_ID",navId);
			DataRow row = navigaterManage.getRecord(queryParam);
			String personalThemes = row.getString("PERSONAL_THEMES");
			if (personalThemes != null && personalThemes.length() > 0){
				personalThemes = personalThemes+","+themeId;
			}else{
				personalThemes = themeId;
			}
			navigaterManage.savePersonalThemes(navId, personalThemes);
		}else{
			User user = (User)this.getUser();
			PersonalFavorite personalFavorite = (PersonalFavorite)user.getExtendProperties().get(AttributeKeys.PERSONAL_FAVORITE_OBJECT_KEY);
			if (personalFavorite != null){
				personalFavorite.setThemeId(navId,themeId);
				SecurityGroupManage securityGroupManage = this.lookupService(SecurityGroupManage.class);
				String userCode = user.getUserCode();
				try {
					ObjectMapper objectMapper = new ObjectMapper();
					String jsonString = objectMapper.writeValueAsString(personalFavorite);				
					securityGroupManage.updateSecurityUserFavorites(userCode, jsonString);		
					
					UserCacheManager userCacheManager = UserCacheManager.getOnly();
					userCacheManager.removeUser(userCode);
				} catch (Exception e) {
					log.error(e.getLocalizedMessage(), e);
				}
			}
		}
		String rspText = SUCCESS;
		return new AjaxRenderer(rspText);
	}
}