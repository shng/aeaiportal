package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PtStaticDataManage;

public class PtStaticDataManageEditHandler
        extends StandardEditHandler {
    public PtStaticDataManageEditHandler() {
        super();
        this.listHandlerClass = PtStaticDataManageListHandler.class;
        this.serviceId = buildServiceId(PtStaticDataManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("DATA_TYPE",
                     FormSelectFactory.create("STATIC_DATA_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("DATA_TYPE",
                                                                               "HTML")));
        setAttribute("DATA_GRP",
                     FormSelectFactory.create("STATIC_DATA_GRP")
                                      .addSelectedValue(getOperaAttributeValue("DATA_GRP",
                                                                               "default")));
    }

    protected PtStaticDataManage getService() {
        return (PtStaticDataManage) this.lookupService(this.getServiceId());
    }
}
