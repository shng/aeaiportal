package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.MenuItemPersonalManage;
import com.agileai.portal.driver.common.UserPersonalHelper;
import com.agileai.portal.driver.model.PersonalSetting;

public class PersonalWidthSetupHandler extends BaseHandler{
	public static final String CONTAINER_TYPE = "containerType";
	public PersonalWidthSetupHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		String menuItemId = param.get("menuItemId");
		this.setAttributes(param);
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		
		StringBuffer text = new StringBuffer();
		int[] widthScales = personalSetting.getColumnWidthScales();
		int childSize = widthScales.length;
		for (int i=0;i < childSize;i++){
			String width = String.valueOf(widthScales[i]);
			text.append("<input id='childWidth").append(i).append("' name='childWidth").append(i).append("' type='text' value='").append(width).append("' size='2' class='text' />");
			text.append("%");
			if (i < childSize-1){
				text.append("--");	
			}
		}
		this.setAttribute("childSize", String.valueOf(childSize));
		this.setAttribute("widthHtml", text.toString());
		this.setAttribute("menuItemId", param.get("menuItemId"));
		return new LocalRenderer(getPage());
	}
	@PageAction
	public ViewRenderer setupWidth(DataParam param){
		String menuItemId = param.get("menuItemId");
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		for (int i=0;i < 3;i++){
			String childWidth = param.get("childWidth"+i);
			personalSetting.getColumnWidthScales()[i] = Integer.parseInt(childWidth);
		}
		String userCode = user.getUserCode();
		MenuItemPersonalManage menuItemPersonalManage = this.lookupService(MenuItemPersonalManage.class);
		menuItemPersonalManage.savePersonalSetting(userCode, menuItemId, personalSetting);
		return new AjaxRenderer(SUCCESS);
	}	
}