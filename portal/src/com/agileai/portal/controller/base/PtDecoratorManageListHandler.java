package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.portal.bizmoduler.base.PtDecoratorManage;

public class PtDecoratorManageListHandler extends StandardListHandler{
	public PtDecoratorManageListHandler(){
		super();
		this.editHandlerClazz = PtDecoratorManageEditHandler.class;
		this.serviceId = buildServiceId(PtDecoratorManage.class);
	}
	protected void processPageAttributes(DataParam param) {
		
	}
	protected void initParameters(DataParam param) {
		initParamItem(param,"decorName","");		
	}
	protected PtDecoratorManage getService() {
		return (PtDecoratorManage)this.lookupService(this.getServiceId());
	}
}
