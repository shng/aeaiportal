package com.agileai.portal.controller.uui;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class RegistryResultHandler
        extends BaseHandler {
	
    public RegistryResultHandler() {
        super();
    }
    
	public ViewRenderer prepareDisplay(DataParam param){
		this.processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
	protected void processPageAttributes(DataParam param){
		String result = param.get("result");
		String mailAddress = param.get("mailAddress");
		String operaType = param.get("operaType");
		this.setAttributes(param);
		if ("userRegistry".equals(operaType)){
			if ("success".equals(result)){
				this.setAttribute("messages", "用户注册成功，激活链接已发至邮箱"+mailAddress+"，请查收！");
			}else{
				this.setAttribute("messages", "用户注册失败，请点击浏览器的返回按钮，检查注册信息，如：邮件地址！");
			}			
		}else if ("findPassword".equals(operaType)){
			if ("success".equals(result)){
				this.setAttribute("messages", "找回密码成功，重置链接已发至邮箱"+mailAddress+"，请查收！");
			}else{
				this.setAttribute("messages", "找回密码失败，请联系数通畅联！");
			}
		}
	}
}