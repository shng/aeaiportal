package com.agileai.portal.handler;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.Group;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class RedirectHandler extends BaseHandler {
	private static List<String> MobileModes = new ArrayList<String>();
	
	public ViewRenderer prepareDisplay(DataParam param) {
		User user = (User) getUser();
		if (isFromeMobile()){
			if("guest".equals(user.getUserCode())){
				return new RedirectRenderer("/portal/request/m9/index.ptml");
			}else{
				if (isRigistryUserGroup(user)){
					return new RedirectRenderer("/portal/request/m9/index.ptml");
				}else{
					return new RedirectRenderer("/portal/request/m3/index.ptml");
				}						
			}
		}else{
			if("guest".equals(user.getUserCode())){
				return new RedirectRenderer("/portal/request/09/index.ptml");
			}else{
				if (isRigistryUserGroup(user)){
					return new RedirectRenderer("/portal/request/09/index.ptml");
				}else{
					return new RedirectRenderer("/portal/request/03/index.ptml");
				}						
			}			
		}
	}
	
	private boolean isRigistryUserGroup(User user){
		boolean result = false;
		List<Group> grpList = user.getGroupList();
		for (int i=0;i < grpList.size();i++){
			Group grp = grpList.get(i);
			if ("NewUsers".equals(grp.getGroupCode())){
				result = true;
				break;
			}
		}
		return result;
	}
	
	private  boolean isFromeMobile(){
		boolean result = false;
		if (MobileModes.isEmpty()){
			MobileModes.add(".*iPhone.*");
			MobileModes.add(".*Android.*");
			MobileModes.add(".*Safari.*Pre.*");
			MobileModes.add(".*Nokia.*AppleWebKit.*");
		}
		String userAgent = request.getHeader("User-Agent");
		if (userAgent != null){
			for (int i=0;i < MobileModes.size();i++){
				String expression = MobileModes.get(i);
				if (userAgent.matches(expression)) {
					result = true;
					break;
				}
			}
		}else{
			result = false;
		}
		return result;
	}	
}
