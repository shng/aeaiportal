package com.agileai.portal.extend;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexHelper {
	
	 public String replace(String content,String regxp,String replacement) {
//    	String regxp = "<([^>]*)>";
		Pattern pattern = Pattern.compile(regxp);
		Matcher matcher = pattern.matcher(content);
		StringBuffer sb = new StringBuffer();
		boolean result1 = matcher.find();
		while (result1) {
			matcher.appendReplacement(sb, replacement);
			result1 = matcher.find();
		}
		matcher.appendTail(sb);
		return sb.toString();
	 }
	 
	 public String extractString(String content,String regxp){
		 String result = ""; 
		 Pattern pat = Pattern.compile(regxp);  
		 Matcher matcher = pat.matcher(content);     
		 while (matcher.find()) { 
		     result = content.substring(matcher.start(),matcher.end());
		     break;
		 }
		 return result;
	 }
	 
}
