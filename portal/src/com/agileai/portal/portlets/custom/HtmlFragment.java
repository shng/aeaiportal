package com.agileai.portal.portlets.custom;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.common.PortletRequestHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.portlets.PortletCacheManager;
import com.agileai.util.StringUtil;

public class HtmlFragment extends GenericPotboyPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String isContent = preferences.getValue("isContent", null);
		String contentId = preferences.getValue("contentId", null);
		String dataURL = preferences.getValue("dataURL", null);

		if (!StringUtil.isNullOrEmpty(isContent) 
				&& (!StringUtil.isNullOrEmpty(contentId) || !StringUtil.isNullOrEmpty(dataURL))){
			request.setAttribute("isSetting", "Y");
		}else{
			request.setAttribute("isSetting", "N");
		}
		
		String isSetting = (String)request.getAttribute("isSetting");
		if ("Y".equals(isSetting)){
			if ("Y".equals(isContent)){
				String urlPrefix = PortletCacheManager.buildDataURLPefix(request);
				dataURL = urlPrefix+"/resource?ContentProvider&contentId="+contentId;
			}else{
				dataURL = getDataURL(request);
				if (dataURL.startsWith("/portal")){
					HttpServletRequest servletRequest = PortletRequestHelper.getRequestContext(request).getServletRequest();
					String serverName = servletRequest.getServletContext().getInitParameter("serverName");
					dataURL = serverName + dataURL;
				}
			}
			try {
				String html = this.retrieveHtml(preferences, dataURL);
				request.setAttribute("content", html);
			} catch (Exception e) {
				request.setAttribute("content", "");
			}
		}
		super.doView(request, response);
	}
	private String retrieveHtml(PortletPreferences preferences,String dataURL) throws Exception{
		String ajaxData = "";
		String cacheMinutes = (String)preferences.getValue("cacheMinutes",defaultCacheMinutes);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		ajaxData = PortletCacheManager.getOnly().getCachedData(isCache, dataURL, cacheMinutes);
		return ajaxData;
	}
	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String isContent = preferences.getValue("isContent", "N");
		String contentId = preferences.getValue("contentId", null);
		String dataURL = preferences.getValue("dataURL", null);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String cacheMinutes = preferences.getValue("cacheMinutes", defaultCacheMinutes);
		
		request.setAttribute("isContent", isContent);
		request.setAttribute("contentId", contentId);
		request.setAttribute("dataURL", dataURL);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		request.setAttribute("isCache", isCache);
		request.setAttribute("cacheMinutes", cacheMinutes);
		
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String isContent = request.getParameter("isContent");
		String contentId = request.getParameter("contentId");
		String dataURL = request.getParameter("dataURL");
		String defaultVariableValues = request.getParameter("defaultVariableValues");
		String isCache = request.getParameter("isCache");
		String cacheMinutes = request.getParameter("cacheMinutes");
		
		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("isContent", isContent);
		preferWapper.setValue("contentId", contentId);
		preferWapper.setValue("dataURL", dataURL);
		preferWapper.setValue("defaultVariableValues", defaultVariableValues);
		preferWapper.setValue("isCache", isCache);
		preferWapper.setValue("cacheMinutes", cacheMinutes);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
}
