package com.agileai.portal.portlets.slider;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.portlets.PortletCacheManager;
import com.agileai.util.StringUtil;

public class FlashSlider extends GenericPotboyPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String sliderWidth = preferences.getValue("sliderWidth", null);
		String sliderHeight = preferences.getValue("sliderHeight", null);
		String textHeight = preferences.getValue("textHeight", null);
		String isContent = preferences.getValue("isContent", null);
		String contentId = preferences.getValue("contentId", null);
		String dataURL = preferences.getValue("dataURL", null);
		
		if (!StringUtil.isNullOrEmpty(sliderWidth) && !StringUtil.isNullOrEmpty(sliderHeight) 
				&& !StringUtil.isNullOrEmpty(textHeight) && !StringUtil.isNullOrEmpty(isContent) 
				&& (!StringUtil.isNullOrEmpty(contentId) || !StringUtil.isNullOrEmpty(dataURL))){
			request.setAttribute("isSetting", "Y");
		}else{
			request.setAttribute("isSetting", "N");
		}
		
		String isSetting = (String)request.getAttribute("isSetting");
		String jsonData = "";
		try {
			if ("Y".equals(isSetting)){
				if ("Y".equals(isContent)){
					String urlPrefix = PortletCacheManager.buildDataURLPefix(request);
					dataURL = urlPrefix+"/resource?ContentProvider&contentId="+contentId;
					jsonData = this.retrieveHtml(preferences, dataURL);
				}else{
					dataURL = getDataURL(request);
					jsonData = this.retrieveHtml(preferences, dataURL);
				}
				this.processJsonData(request, jsonData);
				int intSwfheight = Integer.parseInt(sliderHeight)+Integer.parseInt(textHeight);
				request.setAttribute("swfheight", String.valueOf(intSwfheight));
				request.setAttribute("sliderWidth", sliderWidth);
				request.setAttribute("sliderHeight", sliderHeight);
				request.setAttribute("textHeight", textHeight);
			}
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
		}
		
	
		super.doView(request, response);
	}
	
	private void processJsonData(RenderRequest request,String jsonData) throws JSONException{
		JSONArray jsonArray = new JSONArray(jsonData);
		int length = jsonArray.length();
		StringBuffer pics = new StringBuffer();
		StringBuffer links = new StringBuffer();
		StringBuffer texts = new StringBuffer();
		for (int i=0;i < length;i++){
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			String pic = jsonObject.getString("image");
			String link = jsonObject.getString("link");
			String text = jsonObject.getString("text");
			pics.append(pic).append("|");
			links.append(link).append("|");
			texts.append(text).append("|");
		}
		if (pics.length() > 0){
			request.setAttribute("pics", pics.substring(0,pics.length()-1));
		}
		if (links.length() > 0){
			request.setAttribute("links", links.substring(0,links.length()-1));
		}
		if (texts.length() > 0){
			request.setAttribute("texts", texts.substring(0,texts.length()-1));
		}
	}
	
	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String sliderWidth = preferences.getValue("sliderWidth", null);
		String sliderHeight = preferences.getValue("sliderHeight", null);
		String textHeight = preferences.getValue("textHeight", "18");
		String isContent = preferences.getValue("isContent", "Y");
		String contentId = preferences.getValue("contentId", null);
		String dataURL = preferences.getValue("dataURL", null);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String cacheMinutes = preferences.getValue("cacheMinutes", defaultCacheMinutes);
		
		request.setAttribute("sliderWidth", sliderWidth);
		request.setAttribute("sliderHeight", sliderHeight);
		request.setAttribute("textHeight", textHeight);
		request.setAttribute("isContent", isContent);
		request.setAttribute("contentId", contentId);
		request.setAttribute("dataURL", dataURL);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		request.setAttribute("isCache", isCache);
		request.setAttribute("cacheMinutes", cacheMinutes);
		
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String sliderWidth = request.getParameter("sliderWidth");
		String sliderHeight = request.getParameter("sliderHeight");
		String textHeight = request.getParameter("textHeight");
		String isContent = request.getParameter("isContent");
		String contentId = request.getParameter("contentId");
		String dataURL = request.getParameter("dataURL");
		String defaultVariableValues = request.getParameter("defaultVariableValues");
		String isCache = request.getParameter("isCache");
		String cacheMinutes = request.getParameter("cacheMinutes");
		
		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("sliderWidth", sliderWidth);
		preferWapper.setValue("sliderHeight", sliderHeight);
		preferWapper.setValue("textHeight", textHeight);
		preferWapper.setValue("isContent", isContent);
		preferWapper.setValue("contentId", contentId);
		preferWapper.setValue("dataURL", dataURL);
		preferWapper.setValue("defaultVariableValues", defaultVariableValues);
		preferWapper.setValue("isCache", isCache);
		preferWapper.setValue("cacheMinutes", cacheMinutes);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	
	private String retrieveHtml(PortletPreferences preferences,String dataURL) throws Exception{
		String ajaxData = "";
		String cacheMinutes = (String)preferences.getValue("cacheMinutes",defaultCacheMinutes);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		ajaxData = PortletCacheManager.getOnly().getCachedData(isCache, dataURL, cacheMinutes);
		return ajaxData;
	}
}
