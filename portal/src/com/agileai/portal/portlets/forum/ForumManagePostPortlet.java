package com.agileai.portal.portlets.forum;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;
import com.agileai.portal.driver.Resource;
import com.agileai.portal.driver.common.PortletRequestHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.extend.forum.ForumPrivilegeHelper;
import com.agileai.portal.portlets.BaseMashupPortlet;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class ForumManagePostPortlet extends BaseMashupPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		
		String contentIdParamKey = preferences.getValue("contentIdParamKey", null);
		
		Principal principal = request.getUserPrincipal();
		String currentUserId = principal.getName();

		String contentIdDefValue = preferences.getValue("contentIdDefValue", null);
		
		ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
		HttpServletRequest httpServletRequest = PortletRequestHelper.getRequestContext(request).getServletRequest();
		String contentId = httpServletRequest.getParameter(contentIdParamKey);
		if (StringUtil.isNullOrEmpty(contentId)) {
			contentId = contentIdDefValue;
		}
		
		String fciId = forumSectionInfoTreeManage.getFciIdRecord(contentId);
		if(fciId != null){
			Profile profile = (Profile)request.getUserPrincipal();
			User user = (User)profile.getUser();
			ForumPrivilegeHelper forumPrivilegeHelper = new ForumPrivilegeHelper(user);
			if(forumPrivilegeHelper.isModerator(fciId) || forumPrivilegeHelper.isForumAdmin()){
				request.setAttribute("showManageBtn", "showManageBtn");
			}else{
				request.setAttribute("showManageBtn", "disShowManageBtn");
			}
			if(forumPrivilegeHelper.isGuest()){
				request.setAttribute("isGuestId", "isGuestId");
			}
			
			DataRow managePostInfoRow = forumSectionInfoTreeManage.getManagePostInfoRecord(contentId);
			int count = forumSectionInfoTreeManage.getReplyCount(contentId);
			String fpmTypeformSelect = managePostInfoRow.getString("FPM_TYPE");
			String fpmTypeName = managePostInfoRow.getString("FPM_TYPE_NAME");
			String fpmTitle = managePostInfoRow.getString("FPM_TITLE");
			String fpmState = managePostInfoRow.getString("FPM_STATE");
			String fpmReport = managePostInfoRow.getString("FPM_REPORT");
			String fuProfile = managePostInfoRow.getString("FU_PROFILE");
			String fuCode= managePostInfoRow.getString("FU_CODE");
			String userCode = user.getUserCode();
			
			if(userCode.equals(fuCode) ||forumPrivilegeHelper.isModerator(fciId) || forumPrivilegeHelper.isForumAdmin()){
				request.setAttribute("showDelBtn", "showDelBtn");
			}else{
				request.setAttribute("showDelBtn", "disshowDelBtn");
			}
			
			if("HAS_END".equals(fpmState) && userCode.equals(fuCode)){
				request.setAttribute("hasEndBtn", "hasEndBtn");
			}else{
				request.setAttribute("hasEndBtn", "disHasEndBtn");
			}
			
			String fpmContent = managePostInfoRow.getString("FPM_CONTENT");
			
			if("HAS_REPORT".equals(fpmReport)){
				request.setAttribute("hasReport", "hasReport");
			}else if("HAS_CONFIRM_REPORT".equals(fpmReport)){
				request.setAttribute("hasReport", "hasConfirmReport");
			}else{
				request.setAttribute("hasReport", "disHasReport");
			}
			
			Date fpmCreateTime = (Date) managePostInfoRow.get("FPM_CREATE_TIME");
			int fpmClickNumber = managePostInfoRow.getInt("FPM_CLICK_NUMBER");
			String fpmRewardPoints = managePostInfoRow.getString("FPM_REWARD_POINTS");
			String fuLevel = managePostInfoRow.getString("CODE_NAME");
			String fuIntegral = managePostInfoRow.getString("FU_INTEGRAL");
			String fuName = managePostInfoRow.getString("FU_NAME");
			String fpmExpression = managePostInfoRow.getString("FPM_EXPRESSION");
			
			List<DataRow> recordList = forumSectionInfoTreeManage.findforumReplrtRecords(contentId);
			String forumReplrtRecords ="";
			for(int i=0;i<recordList.size();i++){
				DataRow row = recordList.get(i);
				String frrTime = row.stringValue("FRR_TIME");
				if(!frrTime.isEmpty()){
					frrTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL,row.getTimestamp("FRR_TIME"));
					forumReplrtRecords = forumReplrtRecords+"&nbsp;" +"&nbsp;举报类型:&nbsp;"+row.getString("CODE_NAME")+"&nbsp;举报人:&nbsp;"+row.getString("FU_NAME")+"&nbsp;举报时间:&nbsp;"+frrTime+"<br/>";
				}
			}
			request.setAttribute("forumReplrtRecords", forumReplrtRecords);
			request.setAttribute("fpmTitle", fpmTitle);
			request.setAttribute("fpmTypeCode", fpmTypeformSelect);
			request.setAttribute("fpmTypeName", fpmTypeName);
			request.setAttribute("fpmContent", fpmContent);
			request.setAttribute("fpmCreateTime", fpmCreateTime);
			request.setAttribute("fpmClickNumber", fpmClickNumber);
			request.setAttribute("fpmRewardPoints", fpmRewardPoints);
			request.setAttribute("fuLevel", fuLevel);
			request.setAttribute("fuIntegral", fuIntegral);
			request.setAttribute("fuName", fuName);
			
			if("".equals(fuProfile) || null == fuProfile || "null".equals(fuProfile)){
				request.setAttribute("imgUrl", "/portal/images/user_logined.png");
			}else{
				request.setAttribute("imgUrl", fuProfile);
			}
			request.setAttribute("count", count);
			request.setAttribute("fpmExpression", fpmExpression);
			
		}else{
			request.setAttribute("imgUrl", "/portal/images/user_logined.png");
			request.setAttribute("fpmContent", "该贴已被删除！");
		}
		request.setAttribute("fpmId", contentId);
		request.setAttribute("currentUserId", currentUserId);
		request.setAttribute("portletId", request.getWindowID());

		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String contentIdParamKey = preferences.getValue("contentIdParamKey", null);
		String contentIdDefValue = preferences.getValue("contentIdDefValue", null);

		request.setAttribute("contentIdParamKey", contentIdParamKey);
		request.setAttribute("contentIdDefValue", contentIdDefValue);
				
		super.doEdit(request, response);
	}


	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String contentIdParamKey = request.getParameter("contentIdParamKey");
		String contentIdDefValue = request.getParameter("contentIdDefValue");

		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("contentIdParamKey", contentIdParamKey);
		preferWapper.setValue("contentIdDefValue", contentIdDefValue);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	
	@Resource(id="load")
	public void load(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		String fpmId = request.getParameter("FPM_ID");
		ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
		int fpmClickNumber = forumSectionInfoTreeManage.getFpmClickNumber(fpmId);
		if(String.valueOf(fpmClickNumber) != null){
			fpmClickNumber = fpmClickNumber + 1;
		}else{
			fpmClickNumber = 0;
		}
		forumSectionInfoTreeManage.updateFpmClickNumberRecord(fpmId,fpmClickNumber);
	}
}
