package com.agileai.portal.portlets.forum;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;
import com.agileai.portal.driver.Resource;
import com.agileai.portal.driver.common.PortletRequestHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.extend.forum.ForumPrivilegeHelper;
import com.agileai.portal.portlets.BaseMashupPortlet;
import com.agileai.util.StringUtil;

public class ForumPublishPostPortlet extends BaseMashupPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException, JSONException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
		
		String contentIdParamKey = preferences.getValue("contentIdParamKey", null);
		Principal principal = request.getUserPrincipal();
		String currentUserId = principal.getName();

		String contentIdDefValue = preferences.getValue("contentIdDefValue", null);
		String fciIdDefValue = preferences.getValue("fciIdDefValue", null);
		
		if (!StringUtil.isNullOrEmpty(contentIdParamKey) && !StringUtil.isNullOrEmpty(contentIdDefValue) 
				&& !StringUtil.isNullOrEmpty(fciIdDefValue)){
			request.setAttribute("isSetting", "Y");
		}else{
			request.setAttribute("isSetting", "N");
		}
		
		Profile profile = (Profile)request.getUserPrincipal();
		User user = (User)profile.getUser();
		ForumPrivilegeHelper forumPrivilegeHelper = new ForumPrivilegeHelper(user);
		if(forumPrivilegeHelper.isGuest()){
			request.setAttribute("isGuestId", "isGuestId");
		}
		String isSetting = (String)request.getAttribute("isSetting");
		if ("Y".equals(isSetting)){
			HttpServletRequest httpServletRequest = PortletRequestHelper.getRequestContext(request).getServletRequest();
			String contentId = httpServletRequest.getParameter(contentIdParamKey);
			String fpmExpression = httpServletRequest.getParameter("fpmExpression");
			
			if(StringUtil.isNullOrEmpty(contentId)){
				contentId = contentIdDefValue;
			}
			String fciPid = forumSectionInfoTreeManage.getFciPid(contentId);
			String fciId = httpServletRequest.getParameter("fciId");
			if(null == fciId ||StringUtil.isNullOrEmpty(fciId)){
				fciId = fciIdDefValue;
			}else{
				fciPid = fciId;
			}
			List<DataRow> moduleRecords = forumSectionInfoTreeManage.findModuleFormSelectRecords(fciPid);
			
			String fciMotto = forumSectionInfoTreeManage.getFciMottoRecord(fciIdDefValue);
			JSONObject jsObject = new JSONObject(fciMotto);
			String menuCode = (String) jsObject.get("menuCode");
			
			FormSelect moduleFormSelect = new FormSelect();
			moduleFormSelect.setKeyColumnName("FCI_ID");
			moduleFormSelect.setValueColumnName("FCI_NAME");
			moduleFormSelect.putValues(moduleRecords);
			moduleFormSelect.setSelectedValue(fciPid);
			request.setAttribute("moduleFciName",moduleFormSelect);
			
			request.setAttribute("fpmExpression", fpmExpression);
			request.setAttribute("fpmId", contentId);
			request.setAttribute("menuCode", menuCode);
			request.setAttribute("currentUserId", currentUserId);
			request.setAttribute("portletId", request.getWindowID());
		}
		super.doView(request, response);
	}

	@RenderMode(name = "edit") 
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String contentIdParamKey = preferences.getValue("contentIdParamKey", null);
		String contentIdDefValue = preferences.getValue("contentIdDefValue", null);
		String fciIdDefValue = preferences.getValue("fciIdDefValue", null);
		
		request.setAttribute("contentIdParamKey", contentIdParamKey);
		request.setAttribute("contentIdDefValue", contentIdDefValue);
		request.setAttribute("fciIdDefValue", fciIdDefValue);
		
		super.doEdit(request, response);
	}


	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String contentIdParamKey = request.getParameter("contentIdParamKey");
		String contentIdDefValue = request.getParameter("contentIdDefValue");
		String fciIdDefValue = request.getParameter("fciIdDefValue");

		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("contentIdParamKey", contentIdParamKey);
		preferWapper.setValue("contentIdDefValue", contentIdDefValue);
		preferWapper.setValue("fciIdDefValue", fciIdDefValue);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	
	@Resource(id="changeFormSelect")
	public void changeFormSelect(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException, JSONException {
		ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
		String fciId = request.getParameter("fciId");
		String fpmId = request.getParameter("fpmId");
		List<DataRow> records = forumSectionInfoTreeManage.findFciIdFormSelectRecords(fciId);
		
		FormSelect formSelect = new FormSelect();
		formSelect.setKeyColumnName("FCI_ID");
		formSelect.setValueColumnName("FCI_NAME");
		formSelect.putValues(records);
		formSelect.setSelectedValue(fpmId);
		
		String responsText = formSelect.getSyntax();
		
		PrintWriter writer = response.getWriter();
		writer.print(responsText);
		writer.close();
	}
}
