package com.agileai.portal.portlets.forum;

import java.io.IOException;
import java.security.Principal;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;
import com.agileai.portal.driver.common.PortletRequestHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.extend.forum.ForumPrivilegeHelper;
import com.agileai.portal.portlets.BaseMashupPortlet;
import com.agileai.util.StringUtil;

public class ForumRepliesPostInfoPortlet extends BaseMashupPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		
		String contentIdParamKey = preferences.getValue("contentIdParamKey", null);
		
		Principal principal = request.getUserPrincipal();
		String currentUserId = principal.getName();

		String contentIdDefValue = preferences.getValue("contentIdDefValue", null);
		HttpServletRequest httpServletRequest = PortletRequestHelper.getRequestContext(request).getServletRequest();
		String contentId = httpServletRequest.getParameter(contentIdParamKey);
		if (StringUtil.isNullOrEmpty(contentId)) {
			contentId = contentIdDefValue;
		}
		ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
		DataRow postInfoRow = forumSectionInfoTreeManage.getForumPostMessageRecord(contentId);
		String fpmTitle = "";
		if(postInfoRow != null){
		String fpmState = postInfoRow.getString("FPM_STATE");
		if("HAS_END".equals(fpmState)){
			request.setAttribute("hasEndBtn", "hasEndBtn");
		}else{
			request.setAttribute("hasEndBtn", "disHasEndBtn");
		}
			fpmTitle = postInfoRow.getString("FPM_TITLE");
		}
		Profile profile = (Profile)request.getUserPrincipal();
		User user = (User)profile.getUser();
		ForumPrivilegeHelper forumPrivilegeHelper = new ForumPrivilegeHelper(user);
		if(forumPrivilegeHelper.isGuest()){
			request.setAttribute("isGuestId", "isGuestId");
		}
		request.setAttribute("fpmId", contentId);
		request.setAttribute("fpmTitle", fpmTitle);
		request.setAttribute("currentUserId", currentUserId);
		request.setAttribute("portletId", request.getWindowID());

		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String contentIdParamKey = preferences.getValue("contentIdParamKey", null);
		String contentIdDefValue = preferences.getValue("contentIdDefValue", null);

		request.setAttribute("contentIdParamKey", contentIdParamKey);
		request.setAttribute("contentIdDefValue", contentIdDefValue);
				
		super.doEdit(request, response);
	}


	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String contentIdParamKey = request.getParameter("contentIdParamKey");
		String contentIdDefValue = request.getParameter("contentIdDefValue");

		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("contentIdParamKey", contentIdParamKey);
		preferWapper.setValue("contentIdDefValue", contentIdDefValue);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	
}
