package com.agileai.portal.portlets.sso;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.sso.SSOPrincipal;
import com.agileai.portal.bizmoduler.sso.UserSSOService;
import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.PortletBean;
import com.agileai.portal.driver.Resource;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.util.StringUtil;

public class SSOListPortlet extends GenericPotboyPortlet{
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String divClass = preferences.getValue("divClass", "FlatMenuList");

		if (!StringUtil.isNullOrEmpty(divClass)){
			request.setAttribute("isSetting", "Y");
		}else{
			request.setAttribute("isSetting", "N");
		}
		
		String isSetting = (String)request.getAttribute("isSetting");
		if ("Y".equals(isSetting)){
			Profile profile = (Profile)request.getPortletSession().getAttribute(Profile.PROFILE_KEY,PortletSession.APPLICATION_SCOPE);
			User user = (User)profile.getUser();
			UserSSOService userSSOService = this.lookupService(UserSSOService.class);
			String userId = getUserId(request);
			List<DataRow> records = userSSOService.findViewApplications(userId);
			List<DataRow> filtedRecords = new ArrayList<DataRow>();
			String resType = com.agileai.hotweb.domain.core.Resource.Type.Application;
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String appId = row.stringValue("APP_ID");
				if (user.containResouce(resType, appId)){
					filtedRecords.add(row);
				}
			}
			PortletBean portletBean = new PortletBean();
			portletBean.setRsList(filtedRecords);
			request.setAttribute("pageBean",portletBean);
			request.setAttribute("divClass", divClass);
		}
		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String divClass = preferences.getValue("divClass", "FlatMenuList");
		request.setAttribute("divClass", divClass);

		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String divClass = request.getParameter("divClass");
		
		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("divClass", divClass);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}	
	
	@Resource(id="processSSO")
	public void processSSO(ResourceRequest request,ResourceResponse response) throws IOException{
		PrintWriter writer = response.getWriter();
		SSOPrincipal principal = new SSOPrincipal();
		String userId = getUserId(request);
		principal.setUserId(userId);
		principal.setUserName(userId);
		PortletSession session = request.getPortletSession(); 
		session.setAttribute(SSOPrincipal.class.getName(), principal,PortletSession.APPLICATION_SCOPE);
		writer.write("success");
		writer.close();
	}
	
	private String getUserId(PortletRequest request){
		String result = "";
		Principal principal = request.getUserPrincipal();
		if (principal != null){
			result = principal.getName();
		}
		return result;
	}
}
