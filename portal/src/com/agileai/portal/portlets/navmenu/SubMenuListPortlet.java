package com.agileai.portal.portlets.navmenu;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;

import com.agileai.hotweb.common.BeanFactory;
import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.driver.model.MenuItem;
import com.agileai.portal.driver.service.PortalConfigService;
import com.agileai.portal.portlets.PortletCacheManager;
import com.agileai.util.StringUtil;

public class SubMenuListPortlet extends GenericPotboyPortlet {
	private static final String NEW_LINE = "\r\n";
	
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String isFromMenuList = preferences.getValue("isFromMenuList", null);
		String parentMenuId = preferences.getValue("parentMenuId", null);
		String dataURL = preferences.getValue("dataURL", null);

		if (!StringUtil.isNullOrEmpty(isFromMenuList) 
				&& (!StringUtil.isNullOrEmpty(parentMenuId) || !StringUtil.isNullOrEmpty(dataURL))){
			request.setAttribute("isSetting", "Y");
		}else{
			request.setAttribute("isSetting", "N");
		}
		
		String isSetting = (String)request.getAttribute("isSetting");
		if ("Y".equals(isSetting)){
			if ("Y".equals(isFromMenuList)){
				PortalConfigService portalConfigService = (PortalConfigService)BeanFactory.instance().getBean("PortalConfigService");
				MenuItem currentItem = this.getCurrentMenuItem(request);
				MenuItem parentMenuItem = portalConfigService.getMenuItem(parentMenuId);
				List<MenuItem> menuItemList = parentMenuItem.getChildren();
				String html = this.buildHtml(currentItem,menuItemList);
				request.setAttribute("content", html);
			}else{
				try {
					String html = this.retrieveHtml(preferences, request);
					request.setAttribute("content", html);
				} catch (Exception e) {
					request.setAttribute("content", "");
				}
			}
			request.setAttribute("isFromMenuList", isFromMenuList);
		}
		super.doView(request, response);
	}
	
	private String buildHtml(MenuItem currentItem,List<MenuItem> menuItemList){
		String result = "";
		StringBuffer temp = new StringBuffer();
		temp.append("<div>").append(NEW_LINE);
		temp.append("	<ul>").append(NEW_LINE);
		for (int i=0;i < menuItemList.size();i++){
			MenuItem tempItem = menuItemList.get(i);
			String menuName = tempItem.getName();
			String menuCode = tempItem.getCode();
			if (currentItem.getCode() != null && currentItem.getCode().equals(menuCode)){
				temp.append("		<li class='selected'><a  hidefocus='true' href='").append(menuCode).append(".ptml'>").append(menuName).append("</a></li>").append(NEW_LINE);
			}else{
				temp.append("		<li><a hidefocus='true' href='").append(menuCode).append(".ptml'>").append(menuName).append("</a></li>").append(NEW_LINE);				
			}
		}
		temp.append("	</ul>").append(NEW_LINE);
		temp.append("</div>").append(NEW_LINE);
		result = temp.toString();
		return result;
	}
	private String retrieveHtml(PortletPreferences preferences,PortletRequest request) throws Exception{
		String result = "";
		String cacheMinutes = (String)preferences.getValue("cacheMinutes",defaultCacheMinutes);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String dataURL = getDataURL(request);
		result = PortletCacheManager.getOnly().getCachedData(isCache, dataURL, cacheMinutes);
		return result;
	}
	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String isFromMenuList = preferences.getValue("isFromMenuList", "Y");
		String parentMenuId = preferences.getValue("parentMenuId", null);
		String dataURL = preferences.getValue("dataURL", null);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);

		request.setAttribute("isFromMenuList", isFromMenuList);
		request.setAttribute("parentMenuId", parentMenuId);
		request.setAttribute("dataURL", dataURL);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
				
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String isFromMenuList = request.getParameter("isFromMenuList");
		String parentMenuId = request.getParameter("parentMenuId");
		String dataURL = request.getParameter("dataURL");
		String defaultVariableValues = request.getParameter("defaultVariableValues");

		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("isFromMenuList", isFromMenuList);
		preferWapper.setValue("parentMenuId", parentMenuId);
		preferWapper.setValue("dataURL", dataURL);
		preferWapper.setValue("defaultVariableValues", defaultVariableValues);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
}
