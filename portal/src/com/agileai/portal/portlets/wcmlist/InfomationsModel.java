package com.agileai.portal.portlets.wcmlist;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataRow;

public class InfomationsModel {
	public static class RecordKeys {
		public static String id = "id";
		public static String title = "title";
		public static String shortTitle = "shortTitle";
		public static String readCount = "readCount";
		public static String publishTime = "publishTime";
	}
	
	private String columnId = null;
	private String columnName = null;
	private String columnTitle = null;

	private String searchParam = null;
	private String keyWordName = null;
	
	private String infoLinkURL = null;
	private String moreLinkURL = null;
	
	private List<DataRow> recordList = new ArrayList<DataRow>();

	public String getColumnId() {
		return columnId;
	}

	public void setColumnId(String columnId) {
		this.columnId = columnId;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnTitle() {
		return columnTitle;
	}

	public void setColumnTitle(String columnTitle) {
		this.columnTitle = columnTitle;
	}

	public String getInfoLinkURL() {
		return infoLinkURL;
	}

	public void setInfoLinkURL(String infoLinkURL) {
		this.infoLinkURL = infoLinkURL;
	}

	public String getMoreLinkURL() {
		return moreLinkURL;
	}

	public void setMoreLinkURL(String moreLinkURL) {
		this.moreLinkURL = moreLinkURL;
	}

	public List<DataRow> getRecordList() {
		return recordList;
	}

	public String getSearchParam() {
		return searchParam;
	}

	public void setSearchParam(String searchParam) {
		this.searchParam = searchParam;
	}

	public String getKeyWordName() {
		return keyWordName;
	}

	public void setKeyWordName(String keyWordName) {
		this.keyWordName = keyWordName;
	}
}
