package com.agileai.portal.portlets.iframe;

import java.io.IOException;
import java.util.HashMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.pluto.container.PortletRequestContext;
import org.codehaus.jettison.json.JSONObject;
import org.mvel2.templates.TemplateRuntime;

import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.common.PortletRequestHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.driver.model.MenuBar;
import com.agileai.portal.driver.model.Theme;
import com.agileai.util.StringUtil;

public class VirtualIframePortlet extends GenericPotboyPortlet {
	
	@SuppressWarnings("rawtypes")
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String iframeScroll = preferences.getValue("iframeScroll", "no");
		String autoExtend = preferences.getValue("autoExtend", "false");
		String iframeHeight = preferences.getValue("iframeHeight", "400");
		HttpServletRequest httpServletRequest = PortletRequestHelper.getRequestContext(request).getServletRequest();
		String currentVirtualMenuId = httpServletRequest.getParameter(MenuBar.VIRTUAL_MENU_KEY);
		HttpSession session = httpServletRequest.getSession(false);
		HashMap<String,JSONObject> jsonMap = MenuBar.getMenuJSONObjectMap(session);
		JSONObject jsonObject = jsonMap.get(currentVirtualMenuId);
		try {
			String url = jsonObject.getString("url");
			String iframeSrc = url;
			if (iframeHeight.indexOf(Theme.ThemeBodyHeight) > -1){
				String template = iframeHeight;
				HashMap vars = this.buildVarMap(request);
				iframeHeight = String.valueOf(TemplateRuntime.eval(template, vars));
				iframeScroll = "yes";
			}else{
				if (jsonObject.has("height") && StringUtil.isNotNullNotEmpty(jsonObject.getString("height"))){
					iframeHeight = jsonObject.getString("height");
				}				
			}
			request.setAttribute("iframeHeight",iframeHeight);
			request.setAttribute("iframeSrc",iframeSrc);
			request.setAttribute("iframeScroll", iframeScroll);
			request.setAttribute("autoExtend", autoExtend);
			super.doView(request, response);	
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(),e);
		}
	}
	
	@SuppressWarnings({"rawtypes", "unchecked" })
	private HashMap buildVarMap(PortletRequest request){
		HashMap result = new HashMap();
		PortletRequestContext portletRequestContext = PortletRequestHelper.getRequestContext(request);
		HttpServletRequest httpRequest = portletRequestContext.getContainerRequest();
		HttpSession portalSession = httpRequest.getSession();
		Theme theme = (Theme)portalSession.getAttribute(Theme.class.getName());
		String height = (String)portalSession.getAttribute(Theme.WindowScreenHeight);
		String outherHeight = theme.getOuterHeight();
		String themeBodyHeight = String.valueOf(Integer.parseInt(height)- Integer.parseInt(outherHeight));
		result.put(Theme.ThemeBodyHeight, themeBodyHeight);
		return result;
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String iframeScroll = preferences.getValue("iframeScroll", "no");
		String autoExtend = preferences.getValue("autoExtend", "false");
		String iframeHeight = preferences.getValue("iframeHeight", "400");
		
		request.setAttribute("iframeHeight", iframeHeight);
		request.setAttribute("iframeScroll", iframeScroll);
		request.setAttribute("autoExtend", autoExtend);
		
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String iframeHeight = request.getParameter("iframeHeight");
		String iframeScroll = StringUtil.isNullOrEmpty(request.getParameter("iframeScroll"))?"no":"yes";
		String autoExtend = StringUtil.isNullOrEmpty(request.getParameter("autoExtend"))?"false":"true";
		
		PreferencesWrapper preferWapper = new PreferencesWrapper();
		preferWapper.setValue("iframeHeight", iframeHeight);
		preferWapper.setValue("iframeScroll", iframeScroll);
		preferWapper.setValue("autoExtend", autoExtend);
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	
}