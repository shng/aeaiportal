package com.agileai.portal.bizmoduler.auth;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.BaseInterface;

public interface SecurityUserMobileManage
        extends BaseInterface {
	public DataRow retrieveUserInfoByOpenId(String wxOpenId);
	public boolean bindUserWxOpenId(String userCode,String wxOpenId);
	public boolean unbindUserWxOpenId(String userCode);
	public List<DataRow> retrieveRegistryUserByUserCode(String userCode);
}

