package com.agileai.portal.bizmoduler.auth;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.BaseService;

public class SecurityUserMobileMangeImpl
        extends BaseService
        implements SecurityUserMobileManage {

	public SecurityUserMobileMangeImpl() {
        super();
    }

	@Override
	public DataRow retrieveUserInfoByOpenId(String wxOpenId) {
		String statementId = this.sqlNameSpace+".getUserCodeRecord";
		DataParam param = new DataParam("wxOpenId",wxOpenId);
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public boolean bindUserWxOpenId(String userCode, String wxOpenId) {
		String statementId = this.sqlNameSpace+".updateWxOpenIdRecord";
		DataParam param = new DataParam("userCode",userCode,"wxOpenId",wxOpenId);
		this.daoHelper.updateRecord(statementId, param);
		return true;
	}

	@Override
	public boolean unbindUserWxOpenId(String userCode) {
		String statementId = this.sqlNameSpace+".updateWxOpenIdRecord";
		DataParam param = new DataParam("userCode",userCode,"wxOpenId",null);
		this.daoHelper.updateRecord(statementId, param);
		return true;
	}

	@Override
	public List<DataRow> retrieveRegistryUserByUserCode(String userCode) {
		String statementId = this.sqlNameSpace+".findUserGroupCodes";
		DataParam param = new DataParam("userCode",userCode);
		List<DataRow> records =  this.daoHelper.queryRecords(statementId, param);
		return records;
	}
}
