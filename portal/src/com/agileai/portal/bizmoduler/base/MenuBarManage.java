package com.agileai.portal.bizmoduler.base;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeManage;

public interface MenuBarManage extends TreeManage{
	boolean isCodeDuplicate(String navId,String menuId,String menuCode);
	List<DataRow> findMenuReocrds(List<String> modalIdList);
	List<DataRow> findMenuInfos(List<String> modalIdList,String navId);
}
