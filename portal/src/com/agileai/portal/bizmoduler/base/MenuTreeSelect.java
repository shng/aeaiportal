package com.agileai.portal.bizmoduler.base;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeSelectService;


public interface  MenuTreeSelect extends TreeSelectService {
	public List<DataRow> queryMobileMenuTreeRecords(String navId,String pagepro,String moduleCodes);
	public List<DataRow> queryPageMenuTreeRecords(String menuPid,String navId,String pagepro,String moduleCodes);
}
