package com.agileai.portal.bizmoduler.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class PtPageTemptManageImpl
        extends StandardServiceImpl
        implements PtPageTemptManage {
    public PtPageTemptManageImpl() {
        super();
    }
	@Override
	public void updateContent(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateContent";
		this.daoHelper.updateRecord(statementId, param);
	}
}
