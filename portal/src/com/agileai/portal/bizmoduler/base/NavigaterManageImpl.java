package com.agileai.portal.bizmoduler.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.hotweb.domain.core.Resource;
import com.agileai.portal.driver.service.PortalConfigService;

public class NavigaterManageImpl extends StandardServiceImpl 
	implements NavigaterManage {
	
	public NavigaterManageImpl(){
		super();
	}
	protected void processPrimaryKeys(DataParam param){
	}
	public void updateRecord(DataParam param) {
		super.updateRecord(param);
		PortalConfigService portalConfigService = (PortalConfigService)this.lookupService("PortalConfigService");
		String navigaterId = param.get("NAV_ID");
		portalConfigService.refreshMenuBar(navigaterId);
	}
	
	public void deletRecord(DataParam param) {
		super.deletRecord(param);
		
		DataParam delParam = new DataParam();
		String navId = param.get("NAV_ID");
		delParam.put("RES_TYPE",Resource.Type.Navigater);
		delParam.put("RES_ID",navId);
		
		String statementId = "SecurityAuthorizationConfig.delRoleAuthRelation";
		this.daoHelper.deleteRecords(statementId, delParam);
		
		statementId = "SecurityAuthorizationConfig.delUserAuthRelation";
		this.daoHelper.deleteRecords(statementId, delParam);
		
		statementId = "SecurityAuthorizationConfig.delGroupAuthRelation";
		this.daoHelper.deleteRecords(statementId, delParam);
	}
	
	public void savePersonalThemes(String navId,String personalThemes){
		String statementId = "menubar.savePersonalThemes";
		DataParam param = new DataParam();
		param.put("NAV_ID",navId,"PERSONAL_THEMES",personalThemes);
		this.daoHelper.updateRecord(statementId, param);
	}	
}