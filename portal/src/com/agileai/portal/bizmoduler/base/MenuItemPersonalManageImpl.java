package com.agileai.portal.bizmoduler.base;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.BaseService;
import com.agileai.portal.bizmoduler.MenuItemPersonalManage;
import com.agileai.portal.driver.model.PersonalSetting;
import com.agileai.util.StringUtil;

public class  MenuItemPersonalManageImpl extends BaseService implements MenuItemPersonalManage{

	@Override
	public void savePersonalSetting(String userCode, String menuItemId,
			String personalSetting) {
		DataRow row = this.getRecord(userCode, menuItemId);
		if (row != null && !row.isEmpty()){
			String statementId = this.sqlNameSpace + ".updateUserPersonalRecord";
			DataParam dataParam = new DataParam();
			dataParam.put("MENU_ID",menuItemId);
			dataParam.put("USER_CODE",userCode);
			dataParam.put("USER_PERSONAL",personalSetting);
			this.daoHelper.updateRecord(statementId,dataParam);		
		}else{
			String statementId = this.sqlNameSpace + ".insertRecord";
			String perId = KeyGenerator.instance().genKey();
			DataParam dataParam = new DataParam();
			dataParam.put("PER_ID",perId);
			dataParam.put("MENU_ID",menuItemId);
			dataParam.put("USER_CODE",userCode);
			dataParam.put("USER_PERSONAL",personalSetting);
			dataParam.put("USE_PERSONAL","Y");	
			this.daoHelper.insertRecord(statementId,dataParam);
		}
	}
	
	@Override
	public void savePersonalSetting(String userCode,String menuItemId,PersonalSetting personalSetting) {
		String jsonString = null;
		ObjectMapper objectMapper = new ObjectMapper();
		if (StringUtil.isNullOrEmpty(personalSetting.getPersonalId())){
			String statementId = this.sqlNameSpace + ".insertRecord";
			String perId = KeyGenerator.instance().genKey();
			personalSetting.setPersonalId(perId);
			personalSetting.setUserCode(userCode);
			personalSetting.setMenuId(menuItemId);
			try {
				jsonString = objectMapper.writeValueAsString(personalSetting);				
			} catch (Exception e) {
				e.printStackTrace();
			}
			DataParam dataParam = new DataParam();
			dataParam.put("PER_ID",perId);
			dataParam.put("MENU_ID",menuItemId);
			dataParam.put("USER_CODE",userCode);
			dataParam.put("USER_PERSONAL",jsonString);
			dataParam.put("USE_PERSONAL","Y");	
			this.daoHelper.insertRecord(statementId,dataParam);
		}else{
			String statementId = this.sqlNameSpace + ".updateUserPersonalRecord";
			personalSetting.setMenuId(menuItemId);
			personalSetting.setUserCode(userCode);
			try {
				jsonString = objectMapper.writeValueAsString(personalSetting);				
			} catch (Exception e) {
				e.printStackTrace();
			}
			DataParam dataParam = new DataParam();
			dataParam.put("MENU_ID",menuItemId);
			dataParam.put("USER_CODE",userCode);
			dataParam.put("USER_PERSONAL",jsonString);
			this.daoHelper.updateRecord(statementId,dataParam);			
		}			
	}
	
	@Override
	public void cleanPersonalSetting(String userCode,String menuItemId) {
		DataParam dataParam = new DataParam();
		dataParam.put("MENU_ID",menuItemId);
		dataParam.put("USER_CODE",userCode);
		String statementId = this.sqlNameSpace + ".deleteRecord";
		this.daoHelper.deleteRecords(statementId,dataParam);
	}

	@Override
	public void updateUsePersonal(String userCode,String menuItemId,boolean usePersonal) {
		String statementId = this.sqlNameSpace + ".updateUsePersonalRecord";
		DataParam dataParam = new DataParam();
		dataParam.put("MENU_ID",menuItemId);
		dataParam.put("USER_CODE",userCode);
		dataParam.put("USE_PERSONAL",usePersonal?"Y":"N");
		this.daoHelper.updateRecord(statementId,dataParam);
	}

	@Override
	public List<DataRow> findRecords(String userCode) {
		String statementId = this.sqlNameSpace + ".findRecords";
		DataParam queryParam = new DataParam("userCode",userCode);
		return this.daoHelper.queryRecords(statementId, queryParam);
	}

	@Override
	public DataRow getRecord(String userCode,
			String menuItemId) {
		String statementId = this.sqlNameSpace + ".getRecord";
		DataParam queryParam = new DataParam("userCode",userCode,"menuId",menuItemId);
		return this.daoHelper.getRecord(statementId, queryParam);
	}
}