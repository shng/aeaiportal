package com.agileai.portal.bizmoduler.base;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.BaseService;
import com.agileai.portal.driver.model.Decorator;
import com.agileai.portal.driver.model.LayoutContainer;
import com.agileai.portal.driver.model.Page;
import com.agileai.portal.driver.model.PortletWindowConfig;
import com.agileai.portal.filter.UserCacheManager;
import com.agileai.util.StringUtil;

public class PageManageImpl extends BaseService 
	implements PageManage {
	
	public PageManageImpl(){
		super();
	}

	public void savePageLayouts(Page page,boolean updateMainlayoutType) {
		String pageId = page.getPageId();
		String statementId = ""; 
				
		if (updateMainlayoutType){
			statementId = sqlNameSpace+"."+"updateMainLayout";
			DataParam param = new DataParam("MAIN_LAYOUT",page.getMainContainerType(),"PAGE_ID",pageId);
			this.daoHelper.updateRecord(statementId, param);	
		}
		
		statementId = sqlNameSpace+"."+"deletePagePortlets";
		this.daoHelper.deleteRecords(statementId, pageId);
		statementId = sqlNameSpace+"."+"deletePageChildLayouts";
		this.daoHelper.deleteRecords(statementId, pageId);

		List<DataParam> layouts = new ArrayList<DataParam>();
		List<DataParam> portlets = new ArrayList<DataParam>();
		this.buildLayouts8Portlets(page, layouts, portlets);
		
		statementId = sqlNameSpace+"."+"insertPageChildLayouts";
		this.daoHelper.batchInsert(statementId,layouts);
		
		statementId = sqlNameSpace+"."+"insertPagePortlets";
		this.daoHelper.batchInsert(statementId,portlets);
		
		UserCacheManager.getOnly().truncateUsers();
	}
   
	private void buildLayouts8Portlets(Page page,List<DataParam> layouts,List<DataParam> portlets){
		List<LayoutContainer> layoutContainerList = page.getLayoutContainers();
		for (int i=0;i < layoutContainerList.size();i++){
			LayoutContainer layoutContainer = layoutContainerList.get(i);
			DataParam layoutParam = new DataParam();
			layoutParam.put("GRID_ID",layoutContainer.getId());
			layoutParam.put("PAGE_ID",page.getPageId());
			layoutParam.put("GRID_PARENT_ID",null);
			layoutParam.put("GRID_CONTAINER_TYPE",layoutContainer.getContainerType());
			layoutParam.put("GRID_ORDER_NUMBER",String.valueOf(i));
			layoutParam.put("GRID_WIDTH",layoutContainer.getWidth());
			layoutParam.put("GRID_STYLE",layoutContainer.getStyle());
			layouts.add(layoutParam);
			
			if (layoutContainer.hasChildContainer()){
				this.loopLayouts8Portlets(layoutContainer, page, layouts, portlets);
			}else{
				List<PortletWindowConfig> pagePortletList = layoutContainer.getPortlets();
				if (pagePortletList != null && !pagePortletList.isEmpty()){
					for (int j=0;j < pagePortletList.size();j++){
						PortletWindowConfig pagePortlet = pagePortletList.get(j);
						DataParam portletParam = this.buildPorletRow(page, layoutContainer, pagePortlet,j);
						portlets.add(portletParam);
					}
				}
			}
		}
	}
	private void loopLayouts8Portlets(LayoutContainer curlayoutContainer,Page page,List<DataParam> layouts,List<DataParam> portlets) {
		List<LayoutContainer> childLayoutContainerList = curlayoutContainer.getChildContainerList();
		for (int i=0;i < childLayoutContainerList.size();i++){
			LayoutContainer tempLayoutContainer = childLayoutContainerList.get(i);
			DataParam layoutParam = new DataParam();
			layoutParam.put("GRID_ID",tempLayoutContainer.getId());
			layoutParam.put("PAGE_ID",page.getPageId());
			layoutParam.put("GRID_PARENT_ID",curlayoutContainer.getId());
			layoutParam.put("GRID_CONTAINER_TYPE",tempLayoutContainer.getContainerType());
			layoutParam.put("GRID_ORDER_NUMBER",String.valueOf(i));
			layoutParam.put("GRID_WIDTH",tempLayoutContainer.getWidth());
			layoutParam.put("GRID_STYLE",tempLayoutContainer.getStyle());
			layouts.add(layoutParam);
			if (tempLayoutContainer.hasChildContainer()){
				this.loopLayouts8Portlets(tempLayoutContainer, page,layouts,portlets);
			}else{
				List<PortletWindowConfig> pagePortletList = tempLayoutContainer.getPortlets();
				if (pagePortletList != null && !pagePortletList.isEmpty()){
					for (int j=0;j < pagePortletList.size();j++){
						PortletWindowConfig pagePortlet = pagePortletList.get(j);
						DataParam portletParam = this.buildPorletRow(page, tempLayoutContainer, pagePortlet,j);
						portlets.add(portletParam);
					}
				}
			}
		}
	}
	private DataParam buildPorletRow(Page page,LayoutContainer layoutContainer,PortletWindowConfig pagePortlet,int order){
		DataParam portletParam = new DataParam();
		portletParam.put("PTLET_ID",pagePortlet.getId());
		portletParam.put("PAGE_ID",page.getPageId());
		portletParam.put("PA_ID",pagePortlet.getPortletAppId());
		portletParam.put("GRID_ID",layoutContainer.getId());
		portletParam.put("PTLET_NAME",pagePortlet.getTitle());
		String decorator = pagePortlet.getDecoratorId();
		if (StringUtil.isNullOrEmpty(decorator)) decorator = PortletWindowConfig.DecoratorType.DEFAULT;
		portletParam.put("PTLET_DECORATOR",decorator);
		String height = pagePortlet.getHeight();
		if (StringUtil.isNullOrEmpty(height)) height = PortletWindowConfig.AUTO_HEIGHT;
		portletParam.put("PTLET_HEIGHT",height);
		portletParam.put("PTLET_ORDER",String.valueOf(order));
		portletParam.put("PTLET_PARAMS",pagePortlet.getParams());
		portletParam.put("PTLET_BODY_STYLE",pagePortlet.getBodyStyle());
		portletParam.put("PTLET_VISIABLE",pagePortlet.isVisiable()?"Y":"N");
		
		String decorParams = Decorator.buildDecortorString(pagePortlet.getDecorParams());
		portletParam.put("PTLET_DECOR_PARAMS",decorParams);
		
//		portletParam.put("PTLET_COLOR",pagePortlet.getColour());
//		portletParam.put("PTLET_ICON_PATH",pagePortlet.getIconPath());
//		portletParam.put("PTLET_MORE_URL",pagePortlet.getMoreURL());
//		portletParam.put("PTLET_ISCIRCULAR",pagePortlet.isCircular()?"Y":"N");
//		portletParam.put("PTLET_CANMAX",pagePortlet.isCanMax()?"Y":"N");
//		portletParam.put("PTLET_CANMIN",pagePortlet.isCanMin()?"Y":"N");
		return portletParam;
	}
}
