package com.agileai.portal.bizmoduler.base;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class PtThemeManageImpl extends StandardServiceImpl 
	implements PtThemeManage {
	
	public PtThemeManageImpl(){
		super();
	}

	@Override
	public List<DataRow> findPersonalRecords(DataParam param) {
		String comeFromNavEdit = param.get("comeFromNavEdit");
		DataParam queryDataParam = new DataParam();
		queryDataParam.append(param);
		if ("Y".equals(comeFromNavEdit)){
			queryDataParam.remove("navId");
			queryDataParam.remove("themeId");
		}
		String statementId = sqlNameSpace+"."+"findPersonalRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, queryDataParam);
		return result;
	}
}
