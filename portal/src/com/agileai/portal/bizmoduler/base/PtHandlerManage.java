package com.agileai.portal.bizmoduler.base;

import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface PtHandlerManage
        extends StandardService {
	public PtHandler getHandler(String handlerId);
}
