package com.agileai.portal.bizmoduler.lang;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface SysMultiLangManage
        extends StandardService {
	public void saveLangConf(String resouceType,DataParam param, List<DataParam> insertRecords, List<DataParam> updateRecords);
}
