package com.agileai.portal.bizmoduler.mobile;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.portal.driver.mobile.AngularWidget;

public class AngularWidgetManageImpl extends StandardServiceImpl
	implements AngularWidgetManage{

	@Override
	public AngularWidget retrieveAngularWidget(String widgetId) {
		AngularWidget result = null;
		if (AngularWidgetCache.containsKey(widgetId)){
			result = AngularWidgetCache.get(widgetId);
		}else{
			if (AngularWidgetCache.isEmpty()){
				List<DataRow> records = this.findRecords(new DataParam());
				if (records != null){
					for (int i=0;i < records.size();i++){
						DataRow row = records.get(i);
						if (row != null && row.size() > 0){
							AngularWidget angularWidget = new AngularWidget();
							angularWidget.setId(row.getString("AW_ID"));
							angularWidget.setCode(row.getString("AW_CODE"));
							angularWidget.setName(row.getString("AW_NAME"));
							angularWidget.setTemplateFile(row.getString("AW_TEMPLATEFILE"));
							angularWidget.setControllerFile(row.getString("AW_CONTROLERFILE"));
							
							AngularWidgetCache.put(angularWidget.getId(), angularWidget);
						}
					}
					result = AngularWidgetCache.get(widgetId);
				}
			}
			else{
				DataParam param = new DataParam("AW_ID",widgetId);
				DataRow row = this.getRecord(param);
				if (row != null && row.size() > 0){
					result = new AngularWidget();
					result.setId(row.getString("AW_ID"));
					result.setCode(row.getString("AW_CODE"));
					result.setName(row.getString("AW_NAME"));
					result.setTemplateFile(row.getString("AW_TEMPLATEFILE"));
					result.setControllerFile(row.getString("AW_CONTROLERFILE"));
				}
				AngularWidgetCache.put(widgetId, result);
			}
		}
		return result;
	}

}
