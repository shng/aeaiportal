package com.agileai.portal.bizmoduler.mobile;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface ExtTaskManage
        extends StandardService {
	List<DataRow> findTaskRecords(String userCode);
	DataRow getTaskRecord(String taskId);
	void updateTaskRecord(String id, String state,String opinionContent);
}
