package com.agileai.portal.bizmoduler.mobile;

import java.util.HashMap;

import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.portal.driver.mobile.AngularWidget;

public interface AngularWidgetManage extends StandardService{
	public static final HashMap<String,String> ControllerFileContents = new HashMap<String,String>();
	public static final HashMap<String,String> TemplateFileContents = new HashMap<String,String>();
	public static final HashMap<String,AngularWidget> AngularWidgetCache = new HashMap<String,AngularWidget>();
	
	public AngularWidget retrieveAngularWidget(String widgetId);
}
