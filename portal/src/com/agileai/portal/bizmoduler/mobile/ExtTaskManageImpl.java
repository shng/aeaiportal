package com.agileai.portal.bizmoduler.mobile;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class ExtTaskManageImpl
        extends StandardServiceImpl
        implements ExtTaskManage {
    public ExtTaskManageImpl() {
        super();
    }
    
	@Override
	public List<DataRow> findTaskRecords(String userCode) {
		List<DataRow> result = null;
		String statementId = sqlNameSpace+".findTaskRecords";
		DataParam param = new DataParam("USER_CODE", userCode);
		result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public DataRow getTaskRecord(String taskId) {
		String statementId = sqlNameSpace+".getTaskRecord";
		DataParam param = new DataParam("TASK_ID", taskId);
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public void updateTaskRecord(String id, String state,String opinionContent) {
		String statementId = sqlNameSpace+".updateTaskRecord";
		DataParam param = new DataParam("TASK_ID",id,"TASK_STATE",state,"TASK_OPINION",opinionContent);
		this.daoHelper.updateRecord(statementId, param);
		
	}    
}