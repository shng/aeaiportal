package com.agileai.portal.bizmoduler.wcm;

import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;

public class KeyWordsTreeManageImpl
        extends TreeManageImpl
        implements KeyWordsTreeManage {
    public KeyWordsTreeManageImpl() {
        super();
        this.idField = "WORD_ID";
        this.nameField = "WORD_NAME";
        this.parentIdField = "WORD_PID";
        this.sortField = "WORD_SORT";
    }
}
