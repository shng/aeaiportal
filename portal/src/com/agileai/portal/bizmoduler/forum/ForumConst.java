package com.agileai.portal.bizmoduler.forum;

public interface ForumConst {
	//用户等级
	public static final String SOLDIER = "SOLDIER";
	public static final String SQUAD_LEADER = "SQUAD_LEADER";
	public static final String PLATOON_LEADER = "PLATOON_LEADER";
	public static final String COMPANY_COMMANDER = "COMPANY_COMMANDER";
	public static final String BATTALION_COMMANDER = "BATTALION_COMMANDER";
	public static final String REGIMENT_COMMANDER = "REGIMENT_COMMANDER";
	public static final String BRIGADE_COMMANDER = "BRIGADE_COMMANDER";
	public static final String DIVISION_COMMANDER = "DIVISION_COMMANDER";
	public static final String CORPS_COMMANDER = "CORPS_COMMANDER";
	public static final String REGION_COMMANDER = "REGION_COMMANDER";
	//用户身份
	public static final String MODERATOR = "MODERATOR";
	public static final String ADMIN = "ADMIN";
	public static final String GUEST = "GUEST";
	public static final String ORDINARY_USERS = "ORDINARY_USERS";
	//帖子类型
	public static final String DEFECT_FEEDBACK = "DEFECT_FEEDBACK";
	public static final String DEVE_SUGGESTIONS = "DEVE_SUGGESTIONS";
	public static final String DIFFICULT_TO_HELP = "DIFFICULT_TO_HELP";
	//判断回复楼主或层主
	public static final String POST_MESSAGE = "POST_MESSAGE";
	//默认头像
	public static final String USERPROFILE = "./images/noProfile.jpg";	
	//待办在办已办
	public static final String TODO_STRING = "待办";
	public static final String ONDO_STRING = "在办";
	public static final String DID_STRING = "已办";
}
