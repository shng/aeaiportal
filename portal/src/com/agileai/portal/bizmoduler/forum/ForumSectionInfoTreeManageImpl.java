package com.agileai.portal.bizmoduler.forum;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;
import com.agileai.util.DateUtil;

public class ForumSectionInfoTreeManageImpl
        extends TreeManageImpl
        implements ForumSectionInfoTreeManage {
    public ForumSectionInfoTreeManageImpl() {
        super();
        this.idField = "FCI_ID";
        this.nameField = "FCI_NAME";
        this.parentIdField = "FCI_PID";
        this.sortField = "FCI_SORT";
    }

	@Override
	public void addUserSecRelColumns(String fciId, List<String> newRelFuIdList) {
		List<DataRow> records = this.findUserSecRelRecords(fciId);
		List<String> fuIdList = getFuIdList(records);
		List<DataParam> paramList = new ArrayList<DataParam>();
		for (int i=0;i < newRelFuIdList.size();i++){
			String fuId = newRelFuIdList.get(i);
			if (fuIdList.contains(fuId)){
				continue;
			}
			DataParam param = new DataParam();
			param.put("FFR_ID", KeyGenerator.instance().genKey());
			param.put("FU_ID",fuId);
			param.put("FCI_ID",fciId);
			paramList.add(param);
		}
		if (paramList.size() > 0){
			String statementId = this.sqlNameSpace+"."+"insertUserSecRelRecord";;
			this.daoHelper.batchInsert(statementId, paramList);			
		}
		
	}
	
	private List<String> getFuIdList(List<DataRow> records){
		List<String> result = new ArrayList<String>();
		for (int i=0;i < records.size();i++){
			DataRow row = records.get(i);
			result.add(row.getString("FU_ID"));
		}
		return result;
	}

	@Override
	public List<DataRow> findUserSecRelRecords(String fciId) {
		String statementId = this.sqlNameSpace+"."+"findUserSecRelRecords";
		DataParam param = new DataParam("fciId",fciId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public List<DataRow> findUserRecords(String fciId) {
		String statementId = this.sqlNameSpace+"."+"findUserRecords";
		DataParam param = new DataParam("fciId",fciId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public void delUserSecRel(String fciId, String fuId) {
		String statementId = this.sqlNameSpace+"."+"delUserSecRel";
		DataParam param = new DataParam();
		param.put("fciId",fciId,"fuId",fuId);
		this.daoHelper.deleteRecords(statementId, param);
		
	}

	@Override
	public List<DataRow> findForumSectionInfoRecords(String fciPid) {
		String statementId = this.sqlNameSpace+"."+"findForumSectionInfoRecords";
		DataParam param = new DataParam("fciPid",fciPid);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public List<DataRow> findModeratorInfoRecords(String fciPid) {
		String statementId = this.sqlNameSpace+"."+"findModeratorInfoRecords";
		DataParam param = new DataParam("fciPid",fciPid);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public List<DataRow> findHotPostRecords(int tabPage,int pageSize) {
		String statementId = this.sqlNameSpace+"."+"findHotPostRecords";
		int oracleStartIndex = tabPage+1;
		int oracleEndIndex = tabPage + pageSize;
		DataParam param = new DataParam("_mysqltabPage_",tabPage,"_mysqlpageSize_",pageSize,
				"_oracleStartIndex_",oracleStartIndex,"_oracleEndIndex_",oracleEndIndex);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}
	
	@Override
	public List<DataRow> findNewPostRecords() {
		String statementId = this.sqlNameSpace+"."+"findNewPostRecords";
		DataParam param = new DataParam();
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public DataRow getManagePostInfoRecord(String fpmId) {
		String statementId = this.sqlNameSpace+"."+"getManagePostInfoRecord";
		DataParam param = new DataParam("fpmId",fpmId);
		DataRow record = this.daoHelper.getRecord(statementId, param);
		return record;
	}

	@Override
	public List<DataRow> findRepliesInfoRecords(String fpmId) {
		String statementId = this.sqlNameSpace+"."+"findRepliesInfoRecords";
		DataParam param = new DataParam("fpmId",fpmId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}
	
	@Override
	public DataRow getPostTodayRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getPostTodayRecord";
		String date = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, new Date());
		param.put("sDateTime", date+" 00:00:00");
		param.put("eDateTime", date+" 23:59:59");
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}
	
	@Override
	public DataRow getPostYesterdayRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getPostYesterdayRecord";
		String date = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -1));
		param.put("sDateTime", date+" 00:00:00");
		param.put("eDateTime", date+" 23:59:59");
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}
	
	@Override
	public DataRow getPostStatisRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getPostStatisRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
		
	}

	@Override
	public DataRow getUsersTotalRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getUsersTotalRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public void createRepliesInfoRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"createRepliesInfoRecord";
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public String getFuIdRecord(String fuCode) {
		String statementId = this.sqlNameSpace+"."+"getFuIdRecord";
		DataParam param = new DataParam("fuCode",fuCode);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		String record = records.getString("FU_ID");
		return record;
	}

	@Override
	public String getFciIdRecord(String fpmId) {
		String statementId = this.sqlNameSpace+"."+"getFciIdRecord";
		DataParam param = new DataParam("fpmId",fpmId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		String record = null;
		if(records != null){
			record = records.getString("FCI_ID");
		}
		return record;
	}

	@Override
	public void createPostMessageRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"createPostMessageRecord";
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public int getReplyCount(String fpmId) {
		String statementId = this.sqlNameSpace+"."+"getReplyCount";
		DataParam param = new DataParam("fpmId",fpmId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		int record = records.getInt("FRI_ID");
		return record;
	}

	@Override
	public List<DataRow> findFormSelectRecords(String fciIdDefValue) {
		String statementId = this.sqlNameSpace+"."+"findFormSelectRecords";
		DataParam param = new DataParam("fciId",fciIdDefValue);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}
	
	@Override
	public List<DataRow> findModuleFormSelectRecords(String fciPid) {
		String statementId = this.sqlNameSpace+"."+"findModuleFormSelectRecords";
		DataParam param = new DataParam("fciId",fciPid);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public String getFuLevelValRecord(String fuLevel) {
		String statementId = this.sqlNameSpace+"."+"getFuLevelValRecord";
		DataParam param = new DataParam("fuLevel",fuLevel);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		String record = records.getString("CODE_NAME");
		return record;
	}

	@Override
	public List<DataRow> findFpmTypeFormSelectRecords() {
		String statementId = this.sqlNameSpace+"."+"findFpmTypeFormSelectRecords";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, null);
		return records;
	}

	@Override
	public String getPostMessageRecord(String fpmId) {
		String statementId = this.sqlNameSpace+"."+"getPostMessageRecord";
		DataParam param = new DataParam("fpmId",fpmId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		String record = records.getString("FPM_USERID");
		return record;
	}

	@Override
	public void updateFpmTypeRecord(String fpmId, String fpmType) {
		String statementId = sqlNameSpace+"."+"updateFpmTypeRecord";
		DataParam param = new DataParam("FPM_ID",fpmId,"FPM_TYPE",fpmType);
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public int getFpmClickNumber(String fpmId) {
		String statementId = this.sqlNameSpace+"."+"getFpmClickNumber";
		DataParam param = new DataParam("fpmId",fpmId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		int record = 0;
		if(records != null){
			record = records.getInt("FPM_CLICK_NUMBER");
		}
		return record;
	}

	@Override
	public void updateFpmClickNumberRecord(String fpmId, int fpmClickNumber) {
		String statementId = sqlNameSpace+"."+"updateFpmClickNumberRecord";
		DataParam param = new DataParam("FPM_ID",fpmId,"FPM_CLICK_NUMBER",fpmClickNumber);
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public int getForumFavoriteRecord(String fpmId, String fuId) {
		String statementId = sqlNameSpace+"."+"getForumFavoriteRecord";
		DataParam param = new DataParam("fpmId",fpmId,"fuId",fuId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		int record = records.getInt("FF_ID");
		return record;
	}

	@Override
	public void createForumFavoriteRecord(String fpmId, String fuId,String fpmTitle) {
		String statementId = this.sqlNameSpace+"."+"createForumFavoriteRecord";
		DataParam param = new DataParam("FF_ID", KeyGenerator.instance().genKey(),"FPM_ID",fpmId,"FU_ID",fuId,"FPM_TITLE",fpmTitle);
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public String getFpmTitleRecord(String fpmId) {
		String statementId = sqlNameSpace+"."+"getFpmTitleRecord";
		DataParam param = new DataParam("fpmId",fpmId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		String record = records.getString("FPM_TITLE");
		return record;
	}

	@Override
	public DataRow getForumPostMessageRecord(String fpmId) {
		String statementId = sqlNameSpace+"."+"getForumPostMessageRecord";
		DataParam param = new DataParam("fpmId",fpmId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		return records;
	}

	@Override
	public String getFciMottoRecord(String fciId) {
		String statementId = sqlNameSpace+"."+"getFciMottoRecord";
		DataParam param = new DataParam("fciId",fciId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		String record = records.getString("FCI_MOTTO");
		return record;
	}

	@Override
	public String getFciPid(String fciIdDefValue) {
		String statementId = sqlNameSpace+"."+"getFciPid";
		DataParam param = new DataParam("fciId",fciIdDefValue);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		String record = records.getString("FCI_PID");
		return record;
	}

	@Override
	public List<DataRow> findforumRepliesInfoRecords(String fpmId) {
		String statementId = this.sqlNameSpace+"."+"findforumRepliesInfoRecords";
		DataParam param = new DataParam("fpmId",fpmId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public void deleForumPostMessage(String fpmId) {
		String statementId = this.sqlNameSpace+"."+"deleForumPostMessage";
		DataParam param = new DataParam();
		param.put("fpmId",fpmId);
		this.daoHelper.deleteRecords(statementId, param);
	}

	@Override
	public void deleForumRepliesInfos(String fpmId) {
		String statementId = this.sqlNameSpace+"."+"deleForumRepliesInfos";
		DataParam param = new DataParam();
		param.put("fpmId",fpmId);
		this.daoHelper.deleteRecords(statementId, param);
	}
	
	@Override
	public DataRow findCodeListRecord(String fuleve) {
		String statementId = sqlNameSpace+"."+"findCodeListRecord";
		DataParam param = new DataParam("fuleve",fuleve);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		return records;
	}
	
	@Override
	public DataRow findFpmTypeRecord(String fpmType) {
		String statementId = sqlNameSpace+"."+"findFpmTypeRecord";
		DataParam param = new DataParam("fpmType",fpmType);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		return records;
	}

	@Override
	public void updateFpmState(String fpmId, String fpmState) {
		String statementId = sqlNameSpace+"."+"updateFpmState";
		DataParam param = new DataParam("FPM_ID",fpmId,"FPM_STATE",fpmState);
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public void updateFriState(String friId, String friState) {
		String statementId = sqlNameSpace+"."+"updateFriState";
		DataParam param = new DataParam("FRI_ID",friId,"FRI_STATE",friState);
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public DataRow getCurrentForumRepliesInfoRecord(String friId) {
		String statementId = sqlNameSpace+"."+"getCurrentForumRepliesInfoRecord";
		DataParam param = new DataParam("friId",friId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		return records;
	}

	@Override
	public void updateFpmSharedPoints(String fpmId, String fpmSharedPoints) {
		String statementId = sqlNameSpace+"."+"updateFpmSharedPoints";
		DataParam param = new DataParam("FPM_ID",fpmId,"FPM_SHARED_POINTS",fpmSharedPoints);
		this.daoHelper.updateRecord(statementId, param);
		
	}

	@Override
	public List<DataRow> findFciIdFormSelectRecords(String fciId) {
		String statementId = this.sqlNameSpace+"."+"findFciIdFormSelectRecords";
		DataParam param = new DataParam("fciId",fciId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public String getPostScores(String typeId,String codeId) {
		String statementId = sqlNameSpace+"."+"getPostScores";
		DataParam param = new DataParam("typeId",typeId,"codeId",codeId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		String record = records.getString("CODE_NAME");
		return record;
	}

	@Override
	public List<DataRow> findShieldWordsList() {
		String statementId = this.sqlNameSpace+"."+"findShieldWordsList";
		DataParam param = new DataParam();
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}
	
	@Override
	public DataRow getSectionInfo(String sectionId ,String typeCode ,String typeId) {
		String statementId = sqlNameSpace+"."+"getSectionInfoRecord";
		DataParam param = new DataParam("FCI_ID",sectionId);
		if("FPM_EXPRESSION".equals(typeId)){
			param.put("FPM_EXPRESSION",typeCode);
		}else if ("POST_TYPE".equals(typeId)) {
			param.put("FPM_TYPE",typeCode);
		}else {
		}
		DataRow record = this.daoHelper.getRecord(statementId, param);
		return record;
	}

	@Override
	public Integer getReplyPostCount(String sectionId) {
		String statementId = sqlNameSpace+"."+"getReplyPostCountRecord";
		DataParam param = new DataParam("FCI_ID",sectionId);
		DataRow record = this.daoHelper.getRecord(statementId, param);
		return record.getInt("ReplyPostCounts");
	}

	@Override
	public String getNewCreateTime(String sectionId) {
		String result = "";
		String statementId = sqlNameSpace+"."+"getNewCreateTimeRecord";
		DataParam param = new DataParam("FCI_ID",sectionId);
		DataRow record = this.daoHelper.getRecord(statementId, param);
		 if(!record.isEmpty()){
			 Date fpmCreateTime = (Date) record.get("FPM_CREATE_TIME");
			 Date friTime = (Date) record.get("FRI_TIME");
			 if(fpmCreateTime!=null&&friTime!=null){
				 if(fpmCreateTime.after(friTime)){
					 result = DateUtil.getDateByType(5, fpmCreateTime);
				 }else{
					 result = DateUtil.getDateByType(5, friTime);
				 } 
			 }else if(fpmCreateTime!=null&&friTime==null){
				 result = DateUtil.getDateByType(5, fpmCreateTime);
			 }else if(fpmCreateTime==null&&friTime!=null){
				 result = DateUtil.getDateByType(5, friTime);
			 }else{
				 result="";
			 }
		 }
		return result;
	}

	@Override
	public List<DataRow> getModeratorInfoRecords(String sectionId) {
		String statementId = this.sqlNameSpace+"."+"getModeratorInfoRecords";
		DataParam param = new DataParam("FCI_ID",sectionId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public void updateFriIssharepoint(String friId,String friIssharepoint) {
		String statementId = sqlNameSpace+"."+"updateFriIssharepoint";
		DataParam param = new DataParam("FRI_ID",friId,"FRI_ISSHAREPOINT",friIssharepoint);
		this.daoHelper.updateRecord(statementId, param);		
	}

	@Override
	public List<DataRow> findPostMessageRecords(String fciCode,int tabPage,int pageSize) {
		String statementId = this.sqlNameSpace+"."+"findPostMessageRecords";
		int oracleStartIndex = tabPage+1;
		int oracleEndIndex = tabPage + pageSize;
		DataParam param = new DataParam("fciCode",fciCode,"_mysqltabPage_",tabPage,"_mysqlpageSize_",pageSize,
				"_oracleStartIndex_",oracleStartIndex,"_oracleEndIndex_",oracleEndIndex);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public List<DataRow> findForumPostInfosByUserCode(String userCode) {
		String statementId = this.sqlNameSpace+"."+"findForumPostInfosByUserCode";
		DataParam param = new DataParam("userCode",userCode);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public List<DataRow> findUserAttentionForumPostInfos(String userCode) {
		String statementId = this.sqlNameSpace+"."+"findUserAttentionForumPostInfos";
		DataParam param = new DataParam("userCode",userCode);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public DataRow getmaxFpmCreateTime(String userCode) {
		String statementId = sqlNameSpace+"."+"getmaxFpmCreateTime";
		DataParam param = new DataParam("userCode",userCode);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		return records;
	}

	@Override
	public void updateFpmReport(String fpmId, String fpmReport) {
		String statementId = sqlNameSpace+"."+"updateFpmReport";
		DataParam param = new DataParam("FPM_ID",fpmId,"FPM_REPORT",fpmReport);
		this.daoHelper.updateRecord(statementId, param);		
	}

	@Override
	public List<DataRow> getSectionFormPostInfos(String category,String searchWord) {
		String statementId = this.sqlNameSpace+"."+"getSectionFormPostInfos";
		DataParam param = new DataParam("category",category,"searchWord",searchWord);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public void updateFriTime(String friId, Date friTime) {
		String statementId = sqlNameSpace+"."+"updateFriTime";
		DataParam param = new DataParam("FRI_ID",friId,"FRI_TIME",friTime);
		this.daoHelper.updateRecord(statementId, param);		
	}

	@Override
	public DataRow getSectionInfo(String sectionCode) {
		String statementId = sqlNameSpace+"."+"getSectionInfo";
		DataParam param = new DataParam("fciCode",sectionCode);
		DataRow record = this.daoHelper.getRecord(statementId, param);
		return record;
	}

	@Override
	public void updatePostInfo(String fpmId, String fpmContent) {
		String statementId = sqlNameSpace+"."+"updatePostInfo";
		DataParam param = new DataParam("FPM_ID",fpmId,"FPM_CONTENT",fpmContent);
		this.daoHelper.updateRecord(statementId, param);	
	}
	
	@Override
	public void updateMobilePostInfo(String fpmId, String mobileFpmContent) {
		String statementId = sqlNameSpace+"."+"updateMobilePostInfo";
		DataParam param = new DataParam("FPM_ID",fpmId,"FPM_CONTENT_MOBILE",mobileFpmContent);
		this.daoHelper.updateRecord(statementId, param);	
	}

	@Override
	public void updateReplyPostInfo(String friId, String friContent) {
		String statementId = sqlNameSpace+"."+"updateReplyPostInfo";
		DataParam param = new DataParam("FRI_ID",friId,"FRI_CONTENT",friContent);
		this.daoHelper.updateRecord(statementId, param);	
	}
	
	@Override
	public void updateMobileReplyPostInfo(String friId, String mobileFriContent) {
		String statementId = sqlNameSpace+"."+"updateMobileReplyPostInfo";
		DataParam param = new DataParam("FRI_ID",friId,"FRI_CONTENT_MOBILE",mobileFriContent);
		this.daoHelper.updateRecord(statementId, param);	
	}

	@Override
	public void updatePostSctionRel(String fpmId, String sectionId) {
		String statementId = sqlNameSpace+"."+"updatePostSctionRel";
		DataParam param = new DataParam("FPM_ID",fpmId,"FCI_ID",sectionId);
		this.daoHelper.updateRecord(statementId, param);
		
	}
	
	@Override
	public void createForumReportRel(String fpfrId,String frrResType,String frrType,String frrUserid,String frrTime) {
		String statementId = this.sqlNameSpace+"."+"createForumReportRel";
		DataParam param = new DataParam("FRR_ID", KeyGenerator.instance().genKey(),
				"FPFR_ID",fpfrId,"FRR_RES_TYPE",frrResType,"FRR_TYPE",frrType,"FRR_USERID",frrUserid,"FRR_TIME",frrTime);
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public List<DataRow> findModeratorRecords(String postId) {
		String statementId = this.sqlNameSpace+"."+"findModeratorRecords";
		DataParam param = new DataParam("fpmId",postId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}
	
	@Override
	public List<DataRow> findModeratoReplyRecords(String replyId) {
		String statementId = this.sqlNameSpace+"."+"findModeratoReplyRecords";
		DataParam param = new DataParam("friId",replyId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public List<DataRow> findforumReplrtRecords(String fciId) {
		String statementId = this.sqlNameSpace+"."+"findforumReplrtRecords";
		DataParam param = new DataParam("fpfrId",fciId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public void deleForumReplrtRecords(String frrIds) {
		String statementId = this.sqlNameSpace+"."+"deleForumReplrtRecords";
		List<DataParam> paramList  = new ArrayList<DataParam>();
		String[] frrIdArray = frrIds.split(",");
		for (int i = 0; i < frrIdArray.length; i++) {
			DataParam param = new DataParam();
			if(!frrIdArray[i].isEmpty()){
				param.put("frrId",frrIdArray[i]);
			}
			paramList.add(param);
		}
		this.daoHelper.batchDelete(statementId, paramList);
	}

	@Override
	public void delForumFavoriteRecord(String postId, String userId) {
		String statementId = this.sqlNameSpace+"."+"delForumFavoriteRecord";
		DataParam param = new DataParam("postId",postId,"userId",userId);
		this.daoHelper.deleteRecords(statementId, param);
	}

	@Override
	public List<DataRow> mobilefindForumReportListInfos(String userId) {
		String statementId = this.sqlNameSpace+"."+"mobilefindForumReportListInfos";
		DataParam param = new DataParam("userId",userId);
		return this.daoHelper.queryRecords(statementId, param);
	}
	
	@Override
	public List<DataRow> mobilefindForumGarbageListInfos(String userId) {
		String statementId = this.sqlNameSpace+"."+"mobilefindForumGarbageListInfos";
		DataParam param = new DataParam("userId",userId);
		return this.daoHelper.queryRecords(statementId, param);
	}

	@Override
	public List<DataRow> mobilefindForumInfosBySectionCode(String sectionCode) {
		String statementId = this.sqlNameSpace+"."+"mobilefindForumInfosBySectionCode";
		DataParam param = new DataParam("sectionCode",sectionCode);
		return this.daoHelper.queryRecords(statementId, param);
	}

	@Override
	public HashMap<String, Integer> findPostListCount() {
		HashMap<String, Integer> result = new HashMap<String, Integer>();
		String statementId = sqlNameSpace+".findPostListCount";
		DataParam param = new DataParam();
		List<DataRow> countList = this.daoHelper.queryRecords(statementId, param);
		for (int i = 0; i < countList.size(); i++) {
			DataRow row = countList.get(i);
			result.put(row.getString("bbsCode"), row.getInt("countNums"));
		}
		return result;
	}

	@Override
	public void increaseClickNumber(String postId) {
		String statementId = this.sqlNameSpace+"."+"getPostMessageRecord";
		DataParam param = new DataParam("fpmId",postId);
		DataRow records = this.daoHelper.getRecord(statementId, param);
		int fpmClickNumber = records.getInt("FPM_CLICK_NUMBER");
		fpmClickNumber = fpmClickNumber+1;
		this.updateFpmClickNumberRecord(postId, fpmClickNumber);
		
	}
}
